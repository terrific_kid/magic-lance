<?php
 
$secure = true;
require_once('start.php');




function run(){
	global $secureEmail;
	
	//Look Record
	$db = new MongoClient();
	$transaction = $db->lcart->transaction;
	$orders = $transaction->find(array('customer.email' => $secureEmail))->sort(array('_id' => -1));
	$cookie = array();
	
	foreach($orders as $order){
		$my_order = $order;
		$my_order['id'] = (string)$order['_id']->{'$id'};
		$my_order['is_subscription'] = 0;
		$my_order['date'] = $order['_id']->getTimestamp();
		
		foreach($my_order['products'] as $product){
			if(!$product['subscription'])continue;
				foreach($product['variations'] as $variation){
					if(!$variation['qty']) continue;
					if($variation['subscription'])$my_order['is_subscription'] = 1;
				}
		}
		array_push($cookie, $my_order);
	}

return $cookie;
}
			

require_once('end.php'); 


?>
