<?php
class LookerSync {

	/**
	 * Set environment
	 * Automatically sets a few variables
	 */
	// const ENVIRONMENT = 'production';
	const ENVIRONMENT = 'development';

	/**
	 * Database handler to the AWS RDS DB
	 * @var object
	 */
	private $db;

	/**
	 * Holdes Infusionsoft iSDK "$app" object
	 * @var object
	 */
	private $iSDK;

	/**
	 * Infusionsoft Application Name
	 * @var string
	 */
	private $infusionsoft_application;

	/**
	 * Infusionsoft API Key
	 * @var string
	 */
	private $infusionsoft_apikey;


	/**
	 * Email Address to email log data to
	 * @var string
	 */
	// private $log_to_address = 'x+26124237253383@mail.asana.com'; // emails data into the IS Error Log Project in Asana
	private $log_to_address = 'thomas@gocodebox.com'; // emails data into the IS Error Log Project in Asana

	/**
	 * Email Address that log emails will come from
	 * @var string
	 */
	private $log_from_address = 'thomas@gocodebox.com'; // if using an Asana address, this should be a member of the Asana Workspace



	/**
	 * Constructor
	 * @return null
	 */
	public function __construct() {
		// $this->sync_start = microtime( true );
		// $this->_dump('==== startup ====');

		// require everything needed
		require_once '../infusion/isdk.php';

		$this->_set_env_vars();

		// initialize the iSDK
		$this->iSDK = new iSDK;


		// connect the iSDK to an Infusionsoft App
		if( !$this->iSDK->cfgCon( $this->infusionsoft_application, $this->infusionsoft_apikey ) ) {

			// log the conncetion error
			$this->_log( array(
				'$this->infusionsoft_application' => $this->infusionsoft_application,
				'$this->infusionsoft_apikey'      => $this->infusionsoft_apikey
			), 'API Connection Failed to Connect!' );

			die('Infusionsoft Connection Failed');
		}

		$this->db = $this->_db_connect();
	}

	/**
	 * Desctructor
	 * @return null
	 */
	public function __destruct() {
		// $this->_dump('==== shutdown ====');
		// $this->_dump('==== exec in: ' . (microtime( true ) - $this->sync_start) . ' seconds ====');
	}


	/**
	 * Dump a variable inside a pre tag for better readability
	 * @param  mixed    $var    anything really
	 * @return null
	 */
	private function _dump( $var ) {
		// kill output unless we're in development
		if( self::ENVIRONMENT != 'development' ) return;

		echo '<pre>'; var_dump( $var ); echo '</pre>';
	}


	/**
	 * Set Instance variables based on environment constant
	 */
	private function _set_env_vars() {

		switch(self::ENVIRONMENT) {

			case 'sandbox':
				$this->infusionsoft_application = 'hn167';
				$this->infusionsoft_apikey = 'df9f8cf08e2d918a23ea1c6b8535a0884681f78601e4d283c43c500eb6ad0de8f795410c2b49c196f91d283ead2b47dc';
			break;


			default: // production, development
				$this->infusionsoft_application = 'it140';
				$this->infusionsoft_apikey = 'bc90d6eaccf8f96f3ab88582111a27ad';
		}
	}


	/**
	 * Connect to the AWS DB
	 * @return {obj} PDO handler or die with error
	 */
	private function _db_connect() {
		$host = 'lifeaidbevco-lookerstore.csf24rzpjd23.us-west-2.rds.amazonaws.com';
		$port = 3306;
		$name = 'lookerstore';
		$username = 'looker_sync';
		$password = 'C8Dku#CO#Fnu6hvn3y034(5Ri)8Ez19C';

		try {
			$db = new PDO("mysql:host={$host};port={$port};dbname={$name}", $username, $password);

			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

			return $db;

		} catch (PDOException $e) {
			die( 'DB ERROR:'. $e->getMessage() );
		}
	}



	// ==============================================================================================================================
	// ==============================================================================================================================
	// ==============================================================================================================================
	// ==============================================================================================================================
	//
	//
	//   UTILITIES
	//
	//
	// ==============================================================================================================================
	// ==============================================================================================================================
	// ==============================================================================================================================
	// ==============================================================================================================================

	/**
	 * Get a 2 charact country code from a full country name
	 * @param  string    $country   full country name
	 * @return string               2 character country code or null if not found
	 */
	private function _country( $country ) {
		if( strlen($country) === 2) return $country;

		$countries = array(
			'AF' => 'Afghanistan',
			'AX' => 'Aland Islands',
			'AL' => 'Albania',
			'DZ' => 'Algeria',
			'AS' => 'American Samoa',
			'AD' => 'Andorra',
			'AO' => 'Angola',
			'AI' => 'Anguilla',
			'AQ' => 'Antarctica',
			'AG' => 'Antigua And Barbuda',
			'AR' => 'Argentina',
			'AM' => 'Armenia',
			'AW' => 'Aruba',
			'AU' => 'Australia',
			'AT' => 'Austria',
			'AZ' => 'Azerbaijan',
			'BS' => 'Bahamas',
			'BH' => 'Bahrain',
			'BD' => 'Bangladesh',
			'BB' => 'Barbados',
			'BY' => 'Belarus',
			'BE' => 'Belgium',
			'BZ' => 'Belize',
			'BJ' => 'Benin',
			'BM' => 'Bermuda',
			'BT' => 'Bhutan',
			'BO' => 'Bolivia',
			'BA' => 'Bosnia And Herzegovina',
			'BW' => 'Botswana',
			'BV' => 'Bouvet Island',
			'BR' => 'Brazil',
			'IO' => 'British Indian Ocean Territory',
			'BN' => 'Brunei Darussalam',
			'BG' => 'Bulgaria',
			'BF' => 'Burkina Faso',
			'BI' => 'Burundi',
			'KH' => 'Cambodia',
			'CM' => 'Cameroon',
			'CA' => 'Canada',
			'CV' => 'Cape Verde',
			'KY' => 'Cayman Islands',
			'CF' => 'Central African Republic',
			'TD' => 'Chad',
			'CL' => 'Chile',
			'CN' => 'China',
			'CX' => 'Christmas Island',
			'CC' => 'Cocos (Keeling) Islands',
			'CO' => 'Colombia',
			'KM' => 'Comoros',
			'CG' => 'Congo',
			'CD' => 'Congo, Democratic Republic',
			'CK' => 'Cook Islands',
			'CR' => 'Costa Rica',
			'CI' => 'Cote D\'Ivoire',
			'HR' => 'Croatia',
			'CU' => 'Cuba',
			'CY' => 'Cyprus',
			'CZ' => 'Czech Republic',
			'DK' => 'Denmark',
			'DJ' => 'Djibouti',
			'DM' => 'Dominica',
			'DO' => 'Dominican Republic',
			'EC' => 'Ecuador',
			'EG' => 'Egypt',
			'SV' => 'El Salvador',
			'GQ' => 'Equatorial Guinea',
			'ER' => 'Eritrea',
			'EE' => 'Estonia',
			'ET' => 'Ethiopia',
			'FK' => 'Falkland Islands (Malvinas)',
			'FO' => 'Faroe Islands',
			'FJ' => 'Fiji',
			'FI' => 'Finland',
			'FR' => 'France',
			'GF' => 'French Guiana',
			'PF' => 'French Polynesia',
			'TF' => 'French Southern Territories',
			'GA' => 'Gabon',
			'GM' => 'Gambia',
			'GE' => 'Georgia',
			'DE' => 'Germany',
			'GH' => 'Ghana',
			'GI' => 'Gibraltar',
			'GR' => 'Greece',
			'GL' => 'Greenland',
			'GD' => 'Grenada',
			'GP' => 'Guadeloupe',
			'GU' => 'Guam',
			'GT' => 'Guatemala',
			'GG' => 'Guernsey',
			'GN' => 'Guinea',
			'GW' => 'Guinea-Bissau',
			'GY' => 'Guyana',
			'HT' => 'Haiti',
			'HM' => 'Heard Island & Mcdonald Islands',
			'VA' => 'Holy See (Vatican City State)',
			'HN' => 'Honduras',
			'HK' => 'Hong Kong',
			'HU' => 'Hungary',
			'IS' => 'Iceland',
			'IN' => 'India',
			'ID' => 'Indonesia',
			'IR' => 'Iran, Islamic Republic Of',
			'IQ' => 'Iraq',
			'IE' => 'Ireland',
			'IM' => 'Isle Of Man',
			'IL' => 'Israel',
			'IT' => 'Italy',
			'JM' => 'Jamaica',
			'JP' => 'Japan',
			'JE' => 'Jersey',
			'JO' => 'Jordan',
			'KZ' => 'Kazakhstan',
			'KE' => 'Kenya',
			'KI' => 'Kiribati',
			'KR' => 'Korea',
			'KW' => 'Kuwait',
			'KG' => 'Kyrgyzstan',
			'LA' => 'Lao People\'s Democratic Republic',
			'LV' => 'Latvia',
			'LB' => 'Lebanon',
			'LS' => 'Lesotho',
			'LR' => 'Liberia',
			'LY' => 'Libyan Arab Jamahiriya',
			'LI' => 'Liechtenstein',
			'LT' => 'Lithuania',
			'LU' => 'Luxembourg',
			'MO' => 'Macao',
			'MK' => 'Macedonia',
			'MG' => 'Madagascar',
			'MW' => 'Malawi',
			'MY' => 'Malaysia',
			'MV' => 'Maldives',
			'ML' => 'Mali',
			'MT' => 'Malta',
			'MH' => 'Marshall Islands',
			'MQ' => 'Martinique',
			'MR' => 'Mauritania',
			'MU' => 'Mauritius',
			'YT' => 'Mayotte',
			'MX' => 'Mexico',
			'FM' => 'Micronesia, Federated States Of',
			'MD' => 'Moldova',
			'MC' => 'Monaco',
			'MN' => 'Mongolia',
			'ME' => 'Montenegro',
			'MS' => 'Montserrat',
			'MA' => 'Morocco',
			'MZ' => 'Mozambique',
			'MM' => 'Myanmar',
			'NA' => 'Namibia',
			'NR' => 'Nauru',
			'NP' => 'Nepal',
			'NL' => 'Netherlands',
			'AN' => 'Netherlands Antilles',
			'NC' => 'New Caledonia',
			'NZ' => 'New Zealand',
			'NI' => 'Nicaragua',
			'NE' => 'Niger',
			'NG' => 'Nigeria',
			'NU' => 'Niue',
			'NF' => 'Norfolk Island',
			'MP' => 'Northern Mariana Islands',
			'NO' => 'Norway',
			'OM' => 'Oman',
			'PK' => 'Pakistan',
			'PW' => 'Palau',
			'PS' => 'Palestinian Territory, Occupied',
			'PA' => 'Panama',
			'PG' => 'Papua New Guinea',
			'PY' => 'Paraguay',
			'PE' => 'Peru',
			'PH' => 'Philippines',
			'PN' => 'Pitcairn',
			'PL' => 'Poland',
			'PT' => 'Portugal',
			'PR' => 'Puerto Rico',
			'QA' => 'Qatar',
			'RE' => 'Reunion',
			'RO' => 'Romania',
			'RU' => 'Russian Federation',
			'RW' => 'Rwanda',
			'BL' => 'Saint Barthelemy',
			'SH' => 'Saint Helena',
			'KN' => 'Saint Kitts And Nevis',
			'LC' => 'Saint Lucia',
			'MF' => 'Saint Martin',
			'PM' => 'Saint Pierre And Miquelon',
			'VC' => 'Saint Vincent And Grenadines',
			'WS' => 'Samoa',
			'SM' => 'San Marino',
			'ST' => 'Sao Tome And Principe',
			'SA' => 'Saudi Arabia',
			'SN' => 'Senegal',
			'RS' => 'Serbia',
			'SC' => 'Seychelles',
			'SL' => 'Sierra Leone',
			'SG' => 'Singapore',
			'SK' => 'Slovakia',
			'SI' => 'Slovenia',
			'SB' => 'Solomon Islands',
			'SO' => 'Somalia',
			'ZA' => 'South Africa',
			'GS' => 'South Georgia And Sandwich Isl.',
			'ES' => 'Spain',
			'LK' => 'Sri Lanka',
			'SD' => 'Sudan',
			'SR' => 'Suriname',
			'SJ' => 'Svalbard And Jan Mayen',
			'SZ' => 'Swaziland',
			'SE' => 'Sweden',
			'CH' => 'Switzerland',
			'SY' => 'Syrian Arab Republic',
			'TW' => 'Taiwan',
			'TJ' => 'Tajikistan',
			'TZ' => 'Tanzania',
			'TH' => 'Thailand',
			'TL' => 'Timor-Leste',
			'TG' => 'Togo',
			'TK' => 'Tokelau',
			'TO' => 'Tonga',
			'TT' => 'Trinidad And Tobago',
			'TN' => 'Tunisia',
			'TR' => 'Turkey',
			'TM' => 'Turkmenistan',
			'TC' => 'Turks And Caicos Islands',
			'TV' => 'Tuvalu',
			'UG' => 'Uganda',
			'UA' => 'Ukraine',
			'AE' => 'United Arab Emirates',
			'GB' => 'United Kingdom',
			'US' => 'United States',
			'UM' => 'United States Outlying Islands',
			'UY' => 'Uruguay',
			'UZ' => 'Uzbekistan',
			'VU' => 'Vanuatu',
			'VE' => 'Venezuela',
			'VN' => 'Viet Nam',
			'VG' => 'Virgin Islands, British',
			'VI' => 'Virgin Islands, U.S.',
			'WF' => 'Wallis And Futuna',
			'EH' => 'Western Sahara',
			'YE' => 'Yemen',
			'ZM' => 'Zambia',
			'ZW' => 'Zimbabwe',
		);

		$find = array_search($country, $countries);

		return ($find) ? $find : null;
	}

	/**
	 * Convert Infusionsoft Date String into a MySQL DATETIME Format
	 * @param  string    $date     Infusionsoft date string ( 'Ymd' . 'T' . 'H:i:s')
	 * @return string              mysql datetime format string
	 */
	private function _date( $date ) {
		return date( 'Y-m-d H:i:s', strtotime( $date ) );
	}


	/**
	 * Clean up phone numbers so they're only numeric and have no spaces
	 * @param  string   $phone    dirty phone number string
	 * @return string             cleaned up phone number string
	 */
	private function _phone( $phone ) {
		return trim( str_replace( array( '(', ')', '-', ' ' ), '', $phone ) );
	}


	/**
	 * Strip http:// from URL strings
	 * @param  string   $url    dirty url string
	 * @return string           cleaned up url string
	 */
	private function _url( $url ) {
		return trim( rtrim( str_replace( array('http://', 'https://', ' ' ) , '', $url ), '/' ) );
	}










	/**
	 * Retrieve contacts
	 * @return [type] [description]
	 */
	public function contacts() {
		$count = $_POST['count'];
		$page = $_POST['page'];

		if( !isset($count) || !isset($page) ) return;

		// get contacts from IS
		$contacts = $this->iSDK->dsQuery( 'Contact' , $count, $page, array( 'Id' => '%' ), array(
		// $contacts = $this->iSDK->dsQuery( 'Contact' , 1, 0, array( 'Id' => '114387' ), array(
			'Address2Street1',
			'Address2Street2',
			'City',
			'City2',
			// 'Company',
			'CompanyID',
			'Country',
			'Country2',
			'DateCreated',
			'Email',
			'EmailAddress2',
			'EmailAddress3',
			'FirstName',
			'Groups',
			'Id',
			'LastName',
			'LastUpdated',
			'LeadSourceId',
			'Leadsource',
			'MiddleName',
			'Phone1',
			'Phone2',
			'Phone3',
			'PostalCode',
			'PostalCode2',
			'State',
			'State2',
			'StreetAddress1',
			'StreetAddress2',
			'Website'
		));

		$this->_dump( $contacts );

		// save tags here for insertion later in one insert statement
		$contacts_tags = array();

		foreach($contacts as $contact):
			$data = array();

			// dates
			if( !empty( $contact['DateCreated']     ) ) {   $data[':created']            = $this->_date( $contact['DateCreated'] ); }
			if( !empty( $contact['LastUpdated']     ) ) {   $data[':updated']            = $this->_date( $contact['LastUpdated'] ); }

			// custom field dates
			if( !empty( $contact['_DateStarted']    ) ) {   $data[':date_start']         = $this->_date( $contact['_DateStarted'] ); }
			if( !empty( $contact['_FridgeDate']     ) ) {   $data[':date_fridge']        = $this->_date( $contact['_FridgeDate']  ); }

			// convert countries to country codes
			if( !empty( $contact['Country']         ) ) {   $data[':bill_country']       = $this->_country( $contact['Country']  ); }
			if( !empty( $contact['Country2']        ) ) {   $data[':ship_country']       = $this->_country( $contact['Country2'] ); }

			// cleanup phone numbers
			if( !empty( $contact['Phone1']          ) ) {   $data[':phone']              = $this->_phone( $contact['Phone1'] ); }
			if( !empty( $contact['Phone2']          ) ) {   $data[':phone_2']            = $this->_phone( $contact['Phone2'] ); }
			if( !empty( $contact['Phone3']          ) ) {   $data[':phone_3']            = $this->_phone( $contact['Phone3'] ); }

			// clean up url
			if( !empty( $contact['Website']         ) ) {   $data[':website']            = $this->_url( $contact['Website'] );  }

			// other fields
			if( !empty( $contact['Address2Street1'] ) ) {   $data[':ship_street']        = $contact['Address2Street1'];   }
			if( !empty( $contact['Address2Street2'] ) ) {   $data[':ship_street_2']      = $contact['Address2Street2'];   }
			if( !empty( $contact['City']            ) ) {   $data[':bill_city']          = $contact['City'];              }
			if( !empty( $contact['City2']           ) ) {   $data[':ship_city']          = $contact['City2'];             }
			if( !empty( $contact['Email']           ) ) {   $data[':email']              = $contact['Email'];             }
			if( !empty( $contact['EmailAddress2']   ) ) {   $data[':email_2']            = $contact['EmailAddress2'];     }
			if( !empty( $contact['EmailAddress3']   ) ) {   $data[':email_3']            = $contact['EmailAddress3'];     }
			if( !empty( $contact['FirstName']       ) ) {   $data[':first_name']         = $contact['FirstName'];         }
			if( !empty( $contact['Id']              ) ) {   $data[':infusionsoft_id']    = $contact['Id'];                }
			if( !empty( $contact['LastName']        ) ) {   $data[':last_name']          = $contact['LastName'];          }
			if( !empty( $contact['MiddleName']      ) ) {   $data[':middle_name']        = $contact['MiddleName'];        }
			if( !empty( $contact['PostalCode']      ) ) {   $data[':bill_postal_code']   = $contact['PostalCode'];        }
			if( !empty( $contact['PostalCode2']     ) ) {   $data[':ship_postal_code']   = $contact['PostalCode2'];       }
			if( !empty( $contact['State']           ) ) {   $data[':bill_state']         = $contact['State'];             }
			if( !empty( $contact['State2']          ) ) {   $data[':ship_state']         = $contact['State2'];            }
			if( !empty( $contact['StreetAddress1']  ) ) {   $data[':bill_street']        = $contact['StreetAddress1'];    }
			if( !empty( $contact['StreetAddress2']  ) ) {   $data[':bill_street_2']      = $contact['StreetAddress2'];    }


			// get the existing company id
			if( !empty( $contact['CompanyID'] ) ) {
				$sth = $this->db->prepare( 'SELECT id FROM Companies WHERE infusionsoft_id = :company_id' );
				$sth->execute( array( ':company_id' => $contact['CompanyID'] ) );
				$res = $sth->fetch();

				$data[':company_id'] = $res['id'];
			}

			// get existing leadsource id
			if( !empty( $contact['LeadSourceId'] ) ) {
				$sth = $this->db->prepare( 'SELECT id FROM Sources WHERE infusionsoft_id = :source_id' );
				$sth->execute( array( ':source_id' => $contact['LeadSourceId'] ) );
				$res = $sth->fetch();

				$data[':source_id'] = $res['id'];
			}


			// insert the contact
			$keys = implode( ',', array_keys( $data ) );
			$sth = $this->db->prepare('INSERT INTO Contacts (' . str_replace(':', '', $keys) . ') VALUES (' . $keys . ')');
			$insert = $sth->execute( $data );
			$contact_id = $this->db->lastInsertId();


			// prep tags for tag assignment
			if( !empty($contact['Groups'] ) ) {

				foreach( explode(',', $contact['Groups']) as $tag ) {
					$sth = $this->db->prepare( 'SELECT id FROM Tags WHERE infusionsoft_id = :tag_id' );
					$sth->execute( array( ':tag_id' => $tag ) );
					$res = $sth->fetch();

					if( $res ) {

						$contacts_tags[] = array(
							':contact_id' => $contact_id,
							':tag_id' => $res['id']
						);

					}

				}


			}

		endforeach;

		// insert all the tags
		$this->db->beginTransaction();
		$sth = $this->db->prepare('INSERT INTO ContactsTags (contact_id, tag_id) VALUES (:contact_id, :tag_id)');
		foreach($contacts_tags as $tag) {
			$sth->execute( $tag );
		}
		$this->db->commit();
	}



	/**
	 * Request Order information from Infusionsoft
	 * @return [type] [description]
	 */
	public function orders() {

		if($_SERVER['REQUEST_METHOD'] != 'POST') return;

		$orders = $this->iSDK->dsQuery( 'Job' , 250, $_POST['page'], array( 'Id' => '%' ), array(
			// 'DueDate',
			// 'JobNotes',
			// 'JobRecurringId',
			// 'JobStatus',
			// 'OrderStatus',
			// 'OrderType',
			// 'ProductId',
			// 'JobTitle',
			// 'StartDate',
			// 'ShipFirstName',
			// 'ShipLastName',
			// 'ShipMiddleName',
			'ContactId', // done
			'DateCreated', // done
			'Id',  // done
			'ShipCity',
			'ShipCompany',
			'ShipCountry',
			'ShipState',
			'ShipStreet1',
			'ShipStreet2',
			'ShipZip'
		) );

		$this->_dump($orders);

		foreach( $orders as $order ):
			$data = array();

			if( !empty( $order['DateCreated'] ) ) {
				$data[':created']       = $this->_date( $order['DateCreated'] );
				$data[':purchase_date'] = $data[':created'];
			}

			if( !empty( $order['Id']              ) ) {   $data[':infusionsoft_order_id'] = $order['Id'];                          }

			// shipping
			if( !empty( $order['ShipCountry']     ) ) {   $data[':ship_country']       = $this->_country( $order['ShipCountry'] ); }
			if( !empty( $order['ShipStreet1']     ) ) {   $data[':ship_street']        = $order['ShipStreet1'];                    }
			if( !empty( $order['ShipStreet2']     ) ) {   $data[':ship_street_2']      = $order['ShipStreet2'];                    }
			if( !empty( $order['ShipCity']        ) ) {   $data[':ship_city']          = $order['ShipCity'];                       }
			if( !empty( $order['ShipZip']         ) ) {   $data[':ship_postal_code']   = $order['ShipZip'];                        }
			if( !empty( $order['ShipState']       ) ) {   $data[':ship_state']         = $order['ShipState'];                      }

			// get the existing contact id
			if( !empty( $order['ContactId'] ) ) {
				$sth = $this->db->prepare( 'SELECT id FROM Contacts WHERE infusionsoft_id = :contact_id' );
				$sth->execute( array( ':contact_id' => $order['ContactId'] ) );
				$res = $sth->fetch();

				$data[':contact_id'] = $res['id'];
			}

			// insert the order
			$keys = implode( ',', array_keys( $data ) );
			$sth = $this->db->prepare('INSERT INTO Orders (' . str_replace(':', '', $keys) . ') VALUES (' . $keys . ')');
			$insert = $sth->execute( $data );
			$order_id = $this->db->lastInsertId();

		endforeach;
	}




	public function invoices() {
		if($_SERVER['REQUEST_METHOD'] != 'POST') return;

		$invoices = $this->iSDK->dsQuery( 'Invoice' , 500, $_POST['page'], array( 'Id' => '%' ), array(
			// 'AffiliateId',
			// 'ContactId',
			// 'CreditStatus',
			// 'Description',
			// 'PayStatus',
			// 'ProductSold',
			// 'RefundStatus',
			// 'Synced',
			// 'LeadAffiliateId',
			// 'PayPlanStatus',
			// 'InvoiceType',
			// 'TotalPaid',
			// 'TotalDue',
			'DateCreated',
			'Id',
			'InvoiceTotal',
			'JobId',
			'PromoCode'
		) );

		$this->_dump($invoices);

		foreach($invoices as $invoice):

			if( !empty( $invoice['Id'] ) )           { $data['infusionsoft_invoice_id'] = $invoice['Id']; }
			if( !empty( $invoice['InvoiceTotal'] ) ) { $data['amount_total']            = $invoice['InvoiceTotal']; }
			// if( !empty( $invoice['JobId'] ) )        { $data['infusionsoft_order_id']   = $invoice['JobId']; }

			// get coupon id
			if( !empty( $invoice['PromoCode'] ) ) {
				$sth = $this->db->prepare( 'SELECT id FROM Coupons WHERE code = :code');
				$sth->execute( array( ':code' => $invoice['PromoCode'] ) );
				$res = $sth->fetch();

				$coupon_id = $res['id'];

				if( !$coupon_id ) {
					$sth = $this->db->prepare( 'INSERT INTO Coupons (code,created) VALUES(:code,:created)' );
					$sth->execute( array(
						':code' => $invoice['PromoCode'],
						':created' => $this->_date( $invoice['DateCreated'] )
					) );
					$coupon_id = $this->db->lastInsertId();
				}

				$data['coupon_id'] = $coupon_id;
			}


			$vals = array_values( $data );
			$vals[] = $invoice['JobId'];

			// $this->_dump($data);
			// $this->_dump( 'UPDATE Orders SET ' . rtrim( implode( ' = ?, ', array_keys( $data ) ), ',' ) . ' = ? WHERE infusionsoft_order_id = ?' );
			// $this->_dump( $vals );



			// save new data
			$sth = $this->db->prepare( 'UPDATE Orders SET ' . rtrim( implode( ' = ?, ', array_keys( $data ) ), ',' ) . ' = ? WHERE infusionsoft_order_id = ?' );
			$sth->execute( $vals );

			// $this->_dump($sth->errorInfo());
		endforeach;
	}



	public function order_items() {

		// $total = $this->iSDK->dsCount( 'OrderItem', array( 'Id' => '%' ) );

		if($_SERVER['REQUEST_METHOD'] != 'POST') return;

		$items = $this->iSDK->dsQuery( 'OrderItem' , 250, $_POST['page'], array( 'Id' => '%' ), array(
			// 'CPU',
			// 'Id',
			// 'ItemDescription',
			// 'ItemName',
			'ItemType',
			// 'Notes',
			'OrderId',
			'PPU',
			'ProductId',
			'Qty',
			// 'SubscriptionPlanId'
		) );

		foreach($items as $item):
			$data = array();


			if( !empty( $item['Qty'] ) ) { $data[':quantity'] = $item['Qty']; }
			if( !empty( $item['PPU'] ) ) { $data[':amount']   = $item['PPU']; }


			// hardcode for faster execution
			if( !empty( $item['ItemType'] ) ) {

				$type = 0;
				switch($item['ItemType']) {

					case '1':  $type = 1;  break;
					case '2':  $type = 2;  break;
					case '3':  $type = 3;  break;
					case '4':  $type = 4;  break;
					case '5':  $type = 5;  break;
					case '6':  $type = 6;  break;
					case '7':  $type = 7;  break;
					case '8':  $type = 8;  break;
					case '9':  $type = 9;  break;
					case '10': $type = 10; break;
					case '12': $type = 11; break;
					case '14': $type = 12; break;

				}

				$data[':item_type_id'] = $type;

			}

			// lookup the product id
			if( !empty( $item['ProductId'] ) ) {

				$sth = $this->db->prepare( 'SELECT id FROM Products WHERE infusionsoft_id = :product_id' );
				$sth->execute( array( ':product_id' => $item['ProductId'] ) );
				$res = $sth->fetch();

			 	$data[':product_id'] = $res['id'];

			}


			// lookup the order id
			if( !empty( $item['OrderId'] ) ) {

				$sth = $this->db->prepare( 'SELECT id, created FROM Orders WHERE infusionsoft_order_id = :order_id' );
				$sth->execute( array( ':order_id' => $item['OrderId'] ) );
				$res = $sth->fetch();

			 	$data[':order_id'] = $res['id'];
			 	$data[':created'] = $res['created'];

			}

			// insert the data
			$keys = implode( ',', array_keys( $data ) );
			$sth = $this->db->prepare('INSERT INTO OrderItems(' . str_replace(':', '', $keys) . ') VALUES (' . $keys . ')');
			$insert = $sth->execute( $data );
		endforeach;

	}


	public function payments() {
		// $total = $this->iSDK->dsCount( 'InvoicePayment', array( 'Id' => '%' ) ); // == 35678

		// $this->_dump( $total );

		if($_SERVER['REQUEST_METHOD'] != 'POST') return;

		$payments = $this->iSDK->dsQuery( 'InvoicePayment' , 250, $_POST['page'], array( 'Id' => '%' ), array(
			'Amt',
			// 'Id',
			'InvoiceId',
			'PayDate',
			// 'PayStatus',
			// 'PaymentId',
			// 'SkipCommission'
		) );

		if( ! count($payments) ) {

			return json_encode(array('stop'=>true));

		}

		foreach( $payments as $payment ):

			$data = array();

			$payment['PayDate'] = $this->_date( $payment['PayDate'] );

			if( !empty( $payment['Amt'] ) )     { $data[':amount']         = $payment['Amt'];     }
			if( !empty( $payment['PayDate'] ) ) { $data[':created']        = $payment['PayDate']; }
			if( !empty( $payment['PayDate'] ) ) { $data[':updated']        = $payment['PayDate']; }
			if( !empty( $payment['PayDate'] ) ) { $data[':payment_date']   = $payment['PayDate']; }

			// lookup the order id & insert if there's an order
			if( !empty( $payment['InvoiceId'] ) ) {

				$sth = $this->db->prepare( 'SELECT id FROM Orders WHERE infusionsoft_invoice_id = :invoice_id' );
				$sth->execute( array( ':invoice_id' => $payment['InvoiceId'] ) );
				$res = $sth->fetch();

			 	$data[':order_id'] = $res['id'];

				// insert the data
				$keys = implode( ',', array_keys( $data ) );
				$sth = $this->db->prepare('INSERT INTO OrderPayments(' . str_replace(':', '', $keys) . ') VALUES (' . $keys . ')');
				$insert = $sth->execute( $data );
			}

		endforeach;

	}

}
// $sync = new LookerSync;
// $sync->contacts();
// $sync->orders();
// $sync->invoices();
// $sync->order_items();
// $sync->payments();










// die();
/**
 * Output a simple form for automating stuff
 */
if($_SERVER['REQUEST_METHOD'] == 'POST') return;
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>
$(document).ready(function(){
	var direction = 'up',
		rand = 1000,
	    counter = 0,
		page = -1,
		// page = 2,
		interval;

	function make_call() {
		counter = 0;
		page = (direction === 'up') ? page + 1 : page - 1;

		// cap pages here

		if( direction === 'up' && page >= 73 ) {

			console.log('SCRIPT HALTED!');
			return;

		}

		if( direction === 'down' && page < 73 ) {

			console.log('SCRIPT HALTED!');
			return;

		}

		// end cap pages

		console.log('starting page ' + page);

		interval = 	setInterval(function(){
			counter = counter + 1;
			console.log(counter);
		}, 1000); // 1 second counter

		$.post(window.location.href, {

			page: page

		}, function(r){

			if( r ) {

				r = JSON.parse( r );

				if( r.stop ) {

					console.log('STOPPING');
					clearInterval(interval);
					return;

				}

			}

			clearInterval( interval );
			console.log('finished page ' + page);

			rand = Math.floor((Math.random() * 5) + 1) * 1000;
			setTimeout(function() {
				make_call();
			}, rand);

		});
	}

	$('button').on('click',function(){

		direction = $('#direction').val();
		page = $('#start-page').val() * 1;

		make_call();

	});

});
</script>
<input id="start-page" value="-1">
<select id="direction">
	<option value="up" selected>Up</option>
	<option value="down">down</option>
</select>
<button>START</button>