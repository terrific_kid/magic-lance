<?php
	
	setlocale(LC_MONETARY, 'en_US');
	 date_default_timezone_set('America/Los_Angeles');


	$body = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=0"/>        
        <title>FA RECEIPT</title>
		<style type="text/css">

			@media only screen and (max-width:767px){
				p {
					line-height:18px !important;
				}
				.table-responsive {
					width: 100%;
					margin-bottom: 15px;
					min-height: .01%;
					overflow-x: auto;
					overflow-y: hidden;
					-ms-overflow-style: -ms-autohiding-scrollbar;
				}
				
								
				.outer-table-wrapper {
					padding: 0 !important;
				}
				.remove-outer-top,
				.remove-outer-bottom {
					height: 0  !important;
				}
								
				.logo-wrapper img {
					width: 100% !important;
					height: auto !important;
				}
				
				.table-header {
					border: none !important;
				}
				
				.table-header,
				.td-content-wrapper,
				.table-content-wrapper,
				.table-content,
				.table-billing-shipping-wrapper,
				.table-item-breakdown-wrapper,
				.table-social-links-wrapper	{
					width: 100% !important;
				}
				
				.table-content-wrapper-col-left,
				.table-content-wrapper-col-right {
					width: 5% !important;
				}
				.table-content-wrapper-col-mid {
					width: 90% !important;
				}
				
				
				.table-item-breakdown > thead > tr > th:first-child,
				.table-item-breakdown > tbody > tr > td:first-child	{
					width: 10% !important;
				}
				.table-item-breakdown > thead > tr > th:nth-child(2),
				.table-item-breakdown > tbody > tr > td:nth-child(2) {
					width: 56% !important;
				}
				
				.quantity {
					display: none;
				}
				.qty {
					display: block !important;
				}
				
				.table-ship-to {
					margin-top: 30px;
				}
				
				.table-billing-shipping-wrapper > tbody > tr > td,
				.table-social-links-wrapper > tbody > tr > td {
					width: 100% !important;
					display: block !important;
				}
				.table-social-links-wrapper > tbody > tr > td {
					margin-bottom: 10px;
				}
				
				.table-social-links-wrapper > tbody > tr > td > table > tbody > tr > td {
					padding-top: 2px;
				}
				
			}
			
		</style>
	</head>
    <body style="margin: 0; padding: 0;">
    	<center>
			<!-- // Begin wrapper -->
        	<table class="outer-table-wrapper" border="0" cellpadding="0" cellspacing="0" width="100%" style="background:#f2f2f2;border-collapse: collapse;padding:20px;font-family:Verdana,Arial,sans-serif;font-size:12px;line-height: 16px;">
				<tbody>
					<tr>
						<td class="remove-outer-top" height="20"></td>
					</tr>
					<tr>
						<td align="center" valign="top">
							<!-- // Begin HEADER -->
							<table class="table-header" border="0" cellpadding="0" cellspacing="0" width="675" style="background:#ffffff;border:1px solid #e8e8e8;">
								<tbody>
									<tr>
										<td><div class="logo-wrapper"><img src="https://www.drinkfitaid.com/media/lifeaid-receipt-header-bg.jpg" alt="LifeAID Beverage Co." width="675" height="110" style="display: block;" /></div></td>
									</tr>
									<tr>
										<td class="td-content-wrapper" width="675">
											<!-- // Begin content wrapper -->
											<table class="table-content-wrapper" border="0" cellpadding="0" cellspacing="0" width="675">
												<tbody>
													<tr>
														<td class="table-content-wrapper-col-left" width="40">&nbsp;</td>
														<td class="table-content-wrapper-col-mid" width="595">
															<!-- // Begin CONTENT -->
															<table class="table-content" border="0" cellpadding="0" cellspacing="0" width="595">
																<tbody>
																	<tr>
																		<td style="border-bottom:1px solid #b5b5b5;"><p style="margin-top:0;text-align:right;"><strong>Receipt #</strong> '.$my_order['_id']->{'$id'}.'</p></td>
																	</tr>
																	<tr>
																		<td height="30">&nbsp;</td>
																	</tr>
																	<tr>
																		<td>
																			<p style="margin-top:0;margin-bottom:0;">Thank you for your order!  Here are the details of your order placed on ';
																			
																			$body .= date('m/d/Y', $my_order['_id']->getTimeStamp()).' at '.date('g:ia',$my_order['_id']->getTimeStamp()).' pst';
																			
																			$body .= '</p><p style="margin-bottom:0;">Your order will ship within 2 business days. Product and merchandise will have different tracking emails. Please check your Spam box first!</p>
																		</td>
																	</tr>
																	<tr>
																		<td height="30">&nbsp;</td>
																	</tr>
																	<tr>
																		<td>																																					
																			<!-- // Begin BILLING/SHIPPING details -->
																			<table class="table-billing-shipping-wrapper" border="0" cellpadding="0" cellspacing="0" width="595">																				
																				<tbody>
																					<tr>
																						<td valign="top" width="40%">																																									
																							<!-- // Begin Billing -->
																							<table border="0" cellpadding="0" cellspacing="0" width="100%">
																								<thead>
																									<tr>
																										<th style="text-align:left;">Bill to:</th>
																									</tr>
																								</thead>
																								<tbody>
																									<tr>
																										<td height="20">&nbsp;</td>
																									</tr>
																									<tr>
																										<td>
																										<table border="0" cellpadding="0" cellspacing="0" width="100%">
																										';
																										
																											$body .= '<tr><td>'.$my_order['customer']['form']['firstName'] . ' ' . $my_order['customer']['form']['lastName'] . '</td></tr>';  
																											if($my_order['customer']['form']['company']) $body .= '<tr><td>'.$my_order['customer']['form']['company'].'</td></tr>'; 
																											$body .= '<tr><td>'. $my_order['customer']['form']['billingAddress1'].'</td></tr>';  
																											if($my_order['customer']['form']['billingAddress2']) $body .= '<tr><td>'.$my_order['customer']['form']['billingAddress2'].'</td></tr>'; 
																											$body .='<tr><td>'.$my_order['customer']['form']['billingCity']. ', '.$my_order['customer']['form']['billingState']. ' ' . $my_order['customer']['form']['billingZip'].'</td></tr>';
																											if($my_order['customer']['form']['billingPhone']) $body .= '<tr><td>'.$my_order['customer']['form']['billingPhone'].'</td></tr>';  
																											$body .= '<tr><td>'.'<a href="mailto:' . $my_order['customer']['email'].' style="color:#1a81e1;text-decoration:none;">'.$my_order['customer']['email'].'</a>'.'</td></tr>';
																										
																										
																										$body .='</table></td>
																									</tr>
																									<tr>
																										<td height="20">&nbsp;</td>
																									</tr>
																									<tr>
																										<td>
																											<strong>Payment Method:</strong><br>Credit Card';
																											 
																										 
																										
																							   $body .='</td>
																									</tr>
																								</tbody>
																							</table>
																						</td>
																						<td valign="top" width="40%">																																									
																							<!-- // Begin Billing -->
																							<table class="table-ship-to" border="0" cellpadding="0" cellspacing="0" width="100%">
																								<thead>
																									<tr>
																										<th style="text-align:left;">Ship to:</th>
																									</tr>
																								</thead>
																								<tbody>
																									<tr>
																										<td height="20">&nbsp;</td>
																									</tr>
																									<tr>
																										<td>';
																											if($my_order['customer']['form']['shipToApo']){																											
																												$body .= '<tr><td>'.$my_order['customer']['form']['firstName'] . ' ' . $my_order['customer']['form']['lastName'] . '</td></tr>';  
																												if($my_order['customer']['form']['company']) $body .= '<tr><td>'.$my_order['customer']['form']['company'].'</td></tr>'; 
																												$body .= '<tr><td>'. $my_order['customer']['form']['apoAddress1'].'</td></tr>';  
																												if($my_order['customer']['form']['apoAddress2']) $body .= '<tr><td>'.$my_order['customer']['form']['apoAddress2'].'</td></tr>'; 
																												$body .='<tr><td>'.$my_order['customer']['form']['apoCity']. ', '.$my_order['customer']['form']['apoState']. ' ' . $my_order['customer']['form']['apoZip'].'</td></tr>';
																												if($my_order['customer']['form']['apoPhone']) $body .= '<tr><td>'.$my_order['customer']['form']['apoPhone'].'</td></tr>';  
																												$body .= '<tr><td>'.'<a href="mailto:' . $my_order['customer']['email'].' style="color:#1a81e1;text-decoration:none;">'.$my_order['customer']['email'].'</a>'.'</td></tr>';
																											

																											}elseif($my_order['customer']['form']['shipToBill']){
																													$body .= '<tr><td>'.$my_order['customer']['form']['firstName'] . ' ' . $my_order['customer']['form']['lastName'] . '</td></tr>';  
																													if($my_order['customer']['form']['company']) $body .= '<tr><td>'.$my_order['customer']['form']['company'].'</td></tr>'; 
																													$body .= '<tr><td>'. $my_order['customer']['form']['billingAddress1'].'</td></tr>';  
																													if($my_order['customer']['form']['billingAddress2']) $body .= '<tr><td>'.$my_order['customer']['form']['billingAddress2'].'</td></tr>'; 
																													$body .='<tr><td>'.$my_order['customer']['form']['billingCity']. ', '.$my_order['customer']['form']['billingState']. ' ' . $my_order['customer']['form']['billingZip'].'</td></tr>';
																													if($my_order['customer']['form']['billingPhone']) $body .= '<tr><td>'.$my_order['customer']['form']['billingPhone'].'</td></tr>';  
																													$body .= '<tr><td>'.'<a href="mailto:' . $my_order['customer']['email'].' style="color:#1a81e1;text-decoration:none;">'.$my_order['customer']['email'].'</a>'.'</td></tr>';
																										

																											}else{
																													$body .= '<tr><td>'.$my_order['customer']['form']['firstName'] . ' ' . $my_order['customer']['form']['lastName'] . '</td></tr>';  
																													if($my_order['customer']['form']['company']) $body .= '<tr><td>'.$my_order['customer']['form']['company'].'</td></tr>'; 
																													$body .= '<tr><td>'. $my_order['customer']['form']['shippingAddress1'].'</td></tr>';  
																													if($my_order['customer']['form']['shippingAddress2']) $body .= '<tr><td>'.$my_order['customer']['form']['shippingAddress2'].'</td></tr>'; 
																													$body .='<tr><td>'.$my_order['customer']['form']['shippingCity']. ', '.$my_order['customer']['form']['shippingState']. ' ' . $my_order['customer']['form']['shippingZip'].'</td></tr>';
																													if($my_order['customer']['form']['shippingPhone']) $body .= '<tr><td>'.$my_order['customer']['form']['shippingPhone'].'</td></tr>';  
																													$body .= '<tr><td>'.'<a href="mailto:' . $my_order['customer']['email'].' style="color:#1a81e1;text-decoration:none;">'.$my_order['customer']['email'].'</a>'.'</td></tr>';
																												
}
																											
																										$body .='</td>
																									</tr>
																									<tr>
																										<td height="20">&nbsp;</td>
																									</tr>
																									<tr>
																										<td>
																											<strong>Shipping:</strong> FedEX Ground
																										</td>
																									</tr>
																								</tbody>
																							</table>
																						</td>
																					</tr>
																				</tbody>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td height="30">&nbsp;</td>
																	</tr>
																	<tr>
																		<td>																																					
																			<!-- // Begin item breakdown wrapper -->
																			<table class="table-item-breakdown-wrapper" border="0" cellpadding="0" cellspacing="0" width="595">																				
																				<tbody>
																					<tr>
																						<td valign="top" width="20%">																																									
																							<!-- // Begin QUANTITY -->
																							<table class="table-item-breakdown" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-top:1px solid #b5b5b5;border-bottom:1px solid #b5b5b5;padding-top:10px;padding-bottom:10px;">
																								<thead>
																									<tr>
																										<th width="20%" style="border-bottom:1px solid #b5b5b5;padding-bottom:10px;text-align:left;"><span class="quantity">Quantity</span></th>
																										<th width="46%" style="border-bottom:1px solid #b5b5b5;padding-bottom:10px;text-align:left;">Description</th>
																										<th width="17%" style="border-bottom:1px solid #b5b5b5;padding-bottom:10px;text-align:left;">Price</th>
																										<th width="17%" style="border-bottom:1px solid #b5b5b5;padding-bottom:10px;text-align:right;">Total</th>
																									</tr>
																								</thead>
																								<tbody>';
																									
																									foreach($my_order['products'] as $product){
																										foreach($product['variations'] as $variation){
																											if(!$variation['qty'])continue;
																											
																											$body .= '<tr>';
																											$body .= '<td width="20%" style="padding-top:10px;">'.$variation['qty'].'</td>';
																											$body .= '<td width="46%" style="padding-top:10px;">'.$product['name'].'</td>';
																											$body .= '<td width="17%" style="padding-top:10px;">$'.money_format("%i",$product['price']).'</td>';
																											$body .= '<td width="17%" style="padding-top:10px;text-align:right;">$'.money_format('%i',round($variation['qty'] * $product['price'],2)).'</td>';
																											$body .= '</tr>';
																										
																										}
																									}
																									
																					   $body .='</tbody>
																							</table>
																						</td>
																					</tr>
																					<tr>
																						<td valign="top" width="20%">																																									
																							<!-- // Begin SUBTOTAL -->
																							<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-bottom:1px solid #b5b5b5;padding-top:10px;padding-bottom:10px;text-align:right;">
																								<tbody>
																									<tr>
																										<td width="80%">Subtotal: </td>
																										<td width="20%">$'.money_format('%i', round($my_order['totals']['subtotal'],2)).'</td>
																									</tr>';
																									
																									if($my_order['promo']){
																										$body .= '<tr>';
																										$body .= '<td width="80%">Promotion ('.$my_order['promo'].'):</td>';
																										$body .= '<td width="20%">'.money_format('%i', $my_order['totals']['off']).'</td>';
																										$body .= '</tr>';
																									}
																									
																									if($my_order['totals']['tax']){
																										$body.='<tr>';
																										$body.='<td width="80%">Tax:</td>';
																										$body.='<td width="20%">$'.money_format('%i', $my_order['totals']['tax']).'</td>';
																										$body.='</tr>';
																									}
																									
																							 $body.='<tr>
																										<td width="80%">Shipping:</td>
																										<td width="20%">$'.money_format('%i', $my_order['totals']['shipping_discount']).'</td>
																									</tr>
																								</tbody>
																							</table>
																						</td>
																					</tr>
																					<tr>
																						<td valign="top" width="20%">																																									
																							<!-- // Begin TOTAL -->
																							<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-bottom:1px solid #b5b5b5;padding-top:10px;padding-bottom:10px;text-align:right;">
																								<tbody>
																									<tr>
																										<td width="80%"><strong>Order total:</strong></td>
																										<td width="20%"><strong>$'.money_format('%i', $my_order['totals']['total']).'</strong></td>
																									</tr>
																								</tbody>
																							</table>
																						</td>
																					</tr>
																				</tbody>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td height="20">&nbsp;</td>
																	</tr>
																	<tr>
																		<td>
																			<p style="margin-bottom:12px">Be sure to LIKE US or Follow Us on social media for upcoming deals and promotions.</p>
																		</td>
																	</tr>
																	<tr>
																		<td>																			
																			<!-- // Begin SOCIAL links columns -->
																			<table class="table-social-links-wrapper" border="0" cellpadding="0" cellspacing="0" width="595">																				
																				<tbody>
																					<tr>
																						<td valign="top" width="30%">																																									
																							<!-- // Begin social FITAID -->
																							<table border="0" cellpadding="0" cellspacing="0" width="100%">
																								<thead>
																									<tr>
																										<th style="text-align:left;">FITAID</th>
																									</tr>
																								</thead>
																								<tbody>
																									<tr>
																										<td><a href="https://www.facebook.com/fitaid" target="_blank" style="color:#1a81e1;text-decoration:none;"><strong>Facebook</strong></a></td>
																									</tr>
																									<tr>
																										<td><a href="https://instagram.com/fitaid" target="_blank" style="color:#1a81e1;text-decoration:none;"><strong>Instagram</strong></a></td>
																									</tr>
																									<tr>
																										<td><a href="https://twitter.com/drinkfitaid" target="_blank" style="color:#1a81e1;text-decoration:none;"><strong>Twitter #FITAID</strong></a></td>
																									</tr>
																								</tbody>
																							</table>
																						</td>
																						<td valign="top" width="30%">																																									
																							<!-- // Begin social FITAID -->
																							<table border="0" cellpadding="0" cellspacing="0" width="100%">
																								<thead>
																									<tr>
																										<th style="text-align:left;">GOLFERAID</th>
																									</tr>
																								</thead>
																								<tbody>
																									<tr>
																										<td><a href="https://www.facebook.com/golferaid" target="_blank" style="color:#1a81e1;text-decoration:none;"><strong>Facebook</strong></a></td>
																									</tr>
																									<tr>
																										<td><a href="http://instagram.com/golferaid" target="_blank" style="color:#1a81e1;text-decoration:none;"><strong>Instagram</strong></a></td>
																									</tr>
																									<tr>
																										<td><a href="https://twitter.com/golferaid" target="_blank" style="color:#1a81e1;text-decoration:none;"><strong>Twitter #GOLFERAID</strong></a></td>
																									</tr>
																								</tbody>
																							</table>
																						</td>
																						<td valign="top" width="30%">																																									
																							<!-- // Begin social FITAID -->
																							<table border="0" cellpadding="0" cellspacing="0" width="100%">
																								<thead>
																									<tr>
																										<th style="text-align:left;">PARTYAID</th>
																									</tr>
																								</thead>
																								<tbody>
																									<tr>
																										<td><a href="http://www.facebook.com/drinkpartyaid" target="_blank" style="color:#1a81e1;text-decoration:none;"><strong>Facebook</strong></a></td>
																									</tr>
																									<tr>
																										<td><a href="http://instagram.com/partyaid" target="_blank" style="color:#1a81e1;text-decoration:none;"><strong>Instagram</strong></a></td>
																									</tr>
																									<tr>
																										<td><a href="http://twitter.com/#!/drinkpartyaid" target="_blank" style="color:#1a81e1;text-decoration:none;"><strong>Twitter #PARTYAID</strong></a></td>
																									</tr>
																								</tbody>
																							</table>
																						</td>
																					</tr>
																				</tbody>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<p style="margin-bottom:0;">If you have any questions or problems with your order, please contact <a href="mailto:support@lifeaidbevco.com" style="color:#1a81e1;text-decoration:none;">support@lifeaidbevco.com</a> or feel free to call us at: 888-558-1113</p>
																			<p style="padding-top:20px;margin-bottom:0;">Thank you!</p>
																			<p style="margin-top:0;margin-bottom:0;">Team LifeAID</p>
																		</td>
																	</tr>
																	<tr>
																		<td height="30">&nbsp;</td>
																	</tr>
																</tbody>
															</table>															
															<!-- // END CONTENT -->
														</td>
														<td class="table-content-wrapper-col-right" width="40">&nbsp;</td>
													</tr>
												</tbody>
											</table>											
											<!-- // END content wrapper -->
										</td>						
									</tr>
								</tbody>
							</table>							
							<!-- // END HEADER -->
						</td>
					</tr>
					<tr>
						<td class="remove-outer-bottom" height="20"></td>
					</tr>
				</tbody>
            </table>			
			<!-- // END wrapper -->
        </center>
    </body>
</html>
	';
	
	?>