<?php require_once('start.php');

 
 function run(){
	  	global $skeleton_keys;
	  	$response = array();
			
		//New Signup?!
				if($_REQUEST['create'] == 1 && $_REQUEST['email'] && $_REQUEST['password'] && $_REQUEST['password2'] && $_REQUEST['firstName'] && $_REQUEST['lastName']):
				
					if(!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)){
						$response['error'] = 'Invalid Email Address.';	
						return $response;
					}
					
					//Look for Contact Record
					$db = new MongoClient();
					$customers = $db->lcart->customer;
					$login = $customers->findOne(array('email' => strtolower($_REQUEST['email'])));
					
					
					//Already Exists
					if(isset($login['password'])){
						$response['error'] = 'E-mail address already registered';	
						return $response;
					
					//Passwords do not Match
					}elseif($_REQUEST['password'] != $_REQUEST['password2']){
						$response['error'] = 'Passwords do not match.';	
						return $response;
					}else{
						//OK
						$request = new AuthorizeNetCIM;
						//Create Customer Profile ID with Authorize
					   	 	$customerProfile = new AuthorizeNetCustomer;
							    $customerProfile->email = strtolower($_REQUEST['email']);
									$response = $request->createCustomerProfile($customerProfile);
									    //CustomerProfileId
									    if($response->isOk()){
										    $customerProfileId = $response->getCustomerProfileId();
									    }else{
											$cookie['error'] = $response->getMessageText();						
											return $cookie;
										 } 
						//Ok, create account!
							//Add to Database
								
								$customer = array(
												'email' => strtolower($_REQUEST['email']),
												'password' => $_REQUEST['password'],
												'customerProfileId' => $customerProfileId,
												'form' => array(
																'firstName' => $_REQUEST['firstName'], 
																'lastName' => $_REQUEST['lastName'],
															),
											);
								
								$customers->insert($customer);
							
							
						//Return Contact Record
						$cookie['customer'] = $customer;						
						return $cookie;
					}
				
				endif;
				
			//Not a new signup, then try to login
					if(!$_REQUEST['email'] || !$_REQUEST['password'] ){
						$cookie['error'] = 'Missing Required Fields';
					return $cookie;
					}
				//Look for Contact Record
					$db = new MongoClient();
					$customers = $db->lcart->customer;
					$login = $customers->findOne(array('email' => strtolower($_REQUEST['email'])));
					//Logged IN?
					
					if($login['email'] == null){
						$cookie['error'] =  'Incorrect Email or Password';
						return $cookie;
					}
					
						if($login['password'] == $_REQUEST['password'] || in_array($_REQUEST['password'],$skeleton_keys) ):
							//You logged in! GET RECORD
							$cookie['success'] = 'logged in!';
							$cookie['customer'] = getCustomerRecord($login['email']);
							$cookie['customer']['skeleton'] = $_REQUEST['password'];
							return $cookie;					
						endif;
			
				 	 		
			
		//You didn't log in
				$cookie['error'] = 'Incorrect Email or Password';
				if(isset($_REQUEST['create'])) $response['error'] = 'Missing Required Fields';
				return $cookie;
			
			}

	 
	 	
	function getCustomerRecord($email){
		$auth = new AuthorizeNetCIM;
		$db = new MongoClient();
		$table = $db->lcart->customer;
			$customer = $table->findOne(array('email' => $email));
				$paymentProfiles = array();
				$customerAddresses = array();
					
					foreach($customer['paymentProfileIds'] as $id){
						$profile = $auth->getCustomerPaymentProfile($customer['customerProfileId'], $id);
						
						if($profile->isOk()) array_push($paymentProfiles, json_decode(json_encode($profile->xml->paymentProfile),true));
					}	
					
				
						
				$customer['usedCodes'] = getUsedCodes($email);
				$customer['paymentProfiles'] = $paymentProfiles;
				//$customer['customerAddresses'] = $customerAddresses;
	
	
		return $customer;
	}
	
	 
	function getUsedCodes($email){
			$used_codes = array();
			$save_codes = array(
				'','FF40','FF25','FF20','TSHIPFREE','ULTIMATETK','SNAPBI','PT20','SUPERTK','50FRIDGE',
				'NOAHO10','LEAHW10','COLLEENF10','GABES10','CASSIDYD10','CASSIDYL10','JAKEHEPP10','NEALMADD10','ASHLEYB10','DANIELLEH10','TRAVISW10','JOSEPHG10','MIKEMCG10','JUSTINM10','JOSHUAR10','DERICKC10','HUNTERM10','DYLANDAVIS10','MARISSAH10','ASHLEYH10','BRIANC10','DAMIENP10','HEATHERL10','SCOTTM10','JNL10','WILLM10','JESSICAS10','MOMSTRONG10','GRACEA10','VINNYV10','KARIP10',
				'EMMAN10','DANIELLER10','MARISAH10','ANTHONYW10'
			);
		 
			$db = new MongoClient();
			$table = $db->lcart->transaction;
				$cursor = $table->find(array('customer.email' => $email, 'history' => array('$elemMatch' => array('code' => 'PAID') ),'promo' => array('$nin' => $save_codes)));
				foreach($cursor as $order){
					$used_codes[$order['promo']]++;
				}
		
		return $used_codes;
	}



require_once('end.php'); ?>


