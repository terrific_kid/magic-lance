<pre><?php
	if($_REQUEST['secret'] != 'keke')die('der!');
	
	require_once('start.php');
	
	echo PHP_EOL . 'Online!' . PHP_EOL;
	 	
	 	$db = new MongoClient();
		$table = $db->lcart->transaction;
		$orders = $table->find( array( 
									'history' => array('$elemMatch' => array('code' => 'PAID')),
									'history.code' => array('$ne' => 'SWIMS'),
									) 
								)->limit(100);
	//Send Orders
	foreach($orders as $my_order){					
		sendSwims($my_order);
	}
	echo PHP_EOL . 'Process Complete!' . PHP_EOL;

?></pre>





<?php		 	
function sendSwims($my_order){
	//API ENDPOINT
	$endpoint = 'https://gospiderweb.com/cgi-bin/smartapi_lifeaid.cgi?cust_id=1174&username=apiLifeaid&password=a4aab58f0f245f51a61b85dd1e329620352f7ab68c496d53299c7012dd083796';			
	
	//Order Details
	$query['cr1'] = $my_order['_id']->{'$id'};
	$query['ct'] = 33;
	
	// Shipping method
	$fedex_ground = 21;
	$fedex_home_delivery = 24;
	$query['method'] = $fedex_ground;
	
	
	//ShipTo
		//Constants
		$query['stemail'] = $my_order['customer']['email'];
		$query['stcountry'] = 'USA';
		//$query['stacct'] = $my_order['order']['ContactId'];
			
		//Name, Company
		if($my_order['customer']['form']['company'] && $my_order['customer']['form']['lastName']){
			$query['stname'] = $my_order['customer']['form']['company'];
			$query['stattn'] = $my_order['customer']['form']['firstName'] . ' ' . $my_order['customer']['form']['lastName'];
		} elseif($my_order['customer']['form']['company']){
			$query['stname'] = $my_order['customer']['form']['company'];
		} else {
			$query['stname'] = $my_order['customer']['form']['firstName'] . ' ' . $my_order['customer']['form']['lastName'];
			$query['method'] = $fedex_home_delivery;
		}
		
		// Attn is required for FedEx.
		if(!$query['stattn']){
			$query['stattn'] = $query['stname'];
		}
	 
		//Shipping Address
	 	$query['staddr'] = $my_order['customer']['form']['shippingAddress1'];
		$query['staddr2'] = $my_order['customer']['form']['shippingAddress2'];
		$query['stcity'] = $my_order['customer']['form']['shippingCity'];
		$query['ststate'] = $my_order['customer']['form']['shippingState'];
		$query['stzip'] = $my_order['customer']['form']['shippingZip'];
		$query['stphone'] = ($my_order['customer']['form']['billingPhone'] ? $my_order['customer']['form']['billingPhone'] : '0000000000');
	
		//Ship to my Billing Address
		if($my_order['customer']['form']['shipToBill'] == 1):
		 	$query['staddr'] = $my_order['customer']['form']['billingAddress1'];
			$query['staddr2'] = $my_order['customer']['form']['billingAddress2'];
			$query['stcity'] = $my_order['customer']['form']['billingCity'];
			$query['ststate'] = $my_order['customer']['form']['billingState'];
			$query['stzip'] = $my_order['customer']['form']['billingZip'];
		    $query['stphone'] = ($my_order['customer']['form']['billingPhone'] ? $my_order['customer']['form']['billingPhone'] : '0000000000');
		endif;
		
		//Ship to my Billing Address
		if($my_order['customer']['form']['shipToApo'] == 1):
		 	$query['staddr'] = $my_order['customer']['form']['apoAddress1'];
			$query['staddr2'] = $my_order['customer']['form']['apoAddress2'];
			$query['stcity'] = $my_order['customer']['form']['apoCity'];
			$query['ststate'] = $my_order['customer']['form']['apoState'];
			$query['stzip'] = $my_order['customer']['form']['apoZip'];
		    $query['stphone'] = ($my_order['customer']['form']['billingPhone'] ? $my_order['customer']['form']['billingPhone'] : '0000000000');
		endif;
	

	
	//Line Items
	$sku_query = '';
	$count = 0;
	foreach($my_order['products'] as $product){
		foreach($product['variations'] as $key => $variation){
			if(!$variation['qty']) continue;
			
			//Add Base Item
			$count++;
			$sku_item['sku'] = $variation['sku'];
			$sku_item['qty'] =  $variation['qty'];
			$sku_query .= '&' . http_build_query($sku_item);
			
			//Add Configured Add-Ons
			foreach($variation['add'] as $addon){
				if($addon['sku']){
				 	$count++;
					$sku_item['sku'] = $addon['sku'];
					$sku_item['qty'] =  $variation['qty'];
					$sku_query .= '&' . http_build_query($sku_item);
				}
			}
			
			//Add Bundled
			foreach($variation['bundle'] as $bundle){
				if($bundle['sku']){
				 	$count++;
					$sku_item['sku'] = $bundle['sku'];
					$sku_item['qty'] =  $variation['qty'];
					$sku_query .= '&' . http_build_query($sku_item);
				}
			}

		}
	}
	//Line Item Count
	$query['licount'] = $count;
	
	//Build Query
	$swim_query = http_build_query($query);
	$url = $endpoint .'&' . $swim_query . $sku_query;
	
		//Send Request
		$response = simplexml_load_file($url);
	
		
			//Success?
			if($response->Status == 'Success'){
			 	echo 'SUCCESS!' . PHP_EOL;
				//Stamp Order as Processed
				stamp($my_order, 'SWIMS-TEST-SUCCESS', $response, 'swimsSync');
				return true;
			}
			
			
			//Error
			echo 'ERROR!' . PHP_EOL;
			//Stamp Order as FAILED
			stamp($my_order, 'SWIMS-FAIL',$response, 'swimsSync');
			
			echo PHP_EOL;
			$subject = 'swim-sync';
			
			$body = 'Order #' . $my_order['_id']->{'$id'} . '<br>';
			$body .= 'Name: ' . $my_order['customer']['form']['lastName'] .', '.$my_order['customer']['form']['firstName'] . '<br>';
			$body .= 'Email: ' . $my_order['customer']['email']. '<br>';
			$body .= 'Company: ' . $my_order['customer']['form']['company']. '<br>';
			$body .= '<br><b>SWIMS Error:'.'<br>';
			$body .= print_r($response,true);
			$body .= '</b>';
			$to = 'log@lifeaidbevco.com';
			
			sendEmail($to, $subject, $body);
			sendEmail('seank@smartwarehousing.com', $subject, $body);
			sendEmail('kitp@smartwarehousing.com', $subject, $body);
		
return false;
}
				
				
				
			 				

?>



 