<?php 

require_once('start.php');
 


function run(){
	global $app;
	global $contact;
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$result = null;
	
	if($request){
		$promo = preg_replace("/[^A-Za-z0-9 ]/", '', $request->promo);
		$products = json_encode($request->products);
		$mode = $request->mode;
		$discounter = new l_Discounter();
		$result = $discounter->handle_promo($products, $promo, $mode);
	}
		
	return $result;
}

	
require_once('end.php'); ?>






