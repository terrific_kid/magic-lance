<?php


// Quick promo creation. Updates $db
function quick_promo(&$db, $code, $amt_off, $group = null){
	$Promos = new Promos();
	$code = strtoupper($code);
	$promo = $Promos->create($code);
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array('subtotal' => array('amt'=>$amt_off, 'op'=>'-'))
	);
	if($group){
		$promo->validations[] = $Promos->create_validation('isvalid_group', 
			array(
				'groups' => array( $group ),
				'op' => 'AND'
			)
		);	
	}
	$db[] = $promo;
}


// Lists all promos (temporary until we store as json file)
function create_promo_db(){
		
	$Promos = new Promos();
	$db = array();

	

	/* FITAID AFFILIATE PROMOS *********************************************************/

	// ---------------------------------------------------------------------
	// Create AUTOSHIP code (affiliate). Purchase 2 autoship cases with a 6 month commitment. Get a free fridge, banner & 3 posters. Add the stand too (not free). Stand is removable.
	$terms = "To qualify for this offer and receive a $397 FITAID refrigerator for FREE, you will be enrolled in a 6 month, 2 case per month auto-ship plan.  If you cancel before the 6 months, you must pay to ship the fridge (undamaged) back to us or you will be charged the full price of $397 for the refrigerator. Additionally, you will have to do 2,000-unbroken burpees for time.  After 6 months, you will be glad you took us up on this amazing offer and will continue making 100% markup on FITAID! Your athletes will also love you more and you get to keep the fridge forever!<br/>
The Posters and Banner are our gift to you. *Stand, lock and shipping not included in the offer.";
	$promo = $Promos->create('AUTOSHIP', $terms);
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'wholesale'
			),
			'op' => 'AND'
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_n_or_greater', // Make sure there are 2 or more
		array(
			'group' => 'case', 
			'slug' => 'fitaid-24-pack-wholesale-autoship',
			'subscription' => 1,
			'n' => 2
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add stand but don't enforce
		array(
			'apply_on_checkout' => false, // Don't apply on checkout
			'products' => array(
				array(
					'slug'=>'fitaid-fridge-stand', 
					'qty'=>1,
				),
				array(
					'slug'=>'fitaid-fridge-lock', 
					'qty'=>1,
				),
			)
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
		array(
			'products' => array(
				array(
					'slug'=>'jackie-perez-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'available-here-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'fitaid-banner', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
				),
				array(
					'slug'=>'fitaid-fridge', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>15
				),
			)
		)
	);
	$db[] = $promo;		



	// ---------------------------------------------------------------------
	// Create LUCKYFA6M code (affiliate). Same as AUTOSHIP
	$terms = "To qualify for this offer and receive a $397 FITAID refrigerator for FREE, you will be enrolled in a 6 month, 2 case per month auto-ship plan.  If you cancel before the 6 months, you must pay to ship the fridge (undamaged) back to us or you will be charged the full price of $397 for the refrigerator. Additionally, you will have to do 2,000-unbroken burpees for time.  After 6 months, you will be glad you took us up on this amazing offer and will continue making 100% markup on FITAID! Your athletes will also love you more and you get to keep the fridge forever!<br/>
The Posters and Banner are our gift to you. *Stand, lock and shipping not included in the offer.";
	$promo = $Promos->create('LUCKYFA6M', $terms);
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'wholesale'
			),
			'op' => 'AND'
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_n_or_greater', // Make sure there are 2 or more
		array(
			'group' => 'case', 
			'slug' => 'fitaid-24-pack-wholesale-autoship',
			'n' => 2
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add stand but don't enforce
		array(
			'apply_on_checkout' => false, // Don't apply on checkout
			'products' => array(
				array(
					'slug'=>'fitaid-fridge-stand', 
					'qty'=>1,
				),
				array(
					'slug'=>'fitaid-fridge-lock', 
					'qty'=>1,
				),
			)
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
		array(
			'products' => array(
				array(
					'slug'=>'jackie-perez-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'available-here-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'fitaid-banner', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
				),
				array(
					'slug'=>'fitaid-fridge', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>15
				),
			)
		)
	);
	$db[] = $promo;		


	// ---------------------------------------------------------------------
	// Create FREEFRIDGE code (affiliate). Buy 10 cases and get a free banner, 3 posters and stand.
	$promo = $Promos->create('FREEFRIDGE');
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'wholesale'
			),
			'op' => 'AND'
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_n_or_greater', // Make sure there are 10
		array(
			'group' => 'case', 
			'slug' => 'fitaid-24-pack-wholesale',
			'n' => 10
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add stand but don't enforce
		array(
			'apply_on_checkout' => false, // Don't apply on checkout
			'products' => array(
				array(
					'slug'=>'fitaid-fridge-stand', 
					'qty'=>1,
				),
			)
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
		array(
			'products' => array(
				array(
					'slug'=>'jackie-perez-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'available-here-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'fitaid-banner', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
				),
				array(
					'slug'=>'fitaid-fridge', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
				),
			)
		)
	);
	$db[] = $promo;	



	// ---------------------------------------------------------------------
	// Create FREEFRIDGE code (affiliate). Buy 10 cases and get a free banner, 3 posters and stand.
	$promo = $Promos->create('FA2CASE14');
	// Only valid for wholesale purchases
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'wholesale'
			),
			'op' => 'AND'
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_n_or_greater', // Make sure there are 10
		array(
			'group' => 'case', 
			'slug' => 'fitaid-24-pack-wholesale',
			'n' => 2
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
		array(
			'products' => array(
				array(
					'slug'=>'fitaid-banner', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
				),
				array(
					'slug'=>'jackie-perez-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'available-here-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'fitaid-punchcards-40pack', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
				),
				array(
					'slug'=>'fitaid-stickers-10pack', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
				),
			)
		)
	);
	$db[] = $promo;
	
	// ---------------------------------------------------------------------
	// Buy 2 cases and get banner, all posters punch cards and stickers
	$promo = $Promos->create('FREEBPPC');
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'wholesale'
			),
			'op' => 'AND'
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_n_or_greater', // Make sure there are 10
		array(
			'group' => 'case', 
			'slug' => 'fitaid-24-pack-wholesale',
			'n' => 2
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
		array(
			'products' => array(
				array(
					'slug'=>'fitaid-banner', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
				),
				array(
					'slug'=>'jackie-perez-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'available-here-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'fitaid-punchcards-40pack', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
				),
				array(
					'slug'=>'fitaid-stickers-10pack', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
				),
			)
		)
	);
	$db[] = $promo;	

	// ---------------------------------------------------------------------
	// Buy a pallet of FA and get 10 free shirt, 2 cases of GA and 2 cases of PA. Free shipping on the entire order
	$promo = $Promos->create('FAPALLET10');
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'wholesale'
			),
			'op' => 'AND'
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_n_or_greater', // Make sure there is 1
		array(
			'slug' => 'fitaid-pallet',
			'n' => 1
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_limit_group_qty', // Limit 10 shirts
		array(
			'group' => 'tops',
			'm'	=> 10,
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
		array(
			'products' => array(
				array(
					'slug'=>'golferaid-24-pack-wholesale', 
					'qty'=>2,
					'max'=>2,
					'discount_price'=>0,
				),
				array(
					'slug'=>'partyaid-24-pack-wholesale', 
					'qty'=>2,
					'max'=>2,
					'discount_price'=>0,
				),
				array(
					'slug' => 'fitaid-black-tshirt-wholesale', 
					'max' => 10,
					'discount_price'=>0,
				),
				array(
					'slug' => 'fitaid-red-tshirt-wholesale', 
					'max' => 10,
					'discount_price'=>0,
				),
				array(
					'slug' => 'fitaid-blue-tshirt-wholesale', 
					'max' => 10,
					'discount_price'=>0,
				),

			)
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array(
			'shipping' => array('amt'=>0, 'op'=>'='),
		)
	);
	$db[] = $promo;	


	// ---------------------------------------------------------------------
	// ANGIE Promo: Buy 3 cases, get 1 FA case free. Free banner & poster.
	$promo = $Promos->create('ANGIE');
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'wholesale'
			),
			'op' => 'AND'
		)
	);
	$promo->validations[] = $Promos->create_validation('isvalid_date', 
		array(
			'start' => '2015-04-01',
			'stop' => '2015-04-30'
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_n_or_greater', // Make sure there are 3
		array(
			'group' => 'case', 
			'slug' => 'fitaid-24-pack-wholesale',
			'n' => 3
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
		array(
			'products' => array(
				array(
					'slug'=>'fitaid-24-pack-wholesale-free', 
					'qty'=>1,
					'max'=>1,
					'shipping'=>0,
					'weight'=>0,
					'discount_price'=>0,
				),
				array(
					'slug'=>'jackie-perez-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'available-here-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'fitaid-banner', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
				),
			)
		)
	);
	$db[] = $promo;

	// ---------------------------------------------------------------------
	// BACKORDER Promo: Minimum 2 case FitAID or 2 case FitAID autoship. FREE case of GolferAID and a case of PartyAID.
	/*
	$promo = $Promos->create('BACKORDER');
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'wholesale'
			),
			'op' => 'AND'
		)
	);
	$promo->validations[] = $Promos->create_validation('isvalid_products_required', 
		array(
			'products_required' => array(
				array(
					'slug' => 'fitaid-24-pack-wholesale',
					'qty' => 2, 
				),
				array(
					'slug' => 'fitaid-24-pack-wholesale-autoship',
					'qty' => 2, 
				),
			),
			'op' => 'OR',
		)
	);	
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
		array(
			'products' => array(
				array(
					'slug'=>'golferaid-24-pack-wholesale-free', 
					'qty'=>1,
					'max'=>1,
					'shipping'=>0,
					'weight'=>0,
					'discount_price'=>0,
				),
				array(
					'slug'=>'partyaid-24-pack-wholesale-free', 
					'qty'=>1,
					'max'=>1,
					'shipping'=>0,
					'weight'=>0,
					'discount_price'=>0,
				),
			)
		)
	);
	$db[] = $promo;	
	*/
	
	// ---------------------------------------------------------------------
	// Buy 2 cases, get a free t-shirt
	/*
	$terms = "<span><a href='/reorders#gear'>Click here</a> to Redeem your FREE T-Shirt!</span>";
	$promo = $Promos->create('CHELSEA', $terms);
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'wholesale'
			),
			'op' => 'AND'
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_n_or_greater', // Make sure there are 2
		array(
			'group' => 'case', 
			'slug' => 'fitaid-24-pack-wholesale',
			'n' => 2
		)
	);
	
	$promo->rules[] = $Promos->create_rule('applyrule_limit_group_qty', // Limit one shirt
		array(
			'group' => 'tops',
			'm'	=> 1,
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
		array(
			'products' => array(
				array(
					'slug'=>'jackie-perez-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'available-here-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'fitaid-banner', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
				),
				array(
					'slug' => 'fitaid-black-tshirt-wholesale', 
					'max' => 1,
					'discount_price'=>0,
				),
				array(
					'slug' => 'fitaid-red-tshirt-wholesale', 
					'max' => 1,
					'discount_price'=>0,
				),
				array(
					'slug' => 'fitaid-blue-tshirt-wholesale', 
					'max' => 1,
					'discount_price'=>0,
				),
				array(
					'slug' => 'fitaid-white-racerback-wholesale', 
					'max' => 1,
					'discount_price'=>0,
				),				
				array(
					'slug' => 'fitaid-black-racerback-wholesale', 
					'max' => 1,
					'discount_price'=>0,
				),
				array(
					'slug' => 'fitaid-pink-racerback-wholesale', 
					'max' => 1,
					'discount_price'=>0,
				),				
			)
		)
	);
	$db[] = $promo;	
	*/

	// ---------------------------------------------------------------------
	// Create ANNIE code (affiliate). Buy 1 or more FA get 1 FA free
	/*
	$promo = $Promos->create('ANNIE');
	$promo->rules[] = $Promos->create_rule('applyrule_n_or_greater', // Make sure there are 10
		array(
			'group' => 'case', 
			'slug' => 'fitaid-24-pack-wholesale',
			'n' => 2
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
		array(
			'products' => array(
				array(
					'slug'=>'jackie-perez-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'available-here-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'fitaid-banner', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
				),
			)
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array(
			'subtotal' => array('amt'=>36, 'op'=>'-'),
		)
	);
	$db[] = $promo;	
	*/

	// ---------------------------------------------------------------------
	// Create NANCY code (affiliate). Buy 2 cases and get free shipping on entire order
	/*
	$promo = $Promos->create('NANCY');
	$promo->rules[] = $Promos->create_rule('applyrule_n_or_greater', // Make sure there are 2
		array(
			'group' => 'case', 
			'slug' => 'fitaid-24-pack-wholesale',
			'n' => 2
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
		array(
			'products' => array(
				array(
					'slug'=>'jackie-perez-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'available-here-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'fitaid-banner', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
				),
			)
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array(
			'shipping' => array('amt'=>0, 'op'=>'='),
		)
	);
	$db[] = $promo;	
	*/
	
	// ---------------------------------------------------------------------
	// Create ISABEL code (affiliate). Buy 10 cases and get free fridge, 2 cases of PA and banners/posters/etc
	/*
	$promo = $Promos->create('ISABEL');
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'wholesale'
			),
			'op' => 'AND'
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_n_or_greater', // Make sure there are 10
		array(
			'group' => 'case', 
			'slug' => 'partyaid-24-pack-wholesale',
			'n' => 12
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
		array(
			'products' => array(
				array(
					'slug'=>'jackie-perez-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'available-here-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'fitaid-banner', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
				),				
			)
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array('subtotal' => array('amt'=>72, 'op'=>'-'))
	);
	$db[] = $promo;	
	*/


/*
	// ---------------------------------------------------------------------
	// Create DIANE code (affiliate). Free case and free banner
	$promo = $Promos->create('DIANE');
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'wholesale'
			),
			'op' => 'AND'
		)
	);
	$promo->validations[] = $Promos->create_validation('isvalid_date', 
		array(
			'start' => '2015-02-10',
			'stop' => '2015-03-01'
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_n_or_greater', // Make sure there are 1
		array(
			'group' => 'case', 
			'slug' => 'fitaid-24-pack-wholesale',
			'n' => 1
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
		array(
			'products' => array(
				array(
					'slug'=>'fitaid-banner', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),				
				array(
					'slug'=>'fitaid-24-pack-wholesale',
					'shipping'=>0,
					'weight'=>0
				),
			)
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array(
			'subtotal' => array('amt'=>36, 'op'=>'-'),
		)
	);
	$db[] = $promo;	

*/


	// ---------------------------------------------------------------------
	// Create FIRESERVICE code (affiliate). Buy 3 cases of fitaid and get a case of partyaid free
	$promo = $Promos->create('FIRESERVICE');
	$promo->rules[] = $Promos->create_rule('applyrule_n_or_greater', // Make sure there are 3
		array(
			'slug' => 'fitaid-24-pack-wholesale',
			'n' => 3
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
		array(
			'products' => array(
				array(
					'slug'=>'partyaid-24-pack-wholesale', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
			)
		)
	);
	$db[] = $promo;	



	// ---------------------------------------------------------------------
	// Create PARTYAID code (affiliate). Buy 8 cases of fitaid and get 2 cases of partyaid free
	$promo = $Promos->create('PARTYAID');
	$promo->rules[] = $Promos->create_rule('applyrule_n_or_greater', // Make sure there are 3
		array(
			'slug' => 'fitaid-24-pack-wholesale',
			'n' => 8
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
		array(
			'products' => array(
				array(
					'slug'=>'partyaid-24-pack-wholesale', 
					'qty'=>2,
					'max'=>2,
					'discount_price'=>0,
					'shipping'=>0
				),
			)
		)
	);
	$db[] = $promo;	



	// ---------------------------------------------------------------------
	// Create GAMESSP code (affiliate). $20 Off shipping.
	$promo = $Promos->create('GAMESSP');
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array(
			'shipping' => array('amt'=>20, 'op'=>'-'),
		)
	);
	$db[] = $promo;	
	


	// ---------------------------------------------------------------------
	// Create 15FRIDGE code (affiliate). $20 Off shipping.
	$promo = $Promos->create('15FRIDGE');
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'wholesale'
			),
			'op' => 'AND'
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
		array(
			'products' => array(
				array(
					'slug'=>'fitaid-fridge', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
				),
			)
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array(
			'shipping' => array('amt'=>15, 'op'=>'+'),
		)
	);
	$db[] = $promo;
	


	// ---------------------------------------------------------------------
	// Create SNAPBI code (affiliate). $10 Off subtotal.
	$promo = $Promos->create('SNAPBI');
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array('subtotal' => array('amt'=>10, 'op'=>'-'))
	);
	$db[] = $promo;
	


	// ---------------------------------------------------------------------
	// Create TKC code (affiliate). $10 Off subtotal.
	$promo = $Promos->create('TKC');
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array('subtotal' => array('amt'=>9.99, 'op'=>'-'))
	);
	$db[] = $promo;



	// ---------------------------------------------------------------------
	// Create FITAID555 code (affiliate). $10 Off subtotal.
	$promo = $Promos->create('FITAID555');
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array('subtotal' => array('amt'=>10, 'op'=>'-'))
	);
	$db[] = $promo;
	

 

	// ---------------------------------------------------------------------
	// TODO: Create 20 Case Mix-n-match code (affiliate). If they buy 20 cases of anything, shipping is free. No code needed

	
	
	
	
	/* FITAID RETAIL PROMOS *********************************************************/





	// ---------------------------------------------------------------------
	// Create 2FREECANS code (retail). Nothing else can be in cart.
	$promo = $Promos->create('2FREECANS');
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'retail'
			),
			'op' => 'AND'
		)
	);
	//$promo->rules[] = $Promos->create_rule('applyrule_empty_cart'); // Delete all products
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
		array(
			'products' => array(
				array(
					'slug'=>'fitaid-2-free-cans', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
				),
			)
		)
	);
	$db[] = $promo;

	// ---------------------------------------------------------------------
	// Create 2FREECANSSV036RD code (retail). Nothing else can be in cart. Add shipping $5.15
	$promo = $Promos->create('2FREECANSSV036RD');
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'retail'
			),
			'op' => 'AND'
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
		array(
			'products' => array(
				array(
					'slug'=>'fitaid-2-free-cans', 
					'qty'=>1,
					'max'=>1,
					'shipping'=>0,
					'discount_price'=>0,
				),
			)
		)
	);
	$db[] = $promo;

	// ---------------------------------------------------------------------
	// Create RCFC code (retail). Take $20 off the order if it has fit club items in it.
	$promo = $Promos->create('RCFC');
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'retail',
			),
			'op' => 'AND'
		)
	);
	$promo->validations[] = $Promos->create_validation('isvalid_products_required', 
		array(
			'products_required' => array(
				array(
					'group' => 'fitclub',
					'qty' => 1, 
				),
			),
			'op' => 'AND',
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array(
			'subtotal' => array('amt'=>20, 'op'=>'-'),
		)
	);
	$db[] = $promo;	



	// ---------------------------------------------------------------------
	// Create RETAILFA code (retail). Take a consumer case and make it 39.60.
	$promo = $Promos->create('RETAILFA');
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'retail'
			),
			'op' => 'AND'
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
		array(
			'products' => array(
				array(
					'slug'=>'fitaid-24-pack', 
					'discount_price'=>39.60,
					'shipping'=>0,
				),
			)
		)
	);
	$db[] = $promo;	



	// ---------------------------------------------------------------------
	// Create PT20 code (retail). Take 20% off the order. This coupon IS reusable.
	$promo = $Promos->create('PT20');
	
	// Only valid for retail purchases
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'retail'
			),
			'op' => 'AND'
		)
	);
	
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array(
			'subtotal' => array('amt'=>0.8, 'op'=>'*'),
		)
	);
	$db[] = $promo;	
	


	// ---------------------------------------------------------------------
	// Create FC20 code (retail). Take 20$ off the order.
	$promo = $Promos->create('FC20');
	
	// Only valid for retail purchases
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'retail'
			),
			'op' => 'AND'
		)
	);
	$promo->validations[] = $Promos->create_validation('isvalid_products_required',
		array(
			'products_required' => array(
				array(
					'group' => 'fitclub',
					'qty' => 1, 
				),
			),
			'op' => 'AND',
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array(
			'subtotal' => array('amt'=>20, 'op'=>'-'),
		)
	);
	$db[] = $promo;	


	// ---------------------------------------------------------------------
	// Create USA10 code (retail). Take 10$ off the order.
	$promo = $Promos->create('USA10');
	
	// Only valid for retail purchases
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'retail'
			),
			'op' => 'AND'
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array(
			'subtotal' => array('amt'=>10, 'op'=>'-'),
		)
	);
	$db[] = $promo;	


	// ---------------------------------------------------------------------
	// Create APO10 code (retail & apo). Take 10$ off the order.
	$promo = $Promos->create('APO10');
	
	// Only valid for retail purchases
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'retail',
				'apo'
			),
			'op' => 'OR'
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array(
			'subtotal' => array('amt'=>10, 'op'=>'-'),
		)
	);
	$db[] = $promo;	


	// ---------------------------------------------------------------------
	// Create FITBUCKS code (retail). Take 10$ off the order.
	$promo = $Promos->create('FITBUCKS');
	
	// Only valid for retail purchases
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'retail'
			),
			'op' => 'AND'
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array(
			'subtotal' => array('amt'=>10, 'op'=>'-'),
		)
	);
	$db[] = $promo;
		

	// ---------------------------------------------------------------------
	// Create OHLSEN code (retail). Take 10$ off the order.
	$promo = $Promos->create('OHLSEN');
	
	// Only valid for retail purchases
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'retail'
			),
			'op' => 'AND'
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array(
			'subtotal' => array('amt'=>10, 'op'=>'-'),
		)
	);
	$db[] = $promo;

	
	// ---------------------------------------------------------------------
	// Create SUPERTK for test transactions 99% off - This coupon IS reusable.
	$promo = $Promos->create('SUPERTK');
	
		
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array(
			'subtotal' => array('amt'=>0.01, 'op'=>'*'),
		)
	);
	$db[] = $promo;	
	
	// ---------------------------------------------------------------------
	// Create TSHIPFREE - used in house for giving free shipping
	$promo = $Promos->create('TSHIPFREE');
	
		
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array(
			'shipping' => array('amt'=>0, 'op'=>'='),
		)
	);
	$db[] = $promo;	

	// ---------------------------------------------------------------------
	// Create FF40. 40% off any purchase. For consumer site only
	$promo = $Promos->create('FF40');
	// Only valid for retail purchases
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'retail'
			),
			'op' => 'AND'
		)
	);	
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array(
			'subtotal' => array('amt'=>0.60, 'op'=>'*'),
		)
	);
	$db[] = $promo;	


	// ---------------------------------------------------------------------
	// Create For consumer site only
	/*
	$promo = $Promos->create('PREORDER');
	// Only valid for retail purchases
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'retail'
			),
			'op' => 'AND'
		)
	);	
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add stand but don't enforce
		array(
			'products' => array(
				array(
					'slug'=>'fitclub-fran', 
					'discount_price'=>44.25,
				),
			)
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add stand but don't enforce
		array(
			'products' => array(
				array(
					'slug'=>'fitclub-gymrat', 
					'discount_price'=>99.75,
				),
			)
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add stand but don't enforce
		array(
			'products' => array(
				array(
					'slug'=>'fitaid-24-pack', 
					'discount_price'=>44.75,
				),
			)
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add stand but don't enforce
		array(
			'products' => array(
				array(
					'slug'=>'fitaid-48-pack', 
					'discount_price'=>99.75,
				),
			)
		)
	);

	$db[] = $promo;	
	*/
	 	
	// ---------------------------------------------------------------------
	$promo = $Promos->create('FAFFE10');
	
	// Only valid for retail purchases
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'retail'
			),
			'op' => 'AND'
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array(
			'subtotal' => array('amt'=>10, 'op'=>'-'),
		)
	);
	$db[] = $promo;
	
		
	// ---------------------------------------------------------------------
    // LINDA Promo: Get 1 FA case for $1
    $promo = $Promos->create('LINDA');
    $promo->validations[] = $Promos->create_validation('isvalid_group', 
        array(
            'groups' => array(
                'wholesale'
            ),
            'op' => 'AND'
        )
    );
    $promo->validations[] = $Promos->create_validation('isvalid_date', 
		array(
			'start' => '2015-05-01',
			'stop' => '2015-05-31'
		)
	);
    $promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
        array(
            'products' => array(
                array(
                    'slug'=>'fitaid-24-pack-wholesale', 
                    'qty'=>1,
                    'max'=>1,
                    'shipping'=>0,
                    'weight'=>0,
                    'discount_price'=>1,
                ),
                array(
                    'slug'=>'fitaid-banner', 
                    'qty'=>1,
                    'max'=>1,
                    'discount_price'=>0,
                    'shipping'=>0
                ),
                array(
                    'slug'=>'jackie-perez-poster', 
                    'qty'=>1,
                    'max'=>1,
                    'discount_price'=>0,
                    'shipping'=>0
                ),
                array(
                    'slug'=>'available-here-poster', 
                    'qty'=>1,
                    'max'=>1,
                    'discount_price'=>0,
                    'shipping'=>0
                ),
                array(
                    'slug'=>'fitaid-punchcards-40pack', 
                    'qty'=>1,
                    'max'=>1,
                    'discount_price'=>0,
                    'shipping'=>0
                ),
            )
        )
    );
    $db[] = $promo;


    // ---------------------------------------------------------------------
    // GOLFER20 Promo: 20% off GolferAid cases
    $promo = $Promos->create('GOLFER20');
    $promo->validations[] = $Promos->create_validation('isvalid_group', 
        array(
            'groups' => array(
                'retail'
            ),
            'op' => 'AND'
        )
    );
    $promo->validations[] = $Promos->create_validation('isvalid_date', 
        array(
            'stop' => '2015-06-14'
        )
    );
    $promo->validations[] = $Promos->create_validation('isvalid_products_required',
        array(
            'products_required' => array(
                array(
                    'slug' => 'golferaid-24-pack',
                    'qty' => 1, 
                ),
                array(
                    'slug' => 'golferaid-48-pack',
                    'qty' => 1, 
                ),
            ),
            'op' => 'OR',
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_set_products',
        array(
            'products' => array(
                array(
                    'slug'=>'golferaid-24-pack', 
                    'discount_price'=>0.8,
                    'op'=>'*'
                ),
                array(
                    'slug'=>'golferaid-48-pack', 
                    'discount_price'=>0.8,
                    'op'=>'*'
                ),
            )
        )
    );
    $db[] = $promo;


    // ---------------------------------------------------------------------
    // 20MEMORIAL Promo: 20% off Memorial Day sale
    $promo = $Promos->create('20MEMORIAL');
    $promo->validations[] = $Promos->create_validation('isvalid_group', 
        array(
            'groups' => array(
                'retail'
            ),
            'op' => 'AND'
        )
    );
    $promo->validations[] = $Promos->create_validation('isvalid_date', 
        array(
            'stop' => '2015-05-17'
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_set_cart',
        array(
            'subtotal' => array('amt'=>0.8, 'op'=>'*'),
        )
    );
    $db[] = $promo;


    // ---------------------------------------------------------------------
    // FRIDGE Promo: Free fridge and extras
    $promo = $Promos->create('FRIDGE');
    $promo->validations[] = $Promos->create_validation('isvalid_group', 
        array(
            'groups' => array(
                'retailer'
            ),
            'op' => 'AND'
        )
    );
    $promo->validations[] = $Promos->create_validation('isvalid_products_required', // 10 or more cases
        array(
            'products_required' => array(
                array(
                    'group' => 'case',
                    'qty' => 10,
                ),
            ),
            'op' => 'AND',
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
        array(
            'products' => array(
                array(
                    'slug'=>'fitaid-fridge-retailer', 
                    'qty'=>1,
                    'max'=>1,
                    'discount_price'=>0,
                    'shipping'=>50
                ),
                array(
                    'slug'=>'jackie-perez-poster', 
                    'qty'=>1,
                    'max'=>1,
                    'discount_price'=>0,
                    'shipping'=>0
                ),
                array(
                    'slug'=>'available-here-poster', 
                    'qty'=>1,
                    'max'=>1,
                    'discount_price'=>0,
                    'shipping'=>0
                ),
                array(
                    'slug'=>'fitaid-banner', 
                    'qty'=>1,
                    'max'=>1,
                    'discount_price'=>0,
                    'shipping'=>0
                )
            )
        )
    );
    $db[] = $promo;


    // ---------------------------------------------------------------------
    // 2PAFREE Promo: Buy 8 cases of FitAID, get 2 cases of PartyAID
    $promo = $Promos->create('2PAFREE');
    $promo->validations[] = $Promos->create_validation('isvalid_group', 
        array(
            'groups' => array(
                'wholesale'
            ),
            'op' => 'AND'
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_n_or_greater',
        array(
            'slug' => 'fitaid-24-pack-wholesale',
            'n' => 8
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_set_products',
        array(
            'products' => array(
                array(
                    'slug'=>'partyaid-24-pack-wholesale', 
                    'qty'=>2,
                    'max'=>2,
                    'discount_price'=>0
                )
            )
        )
    );
    $db[] = $promo;


    // ---------------------------------------------------------------------
    // FITFAM15 Promo: Buy 20 cases, get free shipping & 2 cases of PartyAID
    $promo = $Promos->create('FITFAM15');
    $promo->validations[] = $Promos->create_validation('isvalid_group', 
        array(
            'groups' => array(
                'wholesale'
            ),
            'op' => 'AND'
        )
    );
    $promo->validations[] = $Promos->create_validation('isvalid_date', 
        array(
            'stop' => '2015-08-23'
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_n_or_greater',
        array(
            'slug' => 'fitaid-24-pack-wholesale',
            'n' => 20
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_set_products',
        array(
            'products' => array(
                array(
                    'slug'=>'partyaid-24-pack-wholesale', 
                    'qty'=>2,
                    'max'=>2,
                    'discount_price'=>0
                )
            )
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array(
			'shipping' => array('amt'=>0, 'op'=>'='),
		)
	);
    $db[] = $promo;


    // ---------------------------------------------------------------------
    // 125FRIDGE Promo: Buy 6 cases, get fridge for $125
    $promo = $Promos->create('125FRIDGE');
    $promo->validations[] = $Promos->create_validation('isvalid_group', 
        array(
            'groups' => array(
                'retailer'
            ),
            'op' => 'AND'
        )
    );
    $promo->validations[] = $Promos->create_validation('isvalid_products_required', // 6 or more cases
        array(
            'products_required' => array(
                array(
                    'group' => 'case',
                    'qty' => 6,
                ),
            ),
            'op' => 'AND',
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
        array(
            'products' => array(
                array(
                    'slug'=>'fitaid-fridge-retailer', 
                    'qty'=>1,
                    'max'=>1,
                    'discount_price'=>125,
                    'shipping'=>50
                ),
                array(
                    'slug'=>'available-here-poster', 
                    'qty'=>1,
                    'max'=>1,
                    'discount_price'=>0,
                    'shipping'=>0
                ),
            )
        )
    );
    $db[] = $promo;


	// ---------------------------------------------------------------------
    // 50FRIDGE Promo: Free fridge for retailers
    $promo = $Promos->create('50FRIDGE');
    $promo->validations[] = $Promos->create_validation('isvalid_group', 
        array(
            'groups' => array(
                'retailer'
            ),
            'op' => 'AND'
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_n_or_greater',
        array(
            'slug' => 'fitaid-fridge-retailer',
            'n' => 1
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
        array(
            'products' => array(
                array(
                    'slug'=>'fitaid-fridge-retailer', 
                    'qty'=>1,
                    'max'=>1,
                    'discount_price'=>0
                )
            )
        )
    );
    $db[] = $promo;


    // ---------------------------------------------------------------------
    // JULY20 Promo: 20% off PartyAID
   	$promo = $Promos->create('JULY20');
    $promo->validations[] = $Promos->create_validation('isvalid_group', 
        array(
            'groups' => array(
                'retail'
            ),
            'op' => 'AND'
        )
    );
    $promo->validations[] = $Promos->create_validation('isvalid_date', 
        array(
            'stop' => '2015-06-25'
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_n_or_greater',
        array(
            'slug' => 'partyaid-24-pack',
            'group' => 'partyaid',
            'n' => 1
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
        array(
            'products' => array(
            	array(
                    'slug'=>'partyaid-24-pack',
                    'discount_price'=>0.8,
                    'op'=>'*'
                ),
                array(
                    'slug'=>'partyaid-48-pack',
                    'discount_price'=>0.8,
                    'op'=>'*'
                )
            )
        )
    );
    $db[] = $promo;


	// ---------------------------------------------------------------------
	// 1FC3960 Promo: Buy 4 cases, get $39.60 off
	$promo = $Promos->create('1FC3960');
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'retailer'
			),
			'op' => 'AND'
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_n_or_greater',
		array(
			'group' => 'case',
			'slug' => 'fitaid-24-pack-retailer',
			'n' => 4
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array(
			'subtotal' => array('amt'=>39.60, 'op'=>'-')
		)
	);
	$db[] = $promo;


	// ---------------------------------------------------------------------
	// KELLY Promo: Buy 3 cases of FA, get free stuff
	$promo = $Promos->create('KELLY');
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'wholesale'
			),
			'op' => 'AND'
		)
	);
	$promo->validations[] = $Promos->create_validation('isvalid_date', 
		array(
			'stop' => '2015-06-30'
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_n_or_greater',
		array(
			'slug' => 'fitaid-24-pack-wholesale',
			'n' => 3
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
		array(
			'products' => array(
				array(
					'slug'=>'partyaid-24-pack-wholesale', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'fitaid-banner', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'available-here-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'jackie-perez-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'neal-maddox-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				)
			)
		)
	);
	$db[] = $promo;


	// ---------------------------------------------------------------------
	// MARY Promo: Buy 10 cases of FA, get free stuff and free shipping
	$promo = $Promos->create('MARY');
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'wholesale'
			),
			'op' => 'AND'
		)
	);
	$promo->validations[] = $Promos->create_validation('isvalid_date', 
		array(
			'stop' => '2015-07-31'
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_n_or_greater',
		array(
			'slug' => 'fitaid-24-pack-wholesale',
			'n' => 10
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
		array(
			'products' => array(
				array(
					'slug'=>'fitaid-banner', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'fitaid-fridge', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'available-here-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'jackie-perez-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				),
				array(
					'slug'=>'neal-maddox-poster', 
					'qty'=>1,
					'max'=>1,
					'discount_price'=>0,
					'shipping'=>0
				)
			)
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array(
			'shipping' => array('amt'=>0, 'op'=>'='),
		)
	);
	$db[] = $promo;


	// ---------------------------------------------------------------------
	// GACWHO10 Promo: Buy GA, get $10 off
	$promo = $Promos->create('GACWHO10');
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'retail'
			),
			'op' => 'AND'
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_n_or_greater',
		array(
			'group' => 'golferaid',
			'slug' => 'golferaid-24-pack',
			'n' => 1
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array(
			'subtotal' => array('amt'=>10, 'op'=>'-')
		)
	);
	$db[] = $promo;


	// ---------------------------------------------------------------------
	// FITGEAR20 Promo: 20% off apparel
	$promo = $Promos->create('FITGEAR20');
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'retail'
			),
			'op' => 'AND'
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_groups',
		array(
			'groups' => array(
				'apparel' => array(
					'discount_price' => 0.8,
					'op' => '*'
				)
			)
		)
	);
	$db[] = $promo;


	// ---------------------------------------------------------------------
	// STFU Promo: Buy 1 case, get $10 off
	$promo = $Promos->create('STFU');
	$promo->validations[] = $Promos->create_validation('isvalid_group', 
		array(
			'groups' => array(
				'retail'
			),
			'op' => 'AND'
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_n_or_greater',
		array(
			'group' => 'case',
			'slug' => 'fitaid-24-pack',
			'n' => 1
		)
	);
	$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
		array(
			'subtotal' => array('amt'=>10, 'op'=>'-')
		)
	);
	$db[] = $promo;


    // ---------------------------------------------------------------------
    // BUY3GET1 Promo: Buy 3 FA, get 1 PA
    $promo = $Promos->create('BUY3GET1');
    $promo->validations[] = $Promos->create_validation('isvalid_group', 
        array(
            'groups' => array(
                'wholesale'
            ),
            'op' => 'AND'
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_n_or_greater',
        array(
            'slug' => 'fitaid-24-pack-wholesale',
            'n' => 3
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_set_products',
        array(
            'products' => array(
                array(
                    'slug'=>'partyaid-24-pack-wholesale', 
                    'qty'=>1,
                    'max'=>1,
                    'discount_price'=>0
                )
            )
        )
    );
    $db[] = $promo;


    // ---------------------------------------------------------------------
    // B3G1 Promo: Buy 3 FA, get 1 PA
    $promo = $Promos->create('B3G1');
    $promo->validations[] = $Promos->create_validation('isvalid_group', 
        array(
            'groups' => array(
                'retailer'
            ),
            'op' => 'AND'
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_n_or_greater',
        array(
            'slug' => 'fitaid-24-pack-retailer',
            'n' => 3
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_set_products',
        array(
            'products' => array(
                array(
                    'slug'=>'partyaid-24-pack-retailer', 
                    'qty'=>1,
                    'max'=>1,
                    'discount_price'=>0
                )
            )
        )
    );
    $db[] = $promo;


    // ---------------------------------------------------------------------
    // CF6FREE Promo: Buy 6 FA, get free shipping
    $promo = $Promos->create('CF6FREE');
    $promo->validations[] = $Promos->create_validation('isvalid_group', 
        array(
            'groups' => array(
                'wholesale'
            ),
            'op' => 'AND'
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_n_or_greater',
        array(
            'slug' => 'fitaid-24-pack-wholesale',
            'n' => 6
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_set_cart',
        array(
            'shipping' => array('amt'=>0, 'op'=>'='),
        )
    );
    $db[] = $promo;


    // ---------------------------------------------------------------------
    // 15FITLIFE Promo: Buy 4 FA, get 15% off
    $promo = $Promos->create('15FITLIFE');
    $promo->validations[] = $Promos->create_validation('isvalid_group', 
        array(
            'groups' => array(
                'retailer'
            ),
            'op' => 'AND'
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_n_or_greater',
        array(
            'slug' => 'fitaid-24-pack-retailer',
            'n' => 4
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_set_cart',
        array(
            'subtotal' => array('amt'=>0.85, 'op'=>'*')
        )
    );
    $db[] = $promo;


    // ---------------------------------------------------------------------
    // FREEGA1 Promo: Buy 1 FA, get 1 GA
    $promo = $Promos->create('FREEGA1');
    $promo->validations[] = $Promos->create_validation('isvalid_group', 
        array(
            'groups' => array(
                'retailer'
            ),
            'op' => 'AND'
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_n_or_greater',
        array(
            'slug' => 'fitaid-24-pack-retailer',
            'n' => 1
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_set_products',
        array(
            'products' => array(
                array(
                    'slug'=>'golferaid-24-pack-retailer', 
                    'qty'=>1,
                    'max'=>1,
                    'discount_price'=>0
                )
            )
        )
    );
    $db[] = $promo;


    // ---------------------------------------------------------------------
    // CF4FREE Promo: Buy 4 FA, get free shipping
    $promo = $Promos->create('CF4FREE');
    $promo->validations[] = $Promos->create_validation('isvalid_group', 
        array(
            'groups' => array(
                'wholesale'
            ),
            'op' => 'AND'
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_n_or_greater',
        array(
            'group' => 'case',
            'slug' => 'fitaid-24-pack-wholesale',
            'n' => 4
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_set_cart',
        array(
            'shipping' => array('amt'=>0, 'op'=>'=')
        )
    );
    $db[] = $promo;


    // ---------------------------------------------------------------------
    // PA25BM Promo: 25% off PA
    $promo = $Promos->create('PA25BM');
    $promo->validations[] = $Promos->create_validation('isvalid_group', 
        array(
            'groups' => array(
                'retail'
            ),
            'op' => 'AND'
        )
    );
    $promo->validations[] = $Promos->create_validation('isvalid_date', 
        array(
            'stop' => '2015-08-19'
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_n_or_greater',
        array(
            'slug' => 'partyaid-24-pack',
            'group' => 'partyaid',
            'n' => 1
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
        array(
            'products' => array(
            	array(
                    'slug'=>'partyaid-24-pack',
                    'discount_price'=>0.75,
                    'op'=>'*'
                ),
                array(
                    'slug'=>'partyaid-48-pack',
                    'discount_price'=>0.75,
                    'op'=>'*'
                )
            )
        )
    );
    $db[] = $promo;


    // ---------------------------------------------------------------------
    // SUNAID Promo: Purchase $30, get $10 off
    $promo = $Promos->create('SUNAID');
    $promo->validations[] = $Promos->create_validation('isvalid_group', 
        array(
            'groups' => array(
                'retail'
            ),
            'op' => 'AND'
        )
    );
    $promo->validations[] = $Promos->create_validation('isvalid_cart', 
        array(
            'subtotal' => array('amt'=>30, 'op'=>'>=')
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_set_cart',
        array(
            'subtotal' => array('amt'=>10, 'op'=>'-')
        )
    );
    $db[] = $promo;


    // ---------------------------------------------------------------------
    // PAFAM2015 Promo: 20% off PA
    $promo = $Promos->create('PAFAM2015');
    $promo->validations[] = $Promos->create_validation('isvalid_group', 
        array(
            'groups' => array(
                'retail'
            ),
            'op' => 'AND'
        )
    );
    $promo->validations[] = $Promos->create_validation('isvalid_date', 
        array(
            'stop' => '2015-08-26'
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_n_or_greater',
        array(
            'slug' => 'partyaid-24-pack',
            'group' => 'partyaid',
            'n' => 1
        )
    );
    $promo->rules[] = $Promos->create_rule('applyrule_set_products', // Add products and set prices
        array(
            'products' => array(
            	array(
                    'slug'=>'partyaid-24-pack',
                    'discount_price'=>0.8,
                    'op'=>'*'
                ),
                array(
                    'slug'=>'partyaid-48-pack',
                    'discount_price'=>0.8,
                    'op'=>'*'
                )
            )
        )
    );
    $db[] = $promo;

   
	/*
		Add quick promos for retail site
	*/
	$quick_promos = array(
		'INSTA10' => 10,
		'BLONYX' => 10,
		'PALEO' => 10,
		'SOXBOX' => 10,
		'FARYBC10' => 10,
		'FACWHO10' => 10,
		'CF4FIRE' => 10,
		'ERICMAGEE' => 10,
		'JORDANTROYAN' => 10,
		'HOLLYMATA' => 10,
		'FB10' => 10,
		'TW10' => 10,
		'YT10' => 10,
		'GF10' => 10,
		'OCR10' => 10,
		'LB10' => 10,
		'CA1D10' => 10,
		'FITLIFE10' => 10,
		'FITLIFE' => 5,
		'MARINEWOD' => 10,
		'WODTALK' => 10,
		'THEBOX' => 10,
		'ROCKTAPE' => 10,
		'JACKIEPEREZ' => 10,
		'BAILEY' => 10,
		'STEPHPEREZ' => 10,
		'EMILY' => 10,
		'CARRIVEAU' => 10,
		'TAYLOR' => 10,
		'GARYHELMICK' => 10,
		'SARAHSCHOLL' => 10,
		'EMMALEE' => 10,
		'JTROYAN' => 10,
		'PARSONEAULT' => 10,
		'MILLERTIME' => 10,
		'ASMITH' => 10,
		'PJRUBEL' => 10,
		'WHITDABOSS' => 10,
		'VIDAL' => 10,
		'APRIL' => 10,
		'MORRISON' => 10,
		'DB10' => 10,
		'CD1D10' => 10,
		'AEQUITAS' => 10,
		'WODPLANET' => 10,
		'WODBOX' => 15,
		'AMANDASCHWARTZ' => 10,
		'ANNATUNNICLIFFE' => 10,
		'IRONMILE' => 10,
		'SEANTHOMSON' => 10,
		'CRAIGKENNEY' => 10,
		'SETHSILVA' => 10,
		'JORDANCOOK' => 10,
		'ROBBIEDAVIS' => 10,
		'NICOLECAPURSO' => 10,
		'BOE10' => 10,
		'FA2FC10' => 10,
		'ARFITAID' => 10,
		'MFFL10' => 10,
		'ADAM10' => 10,
		'BLUEMAN10' => 10,
		'DUKE10' => 10,
		'NATE10' => 10,
		'RICH10' => 10,
		'SEAN10' => 10,
		'LAUREN10' => 10,
		'BRIERLEY10' => 10,
		'KLING10' => 10,
		'DELGRANDE10' => 10,
		'MORANT10' => 10,
		'ABBOTT10' => 10,
		'LACLAIR10' => 10,
		'HANNAH10' => 10,
		'ASHLEY10' => 10,
		'COLLEEN10' => 10,
		'GABE10' => 10,
		'CASSIDY10' => 10,
		'HUNTER10' => 10,
		'NEAL10' => 10,
		'JACOB10' => 10,
		'NOAHO10' => 10,
		'LEAHW10' => 10,
		'COLLEENF10' => 10,
		'GABES10' => 10,
		'CASSIDYD10' => 10,
		'CASSIDYL10' => 10,
		'JAKEHEPP10' => 10,
		'NEALMADD10' => 10,
		'ASHLEYB10' => 10,
		'DANIELLEH10' => 10,
		'TRAVISW10' => 10,
		'JOSEPHG10' => 10,
		'MIKEMCG10' => 10,
		'JUSTINM10' => 10,
		'JOSHUAR10' => 10,
		'DERICKC10' => 10,
		'HUNTERM10' => 10,
		'DYLANDAVIS10' => 10,
		'MARISSAH10' => 10,
		'ASHLEYH10' => 10,
		'BRIANC10' => 10,
		'DAMIENP10' => 10,
		'HEATHERL10' => 10,
		'SCOTTM10' => 10,
		'JNL10' => 10,
		'WILLM10' => 10,
		'JESSICAS10' => 10,
		'MOMSTRONG10' => 10,
		'GRACEA10' => 10,
		'VINNYV10' => 10,
		'KARIP10' => 10,
		'FA06X15' => 10,
		'GFIT101' => 10,
		'COOP10' => 10,
		'EMMAN10' => 10,
		'DANIELLER10' => 10,
		'MARISAH10' => 10,
		'ANTHONYW10' => 10,
	);
	
	foreach($quick_promos as $code => $amt_off){
			
		if($Promos->find($db, $code)){
			error_log(__FILE__ . " error: Promo $code already exists");
		}
		quick_promo($db, $code, $amt_off, 'retail');
	}
	
	return $db;
	
	
}