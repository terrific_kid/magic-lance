<?php

/*
 *
 * Class for handling promos. Used by discounter. Promo codes are case-insensitve 
 * and will be upper-cased.
 *
 */
 


class Promos {
	
	// Creates a promo
	public function create($name, $terms = ''){
		$promo = new stdClass();
		$promo->name = strtoupper($name);
		$promo->terms = $terms;
		$promo->rules = array();
		$promo->validations = array();
		return $promo;
	}
	
	
	/* 
	 * Creates a rule for a promo. All rule methods must begin with 'applyrule_'
	 * 	$callback: name of the rule callback function.
	 * 	$args: array of args for rule
	 * 	$params: other params
	 */
	public function create_rule($callback, $args = null, $params = null){
		
		$rule = new stdClass();
		if((strpos($callback, 'applyrule_') !== 0) || !method_exists(get_class($this), $callback)){
			error_log(__METHOD__ . ": Error: {$callback} is not a valid callback.");
			return null;
		}
		$rule->callback = $callback;
		$rule->args = $args;
		return $rule;
		
	}


	/* 
	 * Creates a validation for a promo. All validation callback methods must begin with 'isvalid_'
	 * 	$callback: name of the validation callback function.
	 * 	$args: array of args for callback
	 * 	$params: other params
	 */
	public function create_validation($callback, $args = null, $params = null){
		
		$validation = new stdClass();
		if((strpos($callback, 'isvalid_') !== 0) || !method_exists(get_class($this), $callback)){
			error_log(__METHOD__ . " Error: {$callback} is not a valid callback.");
			return null;
		}
		$validation->callback = $callback;
		$validation->args = $args;
		return $validation;
		
	}
	
	
	// Validates a promo. Returns boolean.
	public function validate($db, $promo, $cart){
			
		// No validations required
		if(! $promo->validations || count($promo->validations) == 0)
			return true;
			
		$result = true;
		
		foreach($promo->validations as $validation){
		
			$callback = $validation->callback;
		
			// Run the callback
			if($this->$callback($cart, $validation->args) === false){
			
				$result = false;
				
				// Maybe run an invalid callback here?
			}
			
		}
		
		return $result;
		
	}
	
	
	public function apply_rule($rule, $discounter){
		$callback = $rule->callback;
		$this->$callback($discounter, $rule->args);
	}
	
	
	// Check if a code exists in db and return the promo if found.
	public function find($db, $code){
		$code = strtoupper($code);
		foreach($db as $promo){
			if($promo->name == $code)
				return $promo;
		}
		return null;
	}
		
	
	// Validation Callbacks -----------------------------------------------------------
	
	/*
	 * A validation callback should always return true/false if valid/invalid. If they
	 * need to update cart quantities, they should do so and return true. Callback method
	 * names should start with isvalid_
	 *
	 */
	
	/*
	 * Callback for validating total cart values.
	 */
	public function isvalid_cart($cart = null, $args = null) {
		$valid = true;
		$subtotal = $shipping = 0;

		// Loop through products
		foreach(get_object_vars($cart) as $key => $product){
			
			if($key == 'version') continue;
			
			if($product->variations){
				$variations = get_object_vars($product->variations);
				
				foreach($variations as $key => $var_obj){
					if($var_obj->qty && $var_obj->qty > 0){ // Object in cart
						$subtotal += $product->price;
						$shipping += $product->shipping;
					}
				}
			}
		}

		if (isset($args['subtotal'])) {
			$valid = $valid && $this->compare($subtotal, $args['subtotal']['amt'], $args['subtotal']['op']);
		}
		if (isset($args['shipping'])) {
			$valid = $valid && $this->compare($subtotal, $args['shipping']['amt'], $args['shipping']['op']);
		}

		return $valid;
	}
	

	/*
	 * Callback for requiring that all products be in one or more groups.
	 */	
	public function isvalid_group($cart, $args){
		
		$required_groups = $args['groups'];
		$op = isset($args['op']) ? $args['op'] : 'AND';			
		$bad_products = 0;
			
		// Loop through products
		foreach(get_object_vars($cart) as $key => $product){
			
			$misses = 0;
			if($key == 'version') continue;
			
			if($product->variations){
				$variations = get_object_vars($product->variations);
				
				foreach($variations as $key => $var_obj){
					if($var_obj->qty && $var_obj->qty > 0){
						
						// Object in cart. Check if it's in the required_groups.
						if($product->groups){
	
							foreach($required_groups as $required_group){
								if( !in_array($required_group, $product->groups ))
									$misses++;
							}
							
							if(($op=='AND') && $misses){ // One or more misses
								$bad_products++;
								
							}else if($op == 'OR'){ // Missed all groups
								if($misses >= count($required_groups))
									$bad_products++;
							}
							
						} else {
							$bad_products++;
						}

					}
				}
			}
	
		}
				
		if($bad_products) return FALSE;
		
		return TRUE;
		
	}
	
	
	/*
	 *  Callback for requiring that promo be used between certain dates, inclusively. Specify start, stop or both.
	 *  Example:
	 * 	$promo->validations[] = $Promos->create_validation('isvalid_date', 
		array(
			'start' => '2015-02-01',
			'stop' => '2015-02-12'
		)
	);
	 */	
	public function isvalid_date($cart, $args){
		$from = $args['start'];
		$until = $args['stop'];

		if($from)
			$from = strtotime($from);
		if($until) {
			$parsed = date_parse($until);
			if ($parsed['hour'] === FALSE) {
				$until .= ' 23:59:59';
			}
			$until = strtotime($until);
		}

		$now = defined('OVERRIDE_DATE') ? strtotime(OVERRIDE_DATE) : time();

		if(($from && $from > $now) || ($until && $until < $now))
			return FALSE;
		return TRUE;
	}


	/*
	 * Callback for products_required validation. Multiple products required at given 
	 * quantities. Currently all requirements must be met (AND, not OR).
 	
	 	$args = array(
	 		'products_required' => array ...
	 		'op' => string ... 'AND' or 'OR'
	 	)
	 */
	public function isvalid_products_required($cart = null, $args = null) {

		$is_valid = true;
		
		if(!isset($args['products_required'])){
			return false;
		}
		$products_required = $args['products_required'];
		$results = array();
		
		// Loop through the requirements
		foreach($products_required as $req){
		
			$qty = 0;
			
			// Group requirement
			if(isset($req['group'])){ // Requires products to be in a group
				
				// Loop through products
				foreach(get_object_vars($cart) as $key => $product){
					
					if(isset($product->groups)){
						// We only want the ones in $req['groups']
						if(in_array($req['group'], $product->groups)){
							if($product->variations){
								$variations = get_object_vars($product->variations);
								foreach($variations as $key => $var_obj){
									if($var_obj->qty) 
										$qty += $var_obj->qty;
								}
							}
						}
					}
					
				}
				
			// Product requirement
			}else if(isset($req['slug'])){ // Requires a specific product
				
				$product = $cart->$req['slug'];
				if($product->variations){
					$variations = get_object_vars($product->variations);
					foreach($variations as $key => $var_obj){
						if($var_obj->qty) 
							$qty += $var_obj->qty;
					}
				}
				
			}
			
			$qty_needed = $req['qty'] - $qty;
			
			if($qty_needed > 0){
				$results[] = '0'; // product has not met the requirement
			} else {
				$results[] = '1';
			}
		}
		
		// If 'OR' than just one requirement needs to be satisfied
		if($args['op'] == 'OR'){
			return in_array('1', $results);
			
		// for 'AND' all requirements need to be satisfied
		} else {
			return !in_array('0', $results);
		}

	}
	
	
	// Example validation callback 
	public function isvalid_never($cart = null, $args = null) {
		return false;
	}
	
	
	// Example validation callback 
	public function isvalid_always($cart = null, $args = null) {
		return true;
	}
	
	
	
	// Rule Callbacks -----------------------------------------------------------
	
	/*
	 * A rule callback should always start with 'applyrule_'
	 */

	
	// Rule callback for setting product qts, min, max, price and shipping of a given product
	public function applyrule_set_products($discounter, $args = null) {
		
		// Some rules don't get applied during checkout
		if($discounter->is_checkout && isset($args['apply_on_checkout']) && $args['apply_on_checkout'] == false){
			return;
		}
		
		foreach($args['products'] as $product){
		
			// Set the qty
			if(isset($product['qty'])){ // Always set the quantity before setting price or shipping
				$discounter->set_product_qty($product['slug'], $product['qty']);
			}

			// Set the min/max
			$min = isset($product['min']) ? $product['min'] : NULL;
			$max = isset($product['max']) ? $product['max'] : NULL;
			if( ! ($min === NULL && $max === NULL) ){
				$discounter->set_product_minmax($product['slug'], $min, $max);
			}
			
			// Set the discounted price for all of this product
			if(isset($product['discount_price'])){
				$discounter->set_product_discount($product['slug'], $product['discount_price'], null, $product['op']);
			}
			
			// Set the shipping for all of this product
			if(isset($product['shipping'])){
				$discounter->set_product_shipping($product['slug'], $product['shipping']);
			}
			
			// Set the weight for all of this product
			if(isset($product['weight'])){
				$discounter->set_product_weight($product['slug'], $product['weight']);
			}
						
		}

	}
	

	// Set values for group products
	public function applyrule_set_groups($discounter, $args = null) {
		if (!($groups = $args['groups'])) {
			return;
		}

		$cart = $discounter->cart;
		
		// Loop through products
		foreach(get_object_vars($cart->products) as $key => $product){
			
			if($key == 'version' || !$product || !$product->groups)
				continue;

			// First matching group
			foreach ($product->groups as $group_name) {
				if ($group = $groups[$group_name])
					break;
			}
			if (!$group)
				continue;

			// Set the discounted price for all of this product
			if (isset($group['discount_price']))
				$discounter->set_product_discount($key, $group['discount_price'], null, $group['op']);
		}
	}
	

	// Limit the number of products from a group
	public function applyrule_limit_group_qty($discounter, $args = null) {
		
		if(!isset($args['group'])){
			error_log(__METHOD__ . " Missing group");
			return;
		}
		
		$m = isset($args['m']) ? $args['m'] : 1; // How many products to affect
		$group = isset($args['group']) ? $args['group'] : null;
		$cart = $discounter->cart;
		
		// Loop through products
		foreach(get_object_vars($cart->products) as $key => $product){
			
			if($key == 'version') continue;
						
			if($product && $product->groups){
				
				if(in_array($group, $product->groups)){ // Product is in group
					if($product->variations){

						$variations = get_object_vars($product->variations);
						foreach($variations as $key => $var_obj){
															
							if($var_obj->qty){
																	
								// More than allowed
								if($var_obj->qty > $m){
									
									$var_obj->qty = $m;
									$m = 0;
									error_log("finish discount: qty: $qty m: $m discount: $discount");
									
								// Need to discount more products
								} else {
									$m = $m - $var_obj->qty;
									
								}
								
							}
						}
					}
				}
				
			}
		}
	}
	
	
	// If less than N of a slug or group, add product.
	// 
	// $arg['slug'] is required.
	// $arg['group'] is optional. If specified, will take precedence over slug.
	// $arg['subscription'] optional. will require that product.subscription is this value. only checked if $arg['group'] is specified.
	// $arg['n'] is the quantity needed. Defaults to 1.
	// If n or more don't exist, will increase the quantity of $arg['slug']
	// if $arg['sku'] given, it will be used instead of slug to achieve the proper quantity
	// Automatically applys min to cart and products
	public function applyrule_n_or_greater($discounter, $args = null) {
				
		$slug = isset($args['slug']) ? $args['slug'] : '';
		$group = isset($args['group']) ? $args['group'] : '';
		$n = isset($args['n']) ? $args['n'] : 1;
		$sku = isset($args['sku']) ? $args['sku'] : null;
		$subscription = isset($args['subscription']) ? $args['subscription'] : null;
		
		if(!$slug){
			error_log(__METHOD__ . " Missing slug for args: " . print_r($args, true));
			return false;
		}
			
		// Checking by group	
		if($group){
			
			// Add min to cart_modifier
			if(!property_exists($discounter->cart->cart_modifiers->groups, $group)){
				$discounter->cart->cart_modifiers->groups->$group = new stdClass();
			}
			$discounter->cart->cart_modifiers->groups->$group->min = $n;

			foreach(get_object_vars($discounter->cart->products) as $slug => $product){
				
				if(!is_object($product)) continue;
				
				if(!$product->variations || !$product->groups) continue;
				
				// If subscription was given check it
				if($subscription !== null){
					if(!isset($product->subscription))
						continue;					
				}

				
				$is_match = false;
				
				foreach($product->groups as $pgroup){
					if($group == $pgroup){ // If this product is in the group
						$is_match = true;
					}
				}
				
				if($is_match){
					foreach($product->variations as $key => $val){
						$n -= $val->qty;
					}
				}
				
			}
		
		// Checking by slug
		} else {
			
			if(isset($discounter->cart->products->$slug)){
				$product = $discounter->cart->products->$slug;
				if($product->variations){
					$product->min = $n;
					foreach($product->variations as $key => $val){
						$n -= $val->qty;
					}
				}
			}
			
		}
		
		// If we need more
		if($n > 0){
			$discounter->set_product_qty($args['slug'], $n, $sku, '+');
			// Add message to user here?
			return false;
		}
		return true;

	}


	// Empties out the cart (useful if you want nothing else in the cart but certain products.
	public function applyrule_empty_cart($discounter){
		$discounter->reset_cart();
	}


	// Modify the cart as a whole.
	public function applyrule_set_cart($discounter, $args = null){
		
		if(isset($args['shipping'])){
			$discounter->cart->cart_modifiers->shipping = $args['shipping'];
		}
		
		if(isset($args['subtotal'])){
			$discounter->cart->cart_modifiers->subtotal = $args['subtotal'];
		}
		
	}
	
	
	// PROMO DB -----------------------------------------------------------
	
	// Lists all promos (temporary until we store as json file)
	public function read_db(){
		
		// Read from .php file
		require_once('promo_db.php');
		$promos = create_promo_db();
		
		/*
		// Read from mongodb
		$db = new MongoClient();
		$table = $db->lcart->promos;
		$result = $table->find();
		$promos = array();
		foreach($result as $id => $value){
			$promos[] = $value;
		}
		*/
		
		return $promos;
	}
	
	public function save_db(){
		require_once('promo_db.php');
		$promos = create_promo_db();
		
		$db = new MongoClient();
		$table = $db->lcart->promos;
		
		echo "<pre>";

		foreach($promos as $promo){
			print_r($promo->name);
			$result = $table->update(
				array('name' => $promo->name),
				$promo,
				array('upsert' => true)
			);
			echo "Write result: " . print_r($result, true) . " \n";
		}
		echo "</pre>";
		
	}


	// Print promo codes for output
	public function print_db($db){
		$output = '';
		foreach($db as $promo){
			$output .= $promo->name . "\n";
		}
		return $output;
	}
	

	// HELPERS --------

	// Comparison
	private function compare($a, $b, $op = '=') {
		switch ($op) {
			case '<': return $a < $b;
			case '<=': return $a <= $b;
			case '>': return $a > $b;
			case '>=': return $a >= $b;
			default: return $a == $b;
		}
		return false;
	}
}


