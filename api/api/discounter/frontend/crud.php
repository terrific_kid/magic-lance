<?php

require_once('../promos.php');


if($_POST['action'] == 'add')
	add_promo();
	
elseif($_POST['action'] == 'delete')
	delete_promo();
	



// Add a promo
function add_promo(){
	
	$Promos = new Promos();
		
	$code = trim($_POST['code']);
	$promo = $Promos->create($code);
		
	// Limit to a specific site
	if(isset($_POST['site-limit'])){
		$site_limit = $_POST['site-limit'];
		$promo->validations[] = $Promos->create_validation('isvalid_group', 
			array(
				'groups' => array(
					$site_limit
				),
				'op' => 'AND'
			)
		);		
	}
	
	if(isset($_POST['functions'])){
		foreach($_POST['functions'] as $function){
			
			if($function == 'set-cart'){
				
				$amount = trim($_POST['amount']);
				if(! is_numeric($amount)){
					error_log(__FILE__ . " error non-numeric " . $_POST['amount']);
					continue;
				}
				$op = $_POST['op'];
				
				// Convert from percentage to float
				if($op == '%')
					convert_percent($op, $amount);
				
				$promo->rules[] = $Promos->create_rule('applyrule_set_cart',
					array(
						'subtotal' => array('amt'=>$amount, 'op'=>$op),
					)
				);			
			}
		}
	}
	
	if(isset($_POST['tags'])){
		$tags_raw = explode(',', $_POST['tags']);
		$tags = array();
		foreach($tags_raw as $tag){
			$tag = preg_replace("/[^0-9a-zA-Z]+/", "-", $tag);
			$tag = strtolower(trim($tag, '-'));
			if($tag) 
				$tags[$tag] = 1;
		}
		$promo->admin_tags = $tags;
	}

	if(isset($_POST['description'])){
		$promo->admin_description = $_POST['description'];
	}
	
	echo "<pre>";
	print_r($_REQUEST);
	echo("\n\nPROMO:\n");
	print_r($promo);
	echo "</pre>";
	
	// TODO: convert to json and save
}

// Delete a promo
function delete_promo(){
	echo "deleting";
}

//
function convert_percent(&$op, &$amount){
	$op = '*';
	$amount = (100 - $amount) / 100;
}

?>
