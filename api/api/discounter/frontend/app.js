var promos_admin = {
	id: "#promos_admin",
	initialized: false,
	//api_host: 'apidev.lifeaidbevco.com',
	products: null,
	promos: null,
	promosTmpl: null,
	amt_loaded: .1,
	loaderInterval: null,
	group_to_website: {retail:'Consumer', wholesale:'Affiliate', apo:'APO', retailer:'Retailer'},
	
	// Initialize class
	init: function(){
		var self = this;
		if(!this.initialized){
			$('#loading-modal').modal({'keyboard':false});
			self.loaderInterval = setInterval(function(){ self.checkLoaded() }, 500);
			self.init_triggers();
			
			// Get data
			self.get_products();
			self.get_promos();
			
			self.register_helpers();

		}
		return self;
	},
	
	// Load the products into the object
	get_products: function(){
		var self = this;
		$.ajax({
			url: '/api/getProducts.php',
			dataType: 'json',
			success: function(data){
				self.products = data;
				self.amt_loaded += .5;
				console.log('get_products loaded');
			},
			error: function(jqXHR, status, err){
				self.amt_loaded += .5;
				console.log(err);
			}
		});
	},
	
	// Load the promos into the object
	get_promos: function(){
		var self = this;
		$.ajax({
			url: '/api/discounter/get_promos.php',
			type: 'POST',
			dataType: 'json',
			data: {'tiny':'dancer'},
			success: function(data){
				self.promos = {'promos': data};
				self.sort_promos();
				$('#list_screen').html(self.promosTmpl(self.promos));
				self.amt_loaded += .5;
				console.log('get_promos loaded');
			},
			error: function(jqXHR, status, err){
				self.amt_loaded += .5;
				console.log(err);
			}
		});
	},

	// Sorts promos in order
	sort_promos: function(){
		var self = this;
		if(!self.promos.promos)
			return;
		
		// Sort by promo code alphabetically.
		self.promos.promos.sort(function(a,b){
			var aName = a.name.toLowerCase();
			var bName = b.name.toLowerCase();
			return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
		});
	},
	
	// Interval check for loading screen
	checkLoaded: function() {
		var self = this;
		console.log('checkLoaded:' + self.amt_loaded);
		var w = self.amt_loaded * 100;
		w = ((w < 100)?w:100)  + '%';
		$('.progress-bar', '#loading-modal').css('width', w);
		//$('.progress-bar', '#loading-modal').text(w);
		if(self.amt_loaded >= 1){
			self.initialized = true;
			clearInterval(self.loaderInterval);
			$('#loading-modal').modal('hide');
		}
	},
	
	// Initialize triggers & precompile templates
	init_triggers: function(){
		var self = this;
		
		// Templates
		var promoNewTmpl = Handlebars.compile($("#promo-new-screen").html());
		var ruleCartTmpl = Handlebars.compile($("#rule-cart").html());
		var ruleProductsTmpl = Handlebars.compile($("#rule-products").html());
		self.promosTmpl = Handlebars.compile($("#promos").html());
				
		// Add Promo Btn
		$(self.id).on('click', 'a[data-action="new-promo-trigger"]', function(e){
			$('#view-screen').hide().html(promoNewTmpl()).fadeIn();
			$(this).hide();
			return false;
		});
		// Format the promo code
		$(self.id).on('change', 'input[name="code"]', function(e){
			var code = $(this).val();
			$(this).val(code.trim().toUpperCase().replace(/[^0-9A-Z]/,''));
		});
		$(self.id).on('change', 'select[data-name="rule-change"]', function(e){
			if(this.value == 'set-products'){
				$('.rule-form','#promo-new').html(ruleProductsTmpl(this.value));
				$('select.product-list', '#promo-new').html(self.product_opts());
			}else if(this.value == 'set-cart'){
				$('.rule-form','#promo-new').html(ruleCartTmpl(this.value));
			}
		});
		// Show promo detail
		$(self.id).on('click', 'span[data-action="show-detail"]', function(e){
			var detail = $(this).closest('tr').find('.detail');
			$(detail).slideToggle()
			$('pre', detail).addClass('shrunken');
			return false;
		});	
		
		// double-click preformatted	
		$(self.id).on('click', '.detail pre', function(e){
			$(this).toggleClass('shrunken');
		});	

	},
	
	register_helpers: function(){
		
		var self = this;
		
		Handlebars.registerHelper('count', function(items){
			if(Array.isArray(items)) return items.length;
			return '0';
		});
		Handlebars.registerHelper('stringify', function(obj){
			return JSON.stringify(obj);
		});
		Handlebars.registerHelper('translateCallback', function(options){
			if(promo_translate[this.callback]){
				var out = promo_translate[this.callback](this); // Call the method in promo_translate
			} else {
				var out = this.callback;
			}
			return out;
		});
		Handlebars.registerHelper('validationsToWebsites', function(options){
			
			var out = "<span class=\"websiteExclusiveTag\">";
			var glue = ' ??? ';
			$.each(this.validations, function(i, obj){
				
				if(obj.callback == 'isvalid_group'){
					if(obj.args.op == 'OR'){
						glue = ' or ';
					} else if(obj.args.op == 'AND'){
						glue = ' and ';
					}
					var websites = [];
					$.each(obj.args.groups, function(i,e){
						websites.push(self.group_to_website[e]);
					});
					out = out + websites.join(glue) + '</span>';
				}
			});
			return out;
		});
		Handlebars.registerHelper('validationsToDates', function(options){
			var out = "<span class=\"validDateTag\">";
			$.each(this.validations, function(i, obj){
				if(obj.callback == 'isvalid_date'){
					out += self.niceDate(obj.args.start) +" &harr; "+ self.niceDate(obj.args.stop);	
				}
			});
			return out + '</span>';
		});
	},
	
	// Takes date as Y-m-d and returns as m/d/Y
	niceDate: function(dateStr){
		if (!dateStr)
			return 'Any';
		d = dateStr.split('-');
		if (d.length != 3)
			return dateStr;
		return parseInt(d[1]) +'/'+ parseInt(d[2]) +'/'+ d[0];
	},
	
	// Return product list as options for select
	product_opts: function(selected){
		var self = this;
		var html = '';
		$.each(self.products, function(id, product){
			if(id == 'version') return;
			var name = product.slug +" - " + product.sku;
			html += '<option value="'+ id +'" >'+ name +'</option>'+"\n";
		});
		return html;
	}
}

var promo_translate = {
	
	applyrule_set_cart: function(obj){
		
		var out = "Entire Cart: ";
		
		$.each(obj.args, function(prop, operation){
			var value = operation.amt;
			
			if(operation.op == '*'){
				value = (1 - operation.amt) * 100;
				value = '' + Math.round(value) + '% Off';
			} else if(operation.op == '-'){
				value = '$' + value + ' Off';
			} else if(operation.op == '+'){
				value = '$' + value + ' Added to';
			} else {
				value = ' = $' + value;
			}
			out += value + ' ' + prop;				
		});
		
		return out;	
	},
	
	applyrule_set_products: function(obj){
		
		var out = "Update these Products: \n";
		
		var product_html = "";
		
		if (obj.args.apply_on_checkout === false){
			out += " (not enforced on checkout)\n"
		}
						
		$.each(obj.args.products, function(i, product){
			
			p_html = "";

			if(product.hasOwnProperty('discount_price')) 
				p_html += "<li>Discounted to: $"+ product.discount_price +"</li>\n";

			if(product.hasOwnProperty('max')) 
				p_html += "<li>Max qty allowed: "+ product.max +"</li>\n";

			if(product.hasOwnProperty('qty')) 
				p_html += "<li>Set Qty to: "+ product.qty +"</li>\n";

			if(product.hasOwnProperty('shipping')) 
				p_html += "<li>Set Shipping to: $"+ product.shipping +"</li>\n";			

			if(product.hasOwnProperty('weight')) 
				p_html += "<li>Set weight to: $"+ product.weight +"</li>\n";
							
			product_html += '<li><kbd>'+product.slug+"</kbd><ul>\n" + p_html + "</ul></li>\n";

		});
		
		return out + "<ul>" + product_html + "</ul>\n";	
	},
	
	applyrule_n_or_greater: function(obj){
		var n = obj.args.n;
		var slug = obj.args.slug;
		var group = obj.args.hasOwnProperty('group') ? obj.args.group : null;
		var subscription = obj.args.hasOwnProperty('subscription') ? obj.args.subscription : false;
		var to_purchase = group ? group : slug;
		var out = "Ensure that " + n + " or more <kbd>" + to_purchase + "</kbd> are in the cart.\n";
		if(group) out += "Add <kbd>" + slug + "</kbd> if user doesn't have enough.\n";
		return out;
	},

	applyrule_limit_group_qty: function(obj){
		var out = "Allow no more than " + obj.args.m + " product in group <kbd>" + obj.args.group + "</kbd>.\n";
		return out;
	},

	isvalid_products_required: function(obj){
		var out = "";
		if(obj.args.op == 'OR'){
			out += '1 or more of the following must be true:';
		} else if(obj.args.op == 'AND'){
			out += 'All of the following must be true:';
		}
		out += "<ul>\n";
		$.each(obj.args.products_required, function(i, req){
			console.log(req);
			out += '<li>' + req.qty + " or  more ";
			if(req.group){
				out += '<kbd>' + req.group + '</kbd> products ';
			} else {
				out += '<kbd>' + req.slug + '</kbd>';
			}
			out += " in cart.</li>\n";
		})
		out = out + '</ul>';
		return out;
	},
	
	isvalid_group: function(obj){
		var out = "All products must be in <i>";
		if(obj.args.op == 'OR'){
			out += '1 or more';
		} else if(obj.args.op == 'AND'){
			out += 'All';
		} else {
			out += '???';
		}
		out = out + '</i> of the following groups: <kbd>' + obj.args.groups.join('</kbd>, <kbd>') + '</kbd>';
		return out;	
	},

	isvalid_date: function(obj){
		var out = "Promo only valid ";
		if(obj.args.start && obj.args.stop){
			out += 'between ' + obj.args.start + ' and ' + obj.args.stop;
		} else if(obj.args.start){
			out += ' after ' + obj.args.start;
		} else if(obj.args.stop){
			out += ' before ' + obj.args.stop;
		}
		return out;	
	},
}