<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
		
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	
	<style>
		.show-detail-btn:hover{cursor:pointer}
		pre.shrunken{
			width: 4em;
			padding: .5em;
			height: 1.5em;
			cursor: pointer;
			color: #D2D2D2;
			display: inline-block;
			vertical-align: top;
			line-height: .75rem;
		}
		pre.shrunken:before{
			content:'Code														';
		}
		.websiteExclusiveTag{
			text-transform: capitalize;
		}
		.hspace1{
			margin-left: .5em;
			margin-right: .5em;
		}
		.panel {
			padding:1.25em; 
			margin: .75em 0;
		}
		.validDateTag{
			font-size:.85em;
		}
	</style>
</head>

<body id="promos_admin">
	
<div class="container">
	<h2>Promos <a data-action="new-promo-trigger" class="btn btn-default">New</a></h2>
	<div id="view-screen"></div>
	<div id="list_screen"></div>
</div>

<div id="loading-modal" class="modal fade" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-body">
			<h4>Loading...</h4>
			<div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="5" aria-valuemin="5" aria-valuemax="100" style="width: 5%;"></div></div>
	  </div>
	</div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script id="promo-new-screen" type="text/x-handlebars-template">
{{!-- Template: Add new promo --}}
	<div id="promo-new" class="well">
		<h4>New Promo</h4>
		<form id="new_promo" action="crud.php" method="post" class="form" role="form">
		<input type="hidden" name="action" value="add">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<input type="text" placeholder="Promo Code" class="form-control" name="code" required>
				</div>
				<div class="form-group">
					<h4>Limit promo to...</h4>
					<select name="site-limit" class="form-control">
						<option value="">All sites</option>
						<option value="wholesale">Wholesale</option>
						<option value="retail">Retail</option>
						<option value="apo">APO</option>
					</select>
				</div>
				<div class="form-group">
					<h4>When applied this promo will...</h4>
					<select name="functions[]" class="form-control" data-name="rule-change">
						<option value="">Do nothing</option>
						<option value="set-cart">Give a cart discount</option>
						<option value="set-products">Give a product discount</option>
						<option value="add-free-product">Add a free product</option>
					</select>
					<div class="rule-form form-inline" style="">
					</div>
				</div>
				<div class="form-group">
					<input name="tags" class="form-control" type="text" placeholder="Tags (comma separated)"/>
				</div>
				<div class="form-group">
					<input name="description" class="form-control" type="text" placeholder="Short description" maxlength="128"/>
				</div>
				<div class="form-group" style="padding-top:1em;">
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</div>
		</div>
		</form>
	</div>
</script>

<script id="rule-cart" type="text/x-handlebars-template">
{{!-- Template: cart rule --}}
	<div class="panel">
		Discount
		<select name="property" class="form-control">
			<option value="subtotal">Subtotal</option>
			<option value="shipping">Shipping</option>
		</select>
		By
		<select name="op" class="form-control">
			<option>-</option>
			<option>%</option>
		</select>
		<input name="amount" type="text" placeholder="Amount" class="form-control" required>
	</div>
</script>

<script id="rule-products" type="text/x-handlebars-template">
{{!-- Template: product rule --}}

	<div class="panel">
		To <select name="product" class="product-list form-control">
			<option>Product List</option>
		</select><br/><br/>

		Discount <select name="property" class="form-control">
			<option value="price">Price</option>
			<option value="shipping">Shipping</option>
		</select>
		&nbsp; By
		<select name="op" class="form-control">
			<option>-</option>
			<option>%</option>
		</select>
		
		<input name="amount" type="text" placeholder="Amount" class="form-control" required>
	</div>
</script>

<script id="rule-products-free" type="text/x-handlebars-template">
{{!-- Template: product rule free --}}

	Add <input class="form-control" type="text"/> Free

	<select class="product-list form-control">
		<option>Product List</option>
	</select>

	<input type="text" placeholder="Amount" class="form-control">
</script>

<script id="promos" type="text/x-handlebars-template">
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>Code</th>
						<th>Valid Sites</th>
						<th>Valid Dates</th>
						<th style="width:60%"></th>
					</tr>
				</thead>
				<tbody>
				{{#each promos}}
					<tr>
						<td>
							{{name}}
						</td>
						<td>
							{{#validationsToWebsites}}{{this.validations}}{{/validationsToWebsites}}
						</td>
						<td>
							{{#validationsToDates}}{{this.validations}}{{/validationsToDates}}
						</td>
						<td>
							<span data-action="show-detail" class="show-detail-btn glyphicon glyphicon-eye-open text-muted hspace1"></span>
							{{!-- 
							{{#if rules}}
							<span class="label label-warning pull-right hspace1">{{count rules}} Rules</span>
							{{/if}}
							
							{{#if validations}}
								<span class="label label-info pull-right hspace1">{{count validations}} Validations</span>
							{{/if}}
							--}}
							
							<div class="detail" style="display:none;">
		
								{{#if validations}}
									<h3>Requires that...</h4>
									<ul class="validations">
										{{#each validations}}
											<li class="{{this.callback}}">
												{{#translateCallback}}{{this}}{{/translateCallback}}
												<pre>{{stringify this.args}}</pre>
											</li>
										{{/each}}
									</ul>
								{{/if}}

								{{#if rules}}
									<h3>When applied this promo will...</h4>

									{{#if terms}}
										<ul>
											<li>
												Show the following terms
												<div class="well">{{{this.terms}}}</div>
											</li>
										</ul>
									{{/if}}

									<ul class="rules">
										{{#each rules}}
											<li class="{{this.callback}}">
												{{#translateCallback}}{{this}}{{/translateCallback}}
												<pre>{{stringify this.args}}</pre>
											</li>
										{{/each}}
									</ul>
								{{/if}}
		
							</div>
							
						</td>
					</tr>
				{{/each}}
				</tbody>
			</table>
		</div>
	</div>
</script>



<script src="handlebars-v2.0.0.js"></script>
<script src="app.js"></script>
<script>
$(document).ready(function() {
	
	promos_admin.init();

});
</script>
</body>
</html>