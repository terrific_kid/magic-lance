<?php

/*
 *
 * Class for handling promos and discounts.
 *
 */
 

require_once('promos.php');

class l_Discounter
{

	// TODO: Calculate discount based on product.price, assign final price to product.discount
	// TODO: A cart-wide discount
	// TODO: Send a message back to user
	
	// Create promo admin interface?
			
	public $cart = null;
	
	function __construct() {
		
		$this->cart = new stdClass();
		$this->cart->cart_modifiers = new stdClass(); // Holds data affecting entire cart, a.k.a 'special data'
		$this->cart->cart_modifiers->groups = new stdClass();
		$this->cart->promo = '';
		$this->cart->is_checkout = false;
		$this->Promos = new Promos();
		
		$this->jsonfile = dirname(dirname(__FILE__)) . '/products.json'; // Where the products file is located
		
	}
	
	
	// Get default, clean products. Insert the user's cart options/qtys and use that.
	public function cleanse_products($cart_products){
		
		// Get default products from json file
		if($json_str = file_get_contents($this->jsonfile)){
			$default_products = json_decode($json_str);
		} else {
			error_log(__METHOD__ . " ERROR: couldn't read json file from " . $this->jsonfile);
		}
				
		// Copy only the variations over (i.e. what's in the user's cart)
		foreach(get_object_vars($default_products) as $slug => $product){
			
			if($slug == 'version') continue;
			
			if($cart_products->$slug && $default_products && $default_products->$slug){
				
				// Don't allow 'stoic' products to be added by user. Only discounter does this.
				if($default_products->$slug->groups && in_array('stoic', $default_products->$slug->groups))
					continue;
				
				if($cart_products->$slug->variations)
					$default_products->$slug->variations = $cart_products->$slug->variations;
			}
			
		}
		
		$this->cart->products = $default_products; // Overrwrite the old cart
			
	}
	
	// Get default, clean products. Insert the user's cart options/qtys and use that.
	public function reset_cart(){
		
		// Get default products from json file
		if($json_str = file_get_contents($this->jsonfile)){
			$default_products = json_decode($json_str);
		} else {
			error_log(__METHOD__ . " ERROR: couldn't read json file from " . $this->jsonfile);
		}
		
		$this->cart->products = $default_products; // Overrwrite the old cart
			
	}

	
	// Main handler for promos. Returns cart object with the following properties:
	// 	$cart->products: altered products. always returned.
	// 	$cart->promo: promo code if valid.
	// 	$cart->cart_modifiers: any modifiers to the cart as a whole
	public function handle_promo($products_json, $code, $is_checkout = false) {
	
		// Get users's cart products
		$cart_products = json_decode($products_json); // Products in cart
		if($cart_products === null){
			return null;
		}
		
		$this->is_checkout = $is_checkout;
		
		// Cleanse them (so they can't double-up on promos)
		$this->cleanse_products($cart_products);
			
		$code = strtoupper(trim($code)); // lowercased and trimmed.
										
		// Generate the promo codes (temporary)
		$db = $this->Promos->read_db();
		//error_log("PROMOS\n");
		//error_log($this->Promos->print_db($db));
		
		// Check if the promo exists
		if(!$promo = $this->Promos->find($db, $code)){
			error_log(__METHOD__ . ": That promo code does not exist.");
			return $this->cart; // Return products untouched
		}
		
		// Validate promo
		if($this->Promos->validate($db, $promo, $this->cart->products)){
		
			$this->cart->promo = $code;
			$this->apply_promo($promo); // Valid promo. Apply promo rules. Modify $this->cart
			
		} else {
		
			return $this->cart; // Return products untouched
			
		}
		
		//error_log("PRODUCTS:\n" . print_r($this->cart, true));
		//error_log("PROMO DB:\n" . print_r($db, true));
		
		// Return the modified cart products
		return $this->cart;
		
	}
	
	
	public function debug_cart(){
		
		if(!$this->cart->products){
			error_log("Empty cart."); 
			return;
		}

		error_log("PRODUCTS\n");
		foreach(get_object_vars($this->cart->products) as $k => $v){
			if($v->variations && count(get_object_vars($v->variations))){
				error_log("$k ==> " . print_r($v, true));
			}
		}
		
	}
	
	
	// Applies a promo's rules to the cart
	public function apply_promo($promo){
	
		if(!$promo->rules || count($promo->rules) < 1) return;
				
		foreach($promo->rules as $rule){	
			$this->Promos->apply_rule($rule, $this);
		}
		
		if(isset($promo->terms))
			$this->cart->terms = $promo->terms;
		
	}
	
	
	// Generate the key and add to the product->variations. NOTE: Needs to mirror the same key as lcart!
	public function generate_variation_key($product, $sku){
		
		// Create the variation object
		$variation = new stdClass();
		$variation->sku = $sku;
		
		if(isset($product->subscription)){
			$variation->subscription = new stdClass;
			$variation->subscription->interval = array_shift($product->subscription->values);
		}

		$variation->add = new stdClass;
		$add = new stdClass;
		$add->sku = "";
		$variation->add->{'0'} = $variation->add->{'1'} = $add;

		// Generate the key
		$key = json_encode($variation);
		
		// Add it
		$product->variations->$key = $variation;
		
		return $key;
	}
	
	
	// Adds/Updates cart quantity to a specific amount. If specified, $sku overrides the product sku.
	// $op can be +, - to add/subtract qty from the existing amount
	// $op can be >= to require at least $qty
	public function set_product_qty($slug, $qty=1, $sku = null, $op = null){
			
		if(! $product = $this->cart->products->$slug){
			error_log(__METHOD__ . " Error: couldn't find $slug in cart");
			return;
		}
				
		$sku = $sku ? $sku : $product->sku; // $sku overrides the product sku
				
		$found = null;
		foreach($product->variations as $key => $variation){
			if($variation->sku == $sku)
				$found = $key;
		}
		
		if(!$found){
			$found = $this->generate_variation_key($product, $sku);			
		}
		
		$prod_qty = property_exists($product->variations->$found, 'qty') ? $product->variations->$found->qty : 0;
			
		if ($op == '+') {
			$prod_qty += $qty;
		} else if($op == '-') {
			$prod_qty -= $qty;
		} else if(($op == '>=') && ($prod_qty < $qty)) {
			$prod_qty = $qty;
		} else if(!$op) {
			$prod_qty = $qty;
		} else {
			error_log(__METHOD__ . " Illegal operator '$op'");
		}
				
			
		$prod_qty = ($prod_qty < 0)? 0 : $prod_qty;
						
		$product->variations->$found->qty = $prod_qty;
		
		//$product->in_cart = $prod_qty ? 1:0;
	}

	
	// Sets discount price
	// if $op is + or *, $amt is the amount to operate with. Otherwise, $amt is the new discount price.
	public function set_product_discount($slug, $amt, $sku = null, $op = null){
	
		// TODO: This doesn't work for a product with variations, like a tshirt.
					
		if(! $product = $this->cart->products->$slug){
			error_log(__METHOD__ . " Error: couldn't find $slug in cart");
			return;
		}
				
		$sku = $sku ? $sku : $product->sku; // $sku overrides the product sku
		
		$discount_price = $product->price;
			
		if($op == '*')
			$discount_price *= $amt;
		else if($op == '+')
			$discount_price += $amt;
		else
			$discount_price = $amt;	
			
		$discount_price = ($discount_price < 0)? 0 : $discount_price;
		$product->discount = $discount_price;
		
	}

	
	public function set_product_shipping($slug, $shipping, $sku = null, $op = null){
	
		// TODO: This doesn't work for a product with variations, like a tshirt.
				
		if(! $product = $this->cart->products->$slug){
			error_log(__METHOD__ . " Error: couldn't find $slug in cart");
			return;
		}
				
		$sku = $sku ? $sku : $product->sku; // $sku overrides the product sku
		
		$prod_shipping = $product->shipping;
			
		if($op == '*')
			$prod_shipping *= $shipping;
		else if($op == '+')
			$prod_shipping += $shipping;
		else
			$prod_shipping = $shipping;	
			
		$prod_shipping = ($prod_shipping < 0)? 0 : $prod_shipping;
						
		// TODO: What if we have other variations? to set all variations or just one
		$product->shipping = $prod_shipping;
		
	}

	
	public function set_product_weight($slug, $weight, $sku = null, $op = null){
	
		// TODO: This doesn't work for a product with variations, like a tshirt.
				
		if(! $product = $this->cart->products->$slug){
			error_log(__METHOD__ . " Error: couldn't find $slug in cart");
			return;
		}
				
		$sku = $sku ? $sku : $product->sku; // $sku overrides the product sku
		
		$prod_weight = $product->weight;
			
		if($op == '*')
			$prod_weight *= $weight;
		else if($op == '+')
			$prod_weight += $weight;
		else
			$prod_weight = $weight;	
			
		$prod_weight = ($prod_weight < 0)? 0 : $prod_weight;
						
		// TODO: What if we have other variations? to set all variations or just one
		$product->weight = $prod_weight;
		
	}


	public function set_product_minmax($slug, $min = null, $max = null){
					
		if(! $product = $this->cart->products->$slug){
			error_log(__METHOD__ . " Error: couldn't find $slug in cart");
			return;
		}
		
		if(!is_null($min)){
			$this->cart->products->$slug->min = $min;
		}
		
		if(!is_null($max)){
			$this->cart->products->$slug->max = $max;	
		}
		
	}

	
	// Get the qty of a given product slug. NOTE: this function is untested.
	public function get_product_qty($slug, $sku = null){

		if(! $product = $this->cart->products->$slug){
			error_log(__METHOD__ . " Error: couldn't find $slug in cart");
			return;
		}
				
		$sku = $sku ? $sku : $product->sku; // $sku overrides the product sku
		$qty = 0;
		
		$found = null;
		foreach($product->variations as $key => $variation){
			if($variation->sku == $sku)
				$found = $key;
		}
		
		if(property_exists($product->variations->$found, 'qty')){
			$qty =  $product->variations->$found->qty;
		}
		
		return $qty;
		
	}
	
	
	// Returns cart products.
	public function get_products($format = 'json'){
		return $this->cart->products;
	}
	
	
	// Convenience method. Gets a var. POST overrides GET.
	private function _getVar($name){
		$value = isset($_GET[$name]) ? $_GET[$name] : '';
		$value = isset($_POST[$name]) ? $_POST[$name] : $value; // Post overrides get
		return $value;
	}
	
}

