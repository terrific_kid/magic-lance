<?php 
	
$body = <<<ENDOFMAIL
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=0" />
    <title>FA Reward</title>
  </head>
  <body style="margin: 0; padding: 0;">&#13;
    	<center>&#13;
			&#13;
        	<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #f2f2f2; border-collapse: collapse; font-family: Verdana,Arial,sans-serif; font-size: 12px; line-height: 16px; padding: 20px;" bgcolor="#f2f2f2"><tbody><tr><td height="20"></td>&#13;
					</tr><tr><td align="center" valign="top">&#13;
							&#13;
							<table border="0" cellpadding="0" cellspacing="0" width="675" style="background-color: #ffffff; border: 1px solid #e8e8e8;" bgcolor="#ffffff"><tbody><tr><td><div><img src="https://development.drinkfitaid.com/media/lifeaid-receipt-header-bg.jpg" alt="LifeAID Beverage Co." width="675" height="110" style="display: block;" /></div></td>&#13;
									</tr><tr><td width="675">&#13;
											&#13;
											<table border="0" cellpadding="0" cellspacing="0" width="675"><tbody><tr><td width="40"> </td>&#13;
														<td width="595">&#13;
															&#13;
															<table border="0" cellpadding="0" cellspacing="0" width="595"><tbody><tr><td style="border-bottom-width: 1px; border-bottom-color: #b5b5b5; border-bottom-style: solid;"><p style="margin-top: 0; text-align: right; line-height: 18px !important;" align="right"></p></td>&#13;
																	</tr><tr><td height="30"> </td>&#13;
																	</tr><tr><td>
																		<h1>Medal Get!</h1>
																		&#13;
																			<p style="margin-top: 0; margin-bottom: 0; line-height: 18px !important;">$mail_msg</p>																		</td>&#13;
																	</tr><tr><td height="30"> </td>&#13;
																	</tr><tr><td>&#13;
																			<p style="margin-bottom: 12px; line-height: 18px !important;">Be sure to LIKE US or Follow Us on social media for upcoming deals and promotions.</p>&#13;
																		</td>&#13;
																	</tr><tr><td>																			&#13;
																			&#13;
																			<table border="0" cellpadding="0" cellspacing="0" width="595"><tbody><tr><td valign="top" width="30%">																																									&#13;
																							&#13;
																							<table border="0" cellpadding="0" cellspacing="0" width="100%"><thead><tr><th style="text-align: left;" align="left">FITAID</th>&#13;
																									</tr></thead><tbody><tr><td><a href="https://www.facebook.com/fitaid" target="_blank" style="color: #1a81e1; text-decoration: none;"><strong>Facebook</strong></a></td>&#13;
																									</tr><tr><td><a href="https://instagram.com/fitaid" target="_blank" style="color: #1a81e1; text-decoration: none;"><strong>Instagram</strong></a></td>&#13;
																									</tr><tr><td><a href="https://twitter.com/drinkfitaid" target="_blank" style="color: #1a81e1; text-decoration: none;"><strong>Twitter #FITAID</strong></a></td>&#13;
																									</tr></tbody></table></td>&#13;
																						<td valign="top" width="30%">																																									&#13;
																							&#13;
																							<table border="0" cellpadding="0" cellspacing="0" width="100%"><thead><tr><th style="text-align: left;" align="left">GOLFERAID</th>&#13;
																									</tr></thead><tbody><tr><td><a href="https://www.facebook.com/golferaid" target="_blank" style="color: #1a81e1; text-decoration: none;"><strong>Facebook</strong></a></td>&#13;
																									</tr><tr><td><a href="http://instagram.com/golferaid" target="_blank" style="color: #1a81e1; text-decoration: none;"><strong>Instagram</strong></a></td>&#13;
																									</tr><tr><td><a href="https://twitter.com/golferaid" target="_blank" style="color: #1a81e1; text-decoration: none;"><strong>Twitter #GOLFERAID</strong></a></td>&#13;
																									</tr></tbody></table></td>&#13;
																						<td valign="top" width="30%">																																									&#13;
																							&#13;
																							<table border="0" cellpadding="0" cellspacing="0" width="100%"><thead><tr><th style="text-align: left;" align="left">PARTYAID</th>&#13;
																									</tr></thead><tbody><tr><td><a href="http://www.facebook.com/drinkpartyaid" target="_blank" style="color: #1a81e1; text-decoration: none;"><strong>Facebook</strong></a></td>&#13;
																									</tr><tr><td><a href="http://instagram.com/partyaid" target="_blank" style="color: #1a81e1; text-decoration: none;"><strong>Instagram</strong></a></td>&#13;
																									</tr><tr><td><a href="http://twitter.com/#!/drinkpartyaid" target="_blank" style="color: #1a81e1; text-decoration: none;"><strong>Twitter #PARTYAID</strong></a></td>&#13;
																									</tr></tbody></table></td>&#13;
																					</tr></tbody></table></td>&#13;
																	</tr><tr><td>&#13;
																			<p style="margin-bottom: 0; line-height: 18px !important;">If you have any questions or problems with your order, please contact <a href="mailto:support@lifeaidbevco.com" style="color: #1a81e1; text-decoration: none;">support@lifeaidbevco.com</a> or feel free to call us at: 888-558-1113</p>&#13;
																			<p style="padding-top: 20px; margin-bottom: 0; line-height: 18px !important;">Thank you!</p>&#13;
																			<p style="margin-top: 0; margin-bottom: 0; line-height: 18px !important;">Team LifeAID</p>&#13;
																		</td>&#13;
																	</tr><tr><td height="30"> </td>&#13;
																	</tr></tbody></table></td>&#13;
														<td width="40"> </td>&#13;
													</tr></tbody></table></td>						&#13;
									</tr></tbody></table></td>&#13;
					</tr><tr><td height="20"></td>&#13;
					</tr></tbody></table></center>&#13;
    </body>
</html>


ENDOFMAIL;

?>