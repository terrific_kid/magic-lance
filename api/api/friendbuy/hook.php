<?php 
	
	require_once('/www/api/api/start.php');
	require_once('/www/api/api/checkoutHelper.php');
	require_once('/www/api/api/rewards.php');
	
	// Custom params
	if(isset($_REQUEST['parameters'])){
		$parameters = json_decode($_REQUEST['parameters']);
		if(gettype($parameters) == 'object'){
			$custom_params = $parameters;
		}
		
		// $email = $custom_params->fa_customer_email;		
	}
	
	// Event type
	$event_type = $_REQUEST['type'];
	
	error_log('CUSTOM_PARAMS' . print_r($custom_params, true));
	
	if($event_type == 'share'){
		handle_share($custom_params);
	} else if($event_type == 'conversion'){
		handle_conversion($custom_params);
	}


	// Handle Share Event
	function handle_share($custom_params){
		
		$db = new MongoClient();
		
		$amt = $_REQUEST['shopper_fixed_revenue'];
		$email = $custom_params->fa_customer_email;
		$subject = 'Thanks for sharing!';
		
		// Other data friendbuy might send
		$extra_vars = array(
			'share_id',
			'twitter_message_id',
			'campaign_id',
			'shopper_revenue_share',
			'campaign_name',
			'message',
			'reward_type',
			'ip_address',
			'linkedin_message_id',
			'parameters',
			'created_at',
			'shopper_fixed_revenue',
			'facebook_message_id',
			'shopper_fixed_points',
		);
		
		// Add extra data
		$extra = array();
		foreach($extra_vars as $var){
			if(isset($_REQUEST[$var])){
				$extra[$var] = $_REQUEST[$var];
			}
		}
				
		// Add reward
		if(rewardGive($db, $email, $amt, $extra)){
			
			// Success. Email user
			$amt_fmt = money_format('$%i', $amt);
			$mail_msg = 'Thanks for sharing, ' . $custom_params->fa_customer_fname .
				" you just received $amt_fmt credit, which will be applied towards your next order.";
				
			include('mails/share_reward.php');
			
			error_log("sending mail " . $mail_msg);
			sendEmail($email, $subject, $body);
			
		} else {
			// probably couldn't find the user. Should we email them?
		}
		
	}
	
	
	// Handle Conversion Event
	function handle_conversion($custom_params){
		
		// Add reward
		
		// Email user	
			
	}



?>
<html>
	<head>
	</head>
	<body>
		Good Request from <?php echo $_SERVER['REMOTE_ADDR'] ?> ?>
		<pre>
		REQUEST VARS: <?php echo print_r($_REQUEST, true);?>
		</pre>
	</body>
</html>
<?php

error_log('FRIENBUY HOOK:' . print_r($_REQUEST, true));

?>