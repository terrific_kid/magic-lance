Running...
<?php

/*

Get a list of invoices by date from IS, enter used promo codes into mongo db.

*/
if($_REQUEST['secret'] != 'keke') die('der!');

require_once('/www/api/api/start.php');

$email = trim($_GET['email']);

function findCustomer($db = NULL, $email){
	
	$db || $db = new MongoClient();
	
	$customers = $db->lcart->customer;
	$customer = $customers->findOne(array('email' => $email));
	//error_log('found' . print_r($customer, true));
	return $customer;
}


// Give a reward amount ($amt) to a customer.
function rewardGive($db = NULL, $email, $amt){

	$db || $db = new MongoClient();
	$customers = $db->lcart->customer;
	
	$customer = $customers->findOne(array('email' => $email));
	
	if(!$customer){
		echo "Customer not found;";
		return FALSE;
	}
	
	if(!isset($customer['credit'])){
		$customer['credit'] = array(
			'total' => 0,
			'log' => array()
		);
	}
	$customer['credit']['total'] += $amt;
	$customer['credit']['log'][] = array(
		'amt' => $amt,
		'date' => new MongoDate()
	);
	
	$customers->findAndModify(
		array("email" => $email),
		array('$set' => array('credit' => $customer['credit']))
	);
	
	return TRUE;
		
}


// Apply a reward amount ($amt). $amt should be a positive value.
function rewardApply($db = NULL, $email, $amt){
	
	$db || $db = new MongoClient();
	$customers = $db->lcart->customer;
	
	$customer = $customers->findOne(array('email' => $email));
	
	if(!$customer){
		echo "Customer not found;";
		return;
	}
	
	if(!isset($customer['credit']))
		return FALSE;
		
	if($customer['credit']['total'] < $amt)
		return FALSE;
	
	$amt = 0 - $amt;
	
	$customer['credit']['total'] += $amt;
	$customer['credit']['log'][] = array(
		'amt' => $amt,
		'date' => new MongoDate()
	);
	
	$customers->findAndModify(
		array("email" => $email),
		array('$set' => array('credit' => $customer['credit']))
	);
	
}





?>

 

<pre>
<?php
	
	$db = new MongoClient();
	//rewardGive($db, $email, 1);
	$customer = findCustomer($db, $email);
?>

<?php if($customer['credit']) : ?>

<h2>Your current credit: <?php echo money_format('$%i', $customer['credit']['total']); ?></h2>

<pre>
<?php print_r($customer['credit']); ?>
</pre>

<?php else : ?>
	No credit	
<?php endif; ?>



 Complete!
</pre>