<?php
	
	// Only run in cli-mode
	if (php_sapi_name() != "cli")
		die("Sorry. Cannot run from browser.");
	
	//Start Application
		require_once('start.php');
	//Connect To Database	
		$db = new MongoClient();
		$table = $db->lcart->transaction;

    //Paramaters
    $options = getopt("p:r:l:");
    $page = $options['p'];
    $rows = $options['r'] ? $options['r'] : 500;
    if (!$page || !is_numeric($page) || !is_numeric($rows)) {
        echo "Usage: lscribe.php -p PAGE [-r ROWS]\n";
        exit(1);
    }

    //Debugging
    $LIVE = $options['l'] ? $options['l'] : 0;
?>

<?php  /*Script 1 - Initial Scan and Subscription Setup */	
	//Scan Orders for initial Processing
		$orders = $table->find( array( 
									'history' => array('$elemMatch' => array('code' => 'PAID')),
									'history.code' => array('$ne' => 'LSCRIBE'),
									) 
								);
		if(!$LIVE) $buffer = '*********TEST**********';
		$buffer .= 'Online! ' . date('m/d/y H:i:s', time());
		$buffer .= '<br>'.'--------------------------------------------------------'.'<br>';
		$buffer .= '<br>'.'Processing ' . $orders->count() . ' new order(s).';
		//Scan through PAID orders NOT LSCRIBE
		foreach($orders as $my_order){
		 	$flag = false;
		 	foreach($my_order['products'] as $pkey => $product){
			 	if(!$product['subscription']) continue;
			 	foreach($product['variations'] as $variation){
					if(!$variation['qty'] || !$variation['subscription']) continue;
					$flag = true;		 
				}
			}
			if($flag)stamp($my_order, 'HYDRA','Subscription Marked for Duplication','lscribe');
			stamp($my_order, 'LSCRIBE','Processed by LSCRIBE','lscribe');
		}
		
		 	
?>	

<?php /*Script 2 - Subscription Charging and Autoship  */
		//Now Scan through subscriptions and charge
		$nin = array('CANCEL','CANCELED','CANCELLED','AUTODECLINE','LSCRIBE-ERROR');
		
		$subscriptions = $table->find( array( 
							'history' => array('$elemMatch' => array('code' => 'HYDRA')),
							'history.code' => array('$nin' => $nin),
							) 
						);

        //Pagination
        $skip = $rows * ($page - 1);
        $subscriptions->skip($skip)->limit($rows);
        $count = $subscriptions->count(true);

        //Quit early if no results
        if (!$count) {
            echo "No Subscriptions!\n";
            exit(1);
        }

        $buffer .= '<br>' . 'Scanning ' . ($count ? ($skip + 1).' - '.($skip + $count) : '0') . ' of ' . $subscriptions->count() . ' Active Subscriptions...';

		//For Each Subscription
		foreach($subscriptions as $subscription){
			
			$my_order = $subscription;
						
			$flag = false;
			//Remove Discount
			unset($my_order['discount']);
			unset($my_order['promo']);
			
			foreach($my_order['products'] as $pkey => $product){
			 	//Unset if not subscription product
				if(!$product['subscription'] || !count($product['variations'])){ 
					unset($my_order['products'][$pkey]);
					continue;
				}
						
				//Remove Product Discounts
				$my_order['products'][$pkey]['discount'] = $my_order['products'][$pkey]['price'];
				unset($my_order['products'][$pkey]['shipping_discount']);
					
					//Variation Loop
					foreach($product['variations'] as $vkey => $variation){
						if(!$variation['qty'] || !$variation['subscription']){
							 continue;
						}
						//Remove Add-Ons or Bundles
						unset($my_order['products'][$pkey]['variations'][$vkey]['add']);
						unset($my_order['products'][$pkey]['variations'][$vkey]['bundle']);
						
						if($variation['subscription']['interval']['value'] < 1){
							//Stamp Decline
								$buffer .= '<br>' . "***************************************************************************************************************************";
								$buffer .= '<br>' . $my_order['customer']['form']['lastName'].','.$my_order['customer']['form']['firstName'].' | Order '.$my_order['_id']->{'$id'};
			
								stamp($my_order,'LSCRIBE-ERROR','Error with Subscription Interval','lscribe');
								$buffer .= '<br>' . 'ERROR: Error with Subscription Interval';
								
						
							continue;
						}
						
						
						
						//Set Today and nextCharge
						$today = time();
						if(!$variation['nextCharge']) $variation['nextCharge'] = $subscription['_id']->getTimestamp() + (intval($variation['subscription']['interval']['value']) * 24 * 60 * 60);
						
						//1970 Bug Check!	
						if($variation['nextCharge'] < 315536461){
								$start = $subscription['_id']->getTimestamp();
								$time =	(intval($variation['subscription']['interval']['value']) * 24 * 60 * 60);
									$variation['nextCharge'] = $start + ($time * 2);
							}
							
						//Check if we charge today
						if($variation['nextCharge'] > $today){
							unset($my_order['products'][$pkey]['variations'][$vkey]);
							continue;
						}
						
							
						
					
						
						$buffer .= '<br>' . "***************************************************************************************************************************";
						$buffer .= '<br>' . $my_order['customer']['form']['lastName'].','.$my_order['customer']['form']['firstName'].' | Order '.$my_order['_id']->{'$id'};
						$buffer .= '<br>' . $variation['qty'] . ' x ' . $variation['sku'] . ' | ' . $product['name'] .' | '.$variation['subscription']['interval']['label']. ' | $' . $product['price'] . ' | nextCharge: '.date('m/d/Y H:i:s',$variation['nextCharge']);
					


						//Assume, charged set NextCharged as nextCharged + interval
						$nextCharge = $variation['nextCharge'] + (intval($variation['subscription']['interval']['value']) * 24 * 60 * 60);
						$subscription['products'][$pkey]['variations'][$vkey]['nextCharge'] = $nextCharge;
						if($nextCharge < $subscription['nextCharge'] || !isset($subscription['nextCharge'])) $subscription['nextCharge'] = $nextCharge;
						$flag = true;
						
						
								
					}
			}
			
			
			//If we found a subscription to Charge
			if($flag){
				$new_order = $my_order;
				//unset Id
				unset($new_order['_id']);
				unset($new_order['history']);
				foreach($new_order['products'] as $pkey => $product){
					foreach($product['variations'] as $vkey => $variation){
						unset($new_order['products'][$pkey]['variations'][$vkey]['subscription']);
						unset($new_order['products'][$pkey]['variations'][$vkey]['nextCharge']);
						unset($new_order['nextCharge']);
					}
				}
				
                //Charge
                if ($LIVE) {
                   $result = chargeCard($new_order);
                }
					 
					 
						//Charge successful?
							if($result['success']){
								//Update $subscription Into Database
								unset($subscription['_id']);
								$table->update(
									array('_id' => $my_order['_id']),
									array('$set' => $subscription)
								);
								//Stamp as SPAWNed
								stamp($my_order,'SPAWN',$result['_id'],'lscribe');
								$buffer .= '<br>' . 'SPAWNED: ' . $result['_id'];
						
						//Charge Fail
							}else{
								//Stamp Decline
								$buffer .= '<br>' . "***************************************************************************************************************************";
								$buffer .= '<br>' . $my_order['customer']['form']['lastName'].','.$my_order['customer']['form']['firstName'].' | Order '.$my_order['_id']->{'$id'};
			
								stamp($my_order,'AUTODECLINE','Decline','lscribe');
								$buffer .= '<br>' . 'DECLINE: '.$result['error'];
							}
		
					 
			}
		
		}
		
		$buffer .= '<br>'.'<br>'.'-----------------------------------------------------';
		$buffer .= '<br>'.'EOF';

        echo "<pre>";
		print_r($buffer);
		sendEmail('log@lifeaidbevco.com', 'lscribe', $buffer);
        
	?>
		
