<?php // Rewards functions
	
// Give a reward amount ($amt) to a customer.
function rewardGive($db = NULL, $email, $amt, $extra = null){

	$db || $db = new MongoClient();
	$customers = $db->lcart->customer;
	
	$customer = $customers->findOne(array('email' => $email));
	
	if(!$customer){
		error_log(__FUNCTION__ .  "Customer not found;");
		return FALSE;
	}
	
	if(!isset($customer['credit'])){
		$customer['credit'] = array(
			'total' => 0,
			'log' => array()
		);
	}
	$customer['credit']['total'] += $amt;
	$customer['credit']['log'][] = array(
		'amt' => $amt,
		'date' => new MongoDate(),
		'extra' => $extra
	);
	
	$customers->findAndModify(
		array("email" => $email),
		array('$set' => array('credit' => $customer['credit']))
	);
	
	return TRUE;
		
}


// Apply a reward amount ($amt). $amt should be a positive value.
function rewardApply($db = NULL, $email, $amt){
	
	$db || $db = new MongoClient();
	$customers = $db->lcart->customer;
	
	$customer = $customers->findOne(array('email' => $email));
	
	if(!$customer){
		error_log(__FUNCTION__ .  "Customer not found;");
		return FALSE;
	}
	
	if(!isset($customer['credit'])){
		return FALSE;
	}
		
	if($customer['credit']['total'] < $amt){
		return FALSE;
	}
	
	$amt = 0 - $amt;
	
	$customer['credit']['total'] += $amt;
	$customer['credit']['log'][] = array(
		'amt' => $amt,
		'date' => new MongoDate()
	);
	
	$customers->findAndModify(
		array("email" => $email),
		array('$set' => array('credit' => $customer['credit']))
	);
	
}


