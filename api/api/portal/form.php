<?php include('header.php'); ?>

<style>
	img{max-width: 100%;}
	ul{list-style: none;}
	</style>

		<div class="container">
		<div class="row">
			
			<div class="col-sm-12">
				<h1>Order Form</h1>
			</div>
		</div>
		
		
		<div class="row">
			<div class="col-xs-12 col-sm-4 pull-right">

			
			<?php //List all products ?>
				<accordion close-others="true">
					
					<accordion-group>
						<accordion-heading><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> Affiliate</accordion-heading>
						<accordion close-others="true">
							<product ng-repeat="(slug, product) in system.products" ng-if="product.groups.indexOf('wholesale') > -1 && product.groups.indexOf('stoic') < 0" data-object="product"	data-template="product-portal"></product>
						</accordion>	 
					</accordion-group>
					<accordion-group>
						<accordion-heading><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Consumer</accordion-heading>
						<accordion close-others="true">
					 		<product ng-repeat="(slug, product) in system.products" ng-if="product.groups.indexOf('retail') > -1  && product.groups.indexOf('stoic') < 0" data-object="product" data-template="product-portal"></product>
						</accordion>	 
					</accordion-group>
					<accordion-group>
						<accordion-heading><span class="glyphicon glyphicon-usd" aria-hidden="true"></span> Retailer</accordion-heading>
						<accordion close-others="true">
							<product ng-repeat="(slug, product) in system.products" ng-if="product.groups.indexOf('retailer') > -1  && product.groups.indexOf('stoic') < 0" data-object="product" data-template="product-portal"></product>
						</accordion>	 
					</accordion-group>
					<accordion-group>
						<accordion-heading><span class="glyphicon glyphicon-star" aria-hidden="true"></span> Apo</accordion-heading>
						<accordion close-others="true">
							<product ng-repeat="(slug, product) in system.products" ng-if="product.groups.indexOf('apo') > -1  && product.groups.indexOf('stoic') < 0" data-object="product" data-template="product-portal"></product>
						</accordion>	 
					</accordion-group>

				</accordion>

				<a href="http://goo.gl/forms/uNB4XGiYgg" target="_new_product">Request a new product <small><span class="glyphicon glyphicon-new-window text-muted"></span></small></a>

			</div>

			<div class="clearfix visible-xs-block"></div>
			<div class="col-sm-8">
				<cart data-products="system.products" data-totals="system.totals" data-validate="system.validate()" data-template="cart-portal"></cart>
				<checkout ng-show="system.totals.items" baseurl="https://portal.lifeaidbevco.com/form.php" confirmurl="https://portal.lifeaidbevco.com/confirm.php" data-customer="system.customer" data-products="system.products" data-totals="system.totals" data-discount="system.discount" data-prompt="system.prompt()" data-template="checkout-portal"></checkout>
			</div>
		</div>
		
	 
		
		</div>
	</div>
	
	<?php include('footer.php'); ?>
</body>
</html>