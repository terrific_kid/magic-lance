<?php 
    require_once('../../../config/global.php');
?>


 
<!DOCTYPE html>
<html ng-app="lcart" lang="en">
	<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <!-- bugsnag -->
        <script src="//d2wy8f7a9ursnm.cloudfront.net/bugsnag-2.min.js" data-apikey="86d6965b2c2a2820b6ba443fe799ecbf"></script>
  
    	<!-- AngularJS -->
    	<script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.15/angular.min.js"></script>
 
    	<!-- jQuery-->
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		
		<!-- Bootstrap 3 CSS + AFFIX -->
		<script src="https://development.drinkfitaid.com/wp-content/themes/fitaid/js/bootstrap.min.js"></script> 
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
 
		<!--  Angular Bootstrap UI -->
		<script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.12.1/ui-bootstrap-tpls.min.js"></script>
		
		<!-- lcart -->
		<script type="text/javascript" src="//<?php echo API_HOST ?>/lcart.js?v=<?php echo VERSION_NUMBER; ?>" id="lifeaid-lcart" data-env="<?php echo LIFEAID_ENV ?>"></script>	
		
		<!-- style & efx -->
		<link rel="stylesheet" href="//development.drinkfitaid.com/wp-content/themes/fitaid/css/buy.css">
		<link rel="stylesheet" href="//development.drinkfitaid.com/wp-content/themes/fitaid/css/cart.css">
		<link rel="stylesheet" href="//development.drinkfitaid.com/wp-content/themes/fitaid/css/spinner.css">
		<link rel="stylesheet" href="united.css">
 
		<style>
		a{cursor: pointer !important;}
		.tab-content{padding-top: 1.5em;}
		body{padding-top: 130px;}
		h1{font-size: 3em;}
		h2{font-size: 3em;}
		ul{padding:0; margin: 0;}
		.product-default:hover, .product-default{border: 0;}
		</style>

	</head>
	<body ng-controller="core" ng-cloak>
		
		
			 
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" ng-click="collapse = !collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="">lcart {{version}} </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" ng-init="collapse = true" collapse="collapse">
       <form role="form" method="GET" action="view.php" class="form navbar-form">
					<div  class="form-group">
						<input class="form-control" name="id" placeholder="Order ID #" type="text">
					</div>
				 
					<button type="submit" class="btn btn-success">Search Orders</button>
					 
	 </form>
				
      <ul class="nav navbar-nav">
	      
	      <li><a href="form.php"><span class="glyphicon glyphicon-plus"></span> Order Form</a></li>
       <li role="presentation"> <a href="index.php"> <span class="glyphicon glyphicon-edit"  aria-hidden="true"></span> My Account</a></li>
			<li role="presentation"><a href="errors.php"> <span class="glyphicon glyphicon-flash" aria-hidden="true"></span> Errors </a></li>
		    <li role="presentation"><a href="promos.php"> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Promotions</a></li>
      </ul>
      
       <ul class="nav navbar-nav navbar-right">
	        <li ng-if="system.customer.email"><a ng-click="system.logout()"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> {{system.customer.email}} <b>(logout)</b></a></li>
     
       </ul>
     
				
      
      
       
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>


	 	
	<div class="container">	 
	 

	 
