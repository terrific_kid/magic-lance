<?php include('header.php'); ?>

<?php 
				$swims_errors = swimsErrors();
				$s_errors = subscriptionErrorCheck();
				$auto_errors = autofails();
				$errors = $s_errors + $auto_errors + $swims_errors;
?>
			<div class="col-sm-12">
		 <h1>Errors</h1>
			<?php foreach($errors as $record){ ?>
				<div style="color:#CC0000">
				#<?php echo (string)$record['_id']; ?><br>
				<?php echo $record['customer']['email']; ?><br>
				<?php echo $record['customer']['form']['lastName'] . ', '.  $record['customer']['form']['firstName']; ?><br>
				<?php if(count($record['broken'])) echo 'Errors: ' . print_r($record['broken'], true); ?>
				<hr>
		  		</div>
			<?php } ?>
<?php 
function swimsErrors(){
		$orders = array();
	
		$db = new MongoClient();
		$table = $db->lcart->transaction;
		$found = $table->find( array( 
									'history' => array('$elemMatch' => array('code' => 'SWIMS-FAIL')),
									'history.code' => array('$ne' => 'SWIMS'),
									) 
								);
		
		foreach($found as $record){
			$record['broken'] = array('SWIMS ERROR');
			array_push($orders, $record);
		}
	
	return $orders;
}


//Check for Autofails
function autofails(){
	
	$orders = array();
	
		$db = new MongoClient();
		$table = $db->lcart->transaction;
		$found = $table->find( array( 
									'history' => array('$elemMatch' => array('code' => 'AUTODECLINE')),
									'history.code' => array('$ne' => 'CANCELED'),
									) 
								);
		
		foreach($found as $record){
			$record['broken'] = array('Declined Billing Information');
			array_push($orders, $record);
		}
	
	return $orders;
}



// Iterate through the found entries
function subscriptionErrorCheck(){
	
	$m = new MongoClient();
	$db = $m->selectDB('lcart');
	$collection = new MongoCollection($db, 'transaction');
	$codes = array('CANCEL','CANCELLED','CANCELED');
	$found = $collection->find(array(
								'history' => array('$elemMatch' => array('code' => 'HYDRA')),
								'history.code' => array('$nin' => $codes),
								));
	
	$i = 0;
	$orders = array();
	
	foreach($found as $record){
		
		$i++;
			
			$broken = array();
		 
			
			if($record['customer']['form']['card'] == 1 || $record['customer']['form']['card'] == 88 || !isset($record['customer']['form']['card'])) array_push($broken, 'No Billing Card Set!');
				 
			//Foreach Subscription Product
			foreach($record['products'] as $product){
				if(!isset($product['subscription'])) continue;
			 
				//Make sure Variation->Subscription is Correct
				foreach($product['variations'] as $key => $variations){
					if(!$variations['qty']) continue;
					if( !isset($variations['subscription'])) continue;
					 
					
					//If No Subscription Interval Is Set
					if(!is_numeric($variations['subscription']['interval']['value'])) array_push($broken, 'No Interval Set!');
				
				}
			}			
			
			
					 
				if(count($broken)){  
				
				$record['broken'] = $broken;		 
				array_push($orders, $record);
		 
			 	}	

			
	}
		
		
 	
	 
	return $orders;
}




// Fixes the subscription, adding the subscription and 'add' fields and regenerates the key
function fix_subscription($db, $record, $product){
		
	// Create the variation object
	$variation = new stdClass();
	$variation->sku = $product['sku'];
	
	if(!isset($product->subscription)){	
		$variation->subscription = array(
			'interval' => array(
				'label' => 'Every Month',
				'value' => 30
			)
		);
	}

	$variation->add = new stdClass;
	$add = new stdClass;
	$add->sku = "";
	$variation->add->{'0'} = $variation->add->{'1'} = $add;
	
	// Get the qty
	$v = array_shift($product['variations']);
	if(isset($v['qty']))
		$variation->qty = $v['qty'];

	// Generate the key
	$key = json_encode($variation);
	
	// Add it
	$product['variations'] = array($key => $variation);
	
	//print_r($product['variations']);
	
	$record['products']['fitaid-24-pack-wholesale-autoship'] = $product;
	
	$collection = new MongoCollection($db, 'transaction');
	$mongoID = new MongoID($record['_id']);

	$collection->update(
		array('_id' => $mongoID),
		array('$set' => array(
			'products.fitaid-24-pack-wholesale-autoship' => $product,
		))
	);

	
}



// Deletes a record
function remove($db, $record){
	$collection = new MongoCollection($db, 'transaction');
	$mongoID = new MongoID($record['_id']);
	$collection->remove(array('_id' => $mongoID));
}


?>
 
			
			
			
			
			</div>
	</div>
	</div>
	
	<?php include('footer.php'); ?>
</body>
</html>