<?php 
$secure = true;
require_once('start.php');


	
function run(){
	global $secureEmail;
	
 	//Parse INPUT 
 	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$promo = preg_replace("/[^A-Za-z0-9 ]/", '', $request->promo);
	$products = json_encode($request->products);
	$discounter = new l_Discounter();
    $result = $discounter->handle_promo($products, $promo, true);

    //Remove unused products
    $purchased = new stdClass();
    foreach ($result->products as $slug => $product) {
        foreach ($product->variations as $k => $v) {
            if ($v->qty > 0) {
                $purchased->$slug = $product;    
                continue 2;
            }
        }
    }
    $result->products = $purchased;

		//Setup $my_order
		$my_order = new stdClass();
			$my_order->promo = $result->promo;
			$my_order->customer = $request->customer;
			$my_order->form = $request->form;
			$my_order->discount = $result->cart_modifiers;
			$my_order->products = $result->products;
			//Convert to Array
			$my_order = objectToArray($my_order);
		 
		 
			//Only allow email to be set to $secureEmail from start.php
			$my_order['customer']['email'] = $secureEmail;
			
				
				//Run PayPal
				if($my_order['customer']['form']['card'] == 88) return chargePayPal($my_order);
			
				//Run Credit Card [Authorize.net]
				return chargeCard($my_order);
				
			 
	

}
			

require_once('end.php'); ?>






