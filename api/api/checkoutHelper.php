<?php

		//Define PayPal Libraries
	 	use PayPal\Rest\ApiContext;
		use PayPal\Auth\OAuthTokenCredential;
		use PayPal\Api\Amount;
		use PayPal\Api\Details;
		use PayPal\Api\Item;
		use PayPal\Api\ItemList;
		use PayPal\Api\Payer;
		use PayPal\Api\Payment;
		use PayPal\Api\RedirectUrls;
		use PayPal\Api\Transaction;
		use PayPal\Api\ExecutePayment;
		use PayPal\Api\PaymentExecution;

//********************************** History Stamp Function
function stamp($my_order, $stamp, $message, $user = 'lcart'){
	$db = new MongoClient();
		$table = $db->lcart->transaction;
		$id = $my_order['_id'];
		$date = time();
		$table->update(array('_id' => $id), array('$push' => array('history' => array('code' => $stamp, 'details' => $message, 'user' => $user, 'date' => $date) )));
	}


//********************************** Stores $my_order at local MongoDB, returns $my_order with $_id
function saveToDatabase($my_order){
	global $secureEmail;
	$db = new MongoClient();
	$transaction = $db->lcart->transaction;
	$customer = $db->lcart->customer;
		
		//Seure Customer Record
		unset($my_order['customer']['form']['cardNumber']);
		unset($my_order['customer']['form']['expMonth']);
		unset($my_order['customer']['form']['expYear']);
		unset($my_order['customer']['form']['cvv2']);
		 

		//Insert Order to Orders Table
		$transaction->insert($my_order);
		stamp($my_order, 'CREATED', 'Created at saveToDatabase()');
	
		
		//Insert Customer to Customers Table		
		$customer->findAndModify(
			array("email" => $my_order['customer']['email']),
			array('$set' => array('form' => $my_order['customer']['form']))
		);
		
		
		
return $my_order;
}	

 

//********************************** Main Totals Function	
function total($my_order){
	$results = array(
		'subtotal' => 0,
		'subtotal_discount'=> 0,
		'items'=> 0,
		'shipping'=> 0,
		'shipping_discount'=> null,
		'weight'=>0,
		'tax'=> 0,
		'off'=> 0,
		'total'=> 0,
		'discount' => $my_order['discount']
	);
	
 
	
	$cases = 0;
	foreach($my_order['products'] as $slug => $product){
		$productqty = 0;
		foreach($product['variations'] as $key => $variation) {
			if(!$variation['qty'])continue;
			//Subtotal Normal
			$results['subtotal'] = $results['subtotal'] + ($variation['qty'] * $product['price']);
			//Subtotal Discount
			$results['subtotal_discount'] = $results['subtotal_discount'] + ($variation['qty'] * $product['discount']);
			//Shipping
			$results['shipping'] = $results['shipping'] + ($variation['qty'] * $product['shipping']);
			$results['weight'] = $results['weight'] + ($variation['qty'] * $product['weight']);
			if($product['groups'] && in_array('case', $product['groups'])){
				$cases += $variation['qty'];
			}
		}
	}
		 
	
//Calculate Shipping
	//Weight Shipping
	$add_weight = $results['weight'] * 5;
	if($add_weight > 20) $add_weight = 20;
	$results['shipping'] =  $results['shipping'] + $add_weight;
	// Free shipping with 20+ cases
	if($cases >= 20) $results['shipping'] = 0;
	
	
//Discounts
	//Fix Shipping_discount
	$results['shipping_discount'] = $results['shipping'];
	if($results['discount']){
		//Set Flat Rate off Shipping
		if(isset($results['discount']['shipping'])){
			switch($results['discount']['shipping']['op']){
				case '=':
					$results['shipping_discount'] = $results['discount']['shipping']['amt'];
				break;
			}
		}
		//Set Flat Rate off Subtotal
		if(isset($results['discount']['subtotal'])){
			switch($results['discount']['subtotal']['op']){
				case '-':
					$results['subtotal_discount'] -= $results['discount']['subtotal']['amt'];
				break;
				case '*':
					$results['subtotal_discount'] *= $results['discount']['subtotal']['amt'];
				break;
			}
		}
	}
	
	// If we need to override shipping.
	if(isset($my_order['form']['shipping_override']) && $my_order['form']['shipping_override'] >= 0){
		$results['shipping_override'] = $my_order['form']['shipping_override'];
		$results['shipping_discount'] = $results['shipping_override'];
		$results['shipping'] = $results['shipping_override'];
	}
	
	
	// No negative shipping
	$results['shipping_discount'] =  ($results['shipping_discount'] < 0) ? 0 : $results['shipping_discount'];

	//Round
	$results['subtotal_discount'] = round($results['subtotal_discount'],2);
	$results['shipping_discount'] = round($results['shipping_discount'],2);

	//Amount Off
	$results['off'] = ($results['subtotal'] - $results['subtotal_discount']) + ($results['shipping'] - $results['shipping_discount']);
	$results['off'] = -1 * abs($results['off']);
	
	//Tax
	$results['tax'] = 0;
	if($my_order['customer']['form']['billingState'] == 'CA') $results['tax'] = round($results['subtotal_discount'] * 0.0875,2);
	if($my_order['customer']['form']['taxid']) $results['tax'] = 0;

	//Final Total
	$results['total'] = ($results['subtotal'] + $results['off']) + $results['tax'] + $results['shipping'];
		
	//Create totals
	$my_order['totals'] = $results;
	

return $my_order;
}


//********************************** Credit Card Processor
function chargeCard($my_order){
	
		
		//Set Totals
		$my_order = total($my_order);
			
		if(!$my_order['totals']['total']){
			$cookie['error'] = 'Sorry, your order total must be greater than $0.00 to checkout.';
			return $cookie;
		}

		 	
			 		
	 		//OK
	 		$request = new AuthorizeNetCIM;
	 		$customerProfileId = $my_order['customer']['customerProfileId'];
	 		if($my_order['customer']['form']['card'] != 1) $paymentProfileId = $my_order['customer']['form']['card'];
	 				
	 				
	 				//Add payment profile.
						if(!$paymentProfileId){
						$paymentProfile = new AuthorizeNetPaymentProfile;
							//Card Info
							$paymentProfile->payment->creditCard->cardNumber = $my_order['customer']['form']['cardNumber'];
							$paymentProfile->payment->creditCard->expirationDate = $my_order['customer']['form']['expYear'] . '-' . $my_order['customer']['form']['expMonth'];
							$paymentProfile->payment->creditCard->cardCode = $my_order['customer']['form']['cvv2'];
							
							//Billing Info 
					        $paymentProfile->billTo->firstName    = $my_order['customer']['form']['firstName'];
					        $paymentProfile->billTo->lastName     = $my_order['customer']['form']['lastName'];
					        $paymentProfile->billTo->company      = $my_order['customer']['form']['company'];
					        $paymentProfile->billTo->address      = $my_order['customer']['form']['billingAddress1'].' '.$my_order['customer']['form']['billingAddress2'];
					        $paymentProfile->billTo->city         = $my_order['customer']['form']['billingCity'];
					        $paymentProfile->billTo->state        = $my_order['customer']['form']['billingState'];
					        $paymentProfile->billTo->zip          = $my_order['customer']['form']['billingZip'];
						 
							
								$response = $request->createCustomerPaymentProfile($customerProfileId, $paymentProfile);
									//PaymentProfileId
									if($response->isOk()){
										 $paymentProfileId = $response->getPaymentProfileId();
										 	
										 	//Store $paymentProfileId in Customer Record
												$db = new MongoClient();
												$customer = $db->lcart->customer;
												$push = array('$addToSet' => array('paymentProfileIds'  =>  $paymentProfileId ));
												$customer->update(array('email' => $my_order['customer']['email']), $push);
											
											
										 }else{
											$cookie['error'] = 'Payment: ' . $response->getMessageText();	
										 	return $cookie;
										 }
						}		
											
				
						/* Add shipping address.
						if(!$my_order['form']->customerAddressId){
						$address = new AuthorizeNetAddress;
							$address->firstName = $my_order['customer']['form']['firstName'];
							$address->lastName = $my_order['customer']['form']['lastName'];
							$address->company = $my_order['customer']['form']['shippingCompany'];
							$address->address = $my_order['customer']->form->shippingAddress1.' '.$my_order['customer']->form->shippingAddress2;
							$address->city = $my_order['customer']['form']['shippingCity'];
							$address->state = $my_order['customer']->contact->State2;
							$address->zip = $my_order['customer']->contact->PostalCode2;
							
								$response = $request->createCustomerShippingAddress($customerProfileId, $address);
								//CustomerAddressId
								if($response->isOk()){
									 $customerAddressId = $response->getCustomerAddressId();
									 $table->update(array('email' => $my_order['customer']->contact->Email), array('$push' => array('customerAddressIds' =>  $customerAddressId)));
									 }else{
										$cookie['error'] = $response->getMessageText();	
										return $cookie;
									 }
						}		
								
					 
			 			   */ 
			 			    
			 			    
								    //Sale Info
									    $sale = new AuthorizeNetTransaction;
									    
									    $sale->customerProfileId            = $customerProfileId;
									    $sale->customerPaymentProfileId     = $paymentProfileId;
									    //$sale->customerShippingAddressId    = $customerAddressId;
									 
										$id1 = new MongoId();
										$my_order['_id'] = $id1;
										
										$sale->order->invoiceNumber           = substr((string)$my_order['_id'],-5);
										$sale->order->description           = (string)$my_order['_id'];
										$sale->amount                       = $my_order['totals']['total'];
										
										
										//Tax	
										$tax = true;
										$sale->taxExempt = false;
											if($my_order['customer']['form']['taxid']){
												$tax = false;
												$sale->taxExempt = true;
											}
											if($my_order['totals']['tax']){
												$sale->tax->amount = $my_order['totals']['tax'];
												$sale->tax->name = "Sales Tax";
												$sale->tax->description = "California State Sales Tax";
											}           
										
										//Line Items
										foreach($my_order['products'] as $product){
											foreach($product['variations'] as $key => $variation){
												if(!$variation['qty'] || !$product['price']) continue;
											 
											 error_log(print_r($variation,true));
												
													$lineItem = new AuthorizeNetLineItem;
														$lineItem->itemId      = $variation['sku'];
														$lineItem->name        = substr($product['name'], 0, 30);
														//$lineItem->description = $product['description'];
														$lineItem->quantity    = $variation['qty'];
														$lineItem->unitPrice   = $product['price'];
														$lineItem->taxable     = $tax;
											
															$sale->lineItems[] = $lineItem;
											
											}
										}
					
									 
																	
									$sale->extraOptions->x_duplicate_window = 0;
									
					 				//Charge
									  $response = $request->createCustomerProfileTransaction("AuthCapture", $sale);
									    if($response->isOk()){
										    $transactionResponse = $response->getTransactionResponse();
										    if($transactionResponse->approved){
												$transactionId = $transactionResponse->transaction_id;
												
												
																							
												//Save Card to Order
												$my_order['customer']['form']['card'] = $paymentProfileId;
												
												//Save To database
												$my_order = saveToDatabase($my_order);
												
												//Stamp Order as Paid
												stamp($my_order, 'PAID','Paid VIA Credit Card [Authorize.net] #' . $transactionId);
												
												//Send Reciept
												sendReceipt($my_order);
												
													$cookie['success'] = true;
													$cookie['clear'] = true;
													$cookie['_id'] = $my_order['_id']->{'$id'};
													$cookie['url'] = $my_order['customer']['form']['confirmurl'];
													return $cookie;
													
											//On Decline
											}else{
												//Save To database
												$my_order = saveToDatabase($my_order);
												stamp($my_order,'DECLINE','Credit Card Decline');
												
											 	// Delete payment profile.
											 	$request->deleteCustomerPaymentProfile($customerProfileId, $paymentProfileId);
											 	
											 	//Remove $paymentProfileID from Customer Record
											 	$db = new MongoClient();
												$customer = $db->lcart->customer;
												$pull = array('$pull' => array('paymentProfileIds'  =>  $paymentProfileId ));
												$customer->update(array('email' => $my_order['customer']['email']), $pull);
												
												//Return Error
												$cookie['error'] = $transactionResponse->getMessageText();	
												return $cookie;
											}
												
										}else{
											
											//Save To database
											$my_order = saveToDatabase($my_order);
											stamp($my_order,'DECLINE','Credit Card Decline');
												
											// Delete payment profile.
											$request->deleteCustomerPaymentProfile($customerProfileId, $paymentProfileId);
											
											//Remove $paymentProfileID from Customer Record
											$db = new MongoClient();
												$customer = $db->lcart->customer;
												$pull = array('$pull' => array('paymentProfileIds'  =>  $paymentProfileId ));
												$customer->update(array('email' => $my_order['customer']['email']), $pull);
												
											//Return Error
											$cookie['error'] = $response->getMessageText();	
											return $cookie;
									 	}

									    
		 	
    
			//Response
			$cookie['error'] = $response->getMessageText();
			return $cookie;
	
}
 

//********************************** PayPal Setup Invoice
function chargePayPal($my_order){
		
	
		//Set Totals	
			$my_order = total($my_order);
		 
			 
			//Set PayPal Token/Secret
			$apiContext = new ApiContext(
				new OAuthTokenCredential(
				 
					PAYPAL1,
					PAYPAL2
					
					 
				)
			);
		
		//Define Config
			$apiContext->setConfig(
				array(
					'mode' => PAYPALMODE,
					'http.ConnectionTimeOut' => 30,
					'log.LogEnabled' => true,
					'log.FileName' => '/www/pal.log',
					'log.LogLevel' => 'FINE'
				)
			);
	
		//Begin Paypal Chain
			$payer = new Payer();
				$payer->setPaymentMethod("paypal");
			
		//Add Products
			$paypal_products = array();
			foreach($my_order['products'] as $product){
				foreach($product['variations'] as $key => $variation){
					if(!$variation['qty']) continue;
					
					$item = new Item();
						$item->setName(strval($product['name']))
							->setCurrency('USD')
							->setSku(strval($variation['sku']))
							->setQuantity(intval($variation['qty']))
							->setPrice(floatval($product['price']));
					array_push($paypal_products, $item);
					 

				
				}
			}
		
		
		//Add Promotion
		if($my_order['promo']){
			$item = new Item();
				$item->setName(strval('Promotion'))
					->setCurrency('USD')
					->setSku(strval($my_order['promo']))
					->setQuantity(intval(1))
					->setPrice(floatval($my_order['totals']['off']));
			
			array_push($paypal_products, $item);
		}			
				
			$itemList = new ItemList();
			$itemList->setItems($paypal_products);
		//Shipping, Tax, Subtotal
			$details = new Details();
			$details->setShipping(floatval($my_order['totals']['shipping']))->setTax(floatval($my_order['totals']['tax']))->setSubtotal(floatval($my_order['totals']['subtotal_discount']));
		//Final Total
			$amount = new Amount();
			$amount->setCurrency("USD")
					->setTotal(floatval($my_order['totals']['total']))
					->setDetails($details);
		//Define Invoice
			$transaction = new Transaction();
			$transaction->setAmount($amount)
					->setItemList($itemList)
					->setInvoiceNumber($my_order['_id']->{'$id'});
		//Set URLS
			$baseUrl = $my_order['customer']['form']['baseurl'];
			 
			$redirectUrls = new RedirectUrls();
			$redirectUrls->setReturnUrl("https://".API_HOST."/api/paypalExecute.php")
					->setCancelUrl("$baseUrl?paypal=cancel");
		//RUN PayPal
				$payment = new Payment();
				$payment->setIntent("sale")
					->setPayer($payer)
					->setRedirectUrls($redirectUrls)
					->setTransactions(array($transaction));
				try {
					$payment->create($apiContext);
				} catch (PayPal\Exception\PPConnectionException $ex) {
					$cookie['error'] = 'PayPal Error: ' . $ex;
					return $cookie;
				}			
				foreach($payment->getLinks() as $link) {
					if($link->getRel() == 'approval_url') {
						$redirectUrl = $link->getHref();
						break;
					}
				}
			
				
			//Save To database
			$my_order = saveToDatabase($my_order);
			
			//Save PayPalId To Order
			stamp($my_order,'PAYPALID',$payment->getId()); 
			
			
			//Ok, Succeed & Redirect to Paypal!!
			$cookie['success'] = true;
			$cookie['clear'] = false;
		 	$cookie['url'] = $redirectUrl;
			
			

return $cookie;

}


function sendReceipt($my_order){
	$my_order = total($my_order);
		 
	$to = $my_order['customer']['email'];
	$subject = 'Your Receipt';
	
	//set $body in reciept.php
	include('receipt.php');
	
	sendEmail($to, $subject, $body);
	sendEmail('receipt@lifeaidbevco.com', $subject, $body);
}

 //********************************** Sends SMTP Email Via GOOGLE
function sendEmail($to, $subject, $body){
 	$mail = new PHPMailer;
	$mail->isSMTP();									// Set mailer to use SMTP
	$mail->Host = 'ssl://smtp.gmail.com';				// Specify main and backup server
	$mail->SMTPAuth = true;								// Enable SMTP authentication
	$mail->Username = 'no-reply@lifeaidbevco.com';					// SMTP username
	$mail->Password = 'l1f3m41l';						// SMTP password
	$mail->SMTPSecure = 'ssl';							// Enable encryption, 'ssl' also accepted
	$mail->Port = 465;
	$mail->From = 'no-reply@lifeaidbevco.com';
	$mail->FromName = 'LifeAID Beverage Co.';
	$mail->CharSet = 'UTF-8';
	
	$mail->addAddress($to);
	$mail->isHTML(true);								
	$mail->Subject = $subject;
	$mail->Body	= $body;

	if($mail->send()) {
		return true;
	}

	return false;
	
}




 //********************************** Convert stdClass to Array
 	function objectToArray($d) {
		if (is_object($d)) {
			// Gets the properties of the given object
			// with get_object_vars function
			$d = get_object_vars($d);
		}
		
		if (is_array($d)) {
			/*
			* Return array converted to object
			* Using __FUNCTION__ (Magic constant)
			* for recursive call
			*/
			return array_map(__FUNCTION__, $d);
		}
		else {
			// Return array
			return $d;
		}
	}

?>