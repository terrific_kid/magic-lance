<?php
	require_once("/var/www/library/infusion/isdk.php");

	$app = new iSDK;
	if (!$app->cfgCon("LifeAIDBevCo"))die('Connection Failed!'); 
	 
 
	
//-----------------------------------------------------
// TRANSACTION DATAFEED
//-----------------------------------------------------
if (isset($_POST["FoxyData"])) {
 
 
	//DECRYPT (required)
	//-----------------------------------------------------
	$FoxyData_decrypted = foxycart_decrypt($_POST["FoxyData"]);
	$xml = simplexml_load_string($FoxyData_decrypted, NULL, LIBXML_NOCDATA);
 
 
	//Die if no XML or IS
	if(!$xml) die('busken');
	
	
	
	//For Each Transaction
	foreach($xml->transactions->transaction as $transaction) {
 
		sleep(0.6);
 
		$product_total = (float)$transaction->product_total;
		$tax_total = (float)$transaction->tax_total;
		$shipping_total = (float)$transaction->shipping_total;
		$order_total = (float)$transaction->order_total;
		
		$date = $transaction->transaction_date;
		$product_codes = array();
		$discount_codes = array();
		
		$customer_first_name = (string)$transaction->customer_first_name;
		$customer_last_name = (string)$transaction->customer_last_name;
		
		$customer_email = (string)$transaction->customer_email;
	
		$customer_address1 = (string)$transaction->customer_address1;
		$customer_address2 = (string)$transaction->customer_address2;
		$customer_city = (string)$transaction->customer_city;
		$customer_state = (string)$transaction->customer_state;
		$customer_postal_code = (string)$transaction->customer_postal_code;
		$customer_country = (string)$transaction->customer_country;
		$customer_phone = (string)$transaction->customer_phone;
		
		
 
		//Check for Existing Contact
		$email = $customer_email;
		$returnFields = array('Id');
		$data = $app->findByEmail($email, $returnFields);
		
		//Add Contact If Empty
		if(empty($data)):
		
			$c_id = $app->addCon(
					array(
						'FirstName' => $customer_first_name, 
						'LastName' => $customer_last_name, 
						'Email' => $customer_email,
						'StreetAddress1' => $customer_address1,
						'StreetAddress2' => $customer_address2,
						'City' => $customer_city,
						'State' => $customer_state,
						'PostalCode' => $customer_postal_code,
						'Country' => $customer_country,
						'Phone1' => $customer_phone,
						'LeadSourceId' => $api['LeadSourceId'],
						'ContactType' => 'Customer'
						), 
				'Email');
				
		else:

			$c_id = $app->addWithDupCheck(
					array(
						'FirstName' => $customer_first_name, 
						'LastName' => $customer_last_name, 
						'Email' => $customer_email,
						'StreetAddress1' => $customer_address1,
						'StreetAddress2' => $customer_address2,
						'City' => $customer_city,
						'State' => $customer_state,
						'PostalCode' => $customer_postal_code,
						'Country' => $customer_country,
						'Phone1' => $customer_phone,
						), 
				'Email');
			
		endif;
	
		//For Each Product Information	
		$products =  array();
		$subscriptions =  array();
		$codes = array();
		 

		foreach($transaction->transaction_details->transaction_detail as $transaction_detail) {
		
			//Ennumerate Product Object
			$product = array(
						'product_code' => (string)$transaction_detail->product_code,
						'product_price' => (string)$transaction_detail->product_price,
						'product_name' => (string)$transaction_detail->product_name,
						'product_quantity' => (string)$transaction_detail->product_quantity,
						'product_image' => (string)$transaction_detail->product_image,
						'url' => (string)$transaction_detail->url,
						'image' => (string)$transaction_detail->image,
						'sub_token' => (string)$transaction_detail->sub_token_url,
						'subscription_frequency' => (string)$transaction_detail->subscription_frequency,
						'subscription_startdate' => (string)$transaction_detail->subscription_startdate,
		 
			);
			
			 
 
		 
		
			//Add Product to Infusionsoft
			$returnFields = array('Id');
			$is_product = $app->dsFind('Product',1,0,'Sku',$product['product_code'],$returnFields);
			$new_product= array(
					'ProductName' => $product['product_name'],
					'ProductPrice' => $product['product_price'],
					'Sku' => $product['product_code'],
					'ProductName' => $product['product_name'],
					'Status' => 1,
					'Taxable' => 1,
					'Shippable' => 1
					);

	 
			if(empty($is_product)):
				$p_id = $app->dsAdd("Product", $new_product);
			else:
				$p_id = $is_product[0]['Id'];
			endif;
			
		
			//Add Infusionsoft Product ID to $products
			array_push($products, $p_id);
					
			if(!empty($product['sub_token'])) $active_subscription = 1;
		}
		
 
 
			
			
			
	//Add Coupons to $codes
		foreach($transaction->discounts->discount as $discount_details) {
			$discount_code = (string)$discount_details->code;
			array_push($codes, $discount_code);
		}




	//Add PlaceOrder to Infusionsoft
		$result= $app->placeOrder($c_id,0,0,$products,array(),false,$codes,0,$api['saleAffiliateId']);
		
	//Add Payments to Order		
		$invoice_id = $result['InvoiceId'];
			$pDate = $app->infuDate($date);
			
			$discount = $app->amtOwed($invoice_id);
			$result = $app->manualPmt($invoice_id,$discount,$pDate,'Fake Payment','Non-Real Payment',false);



    //If Subscription Product, Add To 'Active Subscriptions' Group
    	if($active_subscription) $result = $app->grpAssign($c_id, 153);
 
	} 



	
 
	//All Done!
	die("foxy");
 
//-----------------------------------------------------
// NO POST CONTENT SENT
//-----------------------------------------------------
}elseif(isset($_POST["FoxySubscriptionData"])) { 


	 $failedDaysBeforeCancel = 30;
	 $billingReminderFrequencyInDays = 3;
	 $updatePaymentMethodReminderDaysOfTheMonth = array(1,15);
		
		$FoxyData_decrypted = foxycart_decrypt($_POST["FoxySubscriptionData"]);
		$FoxyDataArray = simplexml_load_string($FoxyData_decrypted, NULL, LIBXML_NOCDATA);
	
	 
		foreach($FoxyDataArray->subscriptions->subscription AS $subscription) {
			foreach($subscription->transaction_details->transaction_detail AS $transaction_detail) {
				// do stuff here, collect data, etc...
			}
			$canceled = 0;
			$sendReminder = 0;
			if (date("Y-m-d",strtotime("now")) == date("Y-m-d", strtotime($subscription->end_date))) {
				// this entry was cancelled today...
				$canceled = 1;
			}
			if (!$canceled && $subscription->past_due_amount > 0) {
				$failedDays = floor((strtotime("now") - strtotime($subscription->transaction_date)) / (60 * 60 * 24));
				if ($failedDays > $failedDaysBeforeCancel) {
					$canceled = 1;
				} else {
					if (($failedDays % $billingReminderFrequencyInDays) == 0) {
						$sendReminder = 1;
					}
				}
			}
			if ($canceled) {
		
			//IF canceled, find customer ID
				$c_email = (string)$subscription->customer_email;
			 
				$returnFields = array('Id');
				$c_id = $app->findByEmail($c_email, $returnFields);
				$c_id = $c_id[0]['Id'];
				
			//Remove from 'Active Subscriptions' Group
				$groupId = 153;
				$result = $app->grpRemove($c_id, $groupId);
				 
				  
			
			}
			if ($sendReminder) {
			
			
			}
		}
				
				
				
die("foxy");
		
		

}else {
	die('No Content Received From Datafeed');
}
 
 
 


