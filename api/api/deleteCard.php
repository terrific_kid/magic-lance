<?php
 
$secure = true;
require_once('start.php');




function run(){
	global $secureEmail;
	$postdata = file_get_contents("php://input");
	$data = json_decode($postdata);
		$card = $data->card;
		$card = objectToArray($card);
		
		if($secureEmail && $card){
			$request = new AuthorizeNetCIM;
			// Delete payment profile.
			$response = $request->deleteCustomerPaymentProfile($card['customerProfileId'], $card['paymentProfileId']);
			if($response->isOk()){
				//Store $paymentProfileId in Customer Record
					$db = new MongoClient();
					$customer = $db->lcart->customer;
					$pull = array('$pull' => array('paymentProfileIds'  =>  $card['paymentProfileId'] ));
					$customer->update(array('email' => $secureEmail), $pull);


				$cookie['success'] = 'Card Deleted.';
				return $cookie;
			}
		}
	
	

 
	
$cookie['error'] = 'Some Error!';
return $cookie;
}
			

require_once('end.php'); 


?>
