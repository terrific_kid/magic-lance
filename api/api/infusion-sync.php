<?php
class InfusionsoftSync {

	/**
	 * Set environment
	 * Automatically sets a few variables
	 */
	const ENVIRONMENT = 'production';



	/**
	 * Holds an Infusionsoft Contact ID for the current contact during a loop
	 * Will be false when a contact update has failed or the loop is initiated yet
	 * @var int / bool
	 */
	private $contact_id = false;

	/**
	 * Holds the MongoClient object
	 * @var obj
	 */
	private $db;

	/**
	 * Holdes Infusionsoft iSDK "$app" object
	 * @var object
	 */
	private $iSDK;

	/**
	 * Infusionsoft Application Name
	 * @var string
	 */
	private $infusionsoft_application;

	/**
	 * Infusionsoft API Key
	 * @var string
	 */
	private $infusionsoft_apikey;

	/**
	 * Holds the Mongo Order object during a loop
	 * @var obj / bool
	 */
	private $order = false;

	/**
	 * Email Address to email log data to
	 * @var string
	 */
	private $log_to_address = 'x+26124237253383@mail.asana.com'; // emails data into the IS Error Log Project in Asana
	// private $log_to_address = 'thomas@gocodebox.com'; // emails data into the IS Error Log Project in Asana

	/**
	 * Email Address that log emails will come from
	 * @var string
	 */
	private $log_from_address = 'thomas@gocodebox.com'; // if using an Asana address, this should be a member of the Asana Workspace

	/**
	 * Product ID of the "shipping" product
	 * @var integer
	 */
	private $shipping_product_id = 0;

	/**
	 * Holds Infusionsoft Tags for the Instance
	 * @var array
	 */
	private $tags = array();

	/**
	 * Infusionsoft Tag Category ID of the "Website" Tags
	 * @var integer
	 */
	private $tag_category_id = 0;





	/**
	 * Constructor
	 * @return null
	 */
	public function __construct() {
		// require everything needed
		require_once('start.php');
		require_once('infusion/isdk.php');

		$this->_set_env_vars();

		// initialize the iSDK
		$this->iSDK = new iSDK;


		// connect the iSDK to an Infusionsoft App
		if( !$this->iSDK->cfgCon( $this->infusionsoft_application, $this->infusionsoft_apikey ) ) {

			// log the conncetion error
			$this->_log( array(
				'$this->infusionsoft_application' => $this->infusionsoft_application,
				'$this->infusionsoft_apikey'      => $this->infusionsoft_apikey
			), 'API Connection Failed to Connect!' );

			die('Infusionsoft Connection Failed');

		}


		// initialize Mongo
		$m = new MongoClient();
		$this->db = $m->selectDB('lcart');

		// execute a DB Query
		$orders = $this->_query( );

		// $this->_log( array(
		// 	'$orders' => $orders->count()
		// ), 'Sync Called', true );

		// if we have orders, initate a loop
		if( $orders->count( true ) ) {

			$this->_loop($orders);

		}
	}


	/**
	 * Dump a variable inside a pre tag for better readability
	 * @param  mixed    $var    anything really
	 * @return null
	 */
	private function _dump( $var) {
		// kill output unless we're in development
		if( self::ENVIRONMENT != 'development' ) return;

		echo '<pre>'; var_dump( $var ); echo '</pre>';
	}


	/**
	 * Set Instance variables based on environment constant
	 */
	private function _set_env_vars() {

		switch(self::ENVIRONMENT) {

			case 'development':
				$this->tag_category_id = 35;
				$this->infusionsoft_application = 'hn167';
				$this->infusionsoft_apikey = 'df9f8cf08e2d918a23ea1c6b8535a0884681f78601e4d283c43c500eb6ad0de8f795410c2b49c196f91d283ead2b47dc';
				$this->shipping_product_id = 18;
			break;


			default: // production
				$this->tag_category_id = 25;
				$this->infusionsoft_application = 'it140';
				$this->infusionsoft_apikey = 'bc90d6eaccf8f96f3ab88582111a27ad';
				$this->shipping_product_id = 324;
		}

	}



	/**
	 * Locates the created date stamp out of the transactions history
	 * returns current time() if no CREATED stamp found in history
	 * @return string     Infusionsoft friendly date string
	 */
	private function _get_date() {

		$history = $this->order['history'];

		$ts = 0;

		foreach($history as $stamp) {

			if($stamp['code'] == 'CREATED') {

				$ts = $stamp['date'];

				break; // skip the rest of the loop

			}

		}

		// if create stamp was found, set current timestamp
		if( !$ts ) {

			$ts = time();

		}

		return  $this->iSDK->infuDate( date('d-m-Y', $ts ) );
	}



	/**
	 * Retreive all "Website" tags from Infusionsoft and save them to $this->tags
	 * @todo   find a way to cache tags locally (possibly store in Mongo table)
	 * @return bool     true if no errors encountered, false otherwise
	 */
	private function _get_tags( ) {
		$limit = 1000;
		$page = 0;

		$all_tags = array();
		while(true) {

			$tags = $this->iSDK->dsQuery(
				'ContactGroup',
				$limit,
				$page,
				array( 'GroupCategoryId' => $this->tag_category_id ),
				array( 'Id', 'GroupName')
			);

			if( !is_array($tags) ) {
				$this->_log( array(
					'$tags' => $tags
				), 'Error Retrieving Tags from Infusionsoft' );

				return false;
			}

			// add to the instance array
			$all_tags = array_merge($all_tags, $tags);

			// if theres fewer tags than the limit we need to break the loop
			if( count($tags) < $limit ) {
				break;
			} else {
				$page++;
			}

		}

		foreach($all_tags as $tag) {
			$this->tags[$tag['Id']] = $tag['GroupName'];
		}

		return true;

	}



	/**
	 * Log an array of Data via Email
	 * @param  array   $data        data to log as the body of an email (will be converted to JSON)
	 * @param  string  $subject     subject line of the email
	 * @param  boolean $backtrace   optionally include a full debug_backtrace() in the $data array
	 * @return null
	 */
	private function _log($data = array(), $subject = 'Unknown Error', $backtrace = false) {
		// setup defaults
		$defaults = array(
			'http_host' => $_SERVER['HTTP_HOST'],
			'timestamp' => date('Y-m-d H:i:s'),
			'timezone'  => date_default_timezone_get()
		);

		// include a backtrace
		if($backtrace) {
			$defaults['$_SERVER']  = $_SERVER;
			$defaults['backtrace'] = debug_backtrace();
		}

		// merge the arrays
		$data = array_merge($defaults, $data);

		// add some host info to the subject so we can quickly ignore dev stuff in the logger
		$subject =  $subject . ' (' . $_SERVER['HTTP_HOST'] . ')';

		sendEmail( $this->log_to_address, $subject, json_encode( $data, JSON_PRETTY_PRINT ) );

		$this->_dump('LOG DUMP: ' . $subject );
		$this->_dump( $data );
	}

	/**
	 * Loop through each order and do necessary functions to each of them
	 * @param  obj    $orders  Mongo Cursor Object
	 * @return null
	 */
	private function _loop( $orders ) {

		foreach($orders as $order) {

			$this->_dump($order);

			// setup the order
			$this->order = $order;

			// set an Infusionsoft friendly order date for the order
			$this->order['OrderDate'] = $this->_get_date();

			// check to see if we should update the contact
			if( $this->maybe_sync( 'INFUSION-CONTACT' ) ) {

				// update the contact & get the ID for the loop
				$this->contact_id = $this->contact();

			}
			// no need to update, retreive the contact id by email
			else {

				$this->contact_id = $this->contact_get_id();

			}

			// skip the remaining items in the loop if we don't have a contact_id
			if( !$this->contact_id ) continue;


			// maybe update start dates
			$start_dates = $this->start_dates();
			if( !$start_dates ) continue;


			// check if we need to create an invoice
			if( $this->maybe_sync( 'INFUSION-INVOICE' ) ) {

				// create an invoice
				$invoice = $this->invoice();
				if( !$invoice ) continue;

			}

			// check if we need to apply tags
			if( $this->maybe_sync( 'INFUSION-TAGS' ) ) {
				// apply tags
				$this->tag();
			}


			// check all necessary stamps and do a final "success" stamp if all others are successful
			foreach( array('CONTACT', 'INVOICE', 'TAGS') as $stamp ) {
				// kill if we need to sync again
				if( $this->maybe_sync( 'INFUSION-' . $stamp ) ) return;
			}

			// if we made it this far, add the stamp
			stamp($this->order, 'INFUSION-SUCCESS', 'Contact, Invoice, and Tags completed by infusionSync');
			$this->_dump('order: ' . $this->order['_id'] . ' was synced');

		}

	}


	/*
	 * Retrieve orders from MongoDB
	 * @param  integer   $count   number of results to retrieve
	 * @return obj                Mongo Cursor Object
	 */
	private function _query( $count = 15 ) {
		$collection = new MongoCollection($this->db, 'transaction');
		$results = $collection->find( array(
				'history' => array('$elemMatch' => array('code' => 'PAID')),
				'history.code' => array('$nin' => array( 'INFUSION-SUCCESS', 'INFUSION' ) ) // include old stamp so that we don't reprocess historical orders
			)
		)->limit( $count );

		// if there's no results found, start syncing in old orders
		if( !$results->count( true) ) {

			$start = strtotime('2015-01-13 00:00:00');
			$end =   strtotime('2015-02-27 23:59:59');

			$results = $collection->find( array(
					'history' => array('$elemMatch' => array('code' => 'PAID')),
					'history.0.date' => array( '$gte' => $start , '$lte' => $end ),
					'history.code' => array('$nin' => array( 'INFUSION-SUCCESS' ) ) // new script shouldn't reprocess these
				)
			)->limit( $count );

		}

		$this->_dump( $results->count( true ) );

		return $results;
	}


	/**
	 * Update a contact record with date from the order
	 * @return int     Infusionsoft ContactId of the contact
	 */
	private function contact() {
		$conDat = array();

		// Update Contact Record
		$conDat['Email'] = $this->order['customer']['email'];

	    // Billing Address
	    $conDat['FirstName']      = $this->order['customer']['form']['firstName'];
		$conDat['LastName']       = $this->order['customer']['form']['lastName'];
		$conDat['StreetAddress1'] = $this->order['customer']['form']['billingAddress1'];
		$conDat['StreetAddress2'] = $this->order['customer']['form']['billingAddress2'];
		$conDat['City']           = $this->order['customer']['form']['billingCity'];
		$conDat['State']          = $this->order['customer']['form']['billingState'];
		$conDat['PostalCode']     = $this->order['customer']['form']['billingZip'];
		$conDat['Phone1']         = $this->order['customer']['form']['billingPhone'];

		// Shipping Address
		$conDat['Address2Street1'] = $this->order['customer']['form']['shippingAddress1'];
		$conDat['Address2Street2'] = $this->order['customer']['form']['shippingAddress2'];
		$conDat['City2']           = $this->order['customer']['form']['shippingCity'];
		$conDat['State2']          = $this->order['customer']['form']['shippingState'];
		$conDat['PostalCode2']     = $this->order['customer']['form']['shippingZip'];
		$conDat['Phone2']          = $this->order['customer']['form']['shippingPhone'];

		// TAX ID
		$conDat['SpouseName'] = $this->order['customer']['form']['taxid'];

		/**
		 * TODO: Lookup company and add CompanyId & Record Properly
		 */
		//Company
		$conDat['Company'] = $this->order['customer']['form']['company'];

		//APO Address
		$conDat['Address3Street1'] = $this->order['customer']['form']['apoAddress1'];
		$conDat['Address3Street2'] = $this->order['customer']['form']['apoAddress2'];
		$conDat['City3']           = $this->order['customer']['form']['apoCity'];
		$conDat['State3']          = $this->order['customer']['form']['apoState'];
		$conDat['PostalCode3']     = $this->order['customer']['form']['apoZip'];
		$conDat['Phone3']          = $this->order['customer']['form']['apoPhone'];

		//Update Contact
		$contact_id = $this->iSDK->addWithDupCheck($conDat, 'Email');

		// will return a contact ID, if it doens't return an int log the error
		if( !is_int($contact_id) ) {

			$this->_log( array(
				'$contact_id' => $contact_id
			), 'Contact Update Failed' );

			// kill the rest of this function
			return false;

		}

		stamp($this->order, 'INFUSION-CONTACT', 'Contact record updated by infusionSync');

		return $contact_id;
	}


	/**
	 * Locate an existing customers Infusionsoft ID
	 * @todo               this data should be cached locally somehow, looking it up back through Infusionsoft is an imperfect solution
	 * @return int / bool         contact id if found, false otherwise
	 */
	private function contact_get_id() {

		$contacts = $this->iSDK->findByEmail($this->order['customer']['email'], array('Id'));

		if( is_array( $contacts ) ) {

			// run a special log for potential merges if we have more than one contact returned
			if( count($contacts) > 1 ) {
				$this->_log( array(
					'message'                           => 'first contact was used for sync, might need a manual merge',
					'$contacts'                         => $contacts,
					'$this->order["customer"]["email"]' => $this->order['customer']['email']
				), 'Multiple Infusionsoft Contacts Returned for Customer' );
			}

			return $contacts[0]['Id'];

		}
		// log an error
		else {

			$this->_log( array(
				'$contacts'    => $contacts,
				'$this->order' => $this->order
			), 'Failed to retreive contact id' );

		}

		return false;
	}

	/**
	 * Create an Invoice and add items to it
	 * @return bool
	 */
	private function invoice() {
		$invoice_id = $this->iSDK->blankOrder( $this->contact_id, 'lcart1:: ' . $this->order['promo'], $this->order['OrderDate'], 0, 0 );

		if( !is_int($invoice_id) ) {

			$this->_log( array(
				'$invoice_id'        => $invoice_id,
				'$this->contact_id' => $this->contact_id
			), 'invoice creation failed' );

			return false;

		}

		// update order status
		$this->iSDK->dsUpdate("Job", $invoice_id, array( 'OrderStatus'  => 123 ) );

		if( $this->order['promo'] ) {

			$promo_added = $this->iSDK->addOrderItem( $invoice_id, 0, 7, floatval( $this->order['totals']['off'] ), 1, $this->order['promo'], $this->order['promo'] );

			if( $promo_added != true ) {

				$this->_log( array(
					'$promo_added'       => $promo_added,
					'$invoice_id'        => $invoice_id,
					'$this->contact_id' => $this->contact_id
				), 'invoice creation failed during promo item addition' );

				// delete the order and kill the remaining creation steps
				return $this->invoice_destroy( $invoice_id );

			}
		}

		// Add products
		foreach( $this->order['products'] as $product ){

			foreach( $product['variations'] as $key=>$variation ){
				if( !$variation['qty'] ) continue;

				$product_added = $this->iSDK->addOrderItem( $invoice_id, 0, 4, floatval( $product['price'] ), intval( $variation['qty'] ), $product['name'], '#poison#'.$variation['sku'] );

				if( $product_added != true ) {

					$this->_log( array(
						'$product'           => $product,
						'$variation'         => $variation,
						'$product_added'     => $product_added,
						'$invoice_id'        => $invoice_id,
						'$this->contact_id' => $this->contact_id
					), 'invoice creation failed during product item addition' );

					return $this->invoice_destroy( $invoice_id );
				}
			}
		}

		if( array_key_exists('totals', $this->order) && is_array( $this->order['totals'] ) ) {

			// Add Taxes
			if( $this->order['totals']['tax'] ) {

				$tax_added = $this->iSDK->addOrderItem( $invoice_id, 0, 2, floatval( $this->order['totals']['tax'] ), 1, 'Sales Tax', '' );

				// returns false on failure (true on success)
				if( $tax_added != true ) {

					$this->_log( array(
						'$tax_added'        => $tax_added,
						'$invoice_id'       => $invoice_id,
						'$this->contact_id' => $this->contact_id
					), 'invoice creation failed during tax item addition' );

					// delete the order and kill the remaining creation steps
					return $this->invoice_destroy( $invoice_id );

				}

			}

			// Add Shipping
			if( $this->order['totals']['shipping_discount'] ) {

				$shipping_added = $this->iSDK->addOrderItem( $invoice_id, $this->shipping_product_id, 4, floatval( $this->order['totals']['shipping_discount'] ), 1, '', '' );

				// returns false on failure (true on success)
				if( $shipping_added != true ) {

					$this->_log( array(
						'$shipping_added'    => $shipping_added,
						'$invoice_id'       => $invoice_id,
						'$this->contact_id' => $this->contact_id
					), 'invoice creation failed during shipping item addition' );

					// delete the order and kill the remaining creation steps
					return $this->invoice_destroy( $invoice_id );

				}
			}

			$payment_added = $this->iSDK->manualPmt( $invoice_id, floatval( $this->order['totals']['total'] ), $this->order['OrderDate'], 'WEBSITE', 'PAID IN FULL VIA WEBSITE', false );

			if( $payment_added != true ) {

				$this->_log( array(
					'$payment_added'    => $payment_added,
					'$invoice_id'       => $invoice_id,
					'$this->contact_id' => $this->contact_id,
					'$this->order'      => $this->order
				), 'invoice creation failed during payment application' );

				// delete the order and kill the remaining creation steps
				// return $this->invoice_destroy( $invoice_id );

			}

		} // end totals

		stamp($this->order, 'INFUSION-INVOICE', 'Invoice created by infusionSync');
		return true;
	}

	/**
	 * Delete an Invoice & Order if any part of the invoice creation fails
	 * The invoice will be recreated on the next sync attempt, this method will help cleanup incomplete invoices
	 * @param  int    $invoice_id    Infusionsoft Invoice ID
	 * @return bool                  Always returns false, this function will be the return value of invoice() which should be halted upon an invoice destroy
	 */
	private function invoice_destroy( $invoice_id ) {

		$delete = $this->iSDK->deleteInvoice( $invoice_id );

		if( $delete != true ) {

			$this->_log( array(
				'note'              => 'This invoice *should* have been deleted because a previous api call to create a complete invoice failed. Manual deletion of the Invoice & associated Order may be necessary',
				'$delete'           => $delete,
				'$this->contact_id' => $this->contact_id
			), 'Invoice Destroy Cleanup Failed' );

		}

		// return false on sucessful deletion so that we return false in the invoice() funciton as well
		return false;

	}


	/**
	 * Determine if the record has an INFUSION-CONTACT stamp in its history
	 * @return bool     true if sync is needed (not in history), false if sync is not needed (stamp exists in history)
	 */
	private function maybe_sync( $code ) {

		foreach( $this->order['history'] as $stamp ) {

			if( $stamp['code'] == $code ) return false;

		}

		return true;

	}


	/**
	 * Determine if Fridge & Start Dates need to be set
	 * @return bool    true on success or no update needed, false on failure
	 */
	private function start_dates() {
		// get existing custom field data
		$contact_data = $this->iSDK->loadCon($this->contact_id, array(
			'_DateStarted',
			'_FridgeStarted'
		));


		if( is_array( $contact_data) ) {

			$update = array();

			// update Date Started if no date is set on the contact record
			if( ! array_key_exists( '_DateStarted', $contact_data ) ) {

				$update['_DateStarted'] = (string) date( 'c' );

			}

			// maybe update Fridge Strated if theres no Fridge Date Set
			// this wont bother with loops at all if the fridge is already set
			if( ! array_key_exists( '_FridgeStarted', $contact_data ) ) {

				$break = false;
				// loop through products to see if there's a fridge
				foreach( $this->order['products'] as $slug=>$product ) {

					// loop through variations
					foreach( $product['variations'] as $sku=>$variation ) {
						if( !$variation['qty'] ) continue;

						// fridge is found, add it to the update
						if( $sku == 'VFA90FF00' ) {
							$update['_FridgeStarted'] = (string) date( 'c' );
							$break = true; // break the outer loop
							break;
						}
					}

					// break if the nested loop tells us to break
					if($break) break;
				}

			}

			// we have an update to run
			if( $update ) {

				$updated = $this->iSDK->updateCon( $this->contact_id, $update );

				// if not an int, there was an error updating
				if( !is_int( $updated) ) {

					$this->_log( array(
						'$updated'          => $updated,
						'$this->contact_id' => $this->contact_id
					), 'Contact Start / Fridge Date Update Failed' );

					return false;

				}

			}

		}
		return true;
	}



	// 3
	// apply tags
	private function tag() {

		// retrieve tags into $this->tags and kill function if failure
		if( !$this->_get_tags() ) return false;

		// setup array of tags to apply to a contact
		$to_apply = array();

		// add products to array
		if( is_array( $this->order['products'] ) ) {
			foreach( $this->order['products'] as $product ) {
				foreach( $product['variations'] as $sku=>$variation ) {
					if( !$variation['qty'] ) continue;
					$to_apply[] = $product['slug'];
				}
			}
		}


		// add a promo tag if necessary
		if( isset($this->order['promo']) ) $to_apply[] = $this->order['promo'];

		// apply each tag
		foreach( $to_apply as $tag ) {

			// find the tag id
			$tag_id = array_search( $tag, $this->tags );

			// make a new tag if it doesnt exist
			if( !$tag_id ) {

				$tag_id = $this->iSDK->dsAdd( 'ContactGroup', array(
					'GroupName' => $tag,
					'GroupCategoryId' => $this->tag_category_id
				) );

				if( !is_int($tag_id) ) {
					$this->_log( array(
						'$tag_id'           => $tag_id,
						'$applied'          => $applied,
						'$this->contact_id' => $this->contact_id
					), 'Tag Creation Failed' );

					return false;
				}
			}

			// apply the tag
			$applied = $this->iSDK->grpAssign( $this->contact_id, $tag_id );

			// if( $applied != true ) {

				// $this->_log( array(
				// 	'$tag_id'           => $tag_id,
				// 	'$tag'              => $tag,
				// 	'$applied'          => $applied,
				// 	'$this->contact_id' => $this->contact_id
				// ), 'Tag Application Failed' );

				/**
				 * @note note sure how to handle retry, maybe need to save a retry stamp
				 * tag application will return false if they already have the tag
				 */
				// return false;

			// }
		}

		stamp($this->order, 'INFUSION-TAGS', 'Contact record updated by infusionSync');
		return true;

	}
}
new InfusionsoftSync;
?>