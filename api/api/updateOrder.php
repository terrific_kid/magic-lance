<?php
 
$secure = true;
require_once('start.php');




function run(){
	global $secureEmail;
	//Parse INPUT 
 	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	if($secureEmail && $request->order){	
		
		$my_order = $request->order;
		$my_order = objectToArray($my_order);
		unset($my_order['_id']);
		
		//Look for Contact Record
			$db = new MongoClient();
			$transaction = $db->lcart->transaction;
			$id = new MongoId($my_order['id']);
		
		
		//Insert Customer to Customers Table		
			$transaction->findAndModify(
				array("_id" => $id, 'customer.email' => $secureEmail),
				array('$set' => $my_order)
			);
		
		$cookie['success'] = 'Order Updated!';
		return $cookie;
	}

	$cookie['error'] = 'Error!';
	return $cookie;
	
	
	
	}
			

require_once('end.php'); 


?>
