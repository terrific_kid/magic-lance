<?php
 
$secure = true;
require_once('start.php');




function run(){
	global $secureEmail;
	//Parse INPUT 
 	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	if($secureEmail && $request->id){	
		
		$data = $request->id;
		
		//Call up DB
			$db = new MongoClient();
			$transaction = $db->lcart->transaction;
			$id = new MongoId($data);
		
		
		//Remove Order from Database	
			$transaction->remove(
				array("_id" => $id, 'customer.email' => $secureEmail),
				array('justOne' => true)
			);
		
		$cookie['success'] = 'Order Deleted!';
		return $cookie;
	}

	$cookie['error'] = 'Error!';
	return $cookie;
	
	
	
	}
			

require_once('end.php'); 


?>
