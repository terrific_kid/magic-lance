<?php
 
$secure = true;
require_once('start.php');




function run(){
	global $secureEmail;
	//Parse INPUT 
 	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	
		if($secureEmail && $request->order){
			$my_order = $request->order;
			$my_order = objectToArray($my_order);
			$my_order['_id'] = new MongoId($my_order['id']);
			
			
			if($my_order['customer']['email'] == $secureEmail) stamp($my_order, 'CANCEL', 'Subscription Canceled');
			
			
			$cookie['success'] = 'Subscription Canceled!';
			return $cookie;
		}

	$cookie['error'] = 'Error!';
	return $cookie;
	}
			

require_once('end.php'); 


?>
