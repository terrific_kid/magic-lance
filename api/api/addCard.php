<?php
 
$secure = true;
require_once('start.php');




function run(){
	global $secureEmail;
	$postdata = file_get_contents("php://input");
	$data = json_decode($postdata);
			$card = $data->card;
			$card = objectToArray($card);

	//Add payment profile.
						if($secureEmail && $card){
						$request = new AuthorizeNetCIM;
						$paymentProfile = new AuthorizeNetPaymentProfile;
						
							//Card Info
							$paymentProfile->payment->creditCard->cardNumber = $card['cardNumber'];
							$paymentProfile->payment->creditCard->expirationDate = $card['expYear'] . '-' . $card['expMonth'];
							$paymentProfile->payment->creditCard->cardCode = $card['cvv2'];
							
							//Billing Info 
					        $paymentProfile->billTo->firstName    = $card['firstName'];
					        $paymentProfile->billTo->lastName     = $card['lastName'];
					        $paymentProfile->billTo->company      = $card['company'];
					        $paymentProfile->billTo->address      = $card['billingAddress1'].' '.$card['billingAddress2'];
					        $paymentProfile->billTo->city         = $card['billingCity'];
					        $paymentProfile->billTo->state        = $card['billingState'];
					        $paymentProfile->billTo->zip          = $card['billingZip'];
						 
							
								$response = $request->createCustomerPaymentProfile($card['customerProfileId'], $paymentProfile);
									//PaymentProfileId
									if($response->isOk()){
										 $paymentProfileId = $response->getPaymentProfileId();
										 //Store $paymentProfileId in Customer Record
												$db = new MongoClient();
												$customer = $db->lcart->customer;
												$push = array('$addToSet' => array('paymentProfileIds'  =>  $paymentProfileId ));
												$customer->update(array('email' => $secureEmail), $push);
												$cookie['success'] = 'Card Added!';
												return $cookie;

										 }else{
											$cookie['error'] = 'Payment: ' . $response->getMessageText();	
										 	return $cookie;
										 }
						}		


	
	
return $cookie;
}
			

require_once('end.php'); 


?>
