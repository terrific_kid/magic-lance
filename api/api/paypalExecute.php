<?php
	require_once('start.php');
 
	
	//Define Libraries
	use PayPal\Rest\ApiContext;
	use PayPal\Auth\OAuthTokenCredential;
	use PayPal\Api\Amount;
	use PayPal\Api\Details;
	use PayPal\Api\Item;
	use PayPal\Api\ItemList;
	use PayPal\Api\Payer;
	use PayPal\Api\Payment;
	use PayPal\Api\RedirectUrls;
	use PayPal\Api\Transaction;
	use PayPal\Api\ExecutePayment;
	use PayPal\Api\PaymentExecution;
	
		//Set PayPal Token/Secret
			$apiContext = new ApiContext(
				new OAuthTokenCredential(
				 
					PAYPAL1,
					PAYPAL2
					
					 
				)
			);
		
		//Define Config
			$apiContext->setConfig(
				array(
					'mode' => PAYPALMODE,
					'http.ConnectionTimeOut' => 30,
					'log.LogEnabled' => true,
					'log.FileName' => '/www/pal.log',
					'log.LogLevel' => 'FINE'
				)
			);
				
				
				//Execute Paypal Payment & Stamp PAID 					
				$paypalId = $_REQUEST['paymentId'];
				$db = new MongoClient();
				$table = $db->lcart->transaction;
				$my_order = $table->findOne(array('history' => array('$elemMatch' => array('details' => $paypalId) )));
			
					$payment = Payment::get($paypalId, $apiContext);
					$execution = new PaymentExecution();
					$execution->setPayerId($_GET['PayerID']);
					$result = $payment->execute($execution, $apiContext);
					$result = $result->toArray();
					$paid = $result['transactions'][0]['amount']['total'];
					$transactionId = $result['transactions'][0]['related_resources'][0]['sale']['id'];
					
					
					if($result['state'] == 'approved'){	
							 stamp($my_order, 'PAID','Paid VIA PayPal #' . $transactionId);
							 header('Location: '.$my_order['customer']['form']['confirmurl']);
							 exit;
					}

?>