<?php

/*

Get a list of invoices by date from IS, enter used promo codes into mongo db.

*/
if($_REQUEST['secret'] != 'keke')die('der!');

require_once('/www/api/api/infusion/isdk.php');
require_once('/www/api/api/start.php');

$app = new iSDK;

if (!$app->cfgCon("LifeAIDBevCo"))die('Connection Failed!'); 


function saveLastRunDate($date){
	$my_file = '/tmp/order_sync.txt';
	$handle = fopen($my_file, 'w') or die('Cannot open file:  ' . $my_file);
	fwrite($handle, $date);
	fclose($handle);
}

function getLastRunDate(){
	$my_file = '/tmp/order_sync.txt';
	if($handle = fopen($my_file, 'r')){
		$date = fread($handle, filesize($my_file));
		fclose($handle);
		return $date;		
	}
	return false;
}

// Sync an individual tag.	 
function getOrder($order_id){
	
	global $app;
	$max_results = 10;
	
	$returnFields = array(
		'ContactId',
		'DateCreated',
		'JobTitle',
		'DueDate',
		'Id',
		'JobNotes',
		'JobRecurringId',
		'JobStatus',
		'OrderStatus',
		'OrderType',
		'ProductId',
		'ShipFirstName',
		'ShipLastName',
		'ShipPhone',
		'ShipCompany',
		'ShipCity',
		'ShipState',
		'ShipStreet1',
		'ShipStreet2',
		'ShipZip',
		'StartDate',
	);
		
	$query = array(
		'Id' => $order_id,
	);
		
	$orders = $app->dsQuery("Job", $max_results, 0, $query, $returnFields);
	
	$result = '';
	
	// Error
	if(!is_array($orders)){
		$result = print_r($orders, true);
	}

	// No results
	else if(count($orders) == 0){
		$result = "No results";
	}
	
	else {
		$result = $orders[0];
	}
	
	return $result;
		
}


// Sync an individual tag.	 
function getContact($contact_id){
	
	global $app;
	$max_results = 10;
	
	$returnFields = array(
		'Address2Street1',
		'Address2Street2',
		'Address2Type',
		'Address3Street1',
		'Address3Street2',
		'Address3Type',
		'Anniversary',
		'AssistantName',
		'AssistantPhone',
		'BillingInformation',
		'Birthday',
		'City',
		'City2',
		'City3',
		'Company',
		'CompanyID',
		'ContactNotes',
		'ContactType',
		'Country',
		'Country2',
		'Country3',
		'CreatedBy',
		'Email',
		'EmailAddress2',
		'EmailAddress3',
		'Fax1',
		'Fax1Type',
		'Fax2',
		'Fax2Type',
		'FirstName',
		'Groups',
		'Id',
		'JobTitle',
		'LastName',
		'LastUpdatedBy',
		'MiddleName',
		'Nickname',
		'OwnerID',
		'Password',
		'Phone1',
		'Phone1Ext',
		'Phone1Type',
		'Phone2',
		'Phone2Ext',
		'Phone2Type',
		'Phone3',
		'Phone3Ext',
		'Phone3Type',
		'Phone4',
		'Phone4Ext',
		'Phone4Type',
		'Phone5',
		'Phone5Ext',
		'Phone5Type',
		'PostalCode',
		'PostalCode2',
		'PostalCode3',
		'ReferralCode',
		'SpouseName',
		'State',
		'State2',
		'State3',
		'StreetAddress1',
		'StreetAddress2',
		'Suffix',
		'Title',
		'Username',
		'Validated',
		'Website',
		'ZipFour1',
		'ZipFour2',
		'ZipFour3',
	);
		
	$query = array(
		'Id' => $contact_id,
	);
		
	$contacts = $app->dsQuery("Contact", $max_results, 0, $query, $returnFields);
	
	$result = '';
	
	// Error
	if(!is_array($contacts)){
		$result = print_r($contacts, true);
	}

	// No results
	else if(count($contacts) == 0){
		$result = "No results";
	}
	
	else {
		$result = $contacts[0];
	}
	
	return $result;
		
}



// Get invoices created on a certain day
function syncInvoices($time = '2015-01-01'){
	
	global $app;
	
	$invoices = array();
	
	$max_results = 1000;
	$max_iterations = 10; // Prevent endless loop if something goes wrong
	
	$returnFields = array(
		'ContactId',
		'CreditStatus',
		'DateCreated',
		'Description',
		'Id',
		'InvoiceTotal',
		'InvoiceType',
		'JobId',
		'Id',
		'PayPlanStatus',
		'PayStatus',
		'PromoCode',
		'TotalDue',
		'TotalPaid',
	);
	
	$datecreated = date('Y-m-d', strtotime($time));
	$query = array(
		'DateCreated' => $datecreated.'%',  // Run for a time period of a given hour
		'PayStatus' => 1,
		//'ContactId' => 63783,
	);
			
	$results = $app->dsQuery("Invoice", $max_results, $iterations, $query, $returnFields);
				
		foreach($results as $invoice) {
			
			$order = getOrder($invoice['JobId']);
			
			//try to catch romo Code
			if($order['JobTitle']){
				preg_match("/:: +([^\s]+)/", $order['JobTitle'], $matches);
				if(count($matches) && $matches[1] != '::'){
					$order['promo_code'] = $matches[1];
				}
			}
			
			$invoice['contact'] = getContact($invoice['ContactId']);
			$invoice['order'] = $order;
			$invoices[$invoice['Id']] = $invoice;
		}
		
return $invoices;
}


function createCustomer($my_order){
	//OK
	$db = new MongoClient();
	$customers = $db->lcart->customer;
	
	//Create Account if doesn't already Exit
	$account = $customers->findOne(array('email' => $my_order['customer']['email']));
	if(!$account){
		 
		$request = new AuthorizeNetCIM;
		//Create Customer Profile ID with Authorize
	   	 	$customerProfile = new AuthorizeNetCustomer;
			    $customerProfile->email = $my_order['customer']['email'];
					$response = $request->createCustomerProfile($customerProfile);
					    //CustomerProfileId
					    if($response->isOk()){
						    $customerProfileId = $response->getCustomerProfileId();
					    }else{
							$error = $response->getMessageText();						
							return $error . '<br>';
						 } 
		
		//Ok, create account!
			//Add to Database
				$customers->insert(array(
					'email' => $my_order['customer']['email'],
					'password' => $my_order['customer']['password'],
					'customerProfileId' => $customerProfileId,
				));
		
		return 'Created Customer!<br>';
		}
		
		return 'Found Customer<br>';
}

// Write invoices to db
function writeInvoices($invoices){
	
	$i = 1;
	$buffer = '';
	foreach($invoices as $invoice_id => $invoice) {
		
		$buffer .= "<br>Writing Invoice #$invoice_id";
		 
		$promo = $email = '';
		
		if(isset($invoice['contact']) && isset($invoice['order'])){
			
			$email = $invoice['contact']['Email'];
			
			if(isset($invoice['order']['promo_code'])) 
				$promo = $invoice['order']['promo_code'];
			
		 
			
			//create $my_order format
				$my_order = array();
				$my_order['customer']['email'] = $invoice['contact']['Email'];
				$my_order['customer']['password'] = $invoice['contact']['Password'];
				
				$my_order['customer']['form']['company'] = $invoice['contact']['Company'];
			    $my_order['customer']['form']['firstName'] = $invoice['contact']['FirstName'];
			    $my_order['customer']['form']['lastName'] = $invoice['contact']['LastName'];
			    
			    $my_order['customer']['form']['billingAddress1'] = $invoice['contact']['StreetAddress1'];
			    $my_order['customer']['form']['billingAddress2'] = $invoice['contact']['StreetAddress2'];
				$my_order['customer']['form']['billingCity'] = $invoice['contact']['City'];
				$my_order['customer']['form']['billingState'] = $invoice['contact']['State'];
			    $my_order['customer']['form']['billingZip'] = $invoice['contact']['PostalCode'];
			    
			    $my_order['customer']['form']['shippingAddress1'] = $invoice['contact']['Address2Street1'];
			    $my_order['customer']['form']['shippingAddress2'] = $invoice['contact']['Address2Street2'];
			    $my_order['customer']['form']['shippingCity'] = $invoice['contact']['City2'];
			    $my_order['customer']['form']['shippingState'] = $invoice['contact']['State2'];
			    $my_order['customer']['form']['shippingZip'] = $invoice['contact']['PostalCode2'];
			   
				$my_order['customer']['form']['apoAddress1'] = $invoice['contact']['Address3Street1'];
				$my_order['customer']['form']['apoCity'] = $invoice['contact']['City3'];
				$my_order['customer']['form']['apoState'] = $invoice['contact']['State3'];
			    $my_order['customer']['form']['apoZip'] = $invoice['contact']['PostalCode3'];
				
				$my_order['promo'] = $invoice['order']['promo_code'];
				$my_order['products'] = array('JobId' => $invoice['JobId']);
				$my_order['totals']['total'] = $invoice['InvoiceTotal'];
			
		 
			//Make Sure Customer Exits
				$buffer.= createCustomer($my_order);
			
			//Save into Database (Updates transaction & customer)
				$my_order = saveToDatabase($my_order);
			
			//mark as paid & fulfilled from infusionsoft
				stamp($my_order,'PAID','PAID VIA INFUSIONSOFT JobId#'.$invoice['JobId'],'orders.php');
				stamp($my_order,'SWIMS','DO NOT PROCESS - IMPORTED VIA INFUSIONSOFT','orders.php');
				stamp($my_order,'INFUSION','DO NOT PROCESS - IMPORTED VIA INFUSIONSOFT','orders.php');
		}

	}
	
	return $buffer;
}




?>

 

<pre>
<?php
	
	/*
	 *
	 * Once you've added the code to write to the mongodb in writeInvoices(), here's how to configure:
	 * 1. Set $earlier_date to the oldest order you want.
	 * 2. Set $later date to the most recent order you want.
	 * 3. Uncomment the line below  '// Get Invoices' so the api calls will actually be made.
	 *  
	 */
	$buffer = 'Online!<br>';
	
	$invoices = array();
	date_default_timezone_set('America/New_York');

	$earlier_date = '2014-01-01 00:00'; // This is as far back as we want to go
	$later_date = '2015-01-01 00:00'; // This is where we're starting from
	
	$iterator_date = trim(getLastRunDate());
	if(!$iterator_date){
		$iterator_date = $later_date;
	}
	
	// Do one day at a time
	for($i = 1; $i > 0; $i--){
	
		if(strtotime($iterator_date) >= strtotime($earlier_date)) {
			
		 
			$iterator_date = date("Y-m-d", strtotime("-1 day", strtotime($iterator_date)));
			
			// Get Invoices
			$invoices = syncInvoices($iterator_date);
			
			$buffer .= 'Found ' . count($invoices) . ' orders on ' . $iterator_date;
			
			$buffer .= writeInvoices($invoices);
			
			
			$buffer .= '<br><pre>' . print_r($invoices,true) . '</pre>';
		
		} else {
			 
			break;
		}
	
	}

	// Write to mongodb
	//
	
	// Log last synced time
	saveLastRunDate($iterator_date);

	
	sendEmail('log@lifeaidbevco.com','order-sync',$buffer);
	
?>

 Complete!
</pre>