
describe("Login controller", function() {
    var $controller, $rootScope, $scope, $httpBackend, $modalInstance;

    beforeEach(module('lcart')); //load module

    beforeEach(inject(function($injector) { //load services
        $controller = $injector.get('$controller');
        $rootScope = $injector.get('$rootScope');
        $httpBackend = $injector.get('$httpBackend');
    }));

    beforeEach(function() {
        $scope = $rootScope.$new();
        $modalInstance = {
            close: jasmine.createSpy('modalInstance.close'),
            dismiss: jasmine.createSpy('modalInstance.dismiss'),
            result: {then: jasmine.createSpy('modalInstance.result.then')}
        };
        $controller('login', {$scope: $scope, $modalInstance: $modalInstance}); //instantiate controller
    });

    afterEach(function() { //cleanup
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('try to login', function() { //test tryLogin()
        var dummyData;

        beforeEach(function() {
            dummyData = {email: 'test@test.com', password: 'test'}; //initial data 
            $httpBackend.expectPOST(/\/api\/login.php/).respond(function(method, url, data) { //listen for http request, succeed if input data matches initial data
                var params = parseUrl(url);
                return [200, includesObject(params, dummyData) ? {success: 'Success'} : {error: 'Error'}];
            });
        });

        describe('with valid credentials', function() { //test with properly-set data
            beforeEach(function() {
                populateObject($scope.login, dummyData);
            });
            it('has no error', function() {
                $scope.trylogin(); //code to test
                $httpBackend.flush();
                expect($scope.error.login).toBeNull(); //expected result
            });
            it('broadcasts a login event', function() {
                var listener = jasmine.createSpy();
                $rootScope.$on('login', listener); //listen for login event
                $scope.trylogin(); //code to test
                $httpBackend.flush();
                expect(listener).toHaveBeenCalled(); //expected result
            });
        });

        describe('with invalid credentials', function() { //test with no data
            it('sets an error', function() {;
                $scope.trylogin(); //code to test
                $httpBackend.flush();
                expect($scope.error.login).not.toBeNull(); //expected result
            });
        });
    });

    describe('create an account', function() { //test createAccount()
        var dummyData;

        beforeEach(function() {
            dummyData = {firstName: 'First', lastName: 'Last', email: 'test@test.com', password: 'test', password2: 'test'}; //initial data 
            $httpBackend.expectPOST(/\/api\/login.php/).respond(function(method, url, data) { //listen for http request, succeed if input data matches initial data
                var params = parseUrl(url);
                return [200, includesObject(params, dummyData) ? {success: 'Success'} : {error: 'Error'}];
            });
        });

        describe('with valid data', function() { //test with properly-set data
            beforeEach(function() {
                populateObject($scope.create, dummyData);
            });
            it('has no error', function() {
                $scope.createAccount(); //code to test
                $httpBackend.flush();
                expect($scope.error.create).toBeNull(); //expected result
            });
            it('broadcasts a login event', function() {
                var listener = jasmine.createSpy();
                $rootScope.$on('login', listener); //listen for login event
                $scope.createAccount(); //code to test
                $httpBackend.flush();
                expect(listener).toHaveBeenCalled(); //expected result
            });
        });

        describe('with invalid data', function() { //test with no data
            it('sets an error', function() {;
                $scope.createAccount(); //code to test
                $httpBackend.flush();
                expect($scope.error.create).not.toBeNull(); //expected result
            });
        });
    });

    describe('forgot password', function() { //test forgotPassword()
        var dummyEmail;

        beforeEach(function() {
            dummyEmail = 'test@test.com'; //initial data 
            $httpBackend.expectGET(/\/api\/forgotPassword.php/).respond(function(method, url, data) { //listen for http request, succeed if input data matches initial data
                var params = parseUrl(url);
                return [200, params.email == dummyEmail ? {success: 'Success'} : {error: 'Error'}];
            });
        });
        it('succeeds with valid email', function() { //test with properly-set data
            $scope.forgot.email = dummyEmail;
            $scope.forgotPassword(); //code to test
            $httpBackend.flush();
            expect($scope.success.forgot).not.toBeNull(); //expected results
            expect($scope.error.forgot).toBeNull();
        });
        it('fails with invalid email', function() { //test with no data
            $scope.forgotPassword(); //code to test
            $httpBackend.flush();
            expect($scope.success.forgot).toBeNull() //expected results
            expect($scope.error.forgot).not.toBeNull();
        });
    });
});