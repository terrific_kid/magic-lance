describe('Account directive', function() {
    var $compile, $rootScope, $scope, $httpBackend, $dirScope, element, data;

    beforeEach(module('lcart')); //load module

    beforeEach(inject(function($injector){ //load services
        $compile = $injector.get('$compile');
        $rootScope = $injector.get('$rootScope');
        $httpBackend = $injector.get('$httpBackend');
    }));

    beforeEach(function() {
        $httpBackend.whenGET(/\/templates\/.*\.html/).respond({}); //mock template request
        $httpBackend.expectPOST(/\/api\/myorders.php/).respond({}); //listen for orders request

        $scope = $rootScope.$new();
        element = $compile("<account customer='dummyCustomer'></account>")($scope); //instantiate directive

        $scope.dummyCustomer = {}; //set up scope data
        $scope.$digest(); //process directive
        $httpBackend.flush();
        $dirScope = element.isolateScope(); //get directive scope
    });

    afterEach(function() { //cleanup
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('clear responses', function() { //test clear()
        beforeEach(function() {
            $dirScope.success = 'TEST';
            $dirScope.error = 'TEST';
            $dirScope.clear(); //code to test
        });
        it('has no success reponse', function() {
            expect($dirScope.success).toBeNull(); //expected result
        });
        it('has no error reponse', function() {
            expect($dirScope.error).toBeNull(); //expected result
        });
    });

    describe('add a card', function() { //test addCard()
        var dummyError = 'ERROR', dummyCustomer = {email: 'EMAIL', password: 'PASS'};

        beforeEach(function() {
            $httpBackend.expectPOST(/\/api\/addCard.php/).respond(function(method, url, data) { //listen for http request, succeed if input data matches initial data
                data = JSON.parse(data);
                return [200, includesObject(data.login, dummyCustomer) ? {success: 'Success'} : {error: dummyError}];
            });
        });

        describe('with valid credentials', function() { //valid user
            beforeEach(function() {
                $httpBackend.expectPOST(/\/api\/login.php/).respond(function(method, url, data) { //listen for login request
                    return [200, {error: 'LoginError'}];
                });

                $dirScope.customer = dummyCustomer; //prep credentials
                $dirScope.addCard(); //code to test
                $httpBackend.flush();
            });
            it('stores the credentials', function() {
                expect(includesObject($dirScope.creds, dummyCustomer)).toBeTruthy(); //expected result
            });
        });

        describe('with invalid credentials', function() { //invalid user
            beforeEach(function() {
                $dirScope.addCard(); //code to test
                $httpBackend.flush();
            });
            it('sets an error', function() {
                expect($dirScope.error).toEqual(dummyError); //expected result
            });
        });
    });

    describe('delete a card', function() { //test deleteCard()
        var dummyError = 'ERROR', dummyCard = 'CARD', dummyCreds = {email: 'EMAIL', password: 'PASS'};

        beforeEach(function() {
            $httpBackend.expectPOST(/\/api\/deleteCard.php/).respond(function(method, url, data) { //listen for http request, succeed if input data matches initial data
                data = JSON.parse(data);
                return [200, includesObject(data.login, dummyCreds) && data.card.paymentProfileId == dummyCard ? {success: 'Success'} : {error: dummyError}];
            });

            $dirScope.customer = {}; //prep customer
        });

        describe('with valid credentials', function() { //valid user
            beforeEach(function() {
                $httpBackend.expectPOST(/\/api\/login.php/).respond(function(method, url, data) { //listen for login request
                    return [200, {error: 'LoginError'}];
                });

                $.extend($dirScope.customer, dummyCreds); //prep credentials
                $dirScope.deleteCard(dummyCard); //code to test
                $httpBackend.flush();
            });
            it('stores the credentials', function() {
                expect(includesObject($dirScope.creds, dummyCreds)).toBeTruthy(); //expected result
            });
        });

        describe('with invalid credentials', function() { //invalid user
            beforeEach(function() {
                $dirScope.deleteCard(); //code to test
                $httpBackend.flush();
            });
            it('sets an error', function() {
                expect($dirScope.error).toEqual(dummyError); //expected result
            });
        });
    });

    describe('save customer', function() { //test saveCustomer()
        var dummyError = 'ERROR', dummyCustomer = {form: {}}, dummyNewPassword = 'NEWPASS', dummyCreds = {email: 'EMAIL', password: 'PASS'};

        beforeEach(function() {
            $httpBackend.expectPOST(/\/api\/updateCustomer.php/).respond(function(method, url, data) { //listen for http request, succeed if input data matches initial data
                data = JSON.parse(data);
                return [200, includesObject(data.login, dummyCreds) ? {success: 'Success', reset: data.customer.form.newPassword == dummyNewPassword} : {error: dummyError}];
            });
        });

        describe('with valid credentials', function() { //valid user
            beforeEach(function() {
                $dirScope.customer = dummyCreds; //prep credentials
            });

            describe('with no new password', function() {
                beforeEach(function() {
                    $httpBackend.expectPOST(/\/api\/login.php/).respond(function(method, url, data) { //listen for login request
                        return [200, {error: 'LoginError'}];
                    });
                    $dirScope.saveCustomer(dummyCustomer); //code to test
                    $httpBackend.flush();
                });
                it('stores the credentials', function() {
                    expect(includesObject($dirScope.creds, dummyCreds)).toBeTruthy(); //expected result
                });
            });

            describe('with new password', function() {
                var listener; 

                beforeEach(function() {
                    listener = jasmine.createSpy();
                    $rootScope.$on('logout', listener); //listen for save event

                    dummyCustomer.form.newPassword = dummyNewPassword;
                    $dirScope.saveCustomer(dummyCustomer); //code to test
                    $httpBackend.flush();
                });
                it('broadcasts a logout event', function() {
                    expect(listener).toHaveBeenCalled(); //expected result
                });
            });
        });
    });

    describe('save an order', function() { //test saveOrder()
        var dummySuccess = 'SUCCESS', dummyError = 'ERROR', dummyOrder = 'ORDER', dummyCreds = {email: 'EMAIL', password: 'PASS'};

        beforeEach(function() {
            $httpBackend.expectPOST(/\/api\/updateOrder.php/).respond(function(method, url, data) { //listen for http request, succeed if input data matches initial data
                data = JSON.parse(data);
                return [200, includesObject(data.login, dummyCreds) && data.order == dummyOrder ? {success: dummySuccess} : {error: dummyError}];
            });
        });

        describe('with valid credentials', function() { //valid data
            beforeEach(function() {
                $httpBackend.expectPOST(/\/api\/myorders.php/).respond({}); //listen for orders request

                $dirScope.customer = dummyCreds; //prep credentials
                $dirScope.saveOrder(dummyOrder); //code to test
                $httpBackend.flush();
            });
            it('stores the credentials', function() {
                expect(includesObject($dirScope.creds, dummyCreds)).toBeTruthy(); //expected result
            });
            it('sets a success message', function() {
                expect($dirScope.success).toEqual(dummySuccess); //expected result
            });
        });

        describe('with invalid credentials', function() { //invalid data
            beforeEach(function() {
                $dirScope.saveOrder(); //code to test
                $httpBackend.flush();
            });
            it('sets an error', function() {
                expect($dirScope.error).toEqual(dummyError); //expected result
            });
        });
    });

    describe('delete an order', function() { //test deleteOrder()
        var dummySuccess = 'SUCCESS', dummyError = 'ERROR', dummyId = 'ID', dummyCreds = {email: 'EMAIL', password: 'PASS'};

        beforeEach(function() {
            $httpBackend.expectPOST(/\/api\/deleteOrder.php/).respond(function(method, url, data) { //listen for http request, succeed if input data matches initial data
                data = JSON.parse(data);
                return [200, includesObject(data.login, dummyCreds) && data.id == dummyId ? {success: dummySuccess} : {error: dummyError}];
            });
            spyOn(window, 'confirm').and.returnValue(true); //mock confirm dialog
        });

        describe('with valid credentials', function() { //valid data
            beforeEach(function() {
                $httpBackend.expectPOST(/\/api\/myorders.php/).respond({}); //listen for orders request

                $dirScope.customer = dummyCreds; //prep credentials
                $dirScope.deleteOrder(dummyId); //code to test
                $httpBackend.flush();
            });
            it('stores the credentials', function() {
                expect(includesObject($dirScope.creds, dummyCreds)).toBeTruthy(); //expected result
            });
            it('sets a success message', function() {
                expect($dirScope.success).toEqual(dummySuccess); //expected result
            });
        });

        describe('with invalid credentials', function() { //invalid data
            beforeEach(function() {
                $dirScope.deleteOrder(); //code to test
                $httpBackend.flush();
            });
            it('sets an error', function() {
                expect($dirScope.error).toEqual(dummyError); //expected result
            });
        });
    });

    describe('cancel a subscription', function() { //test cancel()
        var dummySuccess = 'SUCCESS', dummyError = 'ERROR', dummyOrder = 'ORDER', dummyCreds = {email: 'EMAIL', password: 'PASS'};

        beforeEach(function() {
            $httpBackend.expectPOST(/\/api\/cancelSubscription.php/).respond(function(method, url, data) { //listen for http request, succeed if input data matches initial data
                data = JSON.parse(data);
                return [200, includesObject(data.login, dummyCreds) && data.order == dummyOrder ? {success: dummySuccess} : {error: dummyError}];
            });
            spyOn(window, 'confirm').and.returnValue(true); //mock confirm dialog
        });

        describe('with valid credentials', function() { //valid data
            beforeEach(function() {
                $httpBackend.expectPOST(/\/api\/myorders.php/).respond({}); //listen for orders request

                $dirScope.customer = dummyCreds; //prep credentials
                $dirScope.cancel(dummyOrder); //code to test
                $httpBackend.flush();
            });
            it('stores the credentials', function() {
                expect(includesObject($dirScope.creds, dummyCreds)).toBeTruthy(); //expected result
            });
            it('sets a success message', function() {
                expect($dirScope.success).toEqual(dummySuccess); //expected result
            });
        });

        describe('with invalid credentials', function() { //invalid data
            beforeEach(function() {
                $dirScope.cancel(); //code to test
                $httpBackend.flush();
            });
            it('sets an error', function() {
                expect($dirScope.error).toEqual(dummyError); //expected result
            });
        });
    });
});