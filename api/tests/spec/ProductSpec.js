describe('Product directive', function() {
    var $compile, $rootScope, $scope, $httpBackend, $dirScope, element, data;

    beforeEach(module('lcart')); //load module

    beforeEach(inject(function($injector){ //load services
        $compile = $injector.get('$compile');
        $rootScope = $injector.get('$rootScope');
        $httpBackend = $injector.get('$httpBackend');
    }));

    beforeEach(function() {
        $httpBackend.whenGET(/\/templates\/.*\.html/).respond({}); //mock template request
        $scope = $rootScope.$new();
        element = $compile("<product object='dummyProduct'></product>")($scope); //instantiate directive
    });

    afterEach(function() { //cleanup
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('add an item', function() { //test addItem()
        var data;

        beforeEach(function() {
            data = {addToForm: {$valid: true}}; //input data
        });

        describe('of any kind', function() { //test normal items
            beforeEach(function() {
                $scope.dummyProduct = {groups: [], variations: {}}; //product data 
                $scope.$digest(); //process directive
                $httpBackend.flush();
                $dirScope = element.isolateScope(); //get directive scope
            });
            it('sets a positive quantity', function() {
                $dirScope.addItem(data); //code to test
                expect($dirScope.product.variations[$dirScope.key].qty).toBeGreaterThan(0); //expected result
            });
            it('broadcasts a save event', function() {
                var listener = jasmine.createSpy();
                $rootScope.$on('save', listener); //listen for save event
                $dirScope.addItem(data); //code to test
                expect(listener).toHaveBeenCalled(); //expected result
            });
        });

        describe('which is wholesale', function() { //test wholesale items
            beforeEach(function() {
                $scope.dummyProduct = {groups: ['wholesale','case'], variations: {}}; //product data 
                $scope.$digest(); //process directive
                $httpBackend.flush();
                $dirScope = element.isolateScope(); //get directive scope
            });
            it('sets the quantity to 2', function() {
                $dirScope.addItem(data); //code to test
                expect($dirScope.product.variations[$dirScope.key].qty).toEqual(2); //expected result
            });
        });
    });
});