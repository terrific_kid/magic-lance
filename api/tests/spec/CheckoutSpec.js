describe('Checkout directive', function() {
    var $compile, $rootScope, $scope, $httpBackend, $dirScope, $window, element, data, dummyCoupon;

    beforeEach(module('lcart')); //load module

    $window = {location: {}};
    beforeEach(module(function($provide) { //stub $window
        $provide.value('$window', $window);
    }));

    beforeEach(inject(function($injector){ //load services
        $compile = $injector.get('$compile');
        $rootScope = $injector.get('$rootScope');
        $httpBackend = $injector.get('$httpBackend');
    }));

    beforeEach(function() {
        $httpBackend.whenGET(/\/templates\/.*\.html/).respond({}); //mock template request
        $scope = $rootScope.$new();
        element = $compile("<checkout products='dummyProducts' totals='dummyTotals' customer='dummyCustomer' discount='dummyDiscount'></checkout>")($scope); //instantiate directive

        $scope.dummyProducts = {}; //set up scope data
        $scope.dummyTotals = {groupqty: {}};
        $scope.dummyCustomer = {form: {}};

        dummyCoupon = 'TEST'; //initial data;
        $httpBackend.expectPOST(/\/api\/applyCoupon.php/).respond(function(method, url, data) { //listen for http request, succeed if input data matches initial data
            data = JSON.parse(data);
            return [200, data['promo'] == dummyCoupon ? {products: true} : {}];
        });
    });

    afterEach(function() { //cleanup
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('initialize', function() { //test init()
        
        describe('with coupon', function() { //test valid coupon
            var listener;

            beforeEach(function() {
                listener = jasmine.createSpy();
                $rootScope.$on('applyCoupon', listener); //listen for event

                $scope.dummyDiscount = {promo: dummyCoupon};
                $scope.$digest(); //code to test (process directive)
                $httpBackend.flush();
                $dirScope = element.isolateScope(); //get directive scope
            });
           it('broadcasts an applyCoupon event', function() {
                expect(listener).toHaveBeenCalled(); //expected result
            });
        });

        describe('for standard shipping', function() { //test standard

            beforeEach(function() {
                $scope.$digest(); //code to test (process directive)
                $httpBackend.flush();
                $dirScope = element.isolateScope(); //get directive scope
            });
            it('is not flagged as ship to bill', function() {
                expect($dirScope.customer.form.shipToBill).toBeFalsy();
            });
            it('is not flagged as ship to apo', function() {
                expect($dirScope.customer.form.shipToApo).toBeFalsy();
            });
        });

        describe('for apo shipping', function() { //test apo

            beforeEach(function() {
                $scope.dummyTotals.groupqty.apo = true;
                $scope.$digest(); //code to test (process directive)
                $httpBackend.flush();
                $dirScope = element.isolateScope(); //get directive scope
            });
            it('is not flagged as ship to bill', function() {
                expect($dirScope.customer.form.shipToBill).toBeFalsy();
            });
            it('is flagged as ship to apo', function() {
                expect($dirScope.customer.form.shipToApo).toBeTruthy();
            });
        });
    });

    describe('check out', function() { //test checkout()
        var dummyError = 'ERROR', dummyUrl = 'URL', dummyProducts = {TESTSLUG: 'TESTPRODUCT'};

        beforeEach(function() {
            $scope.$digest(); //process directive
            $httpBackend.flush();
            $dirScope = element.isolateScope(); //get directive scope

            $httpBackend.expectPOST(/\/api\/checkout.php/).respond(function(method, url, data) { //listen for http request, succeed if input data matches initial data
                data = JSON.parse(data);
                return [200, includesObject(data.products, dummyProducts) ? {success: 'Success', url: dummyUrl} : {error: dummyError}];
            });
          
            $httpBackend.expectPOST(/\/api\/login.php/).respond(function(method, url, data) { //listen for login http request
                return [200, {error: 'Error'}];
            });

            $window.location = {}; //prep data
            $dirScope.checkoutForm = {$valid: true};
        });
        it('broadcasts a save event', function() {
            var listener = jasmine.createSpy();
            $rootScope.$on('save', listener); //listen for event

            $dirScope.checkout(); //code to test
            $httpBackend.flush();
            expect(listener).toHaveBeenCalled(); //expected result
        });

        describe('with valid products', function() { //test valid data
            beforeEach(function() {
                $dirScope.products = dummyProducts; //product data
                $dirScope.checkout(); //code to test
                $httpBackend.flush();
            });
            it('redirects to new url', function() {
                expect($window.location.href).toBe(dummyUrl); //expected result
            });
        });

        describe('with invalid products', function() { //test invalid data
            beforeEach(function() {
                $dirScope.checkout(); //code to test
                $httpBackend.flush();
            });
            it('sets an error', function() {
                expect($dirScope.error.checkout).toBe(dummyError); //expected result
            });
        });
    });
});