function parseUrl(url) {
    var idx = url.search(/\?/);
    if (idx < 0)
        return {};
    var obj = {}, params = url.substring(idx).replace('?', '').split('&');
    for(var i = 0; i < params.length; ++i) {
        split = params[i].split('=');
        if (split[0])
            obj[split[0]] = split[1];
    }
    return obj;
}

//essentially $.extend, but only for existing properties
function populateObject(obj1, obj2) {
    for (var key in obj1) {
        if (obj2.hasOwnProperty(key))
            obj1[key] = obj2[key];
    }
}

//returns true if the first object includes all of the second object's properties
function includesObject(obj1, obj2) {
    for (var key in obj2) {
        if (!obj1.hasOwnProperty(key) || obj1[key] != obj2[key])
            return false;
    }
    return true;
}