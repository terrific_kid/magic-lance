describe('Portal promos', function() {

    it('sorts promos', function() {
        promos_admin.promos = {
            promos: [{name: 'B'},{name: 'A'}]
        };
        promos_admin.sort_promos()
        expect(promos_admin.promos.promos).toEqual([{name: 'A'},{name: 'B'}]);
    });

    describe('check loaded', function() {
        it('adjusts progress bar', function() {
            spyOn($.fn, 'css');
            promos_admin.amt_loaded = 0.5
            promos_admin.checkLoaded();
            expect($.fn.css).toHaveBeenCalledWith('width', '50%');
        });
        it('initializes when fully loaded', function() {
            $.fn.extend({ //stub modal()
                modal: function() {}
            })
            promos_admin.amt_loaded = 1
            promos_admin.checkLoaded();
            expect(promos_admin.initialized).toBeTruthy();
        });
    });

    describe('nice dates', function() {
        it('converts valid dates', function() {
            expect(promos_admin.niceDate('1-2-3')).toBe('2/3/1');
        });
        it('ignores invalid dates', function() {
            expect(promos_admin.niceDate('1-2')).toBe('1-2');
        });
        it('handles empty dates', function() {
            expect(promos_admin.niceDate('')).toBe('Any');
        });
    });

    it('generates product options', function() {
        promos_admin.products = {
            ONE: {slug: 'SLUG1', sku: 'SKU1'},
            TWO: {slug: 'SLUG2', sku: 'SKU2'}
        };
        expect(promos_admin.product_opts()).toBe('<option value="ONE" >SLUG1 - SKU1</option>\n<option value="TWO" >SLUG2 - SKU2</option>\n');
    });
});