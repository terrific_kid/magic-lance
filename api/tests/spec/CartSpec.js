describe('Cart directive', function() {
    var $compile, $rootScope, $scope, $httpBackend, $dirScope, element, data;

    beforeEach(module('lcart')); //load module

    beforeEach(inject(function($injector){ //load services
        $compile = $injector.get('$compile');
        $rootScope = $injector.get('$rootScope');
        $httpBackend = $injector.get('$httpBackend');
    }));

    beforeEach(function() {
        $httpBackend.whenGET(/\/templates\/.*\.html/).respond({}); //mock template request
        $scope = $rootScope.$new();
        element = $compile("<cart products='dummyProducts'></cart>")($scope); //instantiate directive
    });

    afterEach(function() { //cleanup
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('remove an item', function() { //test remove()
        var product;

        beforeEach(function() {
            product = {groups: [], variations:{dummyKey: {}}}; //product data
            $scope.$digest(); //process directive
            $httpBackend.flush();
            $dirScope = element.isolateScope(); //get directive scope
        });
        it('removes the quantity', function() {
            $dirScope.remove(product, 'dummyKey'); //code to test
            expect(product.variations['dummyKey']).toBeUndefined(); //expected result
        });
        it('broadcasts a save event', function() {
            var listener = jasmine.createSpy();
            $rootScope.$on('save', listener); //listen for save event
            $dirScope.remove(product, 'dummyKey'); //code to test
            expect(listener).toHaveBeenCalled(); //expected result
        });
    });

    describe('update a quantity', function() { //test updateQty()
        var product, listener;

        beforeEach(function() {
            product = {groups: [], variations: {}};
            $scope.$digest(); //process directive
            $httpBackend.flush();
            $dirScope = element.isolateScope(); //get directive scope

            listener = jasmine.createSpy();
            $rootScope.$on('save', listener); //listen for save event
        });

        describe('of any kind', function() { //test normal items
            it('broadcasts a save event', function() {
                $dirScope.updateQty(product, 1); //code to test
                expect(listener).toHaveBeenCalled(); //expected result
            });
        });

        describe('which is wholesale', function() { //test wholesale items
            beforeEach(function() {
                product.groups = ['wholesale','case']; //product data 
            });
            describe('to an even number', function() { //test even quantities
                it('broadcasts a save event', function() {
                    $dirScope.updateQty(product, 2); //code to test
                    expect(listener).toHaveBeenCalled(); //expected result
                });
            });
            describe('to an odd number', function() { //test odd quantities
                it('does not broadcast a save event', function() {
                    $dirScope.updateQty(product, 1); //code to test
                    expect(listener).not.toHaveBeenCalled(); //expected result
                });
            });
        });
    });

    describe('apply a coupon', function() { //test applyCoupon()
        var dummyCoupon, listener;

        beforeEach(function() {
            $scope.$digest(); //process directive
            $httpBackend.flush();
            $dirScope = element.isolateScope(); //get directive scope

            dummyCoupon = 'TEST'; //initial data
            $httpBackend.expectPOST(/\/api\/applyCoupon.php/).respond(function(method, url, data) { //listen for http request, succeed if input data matches initial data
                data = JSON.parse(data);
                return [200, data['promo'] == dummyCoupon ? {products: true} : {}];
            });
        
            listener = jasmine.createSpy();
            $rootScope.$on('applyCoupon', listener); //listen for event
        });

        describe('which is valid', function() { //test valid coupon
            it('broadcasts an applyCoupon event', function() {
                $dirScope.promo = dummyCoupon;
                $dirScope.applyCoupon(); //code to test
                $httpBackend.flush();
                expect(listener).toHaveBeenCalled(); //expected result
            });
        });
        describe('which is invalid', function() { //test invalid coupon
            it('does not broadcast an applyCoupon event', function() {
                $dirScope.applyCoupon(); //code to test
                $httpBackend.flush();
                expect(listener).not.toHaveBeenCalled(); //expected result
            });
        });
    });

    describe('remove the coupon', function() { //test removeCoupon()
        beforeEach(function() {
            $scope.$digest(); //process directive
            $httpBackend.flush();
            $dirScope = element.isolateScope(); //get directive scope

            $httpBackend.expectPOST(/\/api\/applyCoupon.php/).respond(function(method, url, data) { //listen for http request
                data = JSON.parse(data);
                return [200, {}];
            });
        
            listener = jasmine.createSpy();
            $rootScope.$on('applyCoupon', listener); //listen for event

            $dirScope.promo = 'TEST';
            $dirScope.removeCoupon(); //code to test
            $httpBackend.flush();
        });     

        it('does not broadcast an applyCoupon event', function() {
            expect(listener).not.toHaveBeenCalled(); //expected result
        })
        it('removes any saved promotion code', function() {
            expect($dirScope.promo).toBeNull(); //expected result
        });
    });
});