<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Portal Promo Spec Runner</title>

  <link rel="shortcut icon" type="image/png" href="lib/jasmine-2.2.0/jasmine_favicon.png">
  <link rel="stylesheet" href="lib/jasmine-2.2.0/jasmine.css">

  <script src="lib/jasmine-2.2.0/jasmine.js"></script>
  <script src="lib/jasmine-2.2.0/jasmine-html.js"></script>
  <script src="lib/jasmine-2.2.0/boot.js"></script>

  <!-- jQuery -->
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

  <!-- include source files here... -->
  <script src="/api/discounter/frontend/app.js"></script>

  <!-- include spec files here... -->
  <script src="spec/PortalPromoSpec.js"></script>

</head>

<body>
</body>
</html>
