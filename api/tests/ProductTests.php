<?php
class ProductTest extends PHPUnit_Framework_TestCase
{

	/**
	 *
	 * Test valid products
	 *
	 *
	 */
	public function testProducts()
	{
		$json = file_get_contents('../api/products.json');
		$ob = json_decode($json);
		
		// $ob is null because the json cannot be decoded
		$this->assertNotNull($ob);
		
		$slugs = array();
		
		// Check individual products
		foreach($ob as $key => $product)
		{
			$this->assertObjectHasAttribute('slug', $product, '!product missing slug' . print_r($product, true));

			$this->assertObjectHasAttribute('sku', $product, '!product missing sku' . print_r($product, true));

			// Slugs must be unique
			$this->assertNotContains($product->slug, $slugs, '!duplicate slug for ' . $product->slug);
			
			// Slugs should match keys
			$this->assertEquals($key, $product->slug, "!product $key has slug " . $product->slug);

			$slugs[] = $product->slug;
			
		}
		
	}
	
	
	
}

?>
