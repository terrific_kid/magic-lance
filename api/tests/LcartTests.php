<?php
    require_once dirname(dirname(__DIR__)) . '/config/global.php';
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Lcart Spec Runner</title>

  <link rel="shortcut icon" type="image/png" href="lib/jasmine-2.2.0/jasmine_favicon.png">
  <link rel="stylesheet" href="lib/jasmine-2.2.0/jasmine.css">

  <script src="lib/jasmine-2.2.0/jasmine.js"></script>
  <script src="lib/jasmine-2.2.0/jasmine-html.js"></script>
  <script src="lib/jasmine-2.2.0/boot.js"></script>

  <!-- AngularJS -->
  <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.js"></script>
  <script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js"></script>
  <script src="//crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/md5.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular-mocks.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

  <!-- include source files here... -->
  <script src="/lcart.js"></script>

  <!-- include spec files here... -->
  <script src="spec/SpecHelper.js"></script>
  <script src="spec/LoginSpec.js"></script>
  <script src="spec/ProductSpec.js"></script>
  <script src="spec/CartSpec.js"></script>
  <script src="spec/CheckoutSpec.js"></script>
  <script src="spec/AccountSpec.js"></script>

</head>

<body>
</body>
</html>
