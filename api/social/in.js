(
	// Isdfnstagram Follow Button Javascript Module
	// www.instagramfollowbutton.com
	// Developer: Stephen (Augie) Gardner
	// TODO: multilingual support
	function(){
		/**
		 * Application wide default parameters.  Take care when updating
		 */
		var app_defaults = {
			button : {
				container : {
					class : "ig-follow"
				},
				image : {
					class : "ig-camera"
				},
				background : {
					class : "IGF"
				},
				text : {
					class : "ig-text"
				},
				states : {
					default : {
						text : "Follow" // initially sent by server
					},
					done : {
						text : "Following",
						class : "followed"
					}
				}
			},

			template : {
				button : {
					url : 'https://instagramfollowbutton.com/components/instagram/v2/followButton2.php'
				},
				css : {
					url : "https://instagramfollowbutton.com/css/igFollow.css"
				}
			},

			images : {
				default : {
					url : "https://instagramfollowbutton.com/components/instagram/v2/img/ig-camera.png"
				},
				loading : {
					url : "https://instagramfollowbutton.com/components/instagram/v2/img/loading.gif"
				},
				error : {
					url : "https://instagramfollowbutton.com/components/instagram/v2/img/ig-camera.png"
				}
			}
		};
		/**
		 * An instance of an Instagram Follow Button, it's HTML element, and associated properties/events
		 * @param element (an HTML Node)
		 * @constructor
		 */
		var IGFButton = function(element){
			var IGFButton = {
				user : {
					id : false,
					name : false
				},

				data : {},

				init : function(element) {
					this.element = element;
					this.setData();
					this.data.state = "ready";
				},

				/**
				 * Set the data of this button object after initializing
				 */
				setData : function() {
					var data = {
						id : this.element.getAttribute("data-id"),
						ignore : this.element.getAttribute("data-ignore") ? this.element.getAttribute("data-ignore") : false,
						followed : this.element.getAttribute("data-followed") ? this.element.getAttribute("data-followed") : false,
						size : this.element.getAttribute("data-size") ? this.element.getAttribute("data-size") : "small",
						username : this.element.getAttribute("data-username") ? this.element.getAttribute("data-username") : "false",
						showCount : this.element.getAttribute("data-count") ? this.element.getAttribute("data-count") : "false"
					};
					this.data = data;
				},

				/**
				 * Return the parameters of the instagram follow button
				 * @returns {{id: (*|string|Number), size: (*|string|Number), username: (*|string|Number), showCount: (*|string|Number)}}
				 */
				params : function() {
					return {
						id : this.element.getAttribute("data-id"),
						size : this.element.getAttribute("data-size") ? this.element.getAttribute("data-size") : "small",
						username : this.element.getAttribute("data-username") ? this.element.getAttribute("data-username") : "false",
						showCount : this.element.getAttribute("data-count") ? this.element.getAttribute("data-count") : "false"
					}
				},

				click : function() {
					var followed = this.data.followed;
					if(!followed && this.data.state != "loading" && this.data.state != "error") {
						this.loading();
						var extraState = this.user.id + "|" + this.user.name + "|" + this.data.id + "|" + this.referrer;
						var url = "httpss://instagram.com/oauth/authorize/?client_id=" + this.clientID + "&redirect_uri=" + this.redirectURI + "&state=" + extraState + "&response_type=code&scope=relationships";
						IGFHandler.popup = window.open(url,"mywindow","menubar=1,resizable=1,width=500,height=400");
					}
				},

				bindEvents : function() {
					var self = this;
					this.element.onclick = function() {
						self.click();
					};
				},

				/**
				 * Dirty recursive implementation of finding an element by class name without an external library
				 * @author Augie Gardner www.github.com/stephengardner
				 * @param element
				 * @param className
				 * @returns {*}
				 */
				findClass : function(element, className) {
					var foundElement = null, found;
					function recurse(element, className, found) {
						for (var i = 0; i < element.childNodes.length && !found; i++) {
							var el = element.childNodes[i];
							var classes = el.className != undefined? el.className.split(" ") : [];
							for (var j = 0, jl = classes.length; j < jl; j++) {
								if (classes[j] == className) {
									found = true;
									foundElement = element.childNodes[i];
									break;
								}
							}
							if(found)
								break;
							recurse(element.childNodes[i], className, found);
						}
					}
					recurse(element, className, false);
					return foundElement;
				},

				/**
				 * Instantiate the button, ask host server for template and execute the jsonp which will create the button
				 */
				process : function() {
					var self = this;
					$jsonp.fetch(app_defaults.template.button.url, self.params(),
						function(json) {
							self.element.innerHTML = json.response;
							self.image = self.findClass(self.element, app_defaults.button.image.class);
							self.text = self.findClass(self.element, app_defaults.button.text.class);
							self.button = self.findClass(self.element, app_defaults.button.background.class);
							self.user.name = json.user.name;
							self.user.id = json.user.id;
							self.clientID = json.client_id;
							self.redirectURI = json.redirect_uri;
							self.referrer = json.referrer;
							self.bindEvents();
						});
				},

				/**
				 * Callback after follow button has been successfully followed
				 */
				followed : function() {
					if(this.data.username == "true")
						this.text.innerHTML = app_defaults.button.states.done.text + " @" + this.user.name;
					else {
						this.text.innerHTML = app_defaults.button.states.done.text;
					}
					this.button.className += " " + app_defaults.button.states.done.class;
					this.data.followed = true;
					this.done();
				},

				/**
				 * Process loading of follow button action
				 */
				loading : function() {
					this.data.state = "loading";
					this.image.setAttribute("src", app_defaults.images.loading.url);
				},

				/**
				 * Process finishing of follow button action
				 */
				done : function() {
					this.data.state = "done";
					this.image.setAttribute("src", app_defaults.images.default.url);
				},

				/**
				 * Process error for follow button
				 */
				error : function() {
					this.data.state = "error";
					this.text.innerHTML = "Error";
					this.image.setAttribute("src", app_defaults.images.error.url);
				},

				/**
				 * If follow button action failed
				 * @param message
				 * @param meta
				 */
				failed : function(message, meta) {
					this.done();
					IGFHandler.popup.close();
				}
			};
			IGFButton.init(element);
			return IGFButton;
		};

		/**
		 * Handler for Instagram Follow Button buttons
		 * @constructor
		 */
		function IGFHandler() {
			this.buttons = [];
			this.init();
		};

		IGFHandler.prototype = {
			/**
			 * Initialize the Handler
			 */
			init : function() {
				var allFollowButtons = document.querySelectorAll("." + app_defaults.button.container.class);
				for(var i = 0; i < allFollowButtons.length; i++) {
					var ignore = allFollowButtons[i].getAttribute("data-ignore") || !allFollowButtons[i].getAttribute("data-id") || "false";
					if(ignore == "false") {
						this.buttons.push(new IGFButton(allFollowButtons[i]));
					}
				}
				this.getStylesheet();
				this.populateButtons();
				this.bindPostMessage();
			},

			clickButton : function(userID) {
				for(var i = 0; i < allFollowButtons.length; i++) {
					if(allFollowButtons[i].user.id == userID) {
						allFollowButtons[i].click();
					}
				}
			},

			/**
			 * Process actions when the instagram window sends a message back to the child window
			 * @param message
			 */
			receiveMessage : function(message) {
				var obj;
				if(message.data) {
					try {
						obj = JSON.parse(message.data);
					} catch(e) {
						obj = false;
					}
					if(obj && obj.action && obj.action == "followed" && obj.hash) {
						this.processFollowed(obj.hash);
						window.setTimeout(function(){
							this.popup.close();
						}.bind(this), 1000);
					}
					else if(obj && obj.action){ // when the action fails
						for(var i = 0; i < this.buttons.length; i++) {
							this.buttons[i].done();
						}
					}
				}
			},

			/**
			 * Set up the Handler to perform actions on certain window messages
			 */
			bindPostMessage : function() {
				if(window.addEventListener) {
					window.addEventListener('message', function(e){
						if(e.data)
							IGFHandler.receiveMessage(e);
					});
				}
				else if(window.attachEvent) {
					window.attachEvent("onmessage", function(e){
						if(e.data)
							IGFHandler.receiveMessage(e);
					});
				}
			},

			/**
			 * Reflect that an instagram user was followed by altering the button for all identical instances of this button
			 * @param userID
			 */
			processFollowed : function(userID) {
				for(var i = 0; i < this.buttons.length; i++){
					if(this.buttons[i].data.id == userID) {
						this.buttons[i].followed();
					}
				}
			},

			/**
			 * Retrieve the remote stylesheet for the instagram follow button
			 */
			getStylesheet : function() {
				var ss = document.createElement("link");
				ss.type = "text/css";
				ss.rel = "stylesheet";
				ss.href = app_defaults.template.css.url;
				document.getElementsByTagName("head")[0].appendChild(ss);
			},

			/**
			 * Instantiate all buttons on the page
			 */
			populateButtons : function(){
				for(var i = 0; i < this.buttons.length; i++) {
					this.buttons[i].process();
				}
			}
		};

		// Vanilla Javascript JSONP function, allows for multiple jsonp executions in parallel
		// default callback parameter sent in url as jsonp_callback
		var $jsonp = {
			callbackCounter: 0,

			fetch: function(url, data, callback) {
				var fn = 'jsonp_callback' + this.callbackCounter++, params = [], param_name="";
				window[fn] = this.evalJSONP(callback);
				url += "?jsonp_callback=" + fn;
				for(param_name in data) {
					params.push(param_name + "=" + encodeURIComponent(data[param_name]));
				}
				url += (url.indexOf("?")+1 ? "&" : "?");
				url += params.join("&");
				var scriptTag = document.createElement('SCRIPT');
				scriptTag.src = url;
				document.getElementsByTagName('HEAD')[0].appendChild(scriptTag);
			},

			evalJSONP: function(callback) {
				return function(data) {
					var validJSON = false;
					if (typeof data == "string") {
						try {validJSON = JSON.parse(data);} catch (e) {}
					} else {
						// if object
						validJSON = JSON.parse(JSON.stringify(data));
					}
					if (validJSON) {
						callback(validJSON);
					} else {
						throw("JSONP call returned invalid or empty JSON");
					}
				}
			}
		}
		var IGFHandler = new IGFHandler();
	}());
