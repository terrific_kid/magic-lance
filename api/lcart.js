////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

//Get the environment
var script_tag = document.getElementById('lifeaid-lcart')
var api_host = 'api.lifeaidbevco.com';
if(script_tag && script_tag.getAttribute("data-env") == 'dev') api_host = 'apidev.lifeaidbevco.com';

//Create Angular App
	var lcart = angular.module('lcart',['ui.bootstrap']).constant('version','3.3.1');
	
 

////////////////////////////////////////////////////////////////////////////////////
//ldata Factory
	lcart.factory('ldata', ['$http', function($http, version) {
		var factory = {};
		 	
		 	//Delete Order
		 	factory.deleteOrder= function(data){
			 	var request = $http({
					                method: "POST",
					                url: "//"+api_host+"/api/deleteOrder.php",
					                data: {
						                login: data.login,
					                	id: data.id,
					               }
	              });
			return request;
		 	}
		 	
		 	//Delete Card
		 	factory.deleteCard = function(data){
			 	var request = $http({
					                method: "POST",
					                url: "//"+api_host+"/api/deleteCard.php",
					                data: {
						                login: data.login,
					                	card: data.card,
					               }
	              });
			return request;
		 	}
		 	
		 	//Add Card
		 	factory.addCard = function(data){
			 	var request = $http({
					                method: "POST",
					                url: "//"+api_host+"/api/addCard.php",
					                data: {
						                login: data.login,
					                	card: data.card,
					               }
	              });
			return request;
		 	}
		 	
		 	//Flags
		 	factory.getFlags = function (){
			 	flags = {};
			 	if(angular.fromJson(localStorage[version+"_flags"])) flags = angular.fromJson(localStorage[version+"_flags"]);
			 	return flags;
		 	}
		 	
		 	factory.setFlags = function(flags){
			 	localStorage[version+"_flags"] = angular.toJson(flags);
		 	}
		 	
			//Products
			factory.getProductData = function () {
			  return $http.get('//'+api_host+'/api/getProducts.php');
			};
			
			
			
			//ApplyCoupon Function
			factory.applyCoupon = function(data){
			var request = $http({
					                method: "POST",
					                url: "//"+api_host+"/api/applyCoupon.php",
					                data: {
					                    promo: data.promo,
					                    products: data.products,
					                    mode: data.mode,
					                }
	              });  
	 			
			return request;
			};
			
			//Forgot Password
			factory.forgotPassword = function (forgot) {
			    return $http.get('//'+api_host+'/api/forgotPassword.php?email=' + forgot.email);
			};
			
			
			//Login Function
			factory.login = function(creds){
	 			var request = $http({
					                method: "post",
					                url: "//"+api_host+"/api/login.php",
					                params: {
						                firstName: creds.firstName,
						                lastName: creds.lastName,
					                    email: creds.email,
					                    password: creds.password,
					                    password2: creds.password2,
					                    create: creds.create
					                }
	              });
			return request;
			};
			
			
			
			//Checkout Function
			factory.checkout = function(data){
	 			var request = $http({
					                method: "POST",
					                url: "//"+api_host+"/api/checkout.php",
					                data: {
					                	promo: data.promo,
					                	login: data.login,
					                    customer: data.customer,
					                    form: data.form,
					                    discount: data.discount,
					                    products: data.products,
					                }
	              });
			return request;
			};
			
			
			//Load Orders Function
			factory.getMyOrders = function(data){
	 			var request = $http({
					                method: "POST",
					                url: "//"+api_host+"/api/myorders.php",
					                data: {
					                	login: data.login,
					               }
	              });
			return request;
			};
			
			
			//Edit Order Function
			factory.updateOrder = function(data){
	 			var request = $http({
					                method: "POST",
					                url: "//"+api_host+"/api/updateOrder.php",
					                data: {
						                login: data.login,
					                	order: data.order,
					               }
	              });
			return request;
			};
			
			//Edit Customer Function
			factory.updateCustomer = function(data){
	 			var request = $http({
					                method: "POST",
					                url: "//"+api_host+"/api/updateCustomer.php",
					                data: {
						                login: data.login,
					                	customer: data.customer,
					               }
	              });
			return request;
			};



			//Cancel Subscription
			factory.cancel = function(data){
	 			var request = $http({
					                method: "POST",
					                url: "//"+api_host+"/api/cancelSubscription.php",
					                data: {
						                login: data.login,
					                	order: data.order,
					                	
					               }
	              });
			return request;
			};


			

	return factory;
	}]);





////////////////////////////////////////////////////////////////////////////////////
//Button 
lcart.directive('lbutton', function($sce) {
  return {
  	scope: {
	  	trigger: '=',
	  	click: '&',
	},
	 templateUrl: function(tElement, tAttrs) {
            return $sce.trustAsResourceUrl('//'+api_host+'/templates/' + tAttrs.template + '.html');
			},
	transclude: true,
  	restrict: 'AE',
  	link: function($scope, $elem, $attrs) {
    
    	$scope.run = function(){
    		
	    	$scope.trigger = true;
	    	$scope.click();
	    	
	    	
    	}
	   
	}
  };
});



////////////////////////////////////////////////////////////////////////////////////
//Slideout Menu Logic
lcart.directive('menu', function() {
  return {
    scope: false,
  	restrict: 'AE',
    link: function($scope, $elem, $attrs) {
       $scope.one = true;
       $scope.two = true;
       $scope.three = true;
       $scope.four = true;
       $scope.five = true;
       $scope.six = true;
       
       $scope.slideout = function(menu){
       	   $scope.flag = false;
		   if(!$scope[menu]) $scope.flag = true;
		       $scope.resetMenu();
		       $scope[menu] = false;	
		   if($scope.flag) $scope[menu] = true;
	   }
	   $scope.resetMenu = function(){
		   $scope.one = true;
		   $scope.two = true;
		   $scope.three = true;
		   $scope.four = true;
		   $scope.five = true;
		   $scope.six = true;
	   }
    }
  };
});

 

////////////////////////////////////////////////////////////////////////////////////
//Login Prompt Controller
	lcart.controller('login', function($scope, $rootScope, $modalInstance, ldata){
		$scope.panel1 = true;
		$scope.panel2 = false;
		$scope.panel3 = false;
		$scope.buttons = {
			login: false,
			create: false,
			forgot: false,
		}
		$scope.success = {
			forgot: null,
		}
		$scope.error = {
			login: null,
			create: null,
			forgot: null,
		}
		$scope.login = {
			email: null,
			password: null,
		} 
		$scope.create = {
			firstName: null,
			lastName: null,
			email: null,
			password: null,
			password2: null,
			create: 1,
		} 
		$scope.forgot = {
			email: null,
		}
		$scope.ok = function(){
			  $modalInstance.close();
		}
		$scope.showLogin = function(){
			$scope.panel1 = true;
			$scope.panel2 = false;
			$scope.panel3 = false;
		}
		$scope.showCreate = function(){
			$scope.panel1 = false;
			$scope.panel2 = true;
			$scope.panel3 = false;
		}
		$scope.showPassword = function(){
			$scope.panel1 = false;
			$scope.panel2 = false;
			$scope.panel3 = true;
		}
		$scope.trylogin = function(){
				ldata.login($scope.login).success(function (data) {
				 	$scope.buttons.login = false;
					if(data.success){
						 $rootScope.$broadcast('login', data);
						$scope.ok();
					}else{
						$scope.error.login = data.error;
					} 
				});
		}
		$scope.createAccount = function(){
			ldata.login($scope.create).success(function (data) {
				$scope.buttons.create = false;
				if(!data.error){
					$rootScope.$broadcast('login', data);
					$scope.ok();
				}else{
					$scope.error.create = data.error;
				} 
			});
		}
		$scope.forgotPassword = function(){
			ldata.forgotPassword($scope.forgot).success(function (data) {
				 	$scope.buttons.forgot = false;
				 	if(!data.error){
					 	$scope.success.forgot = data.success;
					 	$scope.error.forgot = null;
					}else{
						$scope.success.forgot = null;
						$scope.error.forgot = data.error;
					} 
				});
		}
});

////////////////////////////////////////////////////////////////////////////////////
//Generic Modal Controller
	lcart.controller('modal', function($scope, ldata, $modalInstance){
		$scope.flags = ldata.getFlags();
		
		$scope.ok = function(){
			ldata.setFlags($scope.flags);
			$modalInstance.close();
		}

});


///Special Offer
lcart.controller('special', function($scope, $rootScope, $window, ldata) {
	$scope.form = {};
	
	$scope.applyCoupon = function(){
		
		$scope.cbtn = true;
	
		$scope.payload = {
			promo: $scope.form.promo,
			products: $scope.system.products,
			mode: false,
		}
		ldata.applyCoupon($scope.payload).success(function (data) {
			$scope.promo = null;
			if(data.products){
				$rootScope.$broadcast('applyCoupon',data);
			}
		 	$scope.cbtn = false;
		 	$window.location.href = '/affiliates-start';
				 	
		});
	}
});

///Special Offer
lcart.controller('2freecans', function($scope, $rootScope, $window, ldata) {
	$scope.form = {};
	
	$scope.freecans = function(){
		$scope.cbtn = true;
	
		$scope.payload = {
			promo: '2FREECANS',
			products: $scope.system.products,
			mode: false,
		}
		ldata.applyCoupon($scope.payload).success(function (data) {
			$scope.promo = null;
			 
			if(data.products){
				$rootScope.$broadcast('applyCoupon',data);
			}
		 	$scope.cbtn = false;
		 	$window.location.href = '/cart';
				 	
		});
	}
});


////////////////////////////////////////////////////////////////////////////////////
//Main Core
	lcart.controller('core', function(version, $scope, $sce, $rootScope, $modal, $timeout, $window, ldata) {
			//System Functions	
			$scope.version = version;
			
			console.log(version);
			
			$scope.system = {
				totals: null,
				discount: angular.fromJson(localStorage[$scope.version+"_discount"]),
				customer: angular.fromJson(localStorage[$scope.version+"_customer"]),
				products: angular.fromJson(localStorage[$scope.version+"_products"]),
				flags: ldata.getFlags(),
				init: function(){
				
				 
					if($scope.system.products == null ){
						ldata.getProductData().success(function (data) {
							$scope.system.saveProducts(data);
						});
					}
					$scope.system.total();
				
					
				},
				saveDiscount: function(data){
					if(data) $scope.system.discount = data
					localStorage[$scope.version+"_discount"] = angular.toJson($scope.system.discount);
				},
				saveProducts: function(data){
					if(data) $scope.system.products = data
					localStorage[$scope.version+"_products"] = angular.toJson($scope.system.products);
				},
				saveCustomer: function(data){
					if(data) $scope.system.customer = data;
					if($scope.system.customer)localStorage[$scope.version+"_customer"] = angular.toJson($scope.system.customer);
				},
				resetPromo: function(code){
					$scope.payload = {
						promo: code,
						products: $scope.system.products,
					}
					ldata.applyCoupon($scope.payload).success(function (data) {
					 $rootScope.$broadcast('applyCoupon',data);
					});
				},
				clearCart: function(){
					 angular.forEach($scope.system.products, function(product, slug) {
							product.variations = {};
						});
				},
				total: function(){
				 	$scope.remove = false;
				 	$scope.qty_cases = 0;
					$scope.results = {
						subtotal: 0,
						subtotal_discount: 0,
						items: 0,
						shipping: 0,
						shipping_discount: null,
						weight:0,
						tax: 0,
						off: 0,
						total: 0,
						discount: $scope.system.discount,
						groupqty: {},
					};
					
					//Remove Used Codes
						if($scope.system.customer && $scope.results.discount){
							angular.forEach($scope.system.customer.usedCodes, function(key, code) {
								if(angular.uppercase(code) == $scope.results.discount.promo) $scope.system.resetPromo();
							});
						}
						
						
					//Set Terms	    	
					if($scope.results.discount)$scope.results.discount.safeTerms = $sce.trustAsHtml($scope.results.discount.terms);
			    
					//For all Products
					
					angular.forEach($scope.system.products, function(product, slug) {
					$scope.productqty = 0;
						
						angular.forEach(product.variations, function(variation, sku) {
							if(variation.qty){
								//Items in Cart
								$scope.results.items = ($scope.results.items + variation.qty);
								//Product Qty
								$scope.productqty = ($scope.productqty + variation.qty);
								//Subtotal Normal
								$scope.results.subtotal = $scope.results.subtotal + (variation.qty * product.price);
								//Subtotal Discount
								$scope.results.subtotal_discount = $scope.results.subtotal_discount + (variation.qty * product.discount);
								//Shipping
								$scope.results.shipping = $scope.results.shipping + (variation.qty * product.shipping);
								$scope.results.weight = $scope.results.weight + (variation.qty * product.weight);
								
								if(product.groups && product.groups.indexOf('case') > -1){
									$scope.qty_cases += variation.qty;
								}
							}
						});
						//Tally Global Group Qty
						angular.forEach(product.groups, function(group) {
							if(!$scope.results.groupqty[group]) $scope.results.groupqty[group] = 0;
							$scope.results.groupqty[group] = $scope.results.groupqty[group] + $scope.productqty;
						});
						 
					});
					
					//Weight Shipping
					add_weight = $scope.results.weight * 5;
					if(add_weight > 20) add_weight = 20;
					$scope.results.shipping =  $scope.results.shipping + add_weight;
					
					// Free shipping for 20+ cases
					if($scope.qty_cases >= 20) $scope.results.shipping = 0;
							
							
					//Discounts
						//fix shiping_discount
						$scope.results.shipping_discount = $scope.results.shipping;
						if($scope.system.discount){
							//Set Flat Rate off Shipping
							if($scope.results.discount.cart_modifiers.shipping){
								switch($scope.results.discount.cart_modifiers.shipping.op){
									case '=':
										$scope.results.shipping_discount = $scope.results.discount.cart_modifiers.shipping.amt;
									break;
								}
							}
							//Set Flat Rate off Subtotal
							if($scope.results.discount.cart_modifiers.subtotal){
								switch($scope.results.discount.cart_modifiers.subtotal.op){
									case '-':
										$scope.results.subtotal_discount -= $scope.results.discount.cart_modifiers.subtotal.amt;
									break;
									case '*':
										$scope.results.subtotal_discount *= $scope.results.discount.cart_modifiers.subtotal.amt;
									break;
								}
							}
						 }
						//Shipping Must always be >= 0
						if($scope.results.shipping_discount < 0) $scope.results.shipping_discount = 0;
						//Amount Off
						$scope.results.off = ($scope.results.subtotal - $scope.results.subtotal_discount) + ($scope.results.shipping - $scope.results.shipping_discount);
					
					//Tax
					if($scope.system.customer){
						if($scope.system.customer.form){
							if($scope.system.customer.form.billingState == 'CA') $scope.results.tax = $scope.results.subtotal_discount * 0.0875;
							if($scope.system.customer.form.taxid) $scope.results.tax = 0;
						}
					}
					
					//Final Total
					$scope.results.total = ($scope.results.subtotal - $scope.results.off) + $scope.results.tax + $scope.results.shipping;
					
					//Set System Totals
					$scope.system.totals = $scope.results;
					
					 						 
				},
				prompt: function(){
					var modalInstance = $modal.open({
				      templateUrl: 'login-default.html',
				      controller: 'login',
				      windowClass: 'loginPrompt',
				    });
				    modalInstance.result.then(function () {}, function () {
					 
					    if(!$scope.system.customer){
						    	 $scope.system.prompt();
						    	 }
				    });
				    
				    return modalInstance;
				},
				modal: function(template,time){
					$timeout(function() {
						var modalInstance = $modal.open({
							templateUrl: template,
							controller: 'modal',
							windowClass: 'my_modal',
						});
					}, time);		   
				},
				logout: function(){
					$scope.system.customer = null;
					localStorage[$scope.version+"_customer"] = null;
					$window.location.reload();
				},
				setToCart: function(slug, qty){
					//Setup Variation Object
						//Configurable Add-Ons
			       		 $scope.add = {};
			       		 $scope.add[0] = {};
			       		 $scope.add[1] = {};
       		 
					$scope.options = {
							"sku": $scope.system.products[slug].sku,
							"option1": undefined,
							"option2": undefined,
							"subscription": $scope.system.products[slug].subscription,
							"add": $scope.add,
							"bundle": $scope.system.products[slug].bundle,
						};
						
				    //Create Key
				    $scope.key = angular.toJson($scope.options);
					$scope.options.qty = qty;
					
					$scope.system.products[slug].variations[$scope.key] = $scope.options;

					$scope.system.saveProducts();
					$scope.system.total();
				}
			};
			
			
			
			//Restrict
			$scope.$on('grouprestrict', function(evt,thing) { 
				angular.forEach($scope.system.products, function(product, slug) {
					if(product.variations){							
						flag = true;
						angular.forEach(product.groups, function(group) {
							if(group == thing) flag = false;
						});
						if(flag) {
							product.variations = {};
						}
					}
				});	
			});

 			

			//MiniCart
			$scope.$on('minicart', function(evt,toggle) { 
				 $scope.system.mini_collapse = toggle;
			});
			
			//MiniCart
			$scope.$on('logout', function() { 
				 $scope.system.logout();
				 console.log('test');
			});
			
			//on Cart Change
			$scope.$on('save', function() { 
			 
					 $scope.system.saveProducts();
					 $scope.system.saveCustomer();
					 $scope.system.total();
					 
					 
				 
			});	 
			
			//Login
			$scope.$on('login', function(evt, data) { 
				$scope.system.saveCustomer(data.customer);
			    $scope.system.total();
			});
			
			//Apply Coupon
			$scope.$on('applyCoupon', function(evt, data) { 
				$scope.isUsed = false;
				//Remove Used Codes
						if($scope.system.customer){
							angular.forEach($scope.system.customer.usedCodes, function(key, code) {
								if(angular.uppercase(code) == data.promo) $scope.isUsed = true;
							});
						}
				
				if($scope.isUsed) return false;
				
				$scope.system.saveProducts(data.products);
				 $scope.params = {
					 promo: data.promo,
					 cart_modifiers: data.cart_modifiers,
					 terms: data.terms,
				 }
				$scope.system.saveDiscount($scope.params);
				$scope.system.total();
			});
			
			//Checkout Success
			$scope.$on('checkedout', function(evt, data) { 
				 ldata.getProductData().success(function (data) {
						 $scope.system.saveProducts(data);
						 $scope.system.resetPromo();	
						});
			});

			
		 
		 console.log($scope.system.flags);

			//Initialize
			$scope.system.init();
			
			//Misc Functions
			$scope.affiliatesGetStarted = function(){
			
				$scope.system.clearCart();
				$scope.system.setToCart('fitaid-24-pack-wholesale',10);
				$scope.system.setToCart('fitaid-fridge',1);
				$scope.system.setToCart('fitaid-fridge-stand',1);
				$scope.system.setToCart('fitaid-fridge-lock',1);
				$scope.system.setToCart('fitaid-banner',1);
				$scope.system.setToCart('available-here-poster',1);
				$scope.system.setToCart('jackie-perez-poster',1);
			 
				$window.location.href = '/affiliates-start';
			
			}
	});

////////////////////////////////////////////////////////////////////////////////////
//Product Directive
lcart.directive('product', function($sce, $rootScope) {
  return {
  	scope: {
	  	product: '=object',
	  	products: '=',
	  	strict: '@',
	  	
	  	 
	},
  	restrict: 'AE',
    replace: false,
    templateUrl: function(tElement, tAttrs) {
            return $sce.trustAsResourceUrl('//'+api_host+'/templates/' + tAttrs.template + '.html');
			},
    link: function($scope, $elem, $attrs) {
			//Submitted Button
			$scope.submitted = false;
       		
       		//Configurable Add-Ons
       		 $scope.add = {};
       		 $scope.add[0] = {};
       		 $scope.add[1] = {};
    
	   		 //Hold Sku
	   		 $scope.sku = [];
	   		 
	   		 //Qty
	   		 $scope.myQty = 1;
	   		 			
			//Fitclub sizes
			$scope.$watch('products', function() { //Wait for the product data
				if ($scope.products && $scope.product && $scope.product.groups.indexOf("fitclub") >= 0) { //Determine if product is in the fitclub group
					$scope.sizes = {}; //Initialize the list of sizes
					var colors = $scope.product.option3.values; //Get the available colors for this product
					var slugs = $scope.product.option3.slugs; //Get the associated slugs for this product

					for (var i = 0; i < colors.length; ++i) { //Loop through the available colors
						$scope.sizes[colors[i].value] = $scope.products[slugs[i]].option1.values; //Get the available sizes for this color
					}
				}
			});

       		//AddToCart Function
	   		$scope.addItem = function(scope,allowEven) {
		   	 	 $scope.addToForm = scope.addToForm;
		   	 	 
		   	 	 $scope.subscription = scope.subscription;
		   	 	 
		   	 	 
		   	 	$scope.my_sku = '';
				$scope.submitted = true;
					 
				if(!$scope.addToForm.$valid) {
				
					return false;
				}
				
				//Enforce Even QTY for case+wholesale
				if($scope.product.groups.indexOf('case') != -1 && $scope.product.groups.indexOf('wholesale') != -1 && !allowEven){
					$scope.myQty = 2;
				}
				
				//Build Item SKU
					$scope.sku[0] = {
						'value': $scope.product.sku,
					}
					angular.forEach($scope.sku, function(sku) {
							$scope.my_sku = $scope.my_sku + sku.value;
						});				 				
				
				//Build Configurable Add-On Product SKUS's
					
					angular.forEach($scope.add, function(addons, key) {
							add_sku = '';
							angular.forEach(addons, function(on) {
								if(on.value)add_sku = add_sku + on.value;
							});	
							$scope.add[key]['sku'] = add_sku; 
						});	
					
					$scope.sub = {
						interval: $scope.subscription
					}
					
				//Setup Variation Object
					$scope.options = {
							sku: $scope.my_sku,
							option1: $scope.sku[1],
							option2: $scope.sku[2],
							subscription: $scope.sub,
							add: $scope.add,
							bundle: $scope.product.bundle,
						};
						
				//Create Key
				$scope.key = angular.toJson($scope.options);
						
				//Add Variation Options
				if($scope.product.variations[$scope.key]){
					$scope.product.variations[$scope.key].qty += $scope.myQty; 
					
				}else{
					 $scope.options.qty = $scope.myQty;
					 $scope.product.variations[$scope.key] = $scope.options;
				}
					
			 
				
				//Restict Groups			
					if($scope.strict) $rootScope.$broadcast('grouprestrict', $scope.strict);				
				//Actuate Mini-Cart
				 	$rootScope.$broadcast('minicart', false);
		 
				
				//Save
				$rootScope.$broadcast('save');
				
				 
			}			    
	}
  };
});

////////////////////////////////////////////////////////////////////////////////////
//Product Directive
lcart.directive('cart', function($sce, $rootScope, $window, ldata) {
  return {
  	scope: {
	  	products: '=',
	  	totals: '=',
	},
  	restrict: 'AE',
    replace: false,
    templateUrl: function(tElement, tAttrs) {
            return $sce.trustAsResourceUrl('//'+api_host+'/templates/' + tAttrs.template + '.html');
			},
    link: function($scope, $elem, $attrs) {
	    	
	    	
	    	
	    	$scope.buttons = {
		    	paypal: false,
	    	}
	    	//Remove From Cart
       			 
       			$scope.remove = function(product,key){
		   			delete product.variations[key];
	       			$rootScope.$broadcast('save');
	       		}
	       		
	       		$scope.updateQty = function(product,qty){
			   		//Enforce Even QTY for case+wholesale
					if(product.groups.indexOf('case') != -1 && product.groups.indexOf('wholesale') != -1 && !$scope.allowEven){
						if (qty % 2 == 0) $rootScope.$broadcast('save');
					}else{
						if(qty)$rootScope.$broadcast('save');
					}
				}		   		
		   		//Apply Coupon
		   		$scope.applyCoupon = function(){
			   		$scope.cbtn = true;
			   		
					$scope.payload = {
						promo: $scope.promo,
						products: $scope.products,
						mode: false,
					}
					ldata.applyCoupon($scope.payload).success(function (data) {
						$scope.promo = null;
						 
						if(data.products){
							$rootScope.$broadcast('applyCoupon',data);
						}
					 	$scope.cbtn = false;
					 });
				}
				
				
				
				//Remove Coupon
				$scope.removeCoupon = function(){
					$scope.promo = null;
					$scope.applyCoupon();
				}
		
	    	}		
	    	
	    	
	    	
				
				
	 
  }
});

////////////////////////////////////////////////////////////////////////////////////
//Confirm Directive
lcart.directive('confirm', function($sce, $rootScope) {
  return {
  	scope: {
	},
  	restrict: 'AE',
    replace: false,
    templateUrl: function(tElement, tAttrs) {
            return $sce.trustAsResourceUrl('//'+api_host+'/templates/' + tAttrs.template + '.html');
			},
    link: function($scope, $elem, $attrs) {
				$rootScope.$broadcast('checkedout');
			}		
 }
});

////////////////////////////////////////////////////////////////////////////////////
//My Account Directive
lcart.directive('account', function($sce, $window, $rootScope, ldata) {
  return {
  	scope: {
	  	customer: '=',
	  	prompt: '&',
	  	
	},
  	restrict: 'AE',
    replace: false,
    templateUrl: function(tElement, tAttrs) {
            return $sce.trustAsResourceUrl('//'+api_host+'/templates/' + tAttrs.template + '.html');
			},
    link: function($scope, $elem, $attrs) {
	
	   
	    $scope.$on('login', function(evt, data) { 
					$scope.customer = data.customer;
					$scope.init();
					 
				});
				$scope.init = function(){
					//Error/Success Containers
					$scope.error = false;
					$scope.success = false;
					
					
					//Card
					$scope.card = {
						customerProfileId: $scope.customer.customerProfileId,
					}
					
					$scope.button = {
						save: false,
						cancel: false,
					};
					
					$scope.creds = {
						email: $scope.customer.email,
						password: $scope.customer.password,	
					}
					
					$scope.payload = {
						login: $scope.creds,
					}
					
					ldata.getMyOrders($scope.payload).success(function (data) {
						$scope.orders = data;
					});
				}			
				
				//Check Login	
				if(!$scope.customer){ 
						$scope.prompt();
						}else{
							$scope.init();
							 	
						}
			
				//Clear Errors/Success
				$scope.clear = function(){
					$scope.success = null;
					$scope.error = null;
				}
				
				//Set Billing Card Fix
			    $scope.setBillingCard = function(form){
				   form.card = form.cardSelect.customerPaymentProfileId;
			    }
			    
			    //Set Billing Card Fix
			    $scope.setBillingInit = function(form){
				  angular.forEach($scope.customer.paymentProfiles, function(value, key) {
				  		if(value.customerPaymentProfileId == form.card) form.cardSelect = $scope.customer.paymentProfiles[key];
					});
			    }
					    
				//Add a Card
				$scope.addCard = function(){
					$scope.clear();
					
					//Login Creds
					$scope.creds = {
						email: $scope.customer.email,
						password: $scope.customer.password,	
					}
					
					//Payload to Send
					$scope.payload = {
						login: $scope.creds,
						card: $scope.card,
					}
					
					//Send FUNC
					ldata.addCard($scope.payload).success(function (data) {
						if(data.success){
							ldata.login($scope.creds).success(function (ldata) {
							 	if(ldata.success){
								 	$rootScope.$broadcast('login', ldata);
								 	$scope.success = data.success;
							 	}else{
									$scope.error = 'Login Error, Please Refresh.';
							 	}
							});
						}else{
							$scope.error = data.error;
						}
						
						 
						
					});
					
					
				}
				
				//Delete a Card
				$scope.deleteCard = function(card){
					$scope.clear();
					
					$scope.dcard = {
						customerProfileId: $scope.customer.customerProfileId,
						paymentProfileId: card,
					}
					
					//Login Creds
					$scope.creds = {
						email: $scope.customer.email,
						password: $scope.customer.password,	
					}
					
					//Payload to Send
					$scope.payload = {
						login: $scope.creds,
						card: $scope.dcard,
					}
					
					//Send FUNC
					ldata.deleteCard($scope.payload).success(function (data) {
						if(data.success){
							ldata.login($scope.creds).success(function (ldata) {
							 	if(ldata.success){
								 	$rootScope.$broadcast('login', ldata);
								 	$scope.success = data.success;
							 	}else{
									$scope.error = 'Login Error, Please Refresh.';
							 	}
							});
						}else{
							$scope.error = data.error;
						}
					});
					
				}
				
				//Edit Customer Function	
				$scope.saveCustomer = function(customer){
					$scope.button.save = true;
					
					$scope.clear();
					
					$scope.creds = {
						email: $scope.customer.email,
						password: $scope.customer.password,	
					}
					
					$scope.payload = {
						login: $scope.creds,
						customer: customer,
					}
					
					
					ldata.updateCustomer($scope.payload).success(function (data) {
						if(data.success){
							
							if(data.reset){
								$rootScope.$broadcast('logout');
								console.log('reset');
								}else{
							
							ldata.login($scope.creds).success(function (ldata) {
							 	if(ldata.success){
								 	$rootScope.$broadcast('login', ldata);
								 	$scope.success = data.success;
								 	$scope.button.save = false;
							 	}else{
									$scope.error = 'Login Error, Please Refresh.';
										$scope.button.save = false;
							 	}
							});
							}
						} 
					});
					
				}	
				
				//Edit Order Function	
				$scope.saveOrder = function(order){
					$scope.clear();
					
					$scope.button.save = true;	 
					
					$scope.creds = {
						email: $scope.customer.email,
						password: $scope.customer.password,	
					}
					
					$scope.payload = {
						login: $scope.creds,
						order: order,
					}
					
					
					ldata.updateOrder($scope.payload).success(function (data) {
							if(data.success){
								ldata.getMyOrders($scope.payload).success(function (data) {
									$scope.orders = data;
								});
								$scope.success = data.success;	
								$scope.button.save = false;	 	
						 	}else{
							 	$scope.error = data.error;
						 	}
					
					});
				}	
				
				//Delete Order
				$scope.deleteOrder = function(id){
					if(confirm('Press "OK" to Confirm Deletion')){
						$scope.clear();
					
						$scope.button.save = true;	 
						
						$scope.creds = {
							email: $scope.customer.email,
							password: $scope.customer.password,	
						}
						
						$scope.payload = {
							login: $scope.creds,
							id: id,
						}
						
						ldata.deleteOrder($scope.payload).success(function (data) {
							if(data.success){
								ldata.getMyOrders($scope.payload).success(function (data) {
									$scope.orders = data;
								});
								$scope.success = data.success;	
								$scope.button.save = false;	 	
						 	}else{
							 	$scope.error = data.error;
						 	}
					
					});

					
					}
				}
				
				//Cancel Subscription Function
				$scope.cancel = function(order){
					if(confirm('Press "OK" to Confirm Cancellation')){
						$scope.clear();
						
						$scope.creds = {
							email: $scope.customer.email,
							password: $scope.customer.password,	
						}
						
						$scope.payload = {
							login: $scope.creds,
							order: order,
						}
						
						
						$scope.button.cancel = true;	 
						ldata.cancel($scope.payload).success(function (data) {
							if(data.success){
								ldata.getMyOrders($scope.payload).success(function (data) {
										$scope.orders = data;
								});
								$scope.button.cancel = false;	
								$scope.success = data.success;
							}else{
								$scope.error = data.error;
							}
						});	
					}
				}
	}		
 }
});


////////////////////////////////////////////////////////////////////////////////////
//Product Directive
lcart.directive('checkout', function($sce, $rootScope, $window, ldata) {
  return {
  	scope: {
  		products: '=',
  		totals: '=',
		customer: '=',
		prompt: '&',
		discount: '=',
		baseurl: '@',
		confirmurl: '@',
		
	},
  	restrict: 'AE',
    replace: false,
    templateUrl: function(tElement, tAttrs) {
            return $sce.trustAsResourceUrl('//'+api_host+'/templates/' + tAttrs.template + '.html');
			},
    link: function($scope, $elem, $attrs) {
	    
	   	//Button State
				$scope.button = {
					checkout: false,
					}
	    //Set Submit as false
				$scope.submitted = false;
				
			//Error Message
				$scope.error = {
					checkout: null,
				}
				
	    
	  	 $scope.$on('login', function(evt, data) { 
				$scope.customer = data.customer;
				$scope.init();
				});
	  	
	  	
	  	//Initilize
		$scope.init = function(){
			
					 $scope.form = {};

					
				//Verify Total
					$scope.payload = {
						promo: false,
						products: $scope.products,
						mode: true,
					}
					if($scope.discount) $scope.payload.promo = $scope.discount.promo;
					ldata.applyCoupon($scope.payload).success(function (data) {
					 	if(data.products){
							$rootScope.$broadcast('applyCoupon',data);
						}
						$scope.button.checkout = false;
					});
				
				
				 		$scope.customer.form.shipToBill = false;
						//Is APO?	
						if($scope.totals.groupqty.apo){
							$scope.customer.form.shipToBill = false;
							$scope.customer.form.shipToApo = true;
						}else{
							$scope.customer.form.shipToApo = false;
						}
				
			//URLS
				$scope.customer.form.baseurl = $scope.baseurl;
				$scope.customer.form.confirmurl = $scope.confirmurl;

		}		
		
		//Run Checkout
	    	$scope.checkout = function(){
		    	$scope.submitted = true;
		    	if(!$scope.checkoutForm.$valid) {
			     	var error = $("#checkout-top");
					$('html,body').animate({scrollTop: error.offset().top-97},'slow');
				}else{
					$scope.button.checkout = true;
					$scope.save();
				    if($scope.discount) $scope.promo = $scope.discount.promo;
					$scope.creds = {
		    			email: $scope.customer.email,
		    			password: $scope.customer.password,	
	    			}
		    		$scope.params = {
		    			promo: $scope.promo,
		    			login: $scope.creds,
			    		customer: $scope.customer,
			    		form: $scope.form,
			    		discount: $scope.discount,
			    		products: $scope.products,
			    		}
			    	ldata.checkout($scope.params).success(function (data) {
				    	if(data.success){
			    			ldata.login($scope.creds).success(function (ldata) {
							 	if(!ldata.error)$rootScope.$broadcast('login', ldata);
								//Redirect
								$window.location.href = data.url;	
							});
						}else{
							ldata.login($scope.creds).success(function (ldata) {
							 	if(!ldata.error)$rootScope.$broadcast('login', ldata);
								$scope.button.checkout = false;
								$scope.error.checkout = data.error;
								var error = $("#checkout-top");
								if (error.length) $('html,body').animate({scrollTop: error.offset().top-97},'slow');	
							});
							
							
					 	}
					});
				} 
		    }
		  //Save function		
	    	$scope.save = function(){
		    	$rootScope.$broadcast('save');
		    }
	    	
	     
	    	
	    	//Check Login	
		if(!$scope.customer){ 
				$scope.prompt();
				}else{
					$scope.init();
				}

	    	
	    	
    }
  };
});


////SCROLL TO ANCHOR
$( document ).ready(function() {
    $('a[href^="#"]').click(function() {
	  scrollToAnchor( $(this).attr('href').slice(1) );
	});
});
function scrollToAnchor(aid){
    var aTag = $("a[name='"+ aid +"']");
    $('html,body').animate({scrollTop: aTag.offset().top},'slow');
}

 