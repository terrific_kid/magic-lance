#include /www/config/cache.conf;

	server {
		listen 80;
	    listen 443 ssl;
	
	    server_tokens off;
		server_name development.drinkfitaid.com;
	   
	    ssl_certificate /www/ssl/myssl.crt;
	    ssl_certificate_key /www/ssl/myssl.key;
	 	
	 	root /usr/share/nginx/www/drinkfitaid.com/;
	    index index.php index.html index.htm;
	   
		#Cache everything by default
		set $no_cache 0;
		
		#Don't cache POST requests
		if ($request_method = POST){
		    set $no_cache 1;
			}
		
		#Don't cache if the URL contains a query string
		if ($query_string != ""){
		    set $no_cache 1;
			}

		
		location / {
			try_files $uri $uri/ /index.php?$args;
			}
	
 
		
		location ~ \.php$ {
	        fastcgi_split_path_info ^(.+\.php)(/.+)$;
	        fastcgi_pass unix:/var/run/php5-fpm.sock;
	        fastcgi_index index.php;
	        include fastcgi_params;
					
				#fastcgi_cache MYAPP;
				#fastcgi_cache_valid 200 60m;
				#fastcgi_cache_bypass $no_cache;
				#fastcgi_no_cache $no_cache;		
			}
	
	}
