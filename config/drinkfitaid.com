include /www/config/cache.conf;

	server {
		listen 80;
	    listen 443 ssl;
	
	    server_tokens off;
		server_name drinkfitaid.com www.drinkfitaid.com;
	   
	    ssl_certificate /www/ssl/fitaid.crt;
	    ssl_certificate_key /www/ssl/fitaid.key;
	 	
	 	root /usr/share/nginx/www/drinkfitaid.com/;
	    index index.php index.html index.htm;
	   
		include /www/config/wordpress.conf;    
	
	}
