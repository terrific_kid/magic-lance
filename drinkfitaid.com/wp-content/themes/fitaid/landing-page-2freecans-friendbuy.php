<?php
/* Template Name: 2freecans (friendbuy) Landing Page */
?>
 <?php get_header(); ?>
	
	
		<div class="container twofreecans-landing-wrapper">
			
			<div class="row add-header-height">
				<div class="col-md-12">
					<h1>Try the #1 performance and recovery drink for <span class="big-red-text">free!</span></h1>
					
					<!-- <p class="hidden-xs"><iframe border=0 frameborder=0 class="mobile_hide" id="timerlay-iframe" src="https://timerlay.com/IFrame/1845"></iframe></p> -->
				</div>
			</div>
			<div class="row twofreecans-content">
				<div class="col-md-8 content-left-wrapper">
					<p><img class="feature-image" src="/media/lurong-living-logo.png" alt=""><b>FITAID is NOT an energy drink.</b> FitAID is the only <b>Paleo Friendly performance and recovery beverage</b> made by elite athletes, for athletes. FitAID is made with <b>natural botanical ingredients</b> and has close to 3 grams of <b>targeted supplements to AID your Workout.</b> With things like <b>Glutamine,</b> BCAAs, Omega 3 EFAs, <b>Glucosamine,</b> B Complex, Vitamins C, D, E, Amino Acids, <b>Natural Anti-Inflammatories,</b> Quercetin, CoQ10, and Green Tea Leaf Extract FitAID is the most <b>comprehensive and effective</b> pre and post workout beverage on the market today. On top of that, it's sweetened with raw organic blue agave and stevia and has <b>only 45 calories per can.</b></p>
					<p><small>We will send you 2 FREE cans ASAP so you can see why FITAID is the #1 Drink for Elite Athletes.</small></p>
					<p><small><span class="raleway-asterisk">*</span> Just pay $5.15 for flat rate USPS shipping to your door. Limit 1 per household. Promotional codes may not be used with this item.</small></p>
					
<!-- Friendbuy --------------------------------------------------------->

<style>
.container.red-cross{
	padding: 0;
}
#socialpost .header{
	display: none;
}
.callToAction{
	padding:2em 0 4em 0;
	font-size: 1.15em;
}
.callToAction .btn-lg{
	border-radius: 5px;
	width: 60%;
	margin-left:20%;
	font-size: 1.25em;
}
</style>

<script>
	window['friendbuy'] = window['friendbuy'] || [];
	window['friendbuy'].push(['site', 'site-e463314f-www.drinkfitaid.com']);
	window['friendbuy'].push(['track', 'customer',{
		id: '', //REPLACE WITH YOUR CUSTOMER ID
		email: 'email@email.com' //REPLACE WITH YOUR CUSTOMER'S EMAIL ADDRESS
	}]);

	jQuery(document).ready(function(){
		
		<?php // Update the friendbuy vars with customer data ?>
		var friendBuyUpdate = setInterval(function(){
			var customer = $.parseJSON($('#login-data').attr('data-customer'));
			
			if(customer && customer.email){
				window.clearInterval(friendBuyUpdate);
				window['friendbuy'][1][2]['id'] = customer.customerProfileId;
				window['friendbuy'][1][2]['email'] = customer.email;
				
				// Customize these vars
				var fb_params = {
					fa_customer_email: customer.email,
					fa_customer_fname: customer.form.firstName,
					fa_customer_lname: customer.form.lastName
				};
				var fb_widget_tag = "6U-doI"; <?php // Friendbuy widget tag ?>
				
				
				(function (f, r, n, d, b, y) {
				b = f.createElement(r), y = f.getElementsByTagName(r)[0];b.async = 1;b.src = n;y.parentNode.insertBefore(b, y);
				})(document, 'script', '//djnf6e5yyirys.cloudfront.net/js/friendbuy.min.js');
				
				window['friendbuy'] = window['friendbuy'] || [];
				window['friendbuy'].push([ 'widget', fb_widget_tag, { 
					parameters: fb_params,
				} ]);
	
			}
		}, 1000);
		
	});
	
</script>


					
<div class="callToAction">
	
	<div id="login-data" ng-hide="system.customer.password" data-customer="{{system.customer}}">
		<p><strong>To get this offer 100% and waive shipping and handling, create a simple account so you can earn credit to buy FITAID</strong></p>
		<a class="btn btn-primary btn-lg" ng-click="system.prompt()">LET'S DO THIS!</a>
	</div>
	
	<span ng-show="system.customer.password" style="text-align:center;">
		<div class="friendbuy-6U-doI"></div>
	</span>	
	
</div>

<!-- /Friendbuy --------------------------------------------------------->
					
					<?php /*
					<br/><br/><br/>(old button)
					<div ng-controller="2freecans">
						<button ng-click="freecans()" class="btn btn-lg btn-default">TRY FITAID NOW!</button>				
					</div>
					*/ ?>
					
					<a class="fbox" href="https://www.youtube.com/watch?v=FhuaYAUAdbQ">
						<span>SEE HOW IT WORKS IN 2 MINUTES</span><img src="/media/youtube-how-fitaid-works.jpg" alt="">
					</a>
					
					<a class="fbox" href="https://www.youtube.com/watch?v=U2uzRBa8ekc">
						 <img src="/media/youtube-Jackie-Perez.jpg" alt="">
					</a>
					 
				</div>
				<div class="col-md-4 content-right-wrapper">
				</div>
			</div>
			
			<!-- begin Testimonials -->
			<div class="row testimonials">
				<div class="col-md-12">
					<h3>Derrick Winburn</h3>
					<span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span>
					
					<!-- NOTE: first <p> of testimonial should have the <span.icon-open-quote>, as this class adds the open quote graphic -->
					<p><span class="icon-open-quote"></span> Quality product all around! Actually beneficial ingredients. Tastes awesome! Great guys over at FITAID for sure! Dudes know what they're doing. I'd recommend this stuff to anyone! Should call it Tastier than "The Tasty" though lol just sayin.</p>
					
					
					<h3>Tyrel Johnson</h3>
					<span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span>
					<p><span class="icon-open-quote"></span> Ordered the start up package and it was back ordered. Told Tamara that I needed it for our Grand Opening and she made it happen!!!! That's the epitome of customer service!!!!</p>
					
				</div>
			</div>
			
			
		</div><!-- end container -->
		
<?php get_footer(); ?>
