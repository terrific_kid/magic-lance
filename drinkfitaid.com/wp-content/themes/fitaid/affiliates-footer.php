 	<div class="container">
 	 <div class="col-xs-12">
		<p style="padding: 2em;">
				If you prefer to place your order by phone, call us at (888) 558-1113.<br>
				*If you are not a qualified wholesale account, we will refund your purchase and cancel your order.
		</p>	
	</div>
 	</div>
	  </div><!-- end contaier -->
	
	  <div id="footer">
	 

  
	  
	   <div class="container">
	  <div id="sub_footer">
	  <div class="container">
	 				
				<ul class="col-xs-12 list-inline footer-socials">
	 				<li>
						<a class="socials-instagram" href="//instagram.com/fitaid" target="_blank"><i class="fa fa-instagram"></i></a>
					</li>
	 				<li>
						<a class="socials-twitter" href="//twitter.com/drinkfitaid" target="_blank"><i class="fa fa-twitter"></i></a>
					</li>
	 				<li>
						<a class="socials-facebook" href="//www.facebook.com/fitaid" target="_blank"><i class="fa fa-facebook"></i></a>
					</li>
	 				<li>
						<a class="socials-google-plus" href="//plus.google.com/113134579543723671972" target="_blank"><i class="fa fa-google-plus"></i></a>
					</li>
	 				<li>
						<a class="socials-youtube" href="//www.youtube.com/user/DrinkFitAid" target="_blank"><i class="fa fa-youtube"></i></a>
					</li>
 				</ul>
				
				
 			 
				  <img style="position: absolute; top: 7px; right: 0px;" class="visible-md-block visible-lg-block" src="/media/seals.jpg"  width="207" >
			 
 	 	   
				<p class="col-xs-12 privacy-tos-link" >&copy; 2014 LIFEAID Beverage Co.. <a href="/privacy" title="">Privacy</a> <a href="/terms" title="">Terms & Conditions</a></p>
				
<div class="col-sm-1 hidden-xs"></div>

<div class="col-xs-12 col-sm-10">

<p>Crossfit&reg; is a registered trademark of CrossFit Inc, which is not affiliated with and does not 
necessarily sponsor or endorse the products or services of LifeAID Beverage Co USA.</p>

<p style="padding: 1em; border: 1px solid #787777;">The statements made on this website have not been evaluated by the Food & Drug Administration. 
These products are not intended to diagnose, prevent, treat, or cure any disease.</p>
</div>
<div class="col-sm-1 hidden-xs"></div>

	  </div>
	  </div>
	
		 
  </body>
  
</html>