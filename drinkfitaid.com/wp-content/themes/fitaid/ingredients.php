<?php
/* Template Name: Ingredients */
?>
 <?php get_header(); ?>
	<div class="container-fluid">
	<div class="ingredients-wrapper">
	 	<?php  query_posts( 'cat=5&order=asc'); 
	 		$i = 0;
	 		   if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	 		   	<!-- BEGIN CONTENT -->
				
				
			 
			 
				<div class="container-fluid heading">
					<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
					<div style="background-image: url(<?php echo $image[0]; ?>);" class="hero">
						<?php if(!$i): ?>
						<div class="desktop_shiv hidden-xs"></div>
						<div class="mobile_shiv visible-xs-block"></div>
						<?php endif; ?>
						<div class="wrap"> <h2><?php the_title(); ?> </h2></div>
				   </div>
				</div>


				<div class="col-sm-1"></div>
				<div class="col-sm-10 ingredient-detail"><?php the_content(); ?></div>
				<div class="col-sm-1"></div>
			
				
				<div class="clearfix"></div>
				<!-- END CONTENT -->
			<?php $i++; endwhile; else : ?>
		<?php endif; ?>
		
		
				

				
	</div><!-- end container -->
	</div>
<?php get_footer(); ?>
	  
