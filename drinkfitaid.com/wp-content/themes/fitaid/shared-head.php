<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

<!-- bugsnag -->
<script src="//d2wy8f7a9ursnm.cloudfront.net/bugsnag-2.min.js" data-apikey="86d6965b2c2a2820b6ba443fe799ecbf"></script>

<!-- AngularJS -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.min.js"></script>

<!-- jQuery-->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<!-- Bootstrap 3 CSS + AFFIX -->
<script src="/wp-content/themes/fitaid/js/bootstrap.min.js"></script> 
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<!--  Angular Bootstrap UI -->
<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js"></script>

<!-- lcart -->
<script src="//crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/md5.js"></script>
<script type="text/javascript" src="//<?php echo API_HOST ?>/lcart.js?v=<?php echo VERSION_NUMBER; ?>" id="lifeaid-lcart" data-env="<?php echo LIFEAID_ENV ?>"></script>    

<!-- style & efx -->
<link rel="stylesheet" href="/wp-content/themes/fitaid/style.css">

<!-- scripts for modal images -->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/helpers/jquery.fancybox-media.js"></script>
<script type="">
    $(document).ready(function(){ 
        $('.fbox').fancybox({
                padding: 0,
                openEffect  : 'none',
                closeEffect : 'none',
                helpers : {
                media : {}
            }
        });
    });
</script>

<!-- AdRoll -->
<script type="text/javascript">
    adroll_adv_id ="WRV3XLNUMNB27DRJR2JH7G";
    adroll_pix_id ="DJRFSDBZR5EPHLGBY62OYK";
    (function () {
    var oldonload =window.onload;
    window.onload =function(){
     __adroll_loaded=true;
     var scr= document.createElement("script");
     var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" :"http://a.adroll.com");
     scr.setAttribute('async', 'true');
     scr.type = "text/javascript";
     scr.src= host + "/j/roundtrip.js";
     ((document.getElementsByTagName('head') || [null])[0] ||
     document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
     if(oldonload){oldonload()}};
    }());
</script>