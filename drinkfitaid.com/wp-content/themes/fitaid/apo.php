<?php
/* Template Name: APO Page */
 get_header(); ?>
<div class="container-fluid heading">
 <div style="background-image: url(/media/header_apo.jpg);" class="hero">
			 <div class="desktop_shiv hidden-xs"></div>
			 <div class="mobile_shiv visible-xs-block"></div>
			 <div class="wrap"> <h2>FOR OUR MEN AND WOMEN SERVING - ORDER APO</h2> </div>
 </div>
</div>

	<div class="subnav">
	<?php wp_nav_menu(array('theme_location'=>'apo-menu', 'container_class'=>'container', 'menu_class' => 'col-xs-12')); ?>
	</div>
	
	<div class="container">
	<div class="copy_content">
		 
		 
		<?php include('includes/reviews.php'); ?>
		<div class="clearfix"></div>	
		
		
		<a name="fitaid" class="anchor"></a>
		<product data-object="system.products['fitaid-16-pack-apo']" data-template="product-default" data-strict="apo"></product> 			 
		<product data-object="system.products['partyaid-16-pack-apo']" data-template="product-default" data-strict="apo"></product> 			 
		<product data-object="system.products['golferaid-16-pack-apo']" data-template="product-default" data-strict="apo"></product> 			 
		<div class="clearfix"></div>	

		
	</div>
	</div>
	
	
<div class="contaier-fluid heading">
 <div style="background-image: url(/media/merch_header.jpg);" class="hero">
			 <div class="wrap"> <h2>Gear</h2> </div>
 </div>
</div>


 	<div class="container">
	<div class="copy_content">
				
				
				
								

				<a name="gear" class="anchor"></a>
						 
				<product data-object="system.products['fitaid-black-tshirt-apo']" data-template="product-default" data-strict="apo"></product>
				<product data-object="system.products['fitaid-red-tshirt-apo']" data-template="product-default" data-strict="apo"></product>
				<product data-object="system.products['fitaid-blue-tshirt-apo']" data-template="product-default" data-strict="apo"></product>
				<div class="clearfix"></div>	
					
	</div><!-- end container -->
	</div>
<?php get_footer(); ?>
	  
	  
	  
	  
