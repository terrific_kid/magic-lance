<?php
/* Template Name: Refer Your Gym */
?>
<?php get_header(); ?>



<div class="desktop_shiv hidden-xs"></div>
<div class="mobile_shiv visible-xs-block"></div>
		 
<div class="refer-your-gym-wrapper-background">
	<div class="container refer-your-gym-wrapper">
		
		<div class="col-md-9">
			
			<?php if(isset($_REQUEST['success'])): ?>
			
				<h1>Thanks for your Referral!</h1>
		
				<p>We'll reach out to your gym shortly to start selling the #1 performance and recovery drink!</p>
				<p>In the meantime, go follow us on social media for free offers and incentives.</p>
				
				<ul class="list-inline footer-socials">
					<li>
					<a class="socials-instagram" href="//instagram.com/fitaid" target="_blank"><i class="fa fa-instagram"></i></a>
					</li>
					<li>
					<a class="socials-twitter" href="//twitter.com/drinkfitaid" target="_blank"><i class="fa fa-twitter"></i></a>
					</li>
					<li>
					<a class="socials-facebook" href="//www.facebook.com/fitaid" target="_blank"><i class="fa fa-facebook"></i></a>
					</li>
					<li>
					<a class="socials-google-plus" href="//plus.google.com/113134579543723671972" target="_blank"><i class="fa fa-google-plus"></i></a>
					</li>
					<li>
					<a class="socials-youtube" href="//www.youtube.com/user/DrinkFitAid" target="_blank"><i class="fa fa-youtube"></i></a>
					</li>
				</ul>
			
			
			<?php else: ?>

				<h1>Refer Your Gym</h1>
				
				<p>FITAID Fans... Want to get a <b>FREE case of FITAID, FREE FITAID T-shirt</b> and <b>FREE Shipping?</b><br></p>
				
				<p>Help get us <b>into your Gym</b> by filling out the information below. <b>We'll even give your facility a FREE FITAID refrigerator on their first order!</b> When your gym starts selling FitAID, we will <b>immediately ship YOU</b> all of your free product and <b>FITAID Gear worth over $90!</b></p>
				<p>All you need to do is fill out the info below in 3 easy steps and we'll do the rest. Enjoy!</p>
				<p>
					<b>IMPORTANT NOTE:</b>
					<span class="block-text"><span class="star">*</span>Offer is based on minimum order by participating box/gym.</span>
					<span class="block-text"><span class="star">**</span>This offer is not valid for referring a gym in which you are an owner of.</span>
				</p>
				
				<form id="form-refer-gym" action="/boxform.php" role="form">
				 
					<!-- step 1 -->
					<div class="refer-gym-step"><span>STEP</span><span>1</span></div>					
					<div class="refer-gym-step-heading"><b>ENTER YOUR CONTACT INFO</b> (so we can send you the freebies)</div>
					<div class="form-group">
						<label class="sr-only" for="first-name">First Name</label>
						<input required type="text" class="form-control" id="first-name" placeholder="First Name" name="FirstName">
					</div>
					<div class="form-group">
						<label class="sr-only" for="last-name">Last Name</label>
						<input required type="text" class="form-control" id="last-name" placeholder="Last Name" name="LastName">
					</div>
					<div class="form-group">
						<label class="sr-only" for="email">Email</label>
						<input required type="text" class="form-control" id="email" placeholder="Email" name="Email" >
					</div>
					<div class="form-group">
						<select name="ShirtSize" id="tshirt" class="form-control">
							<option value="">Select Your T-Shirt</option>
							<option value="Women's Tank Top - Small">Women's Tank Top - Small</option>
							<option value="Women's Tank Top - Medium">Women's Tank Top - Medium</option>
							<option value="Women's Tank Top - Large">Women's Tank Top - Large</option>
							<option value="Women's Tank Top - XL">Women's Tank Top - XL</option>
							<option value="Men's Tank Top - Small">Men's Tank Top - Small</option>
							<option value="Men's Tank Top - Medium">Men's Tank Top - Medium</option>
							<option value="Men's Tank Top - Large">Men's Tank Top - Large</option>
							<option value="Men's Tank Top - XL">Men's Tank Top - XL</option>
							<option value="Men's Tank Top - XXL">Men's Tank Top - XXL</option>
						</select>
					</div>
					<div class="form-group">
						<label class="sr-only" for="address1">Shipping Address</label>
						<input required type="text" class="form-control" id="address" placeholder="Address" name="Address1" >
					</div>
					<div class="form-group">
						<label class="sr-only" for="city">City</label>
						<input required type="text" class="form-control" id="city" placeholder="City" name="City" >
					</div>
					<div class="form-group">
						<label class="sr-only" for="state">State</label>
						<input required type="text" class="form-control" id="state" placeholder="State" name="State" >
					</div>
					<div class="form-group">
						<label class="sr-only" for="postalcode">Zip Code</label>
						<input required type="text" class="form-control" id="postalcode" placeholder="Zip Code" name="PostalCode" >
					</div>


					
					<!-- step 2 -->
					<div class="refer-gym-step"><span>STEP</span><span>2</span></div>					
					<div class="refer-gym-step-heading"><b>NOW JUST ENTER YOUR GYM INFORMATION</b></div>
					<div class="form-group">
						<label class="sr-only" for="gym-name">Gym Name</label>
						<input required type="text" class="form-control" id="gym-name" placeholder="Gym Name" name="gym_Company" >
					</div>
					<div class="select-box-wrapper">
						<p><b>GYM TYPE</b></p>
						<label>
							<select name="gym_Type">
								<option  value="affiliate" selected>CrossFit Affiliate</option>
								<option value="martial">Martial Arts Studio</option>
								<option value="fitness">Gym / Fitness Center</option>
								<option  value="Yoga">Yoga Studio</option>
								<option value="other">Other</option>
							</select>
						</label>
					</div>
					<div class="form-group">
						<label class="sr-only" for="gym-owner-first-name">Gym Owner First Name</label>
						<input type="text" class="form-control" id="gym-owner-first-name" placeholder="Gym Owner First Name" name="gym_FirstName" >
					</div>
					<div class="form-group">
						<label class="sr-only" for="gym-owner-last-name">Gym Owner Last Name</label>
						<input type="text" class="form-control" id="gym-owner-last-name" placeholder="Gym Owner Last Name" name="gym_LastName">
					</div>
					<div class="form-group">
						<label class="sr-only" for="gym-address1">Gym Address</label>
						<input type="text" class="form-control" id="gym-address1" placeholder="Gym Address" name="gym_StreetAddress1">
					</div>
					 
					<div class="form-group">
						<label class="sr-only" for="gym-city">Gym City</label>
						<input type="text" class="form-control" id="gym-city" placeholder="Gym City">
					</div>
					<div class="form-group">
						<label class="sr-only" for="gym-state">Gym State</label>
						<input type="text" class="form-control" id="gym-state" placeholder="Gym State"  name="gym_State">
					</div>
					<div class="form-group">
						<label class="sr-only" for="gym-zip-code">Gym Zip Code</label>
						<input type="text" class="form-control" id="gym-zip-code" placeholder="Gym Zip Code" name="gym_PostalCode">
					</div>
					<div class="form-group">
						<label class="sr-only" for="gym-email">Gym Email</label>
						<input required type="text" class="form-control" id="gym-email" placeholder="Gym Email" name="gym_Email">
					</div>
					<div class="form-group">
						<label class="sr-only" for="gym-phone-number">Gym Phone Number</label>
						<input required type="text" class="form-control" id="gym-phone-number" placeholder="Gym Phone Number" name="gym_Phone1">
					</div>					
					
					<!-- step 3 -->
					<div class="last-step-wrapper">
						<div class="refer-gym-step"><span>STEP</span><span>3</span></div>
						<button class="btn btn-lg btn-default" type="submit">SUBMIT</button>
					</div>
					
				</form>
				
			<?php endif; ?>
			
		</div><!-- END col-md-9 -->	
		<div class="col-md-3 col-sm-6">
			<?php include 'sidebar.php';?>
		</div>
		
	</div><!-- end container -->
</div><!-- end background div -->
<?php get_footer(); ?>
