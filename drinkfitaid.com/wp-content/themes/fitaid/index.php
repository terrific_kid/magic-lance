<?php get_header(); ?>
		 	
<div class="container subpage-wrapper">
			
	<div class="col-xs-12 row add-header-height">
		<div class="col-md-9">
			
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		
				<h3><?php the_title();?></h3>
				<p><?php the_content();?></p>
	
			<?php endwhile; else : ?>

				<h3>Page Unavailable</h3>
				<p>This page is currently unavailable.</p>
		 
			<?php endif; ?>
		
			</div><!-- END col-md-9 -->	
			<div class="col-md-3 col-sm-6">
				<?php include 'sidebar.php';?>
			</div>
		</div><!-- END row -->	
		
	</div><!-- end container -->
<?php get_footer(); ?>