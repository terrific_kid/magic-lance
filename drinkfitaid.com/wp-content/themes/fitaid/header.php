<!DOCTYPE html>
<html ng-app="lcart" lang="en">
<head>
	<title>DrinkFitAID.com</title>

	<?php include('shared-head.php'); ?>		

	<!--Start of Zopim Live Chat Script-->
	<script type="text/javascript">
	window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
	d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
	_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
	$.src='//v2.zopim.com/?1z2KtsoEJlvCPeFschzfbS9ULhJ6cVkC';z.t=+new Date;$.
	type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
	</script>
	<!--End of Zopim Live Chat Script-->	    

	<script>(function() {
	var _fbq = window._fbq || (window._fbq = []);
	if (!_fbq.loaded) {
	var fbds = document.createElement('script');
	fbds.async = true;
	fbds.src = '//connect.facebook.net/en_US/fbds.js';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(fbds, s);
	_fbq.loaded = true;
	}
	_fbq.push(['addPixelId', '1506949926239575']);
	})();
	window._fbq = window._fbq || [];
	window._fbq.push(['track', 'PixelInitialized', {}]);
	</script>
	<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=1506949926239575&amp;ev=PixelInitialized" /></noscript>

	<!-- Youtube -->
	<meta name="google-site-verification" content="H_anXGhW7GcFeCvFscXoNdEiKa5ocwOx7-PNOjb-cc8" />
 
</head>
<body ng-controller="core" <?php body_class(); ?> ng-cloak>


   
	<!-- call slideout free shipping/gear -->
	<?php include 'includes/slideout-free-shipping.php';?>

	<!-- call slideout buy fitaid -->
	<?php include 'includes/slideout-buy-fitaid.php';?>

   <?php require_once('includes/forms.php'); ?>
	    
   
   <div id="top_black">
     
	   <ul class="container">
		   
		  <li class="col-sm-6 hidden-xs">
			<a class="arrow_link" ng-click="system.modal('sponsor_type')">LIFEAID SPONSORSHIP</a>
			<a class="arrow_link hspaceLt" href="/locator/">STORE LOCATOR</a>
		  </li> 
		   <li class="col-xs-12 col-sm-6 pull-right">
		     
		      <div class="visible-xs-block" style="position: absolute;">
		      <iframe src="//<?php echo API_HOST ?>/social/instagram_mobile.html" scrolling="no" frameborder="0" style="float: left; border:none; overflow:hidden; width:81px; height:21px;" allowTransparency="true"></iframe>
			
		      <iframe src="//<?php echo API_HOST ?>/social/facebook_mobile.html" scrolling="no" frameborder="0" style="border:none; width:50px; height: 21px; float: left;" allowTransparency="true"></iframe>
		      	
		      </div>
		     
		     <span class="glyphicon glyphicon-phone-alt"></span> <span class="hidden-xs">MIND BLOWING CUSTOMER SERVICE </span>(888) 558-1113
	
		  </li>
		  
	   </ul>
	  
	
   </div><!-- end top_black -->
   
   		<div id="mini-account" ng-show="system.customer.password" class="hidden-xs" data-spy="affix" data-offset-top="33" ng-init="system.acct_collapse = true" collapse="system.acct_collapse">
	   		<div class="container">
		   		<div class="col-xs-12">
			   		<div id="mini-account_content">
				   		<div class="sleeve">
					   		<b>Hi, {{system.customer.form.firstName}}!</b> <a ng-click="system.logout()">(Logout)</a>
					   	</div>
				   	</div>
		   		</div>
	   		</div>
   		</div>
 
   
    	<div id="mini-cart" class="hidden-xs" data-spy="affix" data-offset-top="33" ng-init="system.mini_collapse = true" collapse="system.mini_collapse">
					 <cart data-products="system.products" data-totals="system.totals" data-template="minicart-default"></cart>
					  <div style="background:#f8f7f7; padding: 1em; text-align: center">
						    <a href="/cart" class="btn btn-sm btn-default" style="text-align: center">Go to Checkout</a>
						</div>
					  <a class="mini-close" ng-click="system.mini_collapse = true">CLOSE</a>
		</div><!-- end mini-cart -->
					  
					 
				 
			 
	<div menu id="slideout_buy" data-spy="affix" data-offset-top="33" collapse="one" class="collapse hidden-xs slideout">
		  <div class="container">
			  <div class="col-xs-12" ng-mouseleave="one = true">
			  	 <div class="slideout_pane" >
					 <div class="col-xs-4 col-lg-3">
						 <h6>Buy FitAID</h6>
						 <p>48 and 24 packs</p>
						 <a href="/buy"><img src="/media/nav_img1.jpg"></a>
						  <ul>
							  <li> <a href="/apo">Apo - We Thank You</a></li>
							  <li> <a href="/buy/#partyaid">Buy PartyAID</a></li>
							  <li> <a href="/buy/#golferaid">Buy GolferAID</a></li>
						  </ul>
					 </div>
					
					 <div class="col-lg-1 visible-lg-block"></div>
					
					 <div class="col-xs-4 col-lg-3">
						<h6>Join the FitClub</h6>
						<p>Get FREE Merchandise</p>
						<a href="/fitclub"><img src="/media/nav_img2.jpg"></a>
						<ul>
						  <li> <a href="/fitclub">Get Free Merchandise!</a></li>
						  <li> <a href="/fitclub">Monthly, Every 2 Months and Every 3 Months Delivery Options</a></li>
						</ul>
					 </div>
					
					 <div class="col-lg-1 visible-lg-block"></div>
					
					 <div class="col-xs-4 col-lg-3">
						<h6>FitAID Gear</h6>
						<p>Rock FITAID at your GYM</p>
						<a href="/buy/#gear"><img src="/media/header-feature-img-fitaid-gear.jpg"></a>
						<ul>
						  <li> <a href="/buy/#gear">Shirts, Tanks & Hoodies</a></li>
						  <li> <a href="/buy/#gear">Hats & Power Wraps</a></li>
						  <li> <a href="/buy/#gear">See All</a></li>
						</ul>
					 </div>
					 <div class="clearfix"></div>
				</div>
				
				<div class="clearfix"></div>
				<div class="slideout_footer">
					<div class="col-sm-12">
					
					<?php 
					      $reviews = new WP_Query( 'category_name=reviews&posts_per_page=1&orderby=rand' ); 
					      while ( $reviews->have_posts() ) : $reviews->the_post(); 
					?>
						<h7><?php the_title(); ?><span style="color: #e7ef0b"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span></span></h7>
						<p><?php the_content(); ?></p>
					<?php 
						endwhile; 
						wp_reset_postdata();
					 ?>
										 
										
					</div>
					<div class="clearfix"></div>
			    </div>
			 </div>
		</div>
	</div>
	
	
	
	<div menu id="slideout_how" data-spy="affix" data-offset-top="33" collapse="two" class="collapse hidden-xs slideout">
		  <div class="container">
			  <div class="col-xs-12" ng-mouseleave="two = true">
			  	 <div class="slideout_pane">
					 <div class="col-xs-4 col-lg-3">
					 <h6>Performance & Recovery</h6>
				 
					 
					  <ul>
					  <li> <a class="fbox" href="https://www.youtube.com/watch?v=FhuaYAUAdbQ">How it Works Video</a></li>
					  <li> <a href="/ingredients/">Ingredients list</a></li>
					  <li> <a href="/faq/">Faq</a></li>
					 </div>
					 <div class="col-lg-1 visible-lg-block"></div>
					 <div class="col-xs-4 col-lg-3">
					  <h6>Product Reviews</h6>
					 
					  <ul>
					  <li> <a href="/reviews">What People Are Saying</a></li>
					  <li> <a href="/love-us-write-us-a-review/">Love Us? Write Us A Review</a></li>
					  
					 </div>
					 <div class="col-lg-1 visible-lg-block"></div>
					 <div class="col-xs-4 col-lg-3">
					  <h6>Paleo Friendly</h6>
					 
					  
					  <ul>
					  <li> <a href="/what-is-paleo-friendly/">What is Paleo Friendly?</a></li>
					  <li> <a target="_blank" href="https://www.lurongliving.com/challenge/">Lurong Paleo Challenge Approved</a></li>
					   
					 </div>
					 <div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
				<div class="slideout_footer">
					<div class="col-sm-4">
					<a href="/lurong-paleo-challenge-approved/"><img src="/media/lurong_graphic.jpg"></a>
					</div>
					<div class="col-sm-8">
					<h6>LURONG PALEO CHALLENGE APPROVED</h6>
					<p></p>
					<p>We've been vetted by Lurong Paleo Challenge. So if you are planning on doing a Lurong Paleo Challenge or a Paleo challenge at your gym, FITAID is the drink of choice!  FITAID is made with natural botanical ingredients, only 45 calories and packed with nearly 3 grams of supplements to AID YOUR WORKOUT.</p>
					</div>
					<div class="clearfix"></div>
			    </div>

			 </div>
		</div>
	</div>
	
	<div menu id="slideout_athletes" data-spy="affix" data-offset-top="33" collapse="three"  class="collapse hidden-xs slideout">
		  <div class="container">
			  <div class="col-xs-12" ng-mouseleave="three = true">
			  	 <div class="slideout_pane">
					 <div class="col-xs-3">
						 <h6>Games Athletes</h6>
						 <?php wp_nav_menu( array('theme_location' => 'athletes-main-1', 'container' => false) ); ?>
					 </div>
					 <div class="col-xs-3">
						 <h6>Regional Competitors</h6>
					  	<?php wp_nav_menu( array('theme_location' => 'athletes-main-2', 'container' => false) ); ?>
					 </div>
					 <div class="col-xs-3">
						 <h6>Masters</h6>
						 <?php wp_nav_menu( array('theme_location' => 'athletes-main-3', 'container' => false) ); ?>
					 </div>
					 <div class="col-xs-3">
					 <h6>OCR</h6>
						 <?php wp_nav_menu( array('theme_location' => 'athletes-main-4', 'container' => false) ); ?>
					 </div>
					 <div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
					<div class="slideout_footer">
					<div class="col-sm-2"><img src="/media/noah.jpg"></div>
					<div class="col-sm-10">
						<h6>NOAH OHLSEN</h6>
							<p>Becoming a FITAID athlete has been amazing!  Not only does their product taste amazing, it also helps with my recovery.  There's nothing better than reaching for an ice cold FITAID after a tough workout. Great company, great products and finally something I can stand behind.</p>
							<p>Noah Ohlsen - Top 10 Games competitor</p>

 						<p><a class="arrow_link" href="/athletes">READ MORE</a></p>

					 
					</div>
					<div class="clearfix"></div>
			    </div>

			 </div>
		</div>
	</div>
	
	
	<div menu id="slideout_about" data-spy="affix" data-offset-top="33" collapse="five"  class="collapse hidden-xs slideout">
		  <div class="container">
			  <div class="col-xs-12" ng-mouseleave="five = true">
			  	 <div class="slideout_pane">
					 <div class="col-xs-4 col-lg-3">
					 <h6>About FitAID</h6>
				 
					 
					  <ul>
					  <li> <a href="/about/">FitAID / LifeAID Beverage Co.</a></li>
					  <li> <a href="/partners/">Partners</a></li>
					  <li> <a href="/category/press/">Press</a></li>
					 </ul>
					 </div>
					 <div class="col-lg-1 visible-lg-block"></div>
					 <div class="col-xs-4 col-lg-3">
					  <h6>Sponsorship</h6>
					 
					  <ul>
					  <li> <a href="/event-sponsorship">Are you holding an event?</a></li>
					  <li> <a href="/athlete-sponsorship">Are you an athlete?</a></li>
					  
					 </div>
					 <div class="col-lg-1 visible-lg-block"></div>
					 <div class="col-xs-4 col-lg-3">
					  <h6>General Info</h6>
					 
					  
					  <ul>
					  <li> <a href="/distributors">Current distributors</a></li>
					  <li> <a href="/get-fitaid-at-your-firehouse">Get FitAID at your Firehouse</a></li>
					  <li> <a href="/our-cans">Our Cans</a></li>
					 
					   <li> <a href="/contact">Contact</a></li>
					  <li> <a href="/contact">Customer Support</a></li>
					  
					 </div>
					 <div class="clearfix"></div>
				</div>
			 </div>
		</div>
	</div>
	
	<div menu id="slideout_lifeaid" data-spy="affix" data-offset-top="33" collapse="six"  class="collapse hidden-xs slideout">
		  <div class="container">
			  <div class="col-xs-12" ng-mouseleave="six = true">
			  	 <div class="slideout_pane">
					 <div class="col-xs-4 col-lg-3">
					 <h6>Our Other Products</h6>
					 <p>It's not just FitAID</p>
					 <img src="/media/nav_img5.jpg">
					  <ul>
						  <li> <a href="/our-products/#partyaid">PartyAID</a></li>
						  <li> <a href="/our-products/#golferaid">GolferAID</a></li>
						  <li> <a href="/our-products/#fitaid">FitAID</a></li>
					  </ul>
					  
					 </div>
					 <div class="col-lg-1 visible-lg-block"></div>
					 <div class="col-xs-4 col-lg-3">
					  <h6>Coming Soon</h6>
					  <p>And there's more</p>
					  <img src="/media/nav_img6.jpg">
					  <ul>
						  <li> <a href="/our-products/#lifeaid">LifeAID</a></li>
						  <li> <a href="/our-products/#focusaid">FocusAID</a></li>
						  <li> <a href="/our-products/#travelaid">TravelAID</a></li>
					  </ul>
					  
					 </div>
					 <div class="col-lg-1 visible-lg-block"></div>
					 <div class="col-xs-4 col-lg-3">
					  <h6>LifeAID Beverage Co.</h6>
					  <p>See what we're all about</p>
					  <img src="/media/nav_img7.jpg">
					  <ul>
						  <li> <a href="/about">About Us</a></li>
						  
						   
					  </ul>
					  
					 </div>
					 <div class="clearfix"></div>
				</div>
			 </div>
		</div>
	</div>



 
   
   
	 <nav id="nav" data-spy="affix" data-offset-top="33" class="navbar navbar-default" role="navigation">
	 
	 	 
	 <div style="overflow:visible; position: relative;" class="container">
		
		 
		    <div  class="navbar-header col-xs-12 col-sm-5 col-lg-6">
			 <a href="/cart"  id="mini-cart_mobile" class="visible-xs-block" >
			 <span ng-show="system.totals.items" class="mini badge">{{system.totals.items}}</span>
			   <img  width="33" height="33" src="/media/mini_cart.png">
			   </a>
			  
	              
	              <button type="button" class="navbar-toggle collapsed" ng-click="main_collapse = !main_collapse">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
			      </button>
			      <a class="navbar-brand normal" href="/"><img width="136" src="/media/fitaid_logo.png"> 	</a>
			      <a class="navbar-brand aff" href="/affiliates"><img width="136" src="/media/fitaid_logo.png"> 	</a>
		    </div><!-- end navbar-header -->
		
		 
			<div style="padding-top: 10px; text-align: right;" class="col-sm-7 col-lg-6 pull-right hidden-xs">
				<iframe src="//<?php echo API_HOST ?>/social/facebook_desktop.html" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:87px; height:21px;" allowTransparency="true"></iframe>
		        <iframe src="//<?php echo API_HOST ?>/social/instagram_desktop.html" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:84px; height:21px;" allowTransparency="true"></iframe>
				<iframe src="//<?php echo API_HOST ?>/social/twitter_desktop.html" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:150px; height:21px;" allowTransparency="true"></iframe>
		    </div>
		   
		  
		    <div menu class="collapse navbar-collapse col-sm-6" ng-init="main_collapse = true" collapse="main_collapse" id="fa-header-navbar">
				  <ul class="nav navbar-nav">
				   
				    <li class="aff"><a class="hidden-xs" ng-click="affiliatesGetStarted()">Become An Affiliate</a></li>
				    <li class="aff" style="width: 400px;"><a class="hidden-xs" href="/reorders">Reorders</a></li>
				 
				   
				    
				    <li class="aff"><a class="visible-xs-block" href="/affiliates-start">Become An Affiliate</a></li>
				    <li class="aff"><a class="visible-xs-block" href="/reorders">Reorders</a></li>
				    
				    <li ng-class="{active: !one}"><a class="normal hidden-xs" ng-click="slideout('one');" >Buy Now</a></li>
				    <li ng-class="{active: !two}"><a class="normal hidden-xs" ng-click="slideout('two');" >How It Works</a></li>
				    <li ng-class="{active: !three}"><a class="normal hidden-xs" ng-click="slideout('three');">Athletes</a></li>
				    <li><a id="storeLocatorBtn" class="normal hidden-xs" ng-click="system.modal('sponsor_type')" >Sponsorship</a></li>
				    
				    <li ng-class="{active: !five}"><a class="normal hidden-xs" ng-click="slideout('five');" >About</a></li>
				    <li ng-class="{active: !six}"><a class="normal hidden-xs" ng-click="slideout('six');" >LifeAID Beverage Co.</a></li>
				    
				    <li><a class="col-xs-6 normal visible-xs-block" href="/buy">BUY FITAID</a></li>
				    <li><a class="col-xs-6 normal visible-xs-block" href="/fitclub">FITCLUB</a></li>
				    <li><a class="col-xs-6 normal visible-xs-block" href="/buy#gear">FITAID GEAR</a></li>
				    <li><a class="col-xs-6 normal visible-xs-block" href="/apo">APO</a></li>
				    <li><a class="col-xs-6 normal visible-xs-block" href="/athletes">ATHLETES</a></li>
				    <li><a class="col-xs-6 normal visible-xs-block" ng-click="system.modal('sponsor_type')">SPONSORSHIP</a></li>
				    
				    <li><a class="col-xs-6 normal visible-xs-block" href="/start">START SELLING FITAID</a></li>
				    
				    <li><a class="col-xs-6 normal visible-xs-block" href="/refer-your-gym">REFER YOUR GYM</a></li>
				    <li><a class="col-xs-6 normal visible-xs-block" href="/reviews">REVIEWS</a></li>
				    <li><a class="col-xs-6 normal visible-xs-block" href="/what-is-paleo-friendly">PALEO FRIENDLY</a></li>
				     <li><a class="col-xs-6 normal visible-xs-block" href="/contact">Contact</a></li>
				    












			 
				    
				    
				</ul>
			</div><!-- /.navbar-collapse -->
		 
		   
		  
  		 


			
		    <div  class="col-xs-1 pull-right hidden-xs" >
			  	
			  	<a ng-click="system.mini_collapse = !system.mini_collapse" id="mini-cart-btn">
			  		  <span ng-show="system.totals.items" class="mini badge">{{system.totals.items}}</span>
			  		<img width="33" height="33" src="/media/mini_cart.png">
			  	</a>
			   
				<!-- NEW my account icon -->
			 	<a ng-show="system.customer" ng-click="system.acct_collapse = !system.acct_collapse" class="hidden-xs hidden-sm icon-my-account-wrapper" > 
				 	
				<div class="icon-wrapper"><img src="/media/icon-my-account.png"></div><p>My Account</p>
				</a>
				<!-- END my account icon -->
			   
			   
			  	        </div>
         
         

	 	   <div  class="col-xs-2 visible-md-block visible-lg-block pull-right header-socials-wrapper">
				<ul class="list-inline header-socials">
	 				<li>
						<a class="socials-instagram" href="//instagram.com/fitaid" target="_blank"><i class="fa fa-instagram"></i></a>
					</li>
	 				<li>
						<a class="socials-twitter" href="//twitter.com/drinkfitaid" target="_blank"><i class="fa fa-twitter"></i></a>
					</li>
	 				<li>
						<a class="socials-facebook" href="//www.facebook.com/fitaid" target="_blank"><i class="fa fa-facebook"></i></a>
					</li>
	 				<li>
						<a class="socials-google-plus" href="//plus.google.com/113134579543723671972" target="_blank"><i class="fa fa-google-plus"></i></a>
					</li>
	 				<li>
						<a class="socials-youtube" href="//www.youtube.com/user/DrinkFitAid" target="_blank"><i class="fa fa-youtube"></i></a>
					</li>
 				</ul>
				
	 	   </div>
		  
	
	  </div><!-- end container -->
	  </nav><!-- end nav -->
    
 
	
	<div id="main_content">
	
	  
	 