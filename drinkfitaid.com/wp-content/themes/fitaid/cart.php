<?php
/* Template Name: Cart Page */
 get_header(); ?>
 			
 			<div class="mobile_shiv visible-xs-block"></div>
		 	<div class="desktop_shiv hidden-xs"></div>
	
	<div class="container">
	<div class="copy_content">
		
			<?php include('includes/reviews.php'); ?>
				
			<div style="padding-bottom: 2em;" class="col-sm-8 col-md-9">
				 <?php 
				if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<h3><?php the_title();?></h3>
					<p><?php the_content();?></p>
				<?php endwhile; else : ?><?php endif; ?>
				
					
					
					<cart data-products="system.products" data-totals="system.totals" data-validate="system.validate()" data-template="cart-default"></cart>
			
			
				 
					<div ng-show="system.totals.items"  style="text-align: right;">
			
						
						
						<div class="row">
							
							<div class="col-xs-12 col-sm-12 col-md-5 col-lg-4 pull-right">
							<a class="btn btn-lg btn-gray btn-block" href="/buy">
								<span class="text"><span class="glyphicon glyphicon-shopping-cart"></span> Continue Shopping</span> 
							</a>
							</div>
							
							<div style="padding: 1em;" class="visible-xs-block visible-sm-block clearfix"></div>
							
							<div class="col-xs-12 col-sm-12 col-md-5 col-lg-4 pull-right">
							<a class="btn btn-lg btn-default btn-block" href="/checkout">
								<span class="text"><span class="glyphicon glyphicon-lock"></span> Secure Checkout</span> 
							</a>
							</div>
						</div>
					</div>

			
			<div class="clearfix"></div>
			</div>
			
			<div class="col-sm-4 col-md-3">
			<div class="well well-lg">
			<ul>
				<li>
					<h5>Help</h5>
					<p>If you experience any problem with your order please contact customer service at
					<a href="mailto:support@lifeaidbevco.com">support@lifeaidbevco.com</a>
					or by calling
					1-888-558-1113</p>
					
				</li>

 
				<li>
					<h5>Returns</h5>
				
					<p>You may return your unused product for a full refund within 30 days of purchase date.</p>
					
				</li>
			
				<li>
					<h5>Shipping</h5>
					<p>All orders typically ship within 2 business days once the order has been placed. Orders ship Monday through Friday excluding holidays.</p>
					
				</li>
				
		   </ul>
		   
			</div>
			</div>
			
							
			</div><!-- end copy_content -->



	</div><!-- end container -->
	</div>
<?php get_footer(); ?>
	  
	  
	  
	  
