<?php
/* Template Name: Affiliates Get Started */
 include 'affiliates-header.php'; ?>
 			
 			    <div class="mobile_shiv visible-xs-block"></div>
	<div class="desktop_shiv hidden-xs"></div>
	
		<div id="slider">
			<carousel  interval="5000">
		 
				
				
				<slide>
				<img src="/media/affiliates_slide1-v2.jpg" >
				</slide>
				<slide >
				<img src="/media/affiliates_JoinBoxOwners4b.jpg" >
				</slide>
				<slide >
				<img src="/media/affiliates_slide3-v2.jpg" >
				</slide>
				<slide >
				<img src="/media/affiliates_Instagram2.jpg" >
				</slide>
				<slide >
				<a href="//itunes.apple.com/us/podcast/43-get-10x-more-money-happiness/id675777894?i=314152198&mt=2" target="_blank"><img src="/media/affiliates_Podcast-v2.jpg" ></a>
				</slide>
			</carousel>
		</div>		 	
	<div class="container">
	<div class="copy_content">
		
			<?php include('includes/reviews.php'); ?>
				
			<div style="padding-bottom: 2em;" class="col-sm-8 col-md-9">
				 <?php 
				if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<h3><?php the_title();?></h3>
					<p><?php the_content();?></p>
				<?php endwhile; else : ?><?php endif; ?>	
					
					<cart data-products="system.products" data-totals="system.totals" data-template="cart-default"></cart>
				 
					<div ng-show="system.totals.items" style="text-align: right;">
				
						 
						 <a class="btn btn-lg btn-default" href="/affiliates-checkout">
							<span class="text"><span class="glyphicon glyphicon-lock"></span> Secure Checkout</span> 
						 </a>
	 					
					</div>
			
			<div class="clearfix"></div>
			</div>
			
			<div class="col-sm-4 col-md-3">
			<div class="well well-lg">
			<ul>
				<li>
					<h5>Help</h5>
					<p>If you experience any problem with your order please contact customer service at
					<a href="mailto:support@lifeaidbevco.com">support@lifeaidbevco.com</a>
					or by calling
					1-888-558-1113</p>
					
				</li>

 
				<li>
					<h5>Returns</h5>
				
					<p>You may return your unused product for a full refund within 30 days of purchase date.</p>
					
				</li>
			
				<li>
					<h5>Shipping</h5>
					<p>All orders typically ship within 2 business days once the order has been placed. Orders ship Monday through Friday excluding holidays.</p>
					
				</li>
				
		   </ul>		   
			</div>
			</div>
			
							
			</div><!-- end copy_content -->



	</div><!-- end container -->
	</div>
<?php  include 'affiliates-footer.php'; ?>
	  
	  
	  
	  
