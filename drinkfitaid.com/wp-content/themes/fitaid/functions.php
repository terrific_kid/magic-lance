<?php

// add editor the privilege to edit theme

// get the the role object
$role_object = get_role( 'editor' );

// add $cap capability to this role object
$role_object->add_cap( 'edit_theme_options' );





require_once dirname(ABSPATH) . '/config/global.php';

// Thumbnails
add_theme_support('post-thumbnails');
add_image_size('athlete-large', 566, 580, false);

// Menus
add_action('init', 'fitaid_register_menus');

function fitaid_register_menus() {
	register_nav_menu('athletes-main', __('Athletes Page Menu'));
	register_nav_menu('athletes-main-1', __('Games Athletes Menu'));
	register_nav_menu('athletes-main-2', __('Regional Competitors Menu'));
	register_nav_menu('athletes-main-3', __('Masters Menu'));
	register_nav_menu('athletes-main-4', __('OCR Menu'));
	
	register_nav_menu('buy-menu', __('Buy Page Menu'));
	register_nav_menu('apo-menu', __('APO Page Menu'));
	register_nav_menu('aff-menu', __('Affiliates Page Menu'));
}

// Custom post types
include_once get_template_directory() . '/custom-posts/_athletes_custom_post.php';

// Custom template types
include_once get_template_directory() . '/custom-templates/_twocans.php';