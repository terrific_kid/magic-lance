<?php
/* Template Name: Athlete Sponsorship */
?>
 <?php get_header(); ?>
	<div class="container event-sponsorship-wrapper">
		
		<div class="row add-header-height">
			<div class="col-md-9">
				<h1>Athlete Sponsorship</h1>
				
				<?php if(isset($_REQUEST['success'])) : ?>
					<p>
						<strong>Thank you for your interest in FITAID.</strong> Please note that we get hundreds of requests a month and it may take a bit of time to get back to you. We'll respond as soon as we can and we wish you luck on joining team FITAID!
						<br/><br/>
						Team FitAID
					</p>
				<?php else : ?>
				
				 
					<br/>
					<div class="col-sm-9">
					<form accept-charset="UTF-8" action="https://it140.infusionsoft.com/app/form/process/7ee259e2e5d417a9639599daef591a6c" class="infusion-form" method="POST">
    <input name="inf_form_xid" type="hidden" value="7ee259e2e5d417a9639599daef591a6c" />
    <input name="inf_form_name" type="hidden" value="Athlete Sponsorship" />
    <input name="infusionsoft_version" type="hidden" value="1.40.0.41" />
    <div class="form-group">
        
        <input placeholder="First Name" class="form-control" id="inf_field_FirstName" name="inf_field_FirstName" type="text" />
    </div>
    <div class="form-group">
       
        <input  placeholder="Last Name" class="form-control" id="inf_field_LastName" name="inf_field_LastName" type="text" />
    </div>
    <div class="form-group">
      
        <input  placeholder="Email" class="form-control" id="inf_field_Email" name="inf_field_Email" type="text" />
    </div>
    <div class="form-group">
       
        <input  placeholder="Phone #" class="form-control" id="inf_field_Phone1" name="inf_field_Phone1" type="text" />
    </div>
    <div class="form-group">
        <input placeholder="Sport" class="form-control" id="inf_custom_Sport" name="inf_custom_Sport" type="text" />
    </div>
    <div class="form-group">
        <input placeholder="Major Sport Accomplishments" class="form-control" id="inf_custom_MajorSportAccomplishments" name="inf_custom_MajorSportAccomplishments" type="text" />
    </div>
    <div class="form-group">
        <input placeholder="URL to Instagram" class="form-control" id="inf_custom_URLtoInstagram" name="inf_custom_URLtoInstagram" type="text" />
    </div>
    <div class="form-group">
        <input placeholder="Instagram handle" class="form-control" id="inf_custom_Instagramhandle" name="inf_custom_Instagramhandle" type="text" />
    </div>
    <div class="form-group">
        <input placeholder="# Of Instagram Followers" class="form-control" id="inf_custom_OfInstagramFollowers" name="inf_custom_OfInstagramFollowers" type="text" />
    </div>
    <div class="form-group">
        <input placeholder="URL to Twitter" class="form-control" id="inf_custom_URLtoTwitter" name="inf_custom_URLtoTwitter" type="text" />
    </div>
    <div class="form-group">
        <input placeholder="Twitter Handle" class="form-control" id="inf_custom_TwitterHandle" name="inf_custom_TwitterHandle" type="text" />
    </div>
    <div class="form-group">
        <input placeholder="# Of Twitter Followers" class="form-control" id="inf_custom_OfTwitterFollowers" name="inf_custom_OfTwitterFollowers" type="text" />
    </div>
    <div class="form-group">
        <input placeholder="URL to Facebook" class="form-control" id="inf_custom_URLtoFacebook" name="inf_custom_URLtoFacebook" type="text" />
    </div>
    <div class="form-group">
        <input placeholder="Facebook Page" class="form-control" id="inf_custom_FacebookPage" name="inf_custom_FacebookPage" type="text" />
    </div>
    <div class="form-group">
        <input placeholder="# Of Facebook Page Likes" class="form-control" id="inf_custom_OfFacebookPageLikes" name="inf_custom_OfFacebookPageLikes" type="text" />
    </div>
    <div class="form-group">
        
        <input placeholder="When did you first start Crossfit/OCR" class="form-control" id="inf_custom_WhendidyoufirststartCrossfitOCR" name="inf_custom_WhendidyoufirststartCrossfitOCR" type="text" />
    </div>
    <div class="form-group">
       
        <input  placeholder="Regional Placement" class="form-control" id="inf_custom_RegionalPlacement" name="inf_custom_RegionalPlacement" type="text" />
    </div>
    <div class="form-group">
        
        <input placeholder="CFGames Placement" class="form-control" id="inf_custom_CFGamesPlacement" name="inf_custom_CFGamesPlacement" type="text" />
    </div>
    <div class="form-group">
        
        <input placeholder="Spartan Race World Ranking" class="form-control" id="inf_custom_SpartanRaceWorldRanking" name="inf_custom_SpartanRaceWorldRanking" type="text" />
    </div>
   
    <div class="form-group">
        <textarea placeholder="Other Notable Info" class="form-control" cols="24" id="inf_custom_OtherNotableInfo" name="inf_custom_OtherNotableInfo" rows="5"></textarea>
	</div>
    <div class="form-group">
       
        <input  placeholder="Box/Gym Name" class="form-control" id="inf_custom_BoxGymName" name="inf_custom_BoxGymName" type="text" />
    </div>
    <div class="form-group">
        
        <input placeholder="Current Job Title" class="form-control" id="inf_custom_JobDescription" name="inf_custom_JobDescription" type="text" />
    </div>
    <div class="form-group">
        
        <input placeholder="Address 1" class="form-control" id="inf_field_StreetAddress1" name="inf_field_StreetAddress1" type="text" />
    </div>
    <div class="form-group">
        
        <input placeholder="Address 2" class="form-control" id="inf_field_StreetAddress2" name="inf_field_StreetAddress2" type="text" />
    </div>
    <div class="form-group">
        
        <input placeholder="City" class="form-control" id="inf_field_City" name="inf_field_City" type="text" />
    </div>
    <div class="form-group">
      
        <input   placeholder="State" class="form-control" id="inf_field_State" name="inf_field_State" type="text" />
    </div>
    <div class="form-group">
       
        <input placeholder="Postal Code" class="form-control" id="inf_field_PostalCode" name="inf_field_PostalCode" type="text" />
    </div>
    <div class="infusion-submit">
        <button type="submit" class="btn btn-lg btn-primary" />Submit</button>
    </div>
</form>
</div>
<script type="text/javascript" src="https://it140.infusionsoft.com/app/webTracking/getTrackingCode?trackingId=1eca5d474ef677342484077f73888626"></script>

				<?php endif; ?>
				
			</div><!-- END col-md-9 -->	
			<div class="col-md-3 col-sm-6">
				<?php include 'sidebar.php';?>
			</div>
		</div><!-- END row -->	
		
	</div><!-- end container -->
<?php get_footer(); ?>