<?php
/* Template Name: Affiliates Reorders Page */
include 'affiliates-header.php'; ?>
 
    <div class="mobile_shiv visible-xs-block"></div>
	<div class="desktop_shiv hidden-xs"></div>
	
		<div id="slider">
			<carousel  interval="5000">
				<slide>
					<img src="/media/reorder_slide1-v2.jpg" >
				</slide>
				<slide>
					<img src="/media/affiliates_20CaseFreeShipping-v2.jpg" >
				</slide>
				<slide >
					<img src="/media/affiliates_slide3-v2.jpg" >
				</slide>
				<slide >
				<a href="//itunes.apple.com/us/podcast/43-get-10x-more-money-happiness/id675777894?i=314152198&mt=2" target="_blank"><img src="/media/affiliates_Podcast-v2.jpg" ></a>
				</slide>
			</carousel>
		</div>

	<div class="subnav">
	<?php wp_nav_menu(array('theme_location'=>'aff-menu', 'container_class'=>'container', 'menu_class' => 'col-xs-12')); ?>
	</div>
	
	<div class="container">
	<div class="copy_content">
		 
		 
		<?php include('includes/reviews.php'); ?>
 
		
			<product data-object="system.products['fitaid-24-pack-wholesale']" data-template="product-case" data-strict="wholesale"></product> 			 
			<product data-object="system.products['partyaid-24-pack-wholesale']" data-template="product-case" data-strict="wholesale"></product> 			 
			<product data-object="system.products['golferaid-24-pack-wholesale']" data-template="product-case" data-strict="wholesale"></product> 			 
			
			<div class="clearfix"></div>
			<div class="col-xs-12"><h1 style="padding: 16px; color: white; background:  #e51b24;">AUTO SHIP OPTIONS.  AUTOMATICALLY GET FITAID, PARTYAID & GOLFERAID SHIPPED TO YOUR BOX MONTHLY</h1></div>
			<div class="clearfix"></div>
			<product data-object="system.products['fitaid-24-pack-wholesale-autoship']" data-template="product-case" data-strict="wholesale"></product> 			 
			<product data-object="system.products['partyaid-24-pack-wholesale-autoship']" data-template="product-case" data-strict="wholesale"></product> 			 
			<product data-object="system.products['golferaid-24-pack-wholesale-autoship']" data-template="product-case" data-strict="wholesale"></product> 			 
			
			<div class="clearfix"></div>
				
				
	</div>
	</div>
	
	
<div class="contaier-fluid heading">
 <div style="background-image: url(/media/merch_header.jpg);" class="hero">
			 <div class="wrap"> <h2>Gear</h2> </div>
 </div>
</div>


 	<div class="container">
	<div class="copy_content">
				
				

<a name="gear"></a>
<div class="row">
	<product data-object="system.products['fitaid-punchcards-40pack']" data-template="product-default" data-strict="wholesale"></product>
	<product data-object="system.products['fitaid-black-tshirt-wholesale']" data-template="product-default" data-strict="wholesale"></product>
	<product data-object="system.products['fitaid-red-tshirt-wholesale']" data-template="product-default" data-strict="wholesale"></product>
</div>
<div class="row">
	<product data-object="system.products['fitaid-blue-tshirt-wholesale']" data-template="product-default" data-strict="wholesale"></product>
	<product data-object="system.products['fitaid-gray-snaphat-wholesale']" data-template="product-default" data-strict="wholesale"></product>
	<product data-object="system.products['fitaid-bluewhite-snaphat-wholesale']" data-template="product-default" data-strict="wholesale"></product>
</div>
<div class="row">
	<product data-object="system.products['fitaid-red-snaphat-wholesale']" data-template="product-default" data-strict="wholesale"></product>
	<product data-object="system.products['fitaid-white-snaphat-wholesale']" data-template="product-default" data-strict="wholesale"></product>
	<product data-object="system.products['fitaid-pinkwhite-snaphat-wholesale']" data-template="product-default" data-strict="wholesale"></product>
</div>
<div class="row">
	<product data-object="system.products['fitaid-black-snaphat-wholesale']" data-template="product-default" data-strict="wholesale"></product>
	<product data-object="system.products['fitaid-red-wraps-wholesale']" data-template="product-default" data-strict="wholesale"></product>
	<product data-object="system.products['fitaid-black-wraps-wholesale']" data-template="product-default" data-strict="wholesale"></product>
	
</div>
<div class="row">
	<product data-object="system.products['fitaid-blue-wraps-wholesale']" data-template="product-default" data-strict="wholesale"></product>
	<product data-object="system.products['fitaid-pink-wraps-wholesale']" data-template="product-default" data-strict="wholesale"></product>
	<product data-object="system.products['fitaid-gray-hoodie-wholesale']" data-template="product-default" data-strict="wholesale"></product>
	</div>
<div class="row">
	<product data-object="system.products['fitaid-black-hoodie-wholesale']" data-template="product-default" data-strict="wholesale"></product>
	<product data-object="system.products['fitaid-red-triblend-tank-wholesale']" data-template="product-default" data-strict="wholesale"></product>
	<product data-object="system.products['fitaid-black-triblend-tank-wholesale']" data-template="product-default" data-strict="wholesale"></product>
</div>
<div class="row">
	<product data-object="system.products['fitaid-tahitianblue-triblend-tank-wholesale']" data-template="product-default" data-strict="wholesale"></product>
	<product data-object="system.products['fitaid-fridge']" data-template="product-default" data-strict="wholesale"></product>
	<product data-object="system.products['fitaid-stickers-10pack']" data-template="product-default" data-strict="wholesale"></product>
</div>
<div class="row">
	<product data-object="system.products['fitaid-fridge-stand']" data-template="product-default" data-strict="wholesale"></product>
	<product data-object="system.products['fitaid-fridge-lock']" data-template="product-default" data-strict="wholesale"></product>
	<product data-object="system.products['fitaid-banner']" data-template="product-default" data-strict="wholesale"></product>
</div>
<div class="row">
	<product data-object="system.products['jackie-perez-poster']" data-template="product-default" data-strict="wholesale"></product>
	<product data-object="system.products['neal-maddox-poster']" data-template="product-default" data-strict="wholesale"></product>
</div>




	</div><!-- end container -->
	</div>
<?php  include 'affiliates-footer.php'; ?>
	  
	  
	  
	  
