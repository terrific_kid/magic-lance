<script type="text/ng-template" id="start_type">
			<div class="modal-header">
				<button type="button" class="close" ng-click="ok()"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Facility Type</h4>
			</div><!-- end modal-header -->
			<div class="modal-body">
				
			
				<form role="form">
					<div class="form-group">
					<a style="white-space: normal;" href="/start" class="btn btn-primary btn-lg btn-block">I am a CrossFit&reg; Box Owner &raquo;</a>
					</div>
					
					<div class="form-group">
					<a style="white-space: normal;" href="/gyms" class="btn btn-primary btn-lg btn-block">I am a Gym / Fitness Center / Yoga Owner &raquo;</a>
					</div>
					
					<div class="form-group">
					<a style="white-space: normal;" href="/professionals" class="btn btn-primary btn-lg btn-block">I am a Treating Professional (DC, MD, PT, LAc) &raquo;</a> 
					</div>
					
					<div class="form-group">
					<a style="white-space: normal;" href="/grocery" class="btn btn-primary btn-lg btn-block">I am Grocery / Convenience Store Owner &raquo;</a>
					</div>
					
					<div class="form-group">
					<a style="white-space: normal;" href="/refer-your-gym" class="btn btn-primary btn-lg btn-block">None of the above but I'd like to refer my gym &raquo;</a>
					</div>
				</form>	 					
			
				
			
			</div>
			<div class="modal-footer"></div>
</script>





 

<script type="text/ng-template" id="enter_to_win">
<form accept-charset="UTF-8" action="https://it140.infusionsoft.com/app/form/process/e948cbf3c504c90095bfaae966853d15" class="infusion-form" method="POST">
						
			<div class="modal-header">
				<button type="button" class="close" ng-click="ok()"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Enter to Win a 1 Year Supply of FitAID</h4>
			</div><!-- end modal-header -->
			<div class="modal-body">
				
				<div  class="hidden-xs col-sm-3 pull-right"><img src="/media/enter_to_win.jpg"></div>
					<div class="col-sm-9">
						    <input name="inf_form_xid" type="hidden" value="e948cbf3c504c90095bfaae966853d15" />
						    <input name="inf_form_name" type="hidden" value="DrinkFitAID.com Entry &#a;Lead" />
						    <input name="infusionsoft_version" type="hidden" value="1.35.0.43" />
						    
							 <div class="form-group">
							 <input name="inf_field_FirstName" type="text" class="form-control" placeholder="First Name">
							 </div>
							  <div class="form-group">
							 <input name="inf_field_Email" type="text" class="form-control" placeholder="Email">
							 </div>
							
								 <div class="form-group">
								 <input name="inf_field_PostalCode" type="text" class="form-control" placeholder="Zip">
								 </div>
							 
						
			 	</div>
			 	
			 
			</div>
			<div class="clearfix"></div>
			<div class="modal-footer">
				<div class="row">
								<div class="col-sm-6 form-group">
								 <button ng-click="flags.enter_to_win = 1; ok();" type="submit" class="btn btn-primary btn-lg btn-block">Enter to Win</button>
								</div>
								<div class="col-sm-6 form-group">
								 <a ng-click="flags.enter_to_win = 1; ok();" class="btn btn-link btn-lg btn-block">No Thanks</a>
								</div>
							 </div>
							 
			</div>
			 </form>
</script>

<script type="text/ng-template" id="sponsor_type">
			<div class="modal-header">
				<button type="button" class="close" ng-click="ok()"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Sponsorship Type</h4>
			</div><!-- end modal-header -->
			<div class="modal-body">
				<form role="form">
					<div class="form-group">
					<a style="white-space: normal;" href="/event-sponsorship" class="btn btn-primary btn-lg btn-block">Event Sponsorship</a>
					</div>
					
					<div class="form-group">
					<a style="white-space: normal;" href="/athlete-sponsorship" class="btn btn-primary btn-lg btn-block">Athlete Sponsorship</a>
					</div>
					
					 
					
				</form>	 					
			</div>
			<div class="modal-footer"></div>
</script>



 
<script type="text/ng-template" id="login-default.html">
        <form class="login-form form-horizontal" autocomplete="off" role="form">
		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title text-center" id="myModalLabel">Sign In</h4>
			</div><!-- end modal-header -->
			
			<div ng-show="panel1" class="modal-body">
				<div class="form-group ">
					<label for="inputEmail3" class="col-sm-4 control-label">New Customer</label>
					<div class="col-sm-7">
						<a ng-click="showCreate()" class="btn btn-default"><span class="glyphicon glyphicon-flag"></span> Click Here</a>
					</div>
				</div>
				
				<hr>
				<div ng-show="error.login" class="alert alert-danger" role="alert">{{error.login}}</div>
				<div class="form-group form-group-lg">
					<label for="inputEmail3" class="col-sm-4 control-label">Email Address</label>
					<div class="col-sm-7">
						<input type="text" class="form-control life_username" ng-model="login.email" placeholder="Email" name="Email">
					</div>
				</div>
				
				<div class="form-group form-group-lg">
					<label for="inputEmail3" class="col-sm-4 control-label">Password</label>
					<div class="col-sm-7">
						<input type="password" class="form-control life_password" ng-model="login.password" placeholder="Password" name="Password"><br>
						<a ng-click="showPassword()" class="small">Forgot your password?</a>
					</div>
				</div>
				
			</div><!-- end modal-body -->
			<div ng-show="panel1" class="modal-footer">
				<div class="form-group">
					<div style="text-align: center;">				
					<lbutton data-trigger="buttons.login" data-click="trylogin()" data-template="lbutton-default">
						<span class="glyphicon glyphicon-pencil"></span> Sign In
					</lbutton>
					</div>
				</div>
			</div><!-- end modal-footer -->  
			
			
			<!-- Modal Create Account  -->
			<div ng-show="panel2" class="modal-body">
			
		 <div ng-show="error.create" class="alert alert-danger" role="alert">{{error.create}}</div>
			<div class="form-group form-group-lg">
					<label for="inputFullName" class="col-sm-4 control-label">First Name</label>
					<div class="col-sm-7">
						<input type="text" class="form-control" ng-model="create.firstName" placeholder="First Name" name="FirstName">
					</div>
				</div>
				
				<div class="form-group form-group-lg">
					<label for="inputFullName" class="col-sm-4 control-label">Last Name</label>
					<div class="col-sm-7">
						<input type="text" class="form-control" ng-model="create.lastName" placeholder="Last Name" name="LastName">
					</div>
				</div>
				
				<div class="form-group form-group-lg">
					<label for="inputEmail3" class="col-sm-4 control-label">Email Address</label>
					<div class="col-sm-7">
						<input type="text" class="form-control" ng-model="create.email" placeholder="Email" name="Email">
					</div>
				</div>
				
				<div class="form-group form-group-lg">
					<label for="inputEmail3" class="col-sm-4 control-label">Create Password</label>
					<div class="col-sm-7">
						<input type="password" class="form-control" ng-model="create.password" placeholder="Password" name="Password">
					</div>
				</div>
				
				<div class="form-group form-group-lg">
					<label for="inputEmail3" class="col-sm-4 control-label">Confirm Password</label>
					<div class="col-sm-7">
						<input type="password" class="form-control" ng-model="create.password2" placeholder="Confirm Password" name="confirmPassword"><br>
					</div>
				</div>
				<div class="form-group">
					<div style="text-align: center;">
						<lbutton  data-trigger="buttons.create" data-click="createAccount();" data-template="lbutton-default">
							<span class="glyphicon glyphicon-pencil"></span> Create Account
						</lbutton>
					</div>
				</div>		
				
			</div><!-- end modal-body -->
			<div ng-show="panel2"  class="modal-footer">		
				<h4 class="modal-title text-center" id="myModalLabel">NOT WHAT YOU WANTED?</h4>
				<div class="form-group text-center">
					<br>
					<a class="btn btn-md btn-gray" href="javascript:history.back();"><span class="text"><span class="glyphicon glyphicon-shopping-cart"></span> Back To Shopping</span></a>
					<a ng-click="showLogin()" class="btn btn-md btn-gray"><span class="text"><span class="glyphicon glyphicon-pencil"></span> Back To Sign In</span></a>
				</div>
			</div><!-- end modal-footer -->  
			<!-- END Modal Create Account  -->
			
				<div ng-show="panel3" class="modal-body">
			
				<div ng-show="error.forgot" class="alert alert-danger" role="alert">{{error.forgot}}</div>
				<div ng-show="success.forgot" class="alert alert-success" role="alert">{{success.forgot}}</div>
				
				<div class="form-group form-group-lg">
					<label for="inputEmail3" class="col-sm-4 control-label">Email Address</label>
					<div class="col-sm-7">
						<input type="text" class="form-control" ng-model="forgot.email" placeholder="Email Address" name="ForgotEmail">
					</div>
				</div>				
			</div><!-- end modal-body -->
			
			<div ng-show="panel3"  class="modal-footer">
				<div class="form-group">
					<div style="text-align: center;">
						<a ng-click="showLogin()" class="btn btn-md btn-gray"><span class="text"><span class="glyphicon glyphicon-pencil"></span> Back To Sign In</span></a>
				
						<lbutton data-trigger="buttons.forgot" data-click="forgotPassword();" data-template="lbutton-default">
							<span class="glyphicon glyphicon-arrow-right"></span> Recover Password
						</lbutton>
					</div>
				</div>
			</div><!-- end modal-footer -->  


			   
        </form>   
    </script>

