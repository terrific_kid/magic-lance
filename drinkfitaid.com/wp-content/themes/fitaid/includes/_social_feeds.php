<?php

/*
Displays social feeds for homeapage.

	$num_items: Number of items you want returned
	$format: 'html' or 'json'. Defaults to html.
	$echo: whether or not to output data or simply return. $echo = FALSE to simply return

*/
if(!function_exists('fitaid_get_social_feed')){

	function fitaid_get_social_feed($num_items = 8, $format = 'html', $echo = TRUE){
	
		$item_limit = ($num_items / 2) + 1;
	
		$instagrams = array(); // Stores instagrams
		$tweets = array(); // Stores tweets

	
		// Instagram ------------------------------------------------------------
		
		$url = "https://api.instagram.com/v1/users/225547548/media/recent/?client_id=724fff4867c8479a923b772cab40e41f&count=$item_limit";
		$json = file_get_contents($url, 0, null, null);
		$result = json_decode($json);
		
		foreach ($result->data as $data){
			$item = new stdClass();
			
			$item->url = $data->link;
			$item->image_src = preg_replace('#^https?:#', '', $data->images->low_resolution->url);
			$item->text = '';
			if ($data->caption && $data->caption->text)
				$item->text = $data->caption->text;
			$item->source = 'instagram';
			$instagrams[] = $item;
		}
		
		
			
		// Twitter ------------------------------------------------------------
		
		/*
		Get a bunch of tweets and filter for photos
		*/
		
		require_once(get_template_directory() . '/vendors/TwitterAPIExchange.php');
		
		$settings = array(
			'oauth_access_token' => "540552987-3kOAqtSF9pTZ1fO2920YFJi3vwt8qz2um2LDTwA0",
			'oauth_access_token_secret' => "Aqayw8ZcsVsBaFcdqU0YbbrGDvUX3Gtn1YSJiWfqkPbJd",
			'consumer_key' => "fKHoGfQ1HyFkRpSIIrcJBBjAo",
			'consumer_secret' => "3DT7vmxWGCdQAd3ULykt3cGClBg4lu5w6jHb3CG2wL4lNzf9Z9"
		);
		
		$url = "https://api.twitter.com/1.1/statuses/user_timeline.json";
		$requestMethod = 'GET';
		$twitter_username = 'DrinkFitAid';
		$twitter_count = $item_limit * 4;
		$getfield = "?screen_name=$twitter_username&count=$twitter_count";
		$individual_tweet_url = "http://twitter.com/$twitter_username/status/";
		
		$twitter = new TwitterAPIExchange($settings);		
		
		$json = $twitter->setGetfield($getfield)->buildOauth($url, $requestMethod)->performRequest();
		$results = json_decode($json);
		foreach ($results as $tweet){
			$tweet->user = $tweet->retweeted_status = $tweet->entities->hashtags = null;
			//echo "<!-- imagestream \n\n" . print_r($tweet, true) . "\n-->";
			$item = new stdClass();
			$item->url = $individual_tweet_url . $tweet->id;
			$item->text = $tweet->text;
			$item->source = 'twitter';
			$tweets[] = $item;
		}
		
		$tweets = array_slice($tweets, 0, $item_limit);


		// Combined ------------------------------------------------------------

			
		$items = array();
		
		for($i=0; $i < $item_limit; $i++){
			$items[] = array_shift($instagrams);
			$items[] = array_shift($tweets);
		}
		
		$items = array_slice($items, 0, $num_items); // Return the correct number of items
		
		$output = '';
		
		// Output as html
		if($format == 'html' && $echo){ ?>
	
			<ul>
				<?php foreach($items as $item){
						$text_attr = addslashes($item->text);
						$text_html = strip_tags($item->text); ?>
			
							<?php if($item->source == 'instagram'){ ?>
								<!-- LI FOR INSTAGRAM -->
							
								<li class="<?php echo $item->source; ?>">
								<a target="social" href="<?php echo $item->url; ?>">
									<div class="comments">
										<p><?php echo $item->text; ?></p>
									</div>
									<img width="100%" height="100%" src="<?php echo $item->image_src; ?>">
								</a>	
								</li>
							
								<!-- END LI FOR INSTAGRAM -->
							<?php } ?>
							
							<?php if($item->source == 'twitter'){ 	?>	
							<!-- LI FOR TWITTER -->
							
								<li class="<?php echo $item->source; ?>">
									<a target="social" href="<?php echo $item->url; ?>" ><?php echo $text_html; ?></a>
								</li>	
							
							<!-- END LI FOR TWITTER -->	 
							<?php } ?>
				<?php } ?>
	 			 <div class="clearfix"></div>
			</ul>
			
		<?php 
			// Output as json
		} else if($format == 'json'){
		
			$output = json_encode($items);
			
		}
		
		// DEBUG
		// echo "<!-- imagestream \n\n" . print_r($items, true) . "\n-->";
		
		if($echo) echo $output;
			
		return $output;
	
	}
}