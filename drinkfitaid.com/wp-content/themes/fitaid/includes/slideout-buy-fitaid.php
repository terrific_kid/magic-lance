<?php if(!is_page(300)): ?>
	
		<!-- begin slideout -->
		<div class="pulldown-buy-fitaid-wrapper">
			<div class="pulldown-buy-fitaid">
				<div class="btn-close-pulldown-buy-homepage"></div>
				<a href="/buy" class="btn-click-here"></a>
				<img src="/media/pulldown-buy-fitaid.png" alt="">
			</div>
		</div>	

		
		<!-- begin slideout HOMEPAGE ONLY -->
		<div class="pulldown-buy-fitaid-wrapper-not-homepage">
			<div class="pulldown-buy-fitaid">
				<div class="btn-close-pulldown-buy-not-homepage"></div>
				<a href="/buy" class="btn-click-here"></a>
				<img src="/media/pulldown-buy-fitaid.png" alt="">
			</div>
		</div>	
		
		
		<script type="text/javascript">			
		$().ready(function () {
		
			//Buy FA Slideout
			var isBFS = sessionStorage.getItem('seenonceBFS');

			if(isBFS == null) {
				
				//pulldown div after 10 seconds
				setTimeout(function(){ $('.pulldown-buy-fitaid-wrapper').toggleClass('pull-down'); }, 10000);
				$('.btn-close-pulldown-buy-homepage').click(function(e) {				
			
					sessionStorage.setItem('seenonceBFS', 'yes');
				
					$(this).parents('.pulldown-buy-fitaid-wrapper').toggleClass('remove-pulldown');
					e.preventDefault();
					e.stopPropagation();					
				});	
				
				//pulldown div after 3 seconds
				setTimeout(function(){ $('.pulldown-buy-fitaid-wrapper-not-homepage').toggleClass('pull-down'); }, 3000);
				$('.btn-close-pulldown-buy-not-homepage').click(function(e) {			
			
					sessionStorage.setItem('seenonceBFS', 'yes');
				
					$(this).parents('.pulldown-buy-fitaid-wrapper-not-homepage').toggleClass('remove-pulldown');
					e.preventDefault();
					e.stopPropagation();					
				});	
			}
		});
		</script>
		<!-- end free shipping/gear pulldown -->
		
		<?php endif; ?>