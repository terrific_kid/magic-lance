<?php if(!is_page(300)): ?>
	
		<!-- begin slideout -->
		<div class="pulldown-free-shipping-wrapper hidden-xs">
			<div class="pulldown-free-shipping">
				<div class="btn-close-pulldown"></div>
				<a href="/fitclub" class="btn-see-how"></a>
				<img src="/media/pulldown-free-shipping.png" alt="">
			</div>
		</div>				
		
		<script type="text/javascript">			
		$().ready(function () {
			
			// Free Shipping/Gears Slideout
			var isFSGS = sessionStorage.getItem('seenonceFSGS');

			if(isFSGS == null) {
							
				setTimeout(function(){ $('.pulldown-free-shipping-wrapper').toggleClass('pull-down'); }, 3000);
				$('.btn-close-pulldown').click(function() {	
					
					sessionStorage.setItem('seenonceFSGS', 'yes');	
					$(this).parents('.pulldown-free-shipping-wrapper').toggleClass('remove-pulldown');					
					e.preventDefault();
					e.stopPropagation();	
				});	
			}
		});
		</script>
		<!-- end free shipping/gear pulldown -->
		
		<?php endif; ?>