<?php
	 
	$reviews = new WP_Query( 'category_name=reviews&posts_per_page=10&orderby=rand' );
	$first = true;
	
?>

<?php if($reviews->have_posts()) : ?>

	<ul class="testimonials">
	<?php while( $reviews->have_posts() ) : $reviews->the_post(); ?>
		
		<?php if( strlen(get_the_content()) > 650) { continue; } ?>
			<li class="testimonial <?php echo $first ? 'active' : ''; ?>">
				<p>
					<b class="testimonial-title"><?php the_title(); ?></b> &nbsp; <span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span>
				</p>
				<?php the_content(); ?>
			</li>
		
	<?php 
		$first = false;
		endwhile; 
		wp_reset_postdata();
	?>
	</ul>
	
<?php endif; ?>