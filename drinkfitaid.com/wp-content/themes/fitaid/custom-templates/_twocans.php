<?php
	
/*
	Code for twocans page template
*/

$template_twocans = isset($template_twocans) ? $template_twocans : new Template_Twocans;
add_action('admin_init', array($template_twocans, 'meta_init'));
add_filter('admin_post_thumbnail_html', array($template_twocans, 'image_init'));

class Template_Twocans
{
	
	public $template_filename = 'landing-page-2freecans-v3.php';
	
	// Populated meta vars for this page
	private $metas;
	
    // Check for this template type
    private function is_template() {
        if(isset($_GET['post'])) $post_id = $_GET['post'];
        if(isset($_POST['post_ID'])) $post_id = $_POST['post_ID'];
        if(!isset($post_id)) return false;
    
       return $this->template_filename == get_post_meta($post_id, '_wp_page_template', TRUE);
    }

	// Returns meta data for a given page. Static function used by frontend.
	// Ex: $metas = Template_Twocans::get_detail(get_the_ID());
	public static function get_detail($post_id){
		
		global $post;
		if(!$post_id) $post_id = $post->ID;		
		
		$metas = get_post_meta($post_id);
	
		$fields = array(
			//'cta_url',
			'cta_title_1',
			'cta_title_2',
			'modalvids',
		);
	 
		$data = new stdClass;
		foreach($fields as $field){
			if($metas[$field]) 
				$data->$field = array_shift($metas[$field]);
		}
		
		// Handle videos
		$more_classes = '';
		$modalvids = array();
		if($data->modalvids) $modalvids = unserialize($data->modalvids);
		$html = '<ul class="modals_box">' . "\n";
		foreach($modalvids as $modalvid){
			if(isset($modalvid['active']) && $modalvid['active']){
				$html .= "<li>\n";
				$html .= '<a class="fbox" href="'. $modalvid['video'] .'"><img src="' . $modalvid['thumb'] . '" /></a>' . "\n";
		$html .= "<h2>". $modalvid['title'] ."</h2>\n";
				$html .= "</li>\n";
			}
		}
		$html .= "</ul>\n";
		
		$data->modalvids = count($modalvids)? $html : '';
	
		return $data;
	
	}
	
	
	
	public function meta_init()
	{
		if ($this->is_template()) {
			add_meta_box(
				'twocans_meta_1', 
				'Two Cans Custom Data', 
				array($this,'meta_setup'), 
				'page', 
				'normal', 
				'core');
		}
	
		add_action('save_post', array($this, 'meta_save'));
	}
	

	
	// Creates form for custom vars
	public function meta_setup($post)
	{
		$modal_vids_count = 2;
		
		$this->metas = get_post_meta( $post->ID );
		
		$css = '<style>
#twocan-meta-1 input[type="text"], #twocan-meta-1 input[type="url"], #twocan-meta-1 input[type="email"]{
	width: 90%;
}	
		
</style>';
		$html = $css . "<div id=\"twocan-meta-1\">"; // Opening div
				
		// CTA Buttons
		
		$html .= "<p><label>Call To Action Buttons</label></p>\n";
		
		/*
		$value = $this->get_meta('cta_url');
		$html .= '<p><input type="text" name="cta_url" value="'.$value.'" placeholder="URL" /> <span class="dashicons dashicons-admin-links"></span></p>' . "\n";
		*/

		$value = $this->get_meta('cta_title_1');
		$html .= "<p><label>Top Button</label><br/>\n";
		$html .= '<input type="text" name="cta_title_1" value="'.$value.'" placeholder="Top Button Title" /></p>' . "\n";

		$value = $this->get_meta('cta_title_2');
		$html .= "<p><label>Bottom Button</label><br/>\n";
		$html .= '<input type="text" name="cta_title_2" value="'.$value.'" placeholder="Bottom Button Title" /></p>' . "\n";
		
		$html .= "<br/>";
		
		if(isset($this->metas['modalvids'])) $modalvids = unserialize(array_shift($this->metas['modalvids']));
		
		// modal-video 1(on/off, thumb, url)
		for($i = 0; $i < $modal_vids_count; $i++)
		{
			$modal_label = $i + 1;
			$checked = isset($modalvids[$i]['active']) ? 'checked' : '';
            $title = $modalvids[$i]['title'];
			$thumb = $modalvids[$i]['thumb'];
			$video = $modalvids[$i]['video'];
			$html .= "<p><label>Modal Video #$modal_label</label></p>\n";
			$html .= '<p><input type="checkbox" name="modalvids['.$i.'][active]" '.$checked.'/> Active</p>' . "\n";
            $html .= '<p><input type="text" name="modalvids['.$i.'][title]" value="'.$title.'" placeholder="Title" /></p>' . "\n";
			$html .= '<p><input type="text" name="modalvids['.$i.'][thumb]" value="'.$thumb.'" placeholder="Thumbnail URL" /> <span class="dashicons dashicons-format-image"></span></p>' . "\n";
			$html .= '<p><input type="text" name="modalvids['.$i.'][video]" value="'.$video.'" placeholder="Video URL" /> <span class="dashicons dashicons-format-video"></span></p>' . "\n";
			
			$html .= "<br/>";			
		}
		
		$html .= "</div>\n"; // Closing div
		echo $html;
	}
	

	
	// Called when page is saved. Saves custom vars.
	public function meta_save($post_id)
	{
		if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
			return;
		
		// Add a nonce field here?
				
		$fields = array(
			//'cta_url',
			'cta_title_1',
			'cta_title_2',
			'modalvids',
		);
	
		// Save the fields as post meta
		foreach($fields as $field){
				
			if(isset($_POST[$field])) 
				$value = $_POST[$field];
	
			if(is_array($value)){
				$temp_array = array();
				foreach($value as $val){
					if($val){
						$temp_array[] = $this->format_presave($field, $val);
					}
				}
				$value = $temp_array;
				
			} else {
				$value = $this->format_presave($field, $value);
			}
			update_post_meta($post_id, $field, $value);
		}
		
	}



	// Converts vimeo and youtube urls by getting the oembed
	function format_presave($field, $value){
		
		if($field == 'modalvids'){
	
			// Scrape video src and make it protocol-relative
			if($embed = wp_oembed_get($value['video'])){
				
				preg_match("/src=\"https?:([^\"]+)\"/", $embed, $matches); // Youtube url
				if(count($matches) > 1){
					$video = $matches[1];
				} else {
					preg_match("/src=\"([^\"]+)\"/", $embed, $matches); // Vimeo
					if(count($matches) > 1){
						$video = $matches[1];
					}
				}
				$value['video'] = $video;
				
			}
	
		}
		
		return $value;
	}
	
	
	// Convenience method to get meta or set to empty string
	private function get_meta($key){
		$value = isset($this->metas[$key]) ? esc_html(array_shift($this->metas[$key])) : '';
		return $value;
	}

    public function image_init($content) {
        return $content = ($this->is_template() ? "<p>Image should be at least 2560 x 1240.</p>" : "") . $content;
    }

}
