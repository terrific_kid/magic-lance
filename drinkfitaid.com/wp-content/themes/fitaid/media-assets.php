<?php
/* Template Name: Media Assets */
?>
 <?php get_header(); ?>
 
		<div class="container media-assets-wrapper">
			
			<div class="row add-header-height">
				<div class="col-md-12">
					<h1>FITAID Assets</h1>		

					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th class="thumb-wrapper">Image</th>
									<th class="desc-wrapper">Description</th>
									<th class="btn-download-wrapper">Download</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="thumb-wrapper"><img src="//media.lifeaidbevco.com/archive.png" alt=""></td>
									<td class="desc-wrapper">These are the super hi-resolution vector logos you can use to print on T-Shirts, ads or any other print material.</td>
									<td class="btn-download-wrapper"><a  href="//media.lifeaidbevco.com/download.php?file=FitAID-Vector-Logos-Mech.eps.zip" class="btn btn-default">DOWNLOAD</a></td>
								</tr>
								<tr>
									<td class="thumb-wrapper"><img src="//media.lifeaidbevco.com/FITAID-Available-White.jpg" alt=""></td>
									<td class="desc-wrapper">FITAID Available Here</td>
									<td class="btn-download-wrapper"><a href="//media.lifeaidbevco.com/download.php?file=FITAID-Available-White.jpg" class="btn btn-default">DOWNLOAD</a></td>
								</tr>
								<tr>
									<td class="thumb-wrapper"><img src="//media.lifeaidbevco.com/FITAID-Available-Black.jpg" alt=""></td>
									<td class="desc-wrapper">FITAID Available Here</td>
									<td class="btn-download-wrapper"><a href="//media.lifeaidbevco.com/download.php?file=FITAID-Available-Black.jpg" class="btn btn-default">DOWNLOAD</a></td>
								</tr>
								
								<tr>
									<td class="thumb-wrapper"><img src="//media.lifeaidbevco.com/FITAID-AvailableHere-Christmas.jpg" alt=""></td>
									<td class="desc-wrapper">Post these on your social media accounts announcing FITAID is available at our box!  Can be used on your website as well. </td>
									<td class="btn-download-wrapper"><a href="//media.lifeaidbevco.com/download.php?file=FITAID-AvailableHere-Christmas.jpg" class="btn btn-default">DOWNLOAD</a></td>
								</tr>
								<tr>
									<td class="thumb-wrapper"><img src="//media.lifeaidbevco.com/FITAID-AvailableHere-Noah.jpg" alt=""></td>
									<td class="desc-wrapper">Post these on your social media accounts announcing FITAID is available at our box!  Can be used on your website as well. </td>
									<td class="btn-download-wrapper"><a  href="//media.lifeaidbevco.com/download.php?file=FITAID-AvailableHere-Noah.jpg" class="btn btn-default">DOWNLOAD</a></td>
								</tr>
							</tbody>
						</table>
					</div>					
				</div>
			</div>			
			
			
		</div><!-- end container -->
		
<?php get_footer(); ?>