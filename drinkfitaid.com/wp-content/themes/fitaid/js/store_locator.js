/* 
	Store locator class for lifeaid
	
	Example usage...
	$(document).ready(function() {

		fa_storeLocator.init(); // Initialize
									
		$('#storeLocatorForm').submit(function(){
		
			var address = $('#storeLocatorForm input[name="address"]').val();
			if(fa_storeLocator.search(address) == false){
				console.log('Address could not be found');
			}
			return false;
			
		});

	});
	
*/


	var fa_storeLocator = new function(){
	
		this.start = {};
	
		this.init = function(opts){
		
			if(this.initted) return;
		
			// Initial start point
			this.initial = {
				latitude: 36.8106460,
				longitude: -120.5569760,
			}
			this.max_distance = 1000; // Get locations within this many miles
			this.map = null;
			this.infowindow = new google.maps.InfoWindow();
			this.locations = {};
			this.initial_zoom = 7; // Initial zoom level
			this.search_zoom = 8; // Zoom level when searching
			
			if(opts){
				this.initial_zoom = opts.initial_zoom || this.initial_zoom;
				this.search_zoom = opts.search_zoom || this.search_zoom;
			}
			this.initted = true;
			
			this.search(); // Intial search
			
		};
		
	
		// Find store locations within a certain distance and create markers
		this.getMarkers = function(start){
		
			var self = this;
			this.locations = [];
			
			$.getJSON('//api2.lifeaidbevco.com/api/locations/locations.json', function(data){
			
				$.each(data, function(i, e){
				
					var latlng = new google.maps.LatLng(e.lat, e.lng);
					var marker = new google.maps.Marker({
						position: latlng,
						map: self.map,
						title: e.name
					});
					var end = {
						latitude: e.lat,
						longitude: e.lng
					}
					
					if( self.haversine(start, end, {unit: 'mile'}) < self.max_distance ){
						self.locations.push(e);
						
						google.maps.event.addListener(marker, 'click', function() {
							infowindow.setContent(e.boxCont);
							infowindow.open(self.map, marker);
						});
					}
					
				});
	
			});
			
		};
		
		
		// Searches a given address string
		this.search = function(address){
		
			var geocoder = new google.maps.Geocoder();
			var self = this;
			
			if(!address){
				self.searchLocation(self.initial, self.initial_zoom);
				return;
			}
			
			// Get new start point based on user input
			geocoder.geocode({address: address}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
				
					var start = {
						latitude: results[0].geometry.location.lat(),
						longitude: results[0].geometry.location.lng(),
					}
					self.searchLocation(start, self.search_zoom);
					
				} else {
					self.searchLocation(self.initial, self.initial_zoom);
					return 0;
				}
			});			
		};
		
		// Search a given start location
		this.searchLocation = function (start, zoom){
		
			if(!start) return;
			
			var mapOptions = {
				center: new google.maps.LatLng(start.latitude, start.longitude),
				zoom: zoom,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			
			// Create the map or move to location
			if(this.map){
				var latLng = new google.maps.LatLng(start.latitude, start.longitude);
				this.map.setZoom(zoom);
				this.map.panTo(latLng);
			} else {
				this.map = new google.maps.Map(document.getElementById("locator_map_canvas"), mapOptions);
			}
			
			// Find the stores near this location
			this.getMarkers(start);
						
		};
		
		// Calculate distance between two points on the earth
		this.haversine = function(start, end, options){
		
			var km		= 6371
			var mile	= 3960
			options	 = options || {}
	
			var R = options.unit === 'mile' ?
				mile :
				km
	
			var dLat = this.toRad(end.latitude - start.latitude)
			var dLon = this.toRad(end.longitude - start.longitude)
			var lat1 = this.toRad(start.latitude)
			var lat2 = this.toRad(end.latitude)
	
			var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
							Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2)
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a))
	
			if (options.threshold) {
				return options.threshold > (R * c)
			} else {
				return R * c
			}
			
		};
		
		
		// Convert to radians for haversine
		this.toRad = function(num){
			return num * Math.PI / 180;
		};
		
	}

