<?php
/* Template Name: Store Locator */
?>

<?php 
	get_header();
?>


<div class="container store_locator">
	<div class="copy_content col-xs-12">
	
		<div class="mobile_shiv visible-xs-block "></div>
		<div class="desktop_shiv hidden-xs "></div>
		
		<h3><?php the_title();?></h3>
		<p><?php the_content();?></p>		
		
		<iframe name="map" style="border:0;width:100%;height:500px;" src="<?php echo get_template_directory_uri(); ?>/includes/map.php">
		Loading...
		</iframe>
	
	</div>
</div><!-- end container -->


<?php get_footer(); ?>











