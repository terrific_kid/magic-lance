<?php
/* Template Name: Buy Page */
 get_header(); ?>
 
 
<script type="text/javascript" src="/wp-content/themes/fitaid/js/exit.js"></script>
 
   
<div class="container-fluid heading">
 <div style="background-image: url(/media/buy_header.jpg);" class="hero">
	 
			 <div class="desktop_shiv hidden-xs"></div>
			 <div class="mobile_shiv visible-xs-block"></div>
			 <div class="wrap"> <h2>Buy Now</h2> </div>
	  
 </div>
</div>

	<div class="subnav">
	<?php wp_nav_menu(array('theme_location'=>'buy-menu', 'container_class'=>'container', 'menu_class' => 'col-xs-12')); ?>
	</div>
	
	<div class="container">
	<div class="copy_content">
		  
		<h2 class="text-center">The LIFEAID Family of Products</h2>
		<h2 class="text-center">Each line has a unique supplement profile &#149; Its own refreshing flavor &#149;<br> Natural botanical ingredients &#149; 45 calories per can</h2>
		<br>
				<!-- Begin Products -->
				<a name="fitaid" class="anchor"></a>
				<product data-object="system.products['fitaid-24-pack']" data-template="product-pack" data-strict="retail"></product>
				<product data-object="system.products['partyaid-24-pack']" data-template="product-pack" data-strict="retail"></product>
				<product data-object="system.products['golferaid-24-pack']" data-template="product-pack" data-strict="retail"></product>
				<div class="clearfix"></div>
				
				<a name="partyaid" class="anchor"></a>
				<product data-object="system.products['fitaid-48-pack']" data-template="product-pack" data-strict="retail"></product>
				<product data-object="system.products['partyaid-48-pack']" data-template="product-pack" data-strict="retail"></product>
				<product data-object="system.products['golferaid-48-pack']" data-template="product-pack" data-strict="retail"></product>
				<div class="clearfix"></div>
				
				 	</div>
	</div>
	
	<a name="gear" class="anchor"></a>
<div class="contaier-fluid heading">
 <div style="background-image: url(/media/merch_header.jpg);" class="hero">
			 <div class="wrap"> <h2>Gear</h2> </div>
 </div>
</div>


<div class="container">
	<div class="copy_content">
		<div class="row">		 
			<product data-object="system.products['fitaid-black-tshirt']" data-template="product-default" data-strict="retail"></product>
			<product data-object="system.products['fitaid-red-tshirt']" data-template="product-default" data-strict="retail"></product>
			<product data-object="system.products['fitaid-blue-tshirt']" data-template="product-default" data-strict="retail"></product>
			<div class="clearfix"></div>
		</div> 
		<div class="row">				
			<product data-object="system.products['fitaid-gray-snaphat']" data-template="product-default" data-strict="retail"></product>
			<product data-object="system.products['fitaid-bluewhite-snaphat']" data-template="product-default" data-strict="retail"></product>
			<product data-object="system.products['fitaid-black-snaphat']" data-template="product-default" data-strict="retail"></product>
			<div class="clearfix"></div>
		</div> 
		<div class="row">				
			<product data-object="system.products['fitaid-red-snaphat']" data-template="product-default" data-strict="retail"></product>
			<product data-object="system.products['fitaid-white-snaphat']" data-template="product-default" data-strict="retail"></product>
			<product data-object="system.products['fitaid-pinkwhite-snaphat']" data-template="product-default" data-strict="retail"></product>
			<div class="clearfix"></div>
		</div> 
		<div class="row">
			<product data-object="system.products['fitaid-red-wraps']" data-template="product-default" data-strict="retail"></product>
			<product data-object="system.products['fitaid-black-wraps']" data-template="product-default" data-strict="retail"></product>
			<product data-object="system.products['fitaid-blue-wraps']" data-template="product-default" data-strict="retail"></product>
			<div class="clearfix"></div>
		</div> 
		<div class="row">
			<product data-object="system.products['fitaid-pink-wraps']" data-template="product-default" data-strict="retail"></product>
			<product data-object="system.products['fitaid-gray-hoodie']" data-template="product-default" data-strict="retail"></product>
			<product data-object="system.products['fitaid-black-hoodie']" data-template="product-default" data-strict="retail"></product>
			<div class="clearfix"></div>
		</div>
		
		<div class="row">
			<product data-object="system.products['fitaid-red-triblend-tank']" data-template="product-default" data-strict="retail"></product>
			<product data-object="system.products['fitaid-black-triblend-tank']" data-template="product-default" data-strict="retail"></product>
			<product data-object="system.products['fitaid-tahitianblue-triblend-tank']" data-template="product-default" data-strict="retail"></product>
		</div>
		
		<div class="row">
			<product data-object="system.products['fitaid-white-racerback']" data-template="product-default" data-strict="retail"></product>
			<product data-object="system.products['fitaid-fridge-retail']" data-template="product-default" data-strict="retail"></product>
			<product data-object="system.products['fitaid-black-sunglasses']" data-template="product-default" data-strict="retail"></product>
		</div> 
		
		<div class="row">
			<product data-object="system.products['jackie-perez-poster-retail']" data-template="product-default" data-strict="retail"></product>
			<product data-object="system.products['neal-maddox-poster-retail']" data-template="product-default" data-strict="retail"></product>
			<product data-object="system.products['fitaid-stickers-10pack']" data-template="product-default" data-strict="retail"></product>
		</div> 		
		
	</div>
</div><!-- end container -->
<?php get_footer(); ?>
	  
	  
	  
	  
