<?php
/* Template Name: 2freecans Landing Page V3 */

    get_header();
	
	// Get the page meta data
	$page_metas = Template_Twocans::get_detail(get_the_ID());
	
?>
<link rel="stylesheet" type="text/css" href="/wp-content/themes/fitaid/css/twofreecans-landing-v3.css">
<script type="text/javascript">
$(function() {
    $(document).scroll(function() {
        $('.featured-image').css('margin-top', '-' + $(this).scrollTop()*0.75 + 'px'); //parallax
    });

    //auto rotate testimonials
    var testimonialTimer;
    function startTestimonialTimer(delay) {
        testimonialTimer = setTimeout(function() {
            $('.testimonial-nav .next').click();    
        }, delay || 3000);
    }
    startTestimonialTimer(5000);

    $('.testimonial-nav span').click(function() {
        clearTimeout(testimonialTimer);

        var all = $('.testimonial');
        var idx = all.index($('.testimonial.active'));
        idx += $(this).hasClass('prev') ? -1 : 1;
        idx = idx < 0 ? all.length + idx : idx % all.length;

        var target = all.eq(idx);
        $('.testimonial.active').stop().fadeTo('slow', 0, function() {
            $(this).removeClass('active');
            target.addClass('active').fadeTo('slow', 1, function() {
                startTestimonialTimer();
            });
        });
    });

});
</script>

<div class="twofreecans-landing-wrapper-v3">

    <!-- grab Featured Image -->
    <?php if (has_post_thumbnail( $post->ID ) ): ?>
        <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
        <div class="featured-image"><img width="100%" src="<?php echo $image[0]; ?>" alt=""></div>
    <?php endif; ?>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title"><h1><?php the_title() ?></h1></div>
				
				<div ng-controller="2freecans" class="cta cta-1">
					<button ng-click="freecans()" class="btn btn-lg btn-default">
						<?php echo $page_metas->cta_title_1 ?>
					</button>				
				</div>
				<img class="cans" src="/media/cans-2freecansv3.png">
                <!--<img class="cans" src="/wp-content/themes/fitaid/images/cans.png">-->
            </div>
        </div>
    </div><!-- end container -->

    <div class="mid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="seals-container"><h3 class="seals-text">SECURE SHOPPING</h3><img class="seals" src="/media/seals.jpg"></div>
                    <div class="testimonials">
                        <div class="testimonial-nav"><span class="prev">&lt;</span><span class="next">&gt;</span></div>
                        <?php 
                            $reviews = new WP_Query( 'category_name=reviews&posts_per_page=10&orderby=rand' );
                            $first = true;
                            while ( $reviews->have_posts() ) : 
                                $reviews->the_post();
                                if (strlen(get_the_content()) > 350) {continue;}
                        ?>
                                <div class="testimonial <?php echo $first ? 'active' : ''; ?>"><?php the_content(); ?><b class="testimonial-title">, <?php the_title(); ?></b></div>
                        <?php 
                                $first = false;
                            endwhile; 
                            wp_reset_postdata();
                        ?>
                        <div class="clear"></div>
                    </div><!-- end testimonials -->
    				
                    <div class="left">
        				<?php 
                            if ( have_posts() ) : while ( have_posts() ) : 
                                the_post();
        				        the_content();
        				    endwhile; endif; 
                        ?>
                    </div>
    				
                    <div class="right">
    				    <?php print_r($page_metas->modalvids) ?>
                    </div>
    				
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div><!-- mid -->

    <div class="bot">
        <div class="container">   
            <div class="row">
                <div class="col-md-12">
    				<div ng-controller="2freecans" class="cta cta-2">
    					<button ng-click="freecans()" class="btn btn-lg btn-default">
    						<?php echo $page_metas->cta_title_2 ?>
    					</button>				
    				</div>		

                    <div class="perks">
                        <div><img src="/wp-content/themes/fitaid/images/perk_agave.png"><span>Raw organic<br>blue agave</span><div class="clear"></div></div>
                        <div><img src="/wp-content/themes/fitaid/images/perk_pf.png"><span>Paleo<br>friendly</span><div class="clear"></div></div>
                        <div class="perk-spacer"></div>
                        <div><img src="/wp-content/themes/fitaid/images/perk_energy.png"><span>Not an<br>energy drink</span><div class="clear"></div></div>
                        <div><img src="/wp-content/themes/fitaid/images/perk_gluten.png"><span>Certified<br>gluten free</span><div class="clear"></div></div>
                        <div><img src="/wp-content/themes/fitaid/images/perk_pr.png"><span>Performance<br>& recovery</span><div class="clear"></div></div>
                        <div><img src="/wp-content/themes/fitaid/images/perk_leaf.png"><span>All<br>natural</span><div class="clear"></div></div>
                        <div class="perk-spacer"></div>
                        <div><img src="/wp-content/themes/fitaid/images/perk_target.png"><span>Targeted<br>supplements</span><div class="clear"></div></div>
                        <div><img src="/wp-content/themes/fitaid/images/perk_45.png"><span>Calories<br>per can</span><div class="clear"></div></div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end bot -->

    <div class="fin">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <img class="final-cans" src="/wp-content/themes/fitaid/images/buynow_cans.png">
                    <br>
                    <h3 class="seals-text">SECURE SHOPPING</h3><img class="seals" src="/media/seals.jpg">
                </div>
            </div>
        </div>
    </div><!-- end fin -->

</div>


<?php get_footer(); ?>