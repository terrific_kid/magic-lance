<?php
/* Template Name: Special Offers */
?>
 <?php get_header(); ?>
	<div class="special-offers-wrapper-background">
		<div class="container special-offers-wrapper">
			
			<div class="row">
				<div class="col-md-12">
					<h1>It's Your Lucky Day! <small>You have received one of our special Affiliates offers. This offer will only last until the end of the month so take action now!</small></h1>
					<div class="get-your-promotion-wrapper clearfix">
						<div class="get-started">
							<h2>GET STARTED HERE</h2>
							<form ng-controller="special" ng-submit="applyCoupon()" class="" role="form">
								<div class="form-group">
									<label class="sr-only" for="name">Your Name</label>
									<input type="text" ng-model="form.name" class="form-control" id="name" placeholder="Your Name">
								</div>
								<div class="form-group">
									<label class="sr-only" for="email">Your Email</label>
									<input type="text" ng-model="form.email" class="form-control" id="email" placeholder="Your Email">
								</div>		
								<div class="form-group">
									<label class="sr-only" for="promo-code">Enter Promo Code</label>
									<input type="text" ng-model="form.promo" class="form-control" id="promo-code" placeholder="Enter Promo Code">
								</div>					
								<button class="btn btn-lg btn-default" type="submit">GET YOUR PROMOTION</button>
							</form>
						</div>
						<div class="lurong"><div class="image-wrapper"><img src="/media/lurong-living-logo.png" alt="Lurong Living Paleo Challenge Approved"></div></div>
					</div>
					<p class="fitaid-is">FITAID is <b>NOT an energy drink.</b> FitAID is the only <b>Paleo Friendly performance and recovery beverage</b> made by elite athletes, for athletes. FitAID is made with <b>natural botanical ingredients</b> and has close to <b>3 grams of targeted supplements</b> to AID your Workout. With things like <b>Glutamine, BCAAs, Omega 3</b> EFAs, <b>Glucosamine, B Complex, Vitamins C, D, E, Amino Acids, Natural Anti-Inflammatories, Quercetin, CoQ10</b> and Green Tea Leaf Extract. FitAID is the most comprehensive and <b>effective pre and post workout beverage</b> on the market today. On top of that, it's sweetened with raw organic blue agave and stevia and has <b>only 45 calories per can.</b></p>
				</div>
			</div>
			
		</div><!-- end container -->
	</div><!-- end background div -->
<?php get_footer(); ?>