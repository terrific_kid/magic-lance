<?php
/* Template Name: Checkout Page */
 get_header(); ?>
 			
 			<div class="mobile_shiv visible-xs-block"></div>
		 	<div class="desktop_shiv hidden-xs"></div>
		 	
	<div class="container">
	<div class="copy_content">
		
			<div  ng-show="system.customer">
			
				<div class="col-xs-12">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<h3><?php the_title();?></h3>
						<p><?php the_content();?></p>
				<?php endwhile; else : ?><?php endif; ?>
				 </div>
				
				<checkout ng-show="system.totals.items" baseurl="https://www.drinkfitaid.com/checkout" confirmurl="https://www.drinkfitaid.com/confirm" data-customer="system.customer" data-products="system.products" data-totals="system.totals" data-discount="system.discount" data-prompt="system.prompt()" data-template="checkout-default"></checkout>
				<p class="col-xs-12" ng-hide="system.totals.items">Your cart is empty.</p>
 			
 			</div>
			 
			
							
	</div><!-- end copy_content -->
	</div><!-- end container -->
	 
<?php get_footer(); ?>
	  
	  
	  
	  
