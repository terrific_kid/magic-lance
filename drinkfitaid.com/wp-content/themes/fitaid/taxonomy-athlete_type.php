<?php get_header(); ?>

 
		
<div class="container-fluid heading">
 <div style="background-image: url(/media/hero-athletes.jpg);" class="hero athletes-hero">
			 <div class="desktop_shiv hidden-xs"></div>
			 <div class="mobile_shiv visible-xs-block"></div>
			 <div class="wrap"> <h2>Athletes</h2> </div>
 </div>
</div>
		 
 <div class="subnav">
	<?php wp_nav_menu(array('theme_location'=>'athletes-menu', 'container_class'=>'container', 'menu_class' => 'col-xs-12')); ?>
	</div>
	
	<div class="container">
	<div class="copy_content col-xs-12 taxonomy-athlete-body-wrapper">
		
	
		
		<ul>
			<?php 
				if ( have_posts() ) : while ( have_posts() ) : the_post(); 
					$athlete = athletes_get_detail(get_the_ID()); ?>
					
					<!-- BEGIN CONTENT -->
					
					
					<li class="col-sm-4">
						<div class="athlete-post-wrapper">
							<div class="athlete-post-thumbnail">
									<!-- grab URL of Featured Image -->
									<?php if (has_post_thumbnail( $post->ID ) ): ?>
										<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
										<div class="image-wrapper"><a href="<?php the_permalink(); ?>"><img src="<?php echo $image[0]; ?>" alt=""></a></div>
									<?php endif; ?>
							</div>
							<div class="athlete-post-intro">				
								<h3 class="athlete-name"><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h3>		
								<p class="athlete-region"><a href="<?php the_permalink(); ?>"><?php echo $athlete->region; ?></a></p>
							</div>
						</div>
					</li>
								
				
					<!-- END CONTENT -->
				<?php endwhile; else : ?>
			<?php endif; ?>
		</ul>
		
	</div><!-- end container -->
	</div>
<?php get_footer(); ?>
	  
