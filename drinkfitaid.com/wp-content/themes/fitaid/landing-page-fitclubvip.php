<?php
/* Template Name: FitCLUB VIP Landing Page */
?>
 <?php get_header(); ?>
 
		<div class="container facebook-landing-wrapper">
			
			<div class="row add-header-height">
				<div class="col-md-12">
					<h1>$20 OFF your first FITCLUB order! Use coupon code <span class="big-red-text">FC20</span> at checkout! </h1>
					</div>
			</div>
			<div class="row">
				<div class="col-md-8 content-left-wrapper">
<ul style="margin-bottom: 1em;">
	<li><span style="color: #e51b24" class="glyphicon glyphicon-ok"></span> <b>Free ongoing Shipping</b></li>
	<li><span style="color: #e51b24" class="glyphicon glyphicon-ok"></span> <b>Free Shirt or Tank Top with 1st Order</b></li>
	<li><span style="color: #e51b24" class="glyphicon glyphicon-ok"></span> <b>Free Power Wraps with 1st Order</b></li>
	<li><span style="color: #e51b24" class="glyphicon glyphicon-ok"></span> <b>Free FITAID Stickers with 1st Order</b></li>
	<li><span style="color: #e51b24" class="glyphicon glyphicon-ok"></span> You choose delivery frequency</li>
</ul>

<p style="font-weight: bold;">Up to $55 in free merchandise and shipping, PLUS $20 off that for a total of $75 in savings!</p>

<p style="font-weight: bold;">We lose our butts on this offer.</p>

<p style="font-weight: bold;">FREE MERCHANDISE</p>		
					
				<img src="/media/promo_merchitems.jpg">
				<p style="margin-top: 1em; font-size: 0.8em !important; ">* This offer shows savings and merchandise from the FitClub 48 pack option. Each offer has different savings and free merchandise.</p>
				<a href="/fitclub" class="btn btn-lg btn-default" >START SAVING NOW</a>	
				
				
				<a class="fbox" href="https://www.youtube.com/watch?v=U2uzRBa8ekc">
						 <img src="/media/youtube-Jackie-Perez.jpg" alt="">
					</a>
				
				</div>
				<div class="col-md-4 content-right-wrapper">
					<div class="jackie-perez"><img src="/media/fb10-jackie-perez.jpg"></div>
				</div>
			</div>
			
			<!-- begin Testimonials -->
			<div class="row testimonials">
				<div class="col-md-12">
					<h3>Derrick Winburn</h3>
					<span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span>
					
					<!-- NOTE: first <p> of testimonial should have the <span.icon-open-quote>, as this class adds the open quote graphic -->
					<p><span class="icon-open-quote"></span> Quality product all around! Actually beneficial ingredients. Tastes awesome! Great guys over at FITAID for sure! Dudes know what they're doing. I'd recommend this stuff to anyone! Should call it Tastier than "The Tasty" though lol just sayin.</p>
					
					
					<h3>Tyrel Johnson</h3>
					<span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span>
					<p><span class="icon-open-quote"></span> Ordered the start up package and it was back ordered. Told Tamara that I needed it for our Grand Opening and she made it happen!!!! That's the epitome of customer service!!!!</p>
					
				</div>
			</div>
			
			
		</div><!-- end container -->
		
<?php get_footer(); ?>