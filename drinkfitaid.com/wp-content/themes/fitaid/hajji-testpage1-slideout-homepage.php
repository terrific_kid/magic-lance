<?php

/* Template Name: hajji-testpage1-slideout-homepage */

include('includes/_social_feeds.php');
get_header(); 


?>
 <div ng-if="!system.flags.enter_to_win" ng-init="system.modal('enter_to_win',8000)"></div>
   

  
   
   
    <div class="mobile_shiv visible-xs-block"></div>
	<div class="desktop_shiv hidden-xs"></div>

		
		
		
<div class="container-fluid">


<div id="slider">
	<carousel  interval="5000">
		<slide active="slide.active">
			<a href="/buy"><img src="/media/slide_1.jpg" ></a>
		</slide>
		 <slide >
			<a href="/fitclub"><img src="/media/Home-FitClub.jpg"></a>
		</slide>
		<slide >
			<a href="/athletes"><img src="/media/Home-Athletes.jpg" ></a>
		</slide>
		<slide >
			<a ng-click="system.modal('start_type')"><img src="/media/Home-StartSelling.jpg" ></a>
		</slide>
		<slide >
			<a href="/buy"><img src="/media/Home-TrainDirty.jpg" ></a>
		</slide>
		 
	</carousel>

</div>
 

	<div id="performance_intro">
		 <div class="container">
			
			 <div class="hidden-xs col-md-3 col-sm-3">
				 <div class="red_block block1">
					<a  ng-click="system.modal('start_type')" class="content">
					<img src="/media/start.png">
					</a>
				 </div>
			 </div>
			 <div class="hidden-xs col-md-3 col-sm-3">
				<div class="red_block block2">
				 <a href="/refer-your-gym" class="content">
					 <img src="/media/refer.png">
				 </a>
				</div>
			 </div>
			 <div class="hidden-xs col-md-3 col-sm-3">
				<div class="red_block block3">
				 <a href="/reviews" class="content">
				  <img src="/media/see.png">
				 </a>
				</div>
			 </div>
			 <div class="hidden-xs col-md-3 col-sm-3">
				 <div class="red_block block4">
				  <a href="//www.youtube.com/embed/8HAI553o-tY" class="content fbox">
				   <img src="/media/dan.png">
				  </a>
				 </div>
			 </div>
			 
			 
			 <div class="mobilefix visible-xs-block">
			 <div class="col-xs-6 col-md-3 col-sm-3">
				 <div class="red_block block1">
					<a  href="/start" class="content">
					<img src="/media/start.png">
					</a>
				 </div>
			 </div>
			 <div class="col-xs-6 col-md-3 col-sm-3">
				<div class="red_block block2">
				 <a href="/refer-your-gym" class="content">
					 <img src="/media/refer.png">
				 </a>
				</div>
			 </div>
			 <div class="col-xs-6 col-md-3 col-sm-3">
				<div class="red_block block3">
				 <a href="/reviews" class="content">
				  <img src="/media/see.png">
				 </a>
				</div>
			 </div>
			 <div class="col-xs-6 col-md-3 col-sm-3">
				 <div class="red_block block4">
				   <a href="//www.youtube.com/embed/8HAI553o-tY" class="content fbox">
				   <img src="/media/dan.png">
				  </a>
				 </div>
			 </div>
			 </div>
			 
			 <div class="clearfix"></div>
			 <div class="container-fluid">
			  <p><b>What is FITAID?</b> Simple, the cleanest <b>PERFORMANCE & RECOVERY</b> workout drink on the market, with nearly 3grams of supplements in every drink.  FITAID is <b>NOT an Energy Drink</b>, has no artificial colors or sweeteners. FITAID is All Natural, <b>contains only 45 calories per can</b>, certified gluten free, <b>Lurong Paleo Challenge approved</b>, is specifically designed for the demanding needs of a active and healthy lifestyle...and you won't believe how good it tastes.</p>
				
			 </div>
			 <div class="col-sm-12">
				 
				 <div class="what-is-fitaid-gray-icons-wrapper reusable-classes">
					<ul class="list-inline">
						<li>
							<div class="gray-icon-wrapper border-radius-50pct">
								<div class="overlay-wrapper animate-fade-red">
									<div class="valign animate-fade"><span>Calories<br>Per Can</span></div>
								</div>
								<div class="icon-wrapper valign"><img src="/media/icon-fitaid-45-calories-per-can-white.png" alt=""></div>
							</div>
						</li>
						<li>
							<div class="gray-icon-wrapper border-radius-50pct">
								<div class="overlay-wrapper animate-fade-red">
									<div class="valign animate-fade"><span>All<br>Natural</span></div>
								</div>
								<div class="icon-wrapper valign"><img src="/media/icon-fitaid-all-natural-white.png" alt=""></div>
							</div>
						</li>
						<li>
							<div class="gray-icon-wrapper border-radius-50pct">
								<div class="overlay-wrapper animate-fade-red">
									<div class="valign animate-fade"><span>Targeted Supplements</span></div>
								</div>
								<div class="icon-wrapper valign"><img src="/media/icon-fitaid-targeted-supplements-white.png" alt=""></div>
							</div>
						</li>
						<li>
							<div class="gray-icon-wrapper border-radius-50pct">
								<div class="overlay-wrapper animate-fade-red">
									<div class="valign animate-fade"><span>Paleo<br>Friendly</span></div>
								</div>
								<div class="icon-wrapper valign"><img src="/media/icon-fitaid-paleo-friendly-white.png" alt=""></div>
							</div>
						</li>
						<li>
							<div class="gray-icon-wrapper border-radius-50pct">
								<div class="overlay-wrapper animate-fade-red">
									<div class="valign animate-fade"><span>Certified Gluten Free</span></div>
								</div>
								<div class="icon-wrapper valign"><img src="/media/icon-fitaid-gluten-free-white.png" alt=""></div>
							</div>
						</li>
						<li>
							<div class="gray-icon-wrapper border-radius-50pct">
								<div class="overlay-wrapper animate-fade-red">
									<div class="valign three-lines animate-fade"><span>Raw Organic Blue Agave</span></div>
								</div>
								<div class="icon-wrapper valign"><img src="/media/icon-fitaid-organic-blue-agave-white.png" alt=""></div>
							</div>
						</li>
					</ul>
				 </div>
				 <h3 class="take-a-tour text-center"><b>Take a Tour</b></h3>
			 </div>
		 </div>
	 
		 <div class="container-icon-take-tour"><span class="icon-take-tour"><i class="fa fa-angle-down"></i></span></div>
		 
	</div> <!-- end performance_intro -->
     
     
	<div id="performance">
	
	 	
		
		
		
	
		<div class="container-fluid">
			<h1 class="heading-performance wow fadeInUp animated" data-wow-delay=".5s">Performance</h1>
		</div>
		<div class="container">
		
		
			<div class="col-xs-6 can-left-wrapper"><img class="wow bounceInLeft animated" data-wow-delay=".3s" src="/media/can.png"></div>
		 
			<div class="col-xs-6 performance-list-wrapper">
				<h3 class="heading-when-it-counts wow fadeInUp animated" data-wow-delay=".6s">When It Counts</h3>
				<ul>
					<li>
						<div class="performance-list-item">
							<div class="performance-icon-wrapper border-radius-50pct wow fadeInUp animated" data-wow-delay=".3s">
								<div class="performance-icon valign border-radius-50pct"><img src="/media/icon-performance-electrolytes.png" alt=""></div>
								<div class="border2 valign border-radius-50pct pulsate-inner"></div>
								<div class="border3 valign border-radius-50pct pulsate-outer"></div>
							</div>
							<div class="performance-text wow fadeInUp animated" data-wow-delay=".5s">Electrolytes <span><span class="hidden-xs">- </span>Muscle stamina</span></div>
						</div>
					</li>
					<li>
						<div class="performance-list-item">
							<div class="performance-icon-wrapper border-radius-50pct wow fadeInUp animated" data-wow-delay=".4s">
								<div class="performance-icon valign border-radius-50pct"><img src="/media/icon-performance-quercetin.png" alt=""></div>
								<div class="border2 valign border-radius-50pct pulsate-inner"></div>
								<div class="border3 valign border-radius-50pct pulsate-outer"></div>
							</div>
							<div class="performance-text wow fadeInUp animated" data-wow-delay=".5s">Quercetin <span><span class="hidden-xs">- </span>Performance</span></div>
						</div>
					</li>
					<li>
						<div class="performance-list-item">
							<div class="performance-icon-wrapper border-radius-50pct wow fadeInUp animated" data-wow-delay=".5s">
								<div class="performance-icon valign border-radius-50pct"><img src="/media/icon-performance-green-tea-leaf.png" alt=""></div>
								<div class="border2 valign border-radius-50pct pulsate-inner"></div>
								<div class="border3 valign border-radius-50pct pulsate-outer"></div>
							</div>
							<div class="performance-text wow fadeInUp animated" data-wow-delay=".6s">Green Tea Leaf <span><span class="hidden-xs">- </span>Energy</span></div>
						</div>
					</li>
					<li>
						<div class="performance-list-item">
							<div class="performance-icon-wrapper border-radius-50pct wow fadeInUp animated" data-wow-delay=".6s">
								<div class="performance-icon valign border-radius-50pct"><img src="/media/icon-performance-b-complex.png" alt=""></div>
								<div class="border2 valign border-radius-50pct pulsate-inner"></div>
								<div class="border3 valign border-radius-50pct pulsate-outer"></div>
							</div>
							<div class="performance-text wow fadeInUp animated" data-wow-delay=".7s">Full B Complex <span><span class="hidden-xs">- </span>Endurance</span></div>
						</div>
					</li>
					<li>
						<div class="performance-list-item">
							<div class="performance-icon-wrapper border-radius-50pct wow fadeInUp animated" data-wow-delay=".7s">
								<div class="performance-icon valign border-radius-50pct"><img src="/media/icon-performance-organic-blue-agave.png" alt=""></div>
								<div class="border2 valign border-radius-50pct pulsate-inner"></div>
								<div class="border3 valign border-radius-50pct pulsate-outer"></div>
							</div>
							<div class="performance-text wow fadeInUp animated" data-wow-delay=".8s">Raw Organic Blue Agave <span><span class="hidden-xs">- </span>Low Glycemic Index</span></div>
						</div>
					</li>
					<li>
						<div class="performance-list-item">
							<div class="performance-icon-wrapper border-radius-50pct wow fadeInUp animated" data-wow-delay=".8s">
								<div class="performance-icon valign border-radius-50pct"><img src="/media/icon-performance-replenishment.png" alt=""></div>
								<div class="border2 valign border-radius-50pct pulsate-inner"></div>
								<div class="border3 valign border-radius-50pct pulsate-outer"></div>
							</div>
							<div class="performance-text wow fadeInUp animated" data-wow-delay=".9s">Replenishment <span><span class="hidden-xs">- </span>AID Your Workout</span></div>
						</div>
					</li>
				</ul>
			</div>
		
	 </div>
	 </div><!-- end performance -->
     
     
     <div  id="home_graphic" class="container-fluid">
	     <img width="100%" src="/media/home_graphic1_scaler.png"> 
     </div>
	
	
	<div id="recovery">
		<div class="container-fluid">
			<h1 class="heading-recovery wow fadeInUp animated" data-wow-delay=".1s">Recovery</h1>
		</div>
		 <div class="container">
			  
			 <div class="clearfix"></div>
			 
			 <div class="col-xs-6 can-left-wrapper"><img class="wow bounceInLeft animated" data-wow-delay=".5s" src="/media/can.png"></div>
			 <div class="col-xs-6 recovery-list-wrapper">
				<h3 class="heading-when-you-need-it wow fadeInUp animated" data-wow-delay=".5s">When You Need It</h3>
				<ul>
					<li>
						<div class="recovery-list-item">
							<div class="recovery-icon-wrapper border-radius-50pct wow fadeInUp animated" data-wow-delay=".4s">
								<div class="recovery-icon valign border-radius-50pct"><img src="/media/icon-recovery-glutamine.png" alt=""></div>
								<div class="border2 valign border-radius-50pct pulsate-inner"></div>
								<div class="border3 valign border-radius-50pct pulsate-outer"></div>
							</div>
							<div class="recovery-text wow fadeInUp animated" data-wow-delay=".6s">GLUTAMINE & BCAA's<span><span class="hidden-xs">- </span>Muscle Recovery</span></div>
						</div>
					</li>
					<li>
						<div class="recovery-list-item">
							<div class="recovery-icon-wrapper border-radius-50pct wow fadeInUp animated" data-wow-delay=".5s">
								<div class="recovery-icon valign border-radius-50pct"><img src="/media/icon-recovery-omega-3.png" alt=""></div>
								<div class="border2 valign border-radius-50pct pulsate-inner"></div>
								<div class="border3 valign border-radius-50pct pulsate-outer"></div>
							</div>
							<div class="recovery-text wow fadeInUp animated" data-wow-delay=".7s">OMEGA 3 EFA, CoQ-10<span><span class="hidden-xs">- </span>Cardiovascular Support</span></div>
						</div>
					</li>
					<li>
						<div class="recovery-list-item">
							<div class="recovery-icon-wrapper border-radius-50pct wow fadeInUp animated" data-wow-delay=".6s">
								<div class="recovery-icon valign border-radius-50pct"><img src="/media/icon-recovery-turmeric.png" alt=""></div>
								<div class="border2 valign border-radius-50pct pulsate-inner"></div>
								<div class="border3 valign border-radius-50pct pulsate-outer"></div>
							</div>
							<div class="recovery-text wow fadeInUp animated" data-wow-delay=".9s">TURMERIC <span><span class="hidden-xs">- </span>Anti-inflammatory</span></div>
						</div>
					</li>
					<li>
						<div class="recovery-list-item">
							<div class="recovery-icon-wrapper border-radius-50pct wow fadeInUp animated" data-wow-delay=".7s">
								<div class="recovery-icon valign border-radius-50pct"><img src="/media/icon-recovery-glucosamine.png" alt=""></div>
								<div class="border2 valign border-radius-50pct pulsate-inner"></div>
								<div class="border3 valign border-radius-50pct pulsate-outer"></div>
							</div>
							<div class="recovery-text wow fadeInUp animated" data-wow-delay="1s">GLUCOSAMINE <span><span class="hidden-xs">- </span>Joint Health</span></div>
						</div>
					</li>
					<li>
						<div class="recovery-list-item">
							<div class="recovery-icon-wrapper border-radius-50pct wow fadeInUp animated" data-wow-delay=".8s">
								<div class="recovery-icon valign border-radius-50pct"><img src="/media/icon-recovery-vitamins.png" alt=""></div>
								<div class="border2 valign border-radius-50pct pulsate-inner"></div>
								<div class="border3 valign border-radius-50pct pulsate-outer"></div>
							</div>
							<div class="recovery-text wow fadeInUp animated" data-wow-delay="1.1s">VITAMINS C, E <span><span class="hidden-xs">- </span>Powerful Antioxidants</span></div>
						</div>
					</li>
					<li>
						<div class="recovery-list-item">
							<div class="recovery-icon-wrapper border-radius-50pct wow fadeInUp animated" data-wow-delay=".9s">
								<div class="recovery-icon valign border-radius-50pct"><img src="/media/icon-recovery-vitamin-d.png" alt=""></div>
								<div class="border2 valign border-radius-50pct pulsate-inner"></div>
								<div class="border3 valign border-radius-50pct pulsate-outer"></div>
							</div>
							<div class="recovery-text wow fadeInUp animated" data-wow-delay="1.2s">VITAMIN D <span><span class="hidden-xs">- </span>Immune Response</span></div>
						</div>
					</li>
				</ul>
			</div>
		
			
		 </div>
	 </div><!-- end recovery -->


	 <div id="buynow">
	 <div class="container">
	 
		<div id="buynow_cans" class="pull-right col-xs-12 col-sm-6">
			<img src="/media/buynow_cans.png">
		</div>
		
		<div class="col-xs-12 col-sm-6">
			<div style="padding: 4em 0;">
				<a href="/buy"  class="btn btn-primary btn-lg">Buy Now</a><br>
				<p>45 Calories  +  All Natural  +  No Artificial Anything</p>
			</div>
		</div>
	
	</div><!-- end container -->
	</div><!-- end buynow -->
	
	
	<div id="testimonials">
		<div class="container">
		<?php 
		      $reviews = new WP_Query( 'category_name=reviews&posts_per_page=2&orderby=rand' ); 
		      while ( $reviews->have_posts() ) : $reviews->the_post(); 
		?>
				<h3><?php the_title(); ?></h3>		
				<span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span>
				<p><?php the_content(); ?></p>
		<?php 
			endwhile; 
			wp_reset_postdata();
		 ?>
			
			
		
		</div><!-- end container -->
	</div><!-- end testimonials -->
	
	
	<div id="loveus">
		<div class="container">
			
				<h4 class="hidden-xs col-xs-7">LOVE US on all of our social media platforms!</h4>
				<div class="col-xs-12 col-sm-5">
				<h4 class="visible-xs-block">LOVE US on all of our social media platforms!</h4>
					<ul class="list-inline love-us-socials">
		 				<li>
							<a class="socials-instagram" href="//instagram.com/fitaid" target="_blank"><i class="fa fa-instagram"></i></a>
						</li>
		 				<li>
							<a class="socials-twitter" href="//twitter.com/drinkfitaid" target="_blank"><i class="fa fa-twitter"></i></a>
						</li>
		 				<li>
							<a class="socials-facebook" href="//www.facebook.com/fitaid" target="_blank"><i class="fa fa-facebook"></i></a>
						</li>
		 				<li>
							<a class="socials-google-plus" href="//plus.google.com/113134579543723671972" target="_blank"><i class="fa fa-google-plus"></i></a>
						</li>
		 				<li>
							<a class="socials-youtube" href="//www.youtube.com/user/DrinkFitAid" target="_blank"><i class="fa fa-youtube"></i></a>
						</li>
	 				</ul>
				</div>
		</div>
	</div>

	<div id="images_stream">
		<div class="container-fluid">
		
		<?php fitaid_get_social_feed(15,'html', true); ?>
	 	 
		</div>
	 </div><!-- end images_stream -->
	 
</div><!-- end container-fluid -->


	<!-- js to animate home performance/recovery sections -->
	<script src="/wp-content/themes/fitaid/js/wow.js"></script>
	<script>
		var wow = new WOW(
		  {
			mobile: false // trigger animations on mobile devices (default is true)
		  }
		);
		wow.init();
	</script>	
		
	<?php get_footer(); ?>
	  
	  
	  
	  
