<?php
/* Template Name: Facebook Landing Page */
?>
 <?php get_header(); ?>
 
		<div class="container facebook-landing-wrapper">
			
			<div class="row add-header-height">
				<div class="col-md-12">
					<h1>$10 OFF your order! Use coupon code <span class="big-red-text">FB10</span> at checkout! </h1>
					<!-- <p class="hidden-xs"><iframe border=0 frameborder=0 class="mobile_hide" id="timerlay-iframe" src="https://timerlay.com/IFrame/1845"></iframe></p>	-->				
				</div>
			</div>
			<div class="row facebook-content">
				<div class="col-md-8 content-left-wrapper">
					<p><img class="feature-image" src="/media/lurong-living-logo.png" alt=""><b>FITAID is NOT an energy drink.</b> FitAID is the only <b>Paleo Friendly performance and recovery beverage</b> made by elite athletes, for athletes. FitAID is made with <b>natural botanical ingredients</b> and has close to 3 grams of <b>targeted supplements to AID your Workout.</b> With things like <b>Glutamine,</b> BCAAs, Omega 3 EFAs, <b>Glucosamine,</b> B Complex, Vitamins C, D, E, Amino Acids, <b>Natural Anti-Inflammatories,</b> Quercetin, CoQ10, and Green Tea Leaf Extract FitAID is the most <b>comprehensive and effective</b> pre and post workout beverage on the market today. On top of that, it's sweetened with raw organic blue agave and stevia and has <b>only 45 calories per can.</b></p>
					<a class="btn btn-lg btn-default" href="/buy">BUY NOW</a>				
					
					<a class="fbox" href="https://www.youtube.com/watch?v=FhuaYAUAdbQ">
						<span>SEE HOW IT WORKS IN 2 MINUTES</span><img src="/media/youtube-how-fitaid-works.jpg" alt="">
					</a>
					
					<a class="fbox" href="https://www.youtube.com/watch?v=U2uzRBa8ekc">
						 <img src="/media/youtube-Jackie-Perez.jpg" alt="">
					</a>
					 
				</div>
				<div class="col-md-4 content-right-wrapper">
				</div>
			</div>
			
			<!-- begin Testimonials -->
			<div class="row testimonials">
				<div class="col-md-12">
					<h3>Derrick Winburn</h3>
					<span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span>
					
					<!-- NOTE: first <p> of testimonial should have the <span.icon-open-quote>, as this class adds the open quote graphic -->
					<p><span class="icon-open-quote"></span> Quality product all around! Actually beneficial ingredients. Tastes awesome! Great guys over at FITAID for sure! Dudes know what they're doing. I'd recommend this stuff to anyone! Should call it Tastier than "The Tasty" though lol just sayin.</p>
					
					
					<h3>Tyrel Johnson</h3>
					<span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span>
					<p><span class="icon-open-quote"></span> Ordered the start up package and it was back ordered. Told Tamara that I needed it for our Grand Opening and she made it happen!!!! That's the epitome of customer service!!!!</p>
					
				</div>
			</div>
			
			
		</div><!-- end container -->
		
<?php get_footer(); ?>