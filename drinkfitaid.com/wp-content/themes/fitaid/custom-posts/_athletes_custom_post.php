<?php

/*
 * Athletes custom post type.
 *
 *
 * Installation: 
 * 1. Include this file into your functions.php file. 
 * 2. Deactivate the theme.
 * 3. Reactivate the theme.
 * 
 *
 * To display data on a template page

 * $athlete = athletes_get_detail(get_the_ID());
 * echo $athlete->region;
 * echo $athlete->bio;
 * echo $athlete->videos;
 */



// Returns object with athlete data for detailed display. Use this on your template pages.
function athletes_get_detail($post_id){
error_reporting(0);

	global $post;
	if(!$post_id) $post_id = $post->ID;

	$metas = get_post_meta($post_id);

	$fields = array(
		'nick_name',
		'affiliate',
		'origin',
		'region',
		'started',
		'weight',
		'height',
		'age',
		'top_achievements',
		'bio',
		'twitter_link',
		'facebook_link',
		'instagram_link',
		'videos'
	);
 
	$data = new stdClass;
	foreach($fields as $field){
		if($metas[$field]) $data->$field = array_shift($metas[$field]);
	}
 
 
	
	// Handle videos
	$more_classes = '';
	if($data->videos) $videos = unserialize($data->videos);
	$html = '<div class="videos_box">' . "\n";
	foreach($videos as $url){
		$html .= '<p><div class="embed-responsive embed-responsive-4by3">';
		$html .= '<iframe class="embed-responsive-item" src="' . $url .'"></iframe>';
		$html .= '</div></p>' . "\n";
	}
	$html .= '</div>' . "\n";
	
	if(count($videos))
		$data->videos = $html;
	else
		$data->videos = '';

	return $data;
 

}





// Flush your rewrite rules after theme init (or switch)
add_action( 'after_switch_theme', 'cptui_flush_rewrite_rules' );
function cptui_flush_rewrite_rules() {
	echo "Flushed rewrite rules.";
	flush_rewrite_rules();
}


// Inits custom post type
add_action('init', 'cptui_register_my_cpt_athletes');
function cptui_register_my_cpt_athletes() {

	register_post_type('athletes', array(
		'label' => 'Athletes',
		'description' => '',
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'rewrite' => array('slug' => 'athletes', 'with_front' => true),
		'query_var' => true,
		'supports' => array('title','excerpt','revisions','thumbnail','page-attributes','post_formats'),
		'labels' => array (
			'name' => 'Athletes',
			'singular_name' => 'Athlete',
			'menu_name' => 'Athletes',
			'add_new' => 'Add Athlete',
			'add_new_item' => 'Add New Athlete',
			'edit' => 'Edit',
			'edit_item' => 'Edit Athlete',
			'new_item' => 'New Athlete',
			'view' => 'View Athlete',
			'view_item' => 'View Athlete',
			'search_items' => 'Search Athletes',
			'not_found' => 'No Athletes Found',
			'not_found_in_trash' => 'No Athletes Found in Trash',
			'parent' => 'Parent Athlete',
		)
	) 
);



// Register the athelete taxonomy
register_taxonomy(
	'athlete_type', 
	array(0 => 'athletes',),
	array( 
		'hierarchical' => false,
		'label' => 'Athlete Tags',
		'show_ui' => true,
		'query_var' => true,
		'show_admin_column' => true,
		'show_in_nav_menus' => true,
		'labels' => array (
			'search_items' => '',
			'popular_items' => '',
			'all_items' => '',
			'parent_item' => '',
			'parent_item_colon' => '',
			'edit_item' => '',
			'update_item' => '',
			'add_new_item' => '',
			'new_item_name' => '',
			'separate_items_with_commas' => '',
			'add_or_remove_items' => '',
			'choose_from_most_used' => '',
		)
	)
); 


}



// Add meta/edit boxes for edit page
add_action( 'add_meta_boxes', 'athletes_profile_box' );
function athletes_profile_box() {
	add_meta_box( 
		'athletes_profile_box',
		__( 'Athlete Profile', 'lifeaid_textdomain' ),
		'athletes_profile_box_content',
		'athletes',
		'normal',
		'high'
	);
}



// output form for the edit boxes
function athletes_profile_box_content($post) {

	wp_nonce_field( plugin_basename( __FILE__ ), 'athletes_profile_box_content_nonce' );
	$fields = array(
		array('title'=>'Nick Name', 'id'=>'nick_name', 'placeholder'=> 'Clam Slam'),
		array('title'=>'Current Affiliate', 'id'=>'affiliate', 'placeholder'=> 'CrossFit CSA'),
		array('title'=>'From', 'id'=>'origin', 'placeholder'=> 'Lodi, California'),
		array('title'=>'Region', 'id'=>'region', 'placeholder'=> 'Lodi, California'),
		array('title'=>'Year Started', 'id'=>'started', 'placeholder'=> '2009'),
		array('title'=>'Weight', 'id'=>'weight', 'placeholder'=> '185'),
		array('title'=>'Height', 'id'=>'height', 'placeholder'=> '5 10"'),
		array('title'=>'Age', 'id'=>'age', 'placeholder'=> '29"'),
		array('title'=>'Instagram Url', 'id'=>'instagram_link', 'placeholder'=> 'http://instagram.com/dollyparton'),
		array('title'=>'Twitter Url', 'id'=>'twitter_link', 'placeholder'=> 'http://twitter.com/tonydanza'),
		array('title'=>'Facebook Url', 'id'=>'facebook_link', 'placeholder'=> 'https://www.facebook.com/chucknorris'),
	);

	$metas = get_post_meta( $post->ID );

	$html = '';

	foreach($fields as $field){

		$value = '';
		if(isset($metas[$field['id']]))
			$value = esc_html(array_shift($metas[$field['id']]));

		$html .= "<p>\n";
		$html .= '<label for="'. $field['id'] .'">' . $field['title'] . '</label><br/>'."\n";
		$html .= '<input type="text" style="min-width:30em" id="'. $field['id'] .'" name="'. $field['id'] .'" placeholder="'. $field['placeholder'] .'" value="'. $value .'" />'."\n";
		$html .= "</p>\n";

	}

	$top_achievements = $bio = '';
	$videos = array();
	
	if(isset($metas['top_achievements'])) $top_achievements = esc_html(array_shift($metas['top_achievements']));
	
	if(isset($metas['bio'])) $bio = esc_html(array_shift($metas['bio']));
	
	if(isset($metas['videos'])) $videos = unserialize(array_shift($metas['videos']));

	$html .= "<p>\n";
	$html .= '<label for="top_achievements">Top Achievements</label><br/>'."\n";
	$html .= '<textarea id="top_achievements" name="top_achievements" style="width: 98%; height:4em;">' . $top_achievements;
	$html .= "</textarea>\n";
	$html .= "</p>\n";

	$html .= "<p>\n";
	$html .= '<label for="bio">Bio</label><br/>'."\n";
	$html .= '<textarea id="bio" name="bio" style="width: 98%; height:4em;">' . $bio;
	$html .= "</textarea>\n";
	$html .= "</p>\n";

	// Simple js to clone a video input, blank the value and insert.
	$onclickjs = "jQuery('.athlete_videos .video_link').first().clone().attr('value','').insertBefore('#moreVideos');";
	$video_attrs = 'class="video_link" type="text" style="width:50%;margin:0 1em .75em 0;" name="videos[]" placeholder="http://youtu.be/HpPyTdBnypM"';

	$html .= "<p class=\"athlete_videos\">\n";
	$html .= '<label for="videos">Videos</label><br/>'."\n";

	if(count($videos)){
		foreach($videos as $url){
			$html .= "<input $video_attrs value=\"$url\" />";
		}		
	}
	
	$html .= "<input $video_attrs />";

	$html .= '<a id="moreVideos" href="javascript:void(0)" class="button" onclick="'.$onclickjs.'">+ Add More</a>' . "\n";
	$html .= "</p>\n";

	echo $html;

}



// Saves athlete data.
add_action( 'save_post', 'athletes_profile_box_save' );
function athletes_profile_box_save( $post_id ) {

	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
		return;

	$nonce = isset($_POST['athletes_profile_box_content_nonce']) ? $_POST['athletes_profile_box_content_nonce'] : '';
	if ( !wp_verify_nonce( $nonce, plugin_basename( __FILE__ ) ) )
		return;

	if ( 'page' == $_POST['post_type'] ) {
		if ( !current_user_can( 'edit_page', $post_id ) )
			return;
	} else {
		if ( !current_user_can( 'edit_post', $post_id ) )
			return;
	}

	$fields = array(
		'nick_name',
		'affiliate',
		'origin',
		'region',
		'started',
		'weight',
		'height',
		'age',
		'bio',
		'top_achievements',
		'twitter_link',
		'facebook_link',
		'instagram_link',
		'videos',
	);

	// Save the fields as post meta
	foreach($fields as $field){

		if(isset($_POST[$field])) 
			$value = $_POST[$field];

		if(is_array($value)){
			$temp_array = array();
			foreach($value as $val){
				if($val){
					$temp_array[] = athletes_format_presave($field, $val);
				}
			}
			$value = $temp_array;
			
		} else {
			$value = athletes_format_presave($field, $value);
		}
		update_post_meta($post_id, $field, $value);
	}

}


function athletes_format_presave($field, $value){
	
	if($field == 'videos'){

		// Scrape src and make it protocol-relative
		if($embed = wp_oembed_get($value)){
			
			preg_match("/src=\"https?:([^\"]+)\"/", $embed, $matches); // Youtube url
			if(count($matches) > 1){
				$value = $matches[1];
			} else {
				preg_match("/src=\"([^\"]+)\"/", $embed, $matches); // Vimeo
				if(count($matches) > 1){
					$value = $matches[1];
				}
			}
		}

	}
	
	return $value;
}
