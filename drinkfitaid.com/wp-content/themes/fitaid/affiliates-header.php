<!DOCTYPE html>
<html ng-app="lcart" lang="en">
  <head>
    <title>DrinkFitAID.com Affiliates</title>
    <meta name="robots" content="noindex"><!-- Hide from google, etc -->
  
  	<?php include('shared-head.php'); ?>

  </head>
  <body ng-controller="core" <?php body_class(); ?> ng-cloak>
   
   <?php include 'includes/forms.php'; ?>
	    
   
   <div id="top_black">
     
	   <ul class="container">
		   
		 
		   <li class="col-xs-12 col-sm-6 pull-right">
		     
		      
		     
		     <span class="glyphicon glyphicon-phone-alt"></span> <span class="hidden-xs">MIND BLOWING CUSTOMER SERVICE </span>(888) 558-1113
	
		  </li>
		  
	   </ul>
	  
	
   </div><!-- end top_black -->
   
   
   	<div id="mini-account" ng-show="system.customer.password" class="hidden-xs" data-spy="affix" data-offset-top="33" ng-init="system.acct_collapse = true" collapse="system.acct_collapse">
	   		<div id="mini-account_content">
		   		<div class="sleeve">
			   		<b>Hi, {{system.customer.form.firstName}}!</b> <a ng-click="system.logout()">(Logout)</a>
			   	</div>
		   	</div>
   		</div>

 
   
    	<div id="mini-cart" class="hidden-xs" data-spy="affix" data-offset-top="33" ng-init="system.mini_collapse = true" collapse="system.mini_collapse">
					 <cart data-products="system.products" data-totals="system.totals" data-template="minicart-default"></cart>
					 <div style="background:#f8f7f7; padding: 1em; text-align: center">
						    <a href="/affiliates-start" class="btn btn-sm btn-default" style="text-align: center">Go to Checkout</a>
						</div>

					  <a class="mini-close" ng-click="system.mini_collapse = true">CLOSE</a>
		</div><!-- end mini-cart -->
					  

   
	 <nav id="nav" data-spy="affix" data-offset-top="33" class="navbar navbar-default" role="navigation">
	 
	 	 
	 <div style="overflow:visible; position: relative;" class="container">
		
		 
		    <div  class="navbar-header col-xs-12 col-sm-5 col-lg-6">
			 <a href="/affiliates-start"  id="mini-cart_mobile" class="visible-xs-block" >
			 <span ng-show="system.totals.items" class="mini badge">{{system.totals.items}}</span>
			   <img  width="33" height="33" src="/media/mini_cart.png">
			   </a>
			  
	              
	              <button type="button" class="navbar-toggle collapsed" ng-click="main_collapse = !main_collapse">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
			      </button>
			     <a class="navbar-brand" href="/affiliates"><img width="136" src="/media/fitaid_logo.png"> 	</a>
		    </div><!-- end navbar-header -->
		
		 
			 		  
		    <div menu class="collapse navbar-collapse col-sm-6" ng-init="main_collapse = true" collapse="main_collapse" id="fa-header-navbar">
				  <ul class="nav navbar-nav">
				   
				    <li><a class="hidden-xs" ng-click="affiliatesGetStarted()">Become An Affiliate</a></li>
				    <li style="width: 400px;"><a class="hidden-xs" href="/reorders">Reorders</a></li>
				 
				 <li><a class="visible-xs-block" ng-click="affiliatesGetStarted()">Become An Affiliate</a></li>
				    <li><a class="visible-xs-block" href="/reorders">Reorders</a></li>
				    
				</ul>
			</div><!-- /.navbar-collapse -->
		 
		   
		  
  		 


			
		    <div  class="col-xs-1 pull-right hidden-xs" >
			  	
			  	<a ng-click="system.mini_collapse = !system.mini_collapse" id="mini-cart-btn">
			  		  <span ng-show="system.totals.items" class="mini badge">{{system.totals.items}}</span>
			  		<img width="33" height="33" src="/media/mini_cart.png">
			  	</a>
			 <!-- NEW my account icon -->
				<a ng-show="system.customer.password" ng-click="system.acct_collapse = !system.acct_collapse" class="hidden-xs hidden-sm icon-my-account-wrapper" href="#">
					<div class="icon-wrapper"><img src="/media/icon-my-account.png"></div><p>My Account</p>
				</a>
				<!-- END my account icon -->
	        </div>
         
         

	 	   <div  class="col-xs-2 visible-md-block visible-lg-block pull-right">
				<ul class="list-inline header-socials">
	 				
 				</ul>
				
	 	   </div>
		  
	
	  </div><!-- end container -->
	  </nav><!-- end nav -->
    
 
	
	<div id="main_content">
	
	  
	 