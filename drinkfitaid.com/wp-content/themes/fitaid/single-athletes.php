<?php get_header(); ?>
	 
		<div class="container-fluid heading">
				<div style="background-image: url(/media/hero-athletes.jpg);" class="hero athletes-hero">
					 <div class="desktop_shiv hidden-xs"></div>
					 <div class="mobile_shiv visible-xs-block"></div>
					 <div class="wrap"> <h2>Athletes</h2> </div>
				 </div>
		</div>

		 
	<div class="subnav athletes-filter-wrapper">
	<?php wp_nav_menu(array('theme_location'=>'athletes-menu', 'container_class'=>'container', 'menu_class' => 'col-xs-12')); ?>
	</div>
	
	
	<div class="container athletes-detail-page">
	<div class="copy_content">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); 
				$athlete = athletes_get_detail(get_the_ID()); ?>
				<!-- BEGIN CONTENT -->
				
				
				
				
				<div class="athlete-profile-wrapper">
					<div class="col-sm-7 athlete-profile-photo">
						<!-- grab URL of Featured Image -->
						<?php if (has_post_thumbnail( $post->ID ) ): ?>
							<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
							<img width="100%" src="<?php echo $image[0]; ?>" alt="">
						<?php endif; ?>
					</div>
					
					<div class="col-sm-5">
						<div class="profile">
							<h1><?php the_title() ?></h1>
							<h3><span class="icon-cross"><img src="/media/icon-cross-white.png" alt=""></span>ATHLETE PROFILE<span class="icon-cross"><img src="/media/icon-cross-white.png" alt=""></span></h3>
							<ul>
								<li><label>Nick Name:</label>
								<?php echo $athlete->nick_name ?></li>
					
								<li><label>Current Affiliate:</label>
								<?php echo $athlete->affiliate ?></li>
					
								<li><label>From:</label>
								<?php echo $athlete->origin ?></li>
					
								<li><label>Region:</label>
								<?php echo $athlete->region ?></li>
					
								<li><label>Year Started:</label>
								<?php echo $athlete->started ?></li>
					
								<li><label>Weight:</label>
								<?php echo $athlete->weight ?></li>
					
								<li><label>Height:</label>
								<?php echo $athlete->height ?></li>
					
								<li><label>Age:</label>
								<?php echo $athlete->age ?></li>
								
								
								
								
<?php 
									
								/* Here's how you would get the social links... 
									
								Instagram: <?php echo $athlete->instagram_link ?><br/>
								Twitter: <?php echo $athlete->twitter_link ?><br/>
								Facebook: <?php echo $athlete->facebook_link ?><br/>
								
								To only show if the link is in the database:
								*/
								
			?>
								
								
								

								
								
								
							</ul>
							<br/>
					
							<div>
								<h3><span class="icon-cross"><img src="/media/icon-cross-white.png" alt=""></span>TOP ACHIEVEMENTS<span class="icon-cross"><img src="/media/icon-cross-white.png" alt=""></span></h3>
								<p><?php echo $athlete->top_achievements ?></p>
							</div>				
					</div>
					
				</div>
				<div class="clearfix"></div>
				
					<div class="col-xs-12">						
						<div class="bio">
							<div class="followme-socials-wrapper">
								<ul class="list-inline ul-wrapper">
									<li><h3>Follow Me</h3></li>
									<li>
										<ul class="list-inline followme-socials">
											<li>
												<?php if($athlete->instagram_link) : ?>
													<a class="socials-instagram" href="<?php echo $athlete->instagram_link ?>" target="_blank"><i class="fa fa-instagram"></i></a>
												<?php endif; ?>
											</li>
											<li>
												<?php if($athlete->facebook_link) : ?>
													<a class="socials-facebook" href="<?php echo $athlete->facebook_link ?>" target="_blank"><i class="fa fa-facebook"></i></a>
												<?php endif; ?>
											</li>
											<li>
												<?php if($athlete->twitter_link) : ?>
													<a class="socials-twitter" href="<?php echo $athlete->twitter_link ?>" target="_blank"><i class="fa fa-twitter"></i></a>
												<?php endif; ?>
											</li>
										</ul>
									</li>
								</ul>
							</div>
							
							<h2>BIO</h2>
							<p><?php echo $athlete->bio ?></p>
						</div>
					</div>
				
					<?php if($athlete->videos) : ?>
						<div class="col-xs-12">
							<div class="videobox">
									<h2>VIDEOS</h2>
								<?php echo $athlete->videos; ?>
							</div>
						</div>
					<?php endif; ?>
				
			
				<!-- END CONTENT -->
			<?php endwhile; else : ?>
		<?php endif; ?>
	</div><!-- end container -->
	</div>
 
	  
	
	
 


<?php get_footer(); ?>
	  
	  
	  