<?php
/* Template Name: Event Sponsorship */
?>
 <?php get_header(); ?>
  <div class="desktop_shiv hidden-xs"></div>
			 <div class="mobile_shiv visible-xs-block"></div>
			 
	<div class="container event-sponsorship-wrapper">
		
 
			<div class="col-md-9">
				<h1>Event Sponsorship</h1>
				
				<?php if(isset($_REQUEST['success'])) : ?>
					<p>
						<strong>Thank you for your interest in FITAID sponsoring your next event!</strong> Please note that we get hundreds of requests a month and it may take a bit of time to get back to you.  We'll respond as soon as we can and we wish you much success at your upcoming event!
						<br/><br/>
						Team FitAID
					</p>
				<?php else : ?>
					<p>Thank you for your interest in having FitAid at your upcoming event! For your review, please fill out the form below to get started:</p>
					<br/>
					
					<form accept-charset="UTF-8" action="https://it140.infusionsoft.com/app/form/process/a2c88d51761e492ca733879f0571f5d7" class="infusion-form" method="POST">
    <input name="inf_form_xid" type="hidden" value="a2c88d51761e492ca733879f0571f5d7" />
    <input name="inf_form_name" type="hidden" value="Event Sponsorship" />
    <input name="infusionsoft_version" type="hidden" value="1.35.0.43" />
					<div class="form-group">
						<label for="inf_option_AreyouaFitAIDAffiliate">Are you a FitAID Affiliate?</label>
						<div class="radio">
							<label class="radio-inline" for="inf_option_AreyouaFitAIDAffiliate_654">
								<input id="inf_option_AreyouaFitAIDAffiliate_654" name="inf_option_AreyouaFitAIDAffiliate" type="radio" value="654" />
								Yes
							</label>
							<label class="radio-inline" for="inf_option_AreyouaFitAIDAffiliate_656">
								<input checked="checked" id="inf_option_AreyouaFitAIDAffiliate_656" name="inf_option_AreyouaFitAIDAffiliate" type="radio" value="656" />
								No
							</label>
						</div>
					</div>
					<div class="form-group">
						<label for="inf_custom_MoreInfoOnSelling">If No, Would you like more information on selling FitAID at your Box</label>
						<div class="radio">
							<label class="radio-inline" for="inf_custom_MoreInfoOnSelling_Yes">
								<input id="inf_custom_MoreInfoOnSelling_Yes" name="inf_custom_MoreInfoOnSelling" type="radio" value="Yes" />
								Yes
							</label>
							<label class="radio-inline" for="inf_custom_MoreInfoOnSelling_No">
							<input id="inf_custom_MoreInfoOnSelling_No" name="inf_custom_MoreInfoOnSelling" type="radio" value="No" />
							No
							</label>
						</div>
					</div>
					<div class="form-group">
						<label for="inf_field_Email" class="sr-only" for="email-address">Your Email Address *</label>
						<input class="form-control" id="inf_field_Email" name="inf_field_Email" type="text" placeholder="Your Email Address *"  />
					</div>
					<div class="form-group">
						<label class="sr-only" for="inf_custom_EventName">Event Name *</label>
						<input class="form-control" id="inf_custom_EventName" name="inf_custom_EventName" type="text"  placeholder="Event Name *"/>
					</div>
					<div class="form-group">
						<label class="sr-only" for="inf_custom_WhatYear">What year did the event first start *</label>
						<input class="form-control" id="inf_custom_WhatYear" name="inf_custom_WhatYear" type="text"  placeholder="What year did the event first start *"/>
					</div>
					<div class="form-group">
						<label class="sr-only" for="inf_custom_URLtoEventInfo">URL to Event Website</label>
						<input class="form-control" id="inf_custom_URLtoEventInfo" name="inf_custom_URLtoEventInfo" type="text" placeholder="URL to Event Website" />
					</div>
					<div class="form-group">
						<label class="sr-only" for="inf_custom_URLFacebook">URL to Event Facebook Page</label>
						<input class="sr-only" class="form-group-input-container" id="inf_custom_URLFacebook" name="inf_custom_URLFacebook" type="text" placeholder="URL to Event Facebook Page" />
					</div>
					<div class="form-group">
						<labe class="sr-only"l for="inf_custom_URLInstagram">URL to Event Instagram</label>
						<input class="form-control" id="inf_custom_URLInstagram" name="inf_custom_URLInstagram" type="text" placeholder="URL to Event Instagram"/>
					</div>
					<div class="form-group">
						<label class="sr-only" for="inf_custom_URLtoEventTwitter">URL to Event Twitter</label>
						<input class="form-control" id="inf_custom_URLtoEventTwitter" name="inf_custom_URLtoEventTwitter" type="text" placeholder="URL to Event Twitter"/>
					</div>
					<div class="form-group">
						<label class="sr-only" for="inf_custom_FoodService">Specify which food truck or other food vendor services will be at your event</label>
						<input class="form-control" id="inf_custom_FoodService" name="inf_custom_FoodService" type="text" placeholder="Specify which food truck or other food vendor services will be at your event"/>
					</div>
					<div class="form-group">
						<label class="sr-only" for="inf_field_FirstName">Your First Name *</label>
						<input class="form-control" id="inf_field_FirstName" name="inf_field_FirstName" type="text" placeholder="Your First Name *"  />
					</div>
					<div class="form-group">
						<label class="sr-only" for="inf_field_LastName">Your Last Name *</label>
						<input class="form-control" id="inf_field_LastName" name="inf_field_LastName" type="text" placeholder="Your Last Name *"  />
					</div>
					<div class="form-group">
						<label class="sr-only" for="inf_field_JobTitle">Job Title</label>
						<input class="form-control" id="inf_field_JobTitle" name="inf_field_JobTitle" type="text" placeholder="Job Title"/>
					</div>
					<div class="form-group">
						<label class="sr-only" for="inf_field_Company">Box Name</label>
						<input class="form-control" id="inf_field_Company" name="inf_field_Company" type="text" placeholder="Box Name" />
					</div>
					<div class="form-group">
						<label class="sr-only" for="inf_field_StreetAddress1">Street Address 1 *</label>
						<input class="form-control" id="inf_field_StreetAddress1" name="inf_field_StreetAddress1" type="text" placeholder="Street Address 1 *"  />
					</div>
					<div class="form-group">
						<label class="sr-only" for="inf_field_City">City *</label>
						<input class="form-control" id="inf_field_City" name="inf_field_City" type="text" placeholder="City *"  />
					</div>
					<div class="form-group">
						<label class="sr-only" for="inf_field_State">State *</label>
						<input class="form-control" id="inf_field_State" name="inf_field_State" type="text" placeholder="State *"  />
					</div>
					<div class="form-group">
						<label class="sr-only" for="inf_field_PostalCode">Postal Code *</label>
						<input class="form-control" id="inf_field_PostalCode" name="inf_field_PostalCode" type="text" placeholder="Postal Code *"  />
					</div>
					<div class="form-group">
						<label class="sr-only" for="inf_field_Phone1">Phone *</label>
						<input class="form-control" id="inf_field_Phone1" name="inf_field_Phone1" type="text" placeholder="Phone *"  />
					</div>
					<div class="form-group">
						<label class="sr-only" for="inf_custom_ApproximateParticipants">Approximate Participants *</label>
						<input class="form-control" id="inf_custom_ApproximateParticipants" name="inf_custom_ApproximateParticipants" type="text" placeholder="Approximate Participants *"  />
					</div>
					<div class="form-group">
						<label class="sr-only" for="inf_custom_ApproximateAttendance">Approximate Attendance *</label>
						<input class="form-control" id="inf_custom_ApproximateAttendance" name="inf_custom_ApproximateAttendance" type="text" placeholder="Approximate Attendance *"  />
					</div>
					<div class="form-group">
						<label class="sr-only" for="inf_custom_DateofEvent">Date of Event *</label>
						<input class="form-control" id="inf_custom_DateofEvent" name="inf_custom_DateofEvent" type="text" placeholder="Date of Event *"  />
					</div>
					<button class="btn btn-lg btn-default" type="submit">SUBMIT</button>
				</form>
					<script type="text/javascript" src="https://it140.infusionsoft.com/app/webTracking/getTrackingCode?trackingId=1eca5d474ef677342484077f73888626"></script>
			
				<?php endif; ?>
				
			</div><!-- END col-md-9 -->	
			<div class="col-md-3 col-sm-6">
				<?php include 'sidebar.php';?>
			</div>
		 
		
	</div><!-- end container -->
<?php get_footer(); ?>