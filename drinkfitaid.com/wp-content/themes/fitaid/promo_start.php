<?php /* Template Name: /Start Landing Template */ ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>DrinkFitAID.com</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<!-- jQuery-->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<!-- Bootstrap 3 CSS + AFFIX -->
	<script src="/wp-content/themes/fitaid/js/bootstrap.min.js"></script> 
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<!--  Angular Bootstrap UI -->
	<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js"></script>


	<!-- scripts for modal images -->
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/helpers/jquery.fancybox-media.js"></script>
	
	<!-- style & efx -->
	<link rel="stylesheet" href="/wp-content/themes/fitaid/style.css">
	<link rel="stylesheet" href="/wp-content/themes/fitaid/css/promo_start.css">
	
	<!--Start of Zopim Live Chat Script-->
	<script type="text/javascript">
	window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
	d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
	_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
	$.src='//v2.zopim.com/?1z2KtsoEJlvCPeFschzfbS9ULhJ6cVkC';z.t=+new Date;$.
	type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
	</script>
	<!--End of Zopim Live Chat Script-->	
 
</head>
<body ng-controller="core" <?php body_class(); ?> ng-cloak>
	<a href="//www.youtube.com/watch?v=l9OmfyLI7j4" id="movie"></a>
	<a name="form"></a>
	<div class="container-fluid" id="formbox">
		<div class="container">
			
			<div class="padded">
				
				<div id="yonlogo"><img width="156" src="/media/white_logo.png"></div>
				<h1>
					Just Fill Out The Form Below to Receive Our Exclusive Box Owner Offer And Get Your Free 4 Cans of FITAID Now!
				</h1>
				
				<form accept-charset="UTF-8" action="https://it140.infusionsoft.com/app/form/process/ac40cf9a96f5be0b59c743dc1587b943" class="infusion-form" method="POST">
					<input name="inf_form_xid" type="hidden" value="ac40cf9a96f5be0b59c743dc1587b943" />
					<input name="inf_form_name" type="hidden" value="FitAID Landing Page Get Started" />
					<input name="infusionsoft_version" type="hidden" value="1.35.0.43" />
		
		
					<div id="triedbox">
						<div class="col-sm-12">
							<p style="font-size: 18px;"><b>Have you tried FITAID before?</b></p>
							<p><input checked="checked" id="inf_option_HaveyoutriedFitAIDbefore_1357" name="inf_option_HaveyoutriedFitAIDbefore" type="radio" value="1357" /> No, but I keep hearing about it and I want to.</p>
							<p> <input id="inf_option_HaveyoutriedFitAIDbefore_1359" name="inf_option_HaveyoutriedFitAIDbefore" type="radio" value="1359" /> Yes, and I would love to hear about your current promotions.</p>
						</div>
						<div class="clearfix"></div>
					</div>
				 
					<div class="clearfix"></div>
		
					<div class="col-sm-6">
						<div class="form-group">
							<input type="text" class="form-control"  name="inf_field_FirstName" placeholder="FIRST NAME">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="inf_field_LastName" placeholder="LAST NAME">
						</div>
		
						<div class="form-group">
							<input type="text" class="form-control"  name="inf_field_Phone1" placeholder="BEST PHONE NUMBER">
						</div>
					
						<div class="form-group">
							<input type="text" class="form-control" name="inf_field_Email" placeholder="EMAIL ADDRESS">
						</div>
					
						<div class="form-group">
							<input type="text" class="form-control"  name="inf_custom_GymWebsiteUrl" placeholder="GYM WEBSITE URL">
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<input type="text" class="form-control" name="inf_field_Company" placeholder="BOX NAME">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="inf_field_Address2Street1"  placeholder="BOX ADDRESS">
						</div>
		
						<div class="form-group">
							<input type="text" class="form-control"  name="inf_field_City2"  placeholder="CITY">
						</div>
					
						<div class="form-group">
							<input type="text" class="form-control" name="inf_field_State2" placeholder="STATE">
						</div>
					
						<div class="form-group">
							<input type="text" class="form-control" name="inf_field_PostalCode2" placeholder="ZIP CODE">
						</div>
					</div>
		
					<div class="clearfix"></div>
				  
					<div class="col-sm-6" style="font-size: 12px;">
						<span style="color: red;">*NOTE: This offer is exclusively for Box owners. We will only ship to valid Box owner addresses.</span> I affirm that the name and information provided on this form is true. I give LifeAID Beverage Co. and its representatives, permission to communicate to me via mail, and email. Your information will not be used for any other purpose or sold.
					 	<div style="padding:1em;"></div>
					</div>
		
					<div class="col-sm-6">
						<button type="submit" style="width:100%" class="btn btn-lg btn-primary">GET YOUR FREE 4 PACK NOW!</button>
					</div>
				</form>
				  
				<div class="clearfix"></div>
				
				<div class="row">
					<div class="col-xs-1">
						<span class="testimonial-nav prev glyphicon glyphicon-chevron-left"></span>
					</div>
					<div class="col-xs-10">
						<?php get_template_part('includes/_reviews'); // Testimonials ?>
					</div>
					<div class="col-xs-1">
						<span class="testimonial-nav next glyphicon glyphicon-chevron-right pull-right"></span>
					</div>
				</div>

				<div style="text-align: center;" class="col-xs-12">
					<img style="margin-bottom: 3em;" width="108" id="paleo" src="/media/start_paleo.png">
				</div>
				  
				<h2>Learn more</h2>
			</div>
		</div><!-- end formbox -->
	</div>
	  
	
		<div class="container">
			<h2 style="text-align: center;"><span class="glyphicon glyphicon-chevron-down"></span></h2>
			<div id="start_whats" class="padded">
				<div class="col-xs-12">
				
					<h1>What's in it for you?</h1>
				  	<div class="clearfix" style="padding: 2em;"></div>
				  	<div id="margins" class="col-sm-4"></div><div id="support" class="col-sm-4"></div><div id="g" class="col-xs-12 col-sm-4"></div>
				  	<div class="clearfix"></div>
				  	<p style="text-align: justify; font-size: 1.5em; line-height: 2em; margin: 2em 0em; "><b>What is FITAID?</b> Simple, the cleanest <b>PERFORMANCE &amp; RECOVERY</b> workout drink on the market, with nearly 3grams of supplements in every drink to <b>AID YOUR WORKOUT</b>.  FITAID is <b>NOT an Energy Drink</b>, has no artificial colors, preservatives or sweeteners like sucralose. FITAID is made with natural botanical ingredients, <b>contains only 45 calories per can</b>, certified gluten free, <b>Lurong Paleo Challenge approved</b> and is specifically designed for the demanding needs of an elite athlete.</p>
				</div>
				
				<div class="clearfix"></div>
				<div class="col-sm-6">
					<div id="review">
						<img id="start_bubble" src="/media/start_bubble.png" width="23" height="23">
					 	<p>FITAID not only tastes amazing, but it is packed with all the supplements I love to take.  When you coach, train and are on the road like me, FITAID is the perfect recovery and replenishment drink! We sell them at our gym and have a hard time keeping them in stock.</p>
						<p>I can't live without my FITAID!</p>
						<p><b>Jackie Perez</b> - Trainer, coach and badass</p>
				 	</div>
					<a href="#form" class="btn btn-lg btn-primary">GET MORE INFO NOW!</a>
					<div style="padding:1em;"></div>
				</div>
				<div class="col-sm-6 pull-right">
					<img src="/media/start_jackie.jpg" width="306">
				</div>
			</div>
		    <div class="clearfix"></div>
	  	</div>
	    
	  	<div id="start_reviews" class="container-fluid">
			<div class="container">

				<div class="padded">
					<h1>What other Gym owners are saying about their success</h1>
					<?php 
					$reviews = new WP_Query( 'category_name=reviews&posts_per_page=2&orderby=rand' ); 
					while ( $reviews->have_posts() ) : $reviews->the_post(); 
					?>
					<h3><?php the_title(); ?></h3>		
					<div style="font-size: 1.5em;">
						<span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span>
					</div>
					<p><?php the_content(); ?></p>
					<?php 
					endwhile; 
					wp_reset_postdata();
					?>
					<div style="padding: 1em"></div>
				</div>
			</div>
	  	</div>
	    
	  	<div class="container-fluid">
		  	<div id="start_coaches">
				<div id="start_grey">
				  	<a href="#form" class="btn btn-lg btn-primary">GET MORE INFO NOW!</a>
			  	</div>
		  	</div>
	  	</div>
	    
	  	<div class="container-fluid">
		  	<div id="start_footer">
			    
			   	<div style="padding: 3em 0 3em 0;" id="buynow">
	 <div class="container">
	 
		<div id="buynow_cans" class="pull-right col-xs-12 col-sm-6">
			<img src="/media/buynow_cans.png">
		</div>
		
		<div class="col-xs-12 col-sm-6">
			<div style="padding: 4em 0;">
				<a href="#form"  class="btn btn-primary btn-lg">Get Started</a><br>
				<p>45 Calories  +  Natural Botanical Ingredients  +  No Artificial Anything</p>
			</div>
		</div>
		
		<div style="font-size: 12px;">
			<p class="col-xs-12 privacy-tos-link" >&copy; 2014 LIFEAID Beverage Co.. <a href="/privacy" title="">Privacy</a> <a href="/terms" title="">Terms &amp; Conditions</a></p>
				
<div class="col-sm-1 hidden-xs"></div>

<div class="col-xs-12 col-sm-10">

<p>Crossfit&reg; is a registered trademark of CrossFit Inc, which is not affiliated with and does not 
necessarily sponsor or endorse the products or services of LifeAID Beverage Co USA.</p>

<p style="padding: 1em; border: 1px solid #787777;">The statements made on this website have not been evaluated by the Food &amp; Drug Administration. 
These products are not intended to diagnose, prevent, treat, or cure any disease.</p>
</div>
<div class="col-sm-1 hidden-xs"></div>
		</div>

	</div><!-- end container -->
	</div><!-- end buynow -->

		  	</div>
	  	</div>

	<script type="text/javascript">
		$(document).ready(function() {
			$('#movie').fancybox({
				padding: 0,
				openEffect  : 'none',
				closeEffect : 'none',
				helpers : {
					media : {}
				}
			});
			$('#movie').trigger('click');
		});
		
		$(function() {

			//auto rotate testimonials
			var testimonialTimer;
			function startTestimonialTimer(delay) {
				testimonialTimer = setTimeout(function() {
					testimonialAdvance(false);
					startTestimonialTimer();
				}, delay || 4000);
			}
			startTestimonialTimer(5000);
		
			$('.testimonial-nav').click(function() {
				clearTimeout(testimonialTimer);
				testimonialAdvance($(this).hasClass('prev'));
			});
						
			function testimonialAdvance(prev) {
				var all = $('.testimonial');
				var idx = all.index($('.testimonial.active'));
				idx += prev ? -1 : 1;
				idx = idx < 0 ? all.length + idx : idx % all.length;
		
				var target = all.eq(idx);
				$('.testimonial.active').stop().fadeTo('slow', 0, function() {
					target.addClass('active').fadeTo('slow', 1, function() {
						//startTestimonialTimer();
					});
					$(this).removeClass('active');
				});				
			}
		
		});
	</script>
	    
	</body>
</html>
