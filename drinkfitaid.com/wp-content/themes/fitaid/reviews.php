<?php
/* Template Name: Reviews Page */
 get_header(); ?>
		 	
	<div class="mobile_shiv visible-xs-block "></div>
	<div class="desktop_shiv hidden-xs "></div>
	
	<div class="container">
	<div class="copy_content col-xs-12">
		
		<h1>Reviews</h1>
		<div class="reviews_feature">
			<?php 
			      $reviews = new WP_Query( 'category_name=reviews&posts_per_page=200&orderby=rand' ); 
			      while ( $reviews->have_posts() ) : $reviews->the_post(); 
			?>
			
			 	<div >
					<p class="title"><?php the_title(); ?> <span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span></p>
					<p><?php the_content(); ?> </p>
					 
			 	</div>
					<hr>
			<?php 
				endwhile; 
				wp_reset_postdata();
			 ?>
		 </div>
	</div>
		
			 
				
				
				
	</div><!-- end container -->
	</div>
<?php get_footer(); ?>
	  
	  
	  
	  
