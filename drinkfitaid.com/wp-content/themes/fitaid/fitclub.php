<?php
/* Template Name: FitClub Page v2 */
 get_header(); ?>
<script type="text/javascript" src="/wp-content/themes/fitaid/js/fitexit.js"></script>

	<div class="mobile_shiv visible-xs-block"></div>
	<div class="desktop_shiv hidden-xs"></div>
		
	<div id="slider">
	<carousel  interval="5000">
		<slide active="slide.active">
			<a href="/buy"><img src="/media/Fitclub-Fitclub.jpg" ></a>
		</slide>
		 <slide >
			<a href="/fitclub"><img src="/media/fitclub_slide2.jpg" ></a>
		</slide>
		<slide >
			<a href="/athletes"><img src="/media/fitclub_slide3.jpg" ></a>
		</slide>
		
		 
	</carousel>

</div>

 
	<div class="container">
		<div class="copy_content">
			<?php 
					if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						 
						<p><?php the_content();?></p>
					<?php endwhile; else : ?><?php endif; ?>
			
			
					
					
									 
						<product data-object="system.products['fitclub-fran']" data-products="system.products" data-template="fitclub-fran"></product>
						
						<product data-object="system.products['fitclub-gymrat']" data-products="system.products" data-template="fitclub-gymrat"></product>
					

	 



		</div><!-- end copy_content -->
	</div><!-- end container -->
<?php get_footer(); ?>