
<ul class="hidden-xs hidden-sm sidebar-bricks-wrapper">
	<li>
		<a href="/fitclub">
			<img src="/media/sidebar-brick-join-the-fitclub.jpg" alt="">
			<h1>JOIN THE<br>FITCLUB</h1>
		</a>
	</li>
	<li>
		<a ng-click="system.modal('enter_to_win')">
			<img src="/media/sidebar-brick-win-fully-stocked-fridge.jpg" alt="">
			<h1>WIN A 1 YEAR SUPPLY OF FITAID</h1>
		</a>
	</li>
	<li>
		<a ng-click="system.modal('start_type')">
			<img src="/media/sidebar-brick-start-selling-fitaid.jpg" alt="">
			<h1>START SELLING<br>FITAID</h1>
		</a>
	</li>
	<li>
		<a href="/refer-your-gym">
			<img src="/media/sidebar-brick-refer-a-gym.jpg" alt="">
			<h1>REFER A GYM,<br>MAKE $90</h1>
		</a>
	</li>
	<li>
		<a href="/get-fitaid-at-your-firehouse/">
			<img src="/media/sidebar-brick-fitaid-in-your-firehouse.jpg" alt="">
			<h1>GET FITAID AT<br>YOUR FIREHOUSE</h1>
		</a>
	</li>
	<li>
		<a href="/event-sponsorship">
			<img src="/media/sidebar-brick-event-sponsorship.jpg" alt="">
			<h1>EVENT<br>SPONSORSHIP</h1>
		</a>
	</li>
</ul>

