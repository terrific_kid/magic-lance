<?php
/* Template Name: Affiliates Landing */
 include 'affiliates-header.php'; ?>
    <div class="mobile_shiv visible-xs-block"></div>
	<div class="desktop_shiv hidden-xs"></div>
	
		<div id="slider">
			<carousel  interval="5000">
				<slide>
					<img src="/media/affiliates_StartSelling2.jpg" >
				</slide>
				<slide>
					<img src="/media/affiliates_JoinBoxOwners4b.jpg" >
				</slide>
				<slide>
					<img src="/media/affiliates_canStack2.jpg" >
				</slide>
				<slide >
				<a href="//itunes.apple.com/us/podcast/43-get-10x-more-money-happiness/id675777894?i=314152198&mt=2" target="_blank"><img src="/media/affiliates_Podcast-v2.jpg" ></a>
				</slide>
			</carousel>
		</div>

			
	
	<div class="container">
		<div class="col-xs-12">
		<p style="padding: 2em;"><b>Note:</b> This site is exclusively for Box owners or their authorized representatives. We will verify that you qualify for our wholesale pricing and specials before shipping your order. If you are not a Box owner, please click <a href="https://www.drinkfitaid.com">HERE</a> to go to our consumer site.</p>
		</div>
		<div class="copy_content affiliates-landing-body-wrapper">
			<div class="hidden-xs col-sm-1"></div>
			<div class="col-sm-4 ">
				<div  class="get-started">
					<a ng-click="affiliatesGetStarted()" href="" class="btn btn-default">Get Started Now</a>
					<p>Become an affiliate and start selling the #1 drink for box owners and their athletes.</p>
					<div class="image-wrapper"><a href=""><img src="/media/affiliates-aff-get-started.jpg" alt=""></a></div>
				</div>
			</div>
			<div class="hidden-xs col-sm-1 pull-right"></div>
			<div class="col-sm-4 easy-reorders-wrapper pull-right">
				<div class="easy-reorders">
					<a href="/reorders" class="btn btn-default ">Easy Reorders</a>
					<p>Now that your box is in love with FITAID, try PARTYAID and GOLFERAID. Buy wholesale merchandise for your members, too!</p>
					<div class="image-wrapper"><a href=""><img src="/media/3can-stack-ga-fa-pa.jpg" alt=""></a></div>
				</div>
			</div>
			
		</div><!-- end container -->
	</div>
	
<?php  include 'affiliates-footer.php'; ?>