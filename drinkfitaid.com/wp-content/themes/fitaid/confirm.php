<?php
/* Template Name: Confirm Page */
 get_header(); ?>
		 	
	<div class="mobile_shiv visible-xs-block "></div>
	<div class="desktop_shiv hidden-xs "></div>
	
	<div class="container">
	<div class="copy_content col-xs-12">
		
		
		<confirm data-template="confirm-default"></confirm>
				 		 
				
				
				
	</div><!-- end container -->
	</div>

	<script type="text/javascript">
		$(document).ready(function(){
			var $element = $('confirm');
			var scope = angular.element($element).scope();
			 
			//Addroll Conversion
			  adroll_conversion_value = scope.system.totals.total;
			  adroll_currency = "USD";
			  
			  //Facebook Conversion
				(function() {
				var _fbq = window._fbq || (window._fbq = []);
				if (!_fbq.loaded) {
				var fbds = document.createElement('script');
				fbds.async = true;
				fbds.src = '//connect.facebook.net/en_US/fbds.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(fbds, s);
				_fbq.loaded = true;
				}
				})();
				window._fbq = window._fbq || [];
				window._fbq.push(['track', '6036203148873', {'value':scope.system.totals.total,'currency':'USD'}]);
	
		 });
		</script> 

<?php get_footer(); ?>
	  
	  
	  
	  
