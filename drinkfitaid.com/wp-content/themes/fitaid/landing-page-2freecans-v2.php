<?php /* Template Name: 2freecans Landing Page V2 */ ?>
<!DOCTYPE html>
<html ng-app="lcart" lang="en">
  <head>
    <title>DrinkFitAID.com</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  
    	<!-- AngularJS -->
    	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0/angular.min.js"></script>
 
    	<!-- jQuery-->
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<!-- Bootstrap 3 CSS + AFFIX -->
		<script src="/wp-content/themes/fitaid/js/bootstrap.min.js"></script> 
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
		<!--  Angular Bootstrap UI -->
		<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js"></script>
	
	
		<!-- scripts for modal images -->
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.pack.js"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/helpers/jquery.fancybox-media.js"></script>
		
		<!-- style -->
		<link rel="stylesheet" href="/wp-content/themes/fitaid/style.css">
		<link rel="stylesheet" href="/wp-content/themes/fitaid/css/twofreecans-landing-v2.css">
		
		<script type="">
			$(document).ready(function(){ 
				$('.fbox').fancybox({
						padding: 0,
						openEffect  : 'none',
						closeEffect : 'none',
						helpers : {
						media : {}
					}
				});
				
				
			});
		</script>
		
	</head>
	<body class="twofreecans-landing-v2-wrapper" ng-controller="core" <?php body_class(); ?> ng-cloak>
	
   <?php require_once('includes/forms.php'); ?>
   
		<div class="container-fluid cs-header-wrapper">
			<div class="container">
				<div class="row">					
					<div class="col-md-12">
						<p class="text-center">CUSTOMER SERVICE (888) 558-1113</p>
					</div>
				</div>
			</div>
	    </div>
		<div class="background-jackie-wrapper">
			<div class="background-jackie hidden-xs"></div>
			<div class="container-fluid intro-header-wrapper">
				<div class="container">
					<div class="row">				
						<div class="col-md-12">
							<h1>TRY THE #1 PERFORMANCE AND RECOVERY PRODUCT FOR <span>FREE!</span></h1>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row">					
						<div class="col-md-7">
							<ul>
								<li><span><i class="fa fa-check"></i></span> FITAID is NOT an energy drink</li>
								<li><span><i class="fa fa-check"></i></span> FitAID is the only Paleo Friendly performance and recovery beverage made by elite athletes, for athletes.</li>
								<li><span><i class="fa fa-check"></i></span> 3 grams of targeted supplements to AID your Workout.</li>
								<li><span><i class="fa fa-check"></i></span> Glutamine, Glucosamine, BCAAs, Turmeric, Amino Acids</li>
								<li><span><i class="fa fa-check"></i></span> Only 45 Calories per can</li>
							</ul>
							<p>We will send you <strong>2 FREE cans ASAP</strong> so you can see why FITAID is the #1 Drink for Elite Athletes.</p>
							<div ng-controller="2freecans" class="ng-scope">
								<button ng-click="freecans()" class="btn btn-lg btn-default">TRY FITAID NOW!</button>				
							</div>
							<p style="font-size:90%;">* Just pay $5.15 for flat rate USPS shipping to your door. Limit 1 per household. Promotional codes may not be used with this item.</p>						
							
						</div>
					</div>
				</div>
			</div>
		
			<div class="container-fluid secure-shopping-wrapper">		
				<div class="container">	
					<div class="row">				
						<div class="col-md-7">
							<div class="secure-shopping-container text-center">
								<p class="text-center">Secure Shopping</p>
								<div class="image-wrapper"><img src="/media/2freecans-v2-secure-shopping-authorize.net.jpg" alt></div>
								<div class="image-wrapper"><img src="/media/2freecans-v2-secure-shopping-100percent-securet.jpg" alt></div>
								<div class="image-wrapper"><img src="/media/2freecans-v2-secure-shopping-rapid-ssl.jpg" alt></div>
							</div>
						</div>
					</div>
				</div>
			</div>	
			
		</div><!-- END background-jackie-wrapper -->
		
		
		
		<!-- begin Testimonials -->
		<div class="container testimonials-wrapper">	
			<div class="row">
				<div class="col-md-12">
					<h3>Derrick Winburn</h3>
					<span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span>
					
					<!-- NOTE: first <p> of testimonial should have the <span.icon-open-quote>, as this class adds the open quote graphic -->
					<p><span class="icon-open-quote"></span> Quality product all around! Actually beneficial ingredients. Tastes awesome! Great guys over at FITAID for sure! Dudes know what they're doing. I'd recommend this stuff to anyone! Should call it Tastier than "The Tasty" though lol just sayin.</p>
										
					<h3>Tyrel Johnson</h3>
					<span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span>
					<p><span class="icon-open-quote"></span> Ordered the start up package and it was back ordered. Told Tamara that I needed it for our Grand Opening and she made it happen!!!! That's the epitome of customer service!!!!</p>
					
				</div>
			</div>
	    </div>
		<br /><br />
		
<?php get_footer(); ?>
