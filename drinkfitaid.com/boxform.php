<?php require_once('/usr/share/nginx/www/api/api/infusion/isdk.php'); 

	//Open Infusionsoft Connection
	$app = new iSDK;
	if (!$app->cfgCon("LifeAIDBevCo"))die('Connection Failed!');
	
	error_log('REFER_YOUR_GYM: posted with:' . print_r($_REQUEST, true));
		
	//Add Person
	$person = $app->addWithDupCheck(
		array(
			'FirstName' => $_REQUEST['FirstName'], 
			'LastName' => $_REQUEST['LastName'], 
			'Email' => $_REQUEST['Email'],
			'Address2Street1' => $_REQUEST['Address1'],
			'City2' => $_REQUEST['City'],
			'State2' => $_REQUEST['State'],
			'PostalCode2' => $_REQUEST['PostalCode'],
		), 
		'Email'
	);
	
	error_log('REFER_YOUR_GYM: Contact added. id:' . print_r($person, true));
	
	//Add Tag to Person - FitAID Online Referee - 738
	$app->grpAssign($person, 738);

	//Add Box
	$box = $app->dsAdd(
		"Contact", 
		array(
			'Company' => $_REQUEST['gym_Company'],
			'FirstName' => $_REQUEST['gym_FirstName'],
			'LastName' => $_REQUEST['gym_LastName'],
			'StreetAddress1' => $_REQUEST['gym_StreetAddress1'],
			'City' => $_REQUEST['gym_City'],
			'State' => $_REQUEST['gym_State'],
			'PostalCode' => $_REQUEST['gym_PostalCode'],
			'Phone1' => $_REQUEST['gym_Phone1'],
			'Email' => $_REQUEST['gym_Email']
		)
	);
	
	error_log('REFER_YOUR_GYM: Gym added. id:' . print_r($box, true));
			
	if($_REQUEST['gym_Type'] == 'affiliate'):
		//Add Tag to Box - CF Affiliate Online Referral by Athlete - 736
		$app->grpAssign($box, 736);
	else:
	 	//Add Tag to Other Type - Non CF - /refer-your-box - 1014
		$app->grpAssign($box, 1014);
	endif;
			
		
	//Append Box info to Person Notes
	$notes = $app->dsLoad("Contact", $person, array('ContactNotes'));  	

	$append .= '&#13;**********************************************************************&#13;';
	$append .= date("F j, Y, g:i a") . ' &#13;';
	$append .= '-> Referral &#13;';		
	$append .= 'Name: '. $_REQUEST['gym_Company'] .' &#13;';
	$append .= 'ID: #' . $box . ' &#13;';
	$append .= 'Shirt Size: ' . $_REQUEST['ShirtSize'] . ' &#13;';
	
	$notes['ContactNotes'] .= $append;
	$app->dsUpdate('Contact', $person, array('ContactNotes' => $notes['ContactNotes']));
	
	
	//Append Person to Box Notes
	$notes = $app->dsLoad("Contact", $box, array('ContactNotes'));  	
		
	$append = '&#13;**********************************************************************&#13;';
	$append .= date("F j, Y, g:i a") . ' &#13;';
	$append .= '-> Referred By &#13;';		
	$append .= 'Name: '. $_REQUEST['FirstName'] . ' ' . $_REQUEST['LastName'] . ' &#13;';
	$append .= 'ID: #'.$person.' &#13;';
	
	$notes['ContactNotes'] .= $append;
	$app->dsUpdate(
		'Contact', 
		$box, 
		array(
			'ContactNotes' => $notes['ContactNotes'],
			'_ReferName' => $_REQUEST['FirstName'] . ' ' . $_REQUEST['LastName'],
			'_ReferEmail' => $_REQUEST['Email']
		)
	);
		 				
	header('Location: /refer-your-gym/?success');
?>